Set Implicit Arguments.
Require Export LibCore CFTactics LibReal LibOrder.
Open Scope comp_scope. (* for inequalities *)
Open Scope R_scope.


(********************************************************************)
(** Axiomatization of credits *)

Parameter credits_zero : \$0 = \[].

Parameter credits_split_eq : forall x y,
  x >= 0 -> 
  y >= 0 ->
  \$(x+y) = \$ x \* \$ y.

Lemma credits_split : forall x y,
  (* x >= 0 -> y >= 0 -> TODO: add this *)
  \$(x+y) = \$ x \* \$ y.
Admitted.

Lemma credits_join : forall x y,
  \$ x \* \$ y ==> \$(x+y).
Admitted.

Axiom credits_le : forall x y,
  y <= x ->
  \$ x ==> \$ y.

Axiom credits_ge : forall x y, 
  x >= y -> 
  \$ x ==> \$ y.




(********************************************************************)
(** Updated version of [hsimpl] that handles credits *)

Lemma hsimpl_cancel_impl_1 : forall H H' HA HR HT,
  H ==> H' -> HT ==> HA \* HR -> H \* HT ==> HA \* (H' \* HR).
Admitted.

Lemma hsimpl_cancel_impl_2 : forall H H' HA HR H1 HT,
  H ==> H' -> H1 \* HT ==> HA \* HR -> 
  H1 \* H \* HT ==> HA \* (H' \* HR).
Admitted.

Lemma hsimpl_cancel_impl_3 : forall H H' HA HR H1 H2 HT,
  H ==> H' -> H1 \* H2 \* HT ==> HA \* HR -> 
  H1 \* H2 \* H \* HT ==> HA \* (H' \* HR).
Admitted.

Lemma hsimpl_cancel_impl_4 : forall H H' HA HR H1 H2 H3 HT,
  H ==> H' -> H1 \* H2 \* H3 \* HT ==> HA \* HR -> 
  H1 \* H2 \* H3 \* H \* HT ==> HA \* (H' \* HR).
Admitted.

Lemma hsimpl_cancel_impl_5 : forall H H' HA HR H1 H2 H3 H4 HT,
  H ==> H' -> H1 \* H2 \* H3 \* H4 \* HT ==> HA \* HR -> 
  H1 \* H2 \* H3 \* H4 \* H \* HT ==> HA \* (H' \* HR).
Admitted.

Lemma hsimpl_cancel_impl_6 : forall H H' HA HR H1 H2 H3 H4 H5 HT,
  H ==> H' -> H1 \* H2 \* H3 \* H4 \* H5 \* HT ==> HA \* HR ->
  H1 \* H2 \* H3 \* H4 \* H5 \* H \* HT ==> HA \* (H' \* HR).
Admitted.

Lemma hsimpl_cancel_impl_7 : forall H H' HA HR H1 H2 H3 H4 H5 H6 HT,
  H ==> H' -> H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* HT ==> HA \* HR ->
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H \* HT ==> HA \* (H' \* HR).
Admitted.

Lemma hsimpl_cancel_impl_8 : forall H H' HA HR H1 H2 H3 H4 H5 H6 H7 HT,
  H ==> H' -> H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* HT ==> HA \* HR ->
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H \* HT ==> HA \* (H' \* HR).
Admitted.

Lemma hsimpl_cancel_impl_9 : forall H H' HA HR H1 H2 H3 H4 H5 H6 H7 H8 HT,
  H ==> H' -> H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* HT ==> HA \* HR ->
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H \* HT ==> HA \* (H' \* HR).
Admitted.

Lemma hsimpl_cancel_impl_10 : forall H H' HA HR H1 H2 H3 H4 H5 H6 H7 H8 H9 HT,
  H ==> H' -> H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H9 \* HT ==> HA \* HR ->
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H9 \* H \* HT ==> HA \* (H' \* HR).
Admitted.

Ltac hsimpl_find_data l HL cont ::=
  match HL with
  | hdata _ l \* _ => apply hsimpl_cancel_eq_1
  | _ \* hdata _ l \* _ => apply hsimpl_cancel_eq_2
  | _ \* _ \* hdata _ l \* _ => apply hsimpl_cancel_eq_3
  | _ \* _ \* _ \* hdata _ l \* _ => apply hsimpl_cancel_eq_4
  | _ \* _ \* _ \* _ \* hdata _ l \* _ => apply hsimpl_cancel_eq_5
  | _ \* _ \* _ \* _ \* _ \* hdata _ l \* _ => apply hsimpl_cancel_eq_6
  | _ \* _ \* _ \* _ \* _ \* _ \* hdata _ l \* _ => apply hsimpl_cancel_eq_7
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* hdata _ l \* _ => apply hsimpl_cancel_eq_8
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* hdata _ l \* _ => apply hsimpl_cancel_eq_9
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* hdata _ l \* _ => apply hsimpl_cancel_eq_10
  end; [ cont tt | ].

Ltac hsimpl_find_credits HL cont :=
  match HL with
  | \$ _ \* _ => apply hsimpl_cancel_impl_1
  | _ \* \$ _ \* _ => apply hsimpl_cancel_impl_2
  | _ \* _ \* \$ _ \* _ => apply hsimpl_cancel_impl_3
  | _ \* _ \* _ \* \$ _ \* _ => apply hsimpl_cancel_impl_4
  | _ \* _ \* _ \* _ \* \$ _ \* _ => apply hsimpl_cancel_impl_5
  | _ \* _ \* _ \* _ \* _ \* \$ _ \* _ => apply hsimpl_cancel_impl_6
  | _ \* _ \* _ \* _ \* _ \* _ \* \$ _ \* _ => apply hsimpl_cancel_impl_7
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* \$ _ \* _ => apply hsimpl_cancel_impl_8
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* \$ _ \* _ => apply hsimpl_cancel_impl_9
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* \$ _ \* _ => apply hsimpl_cancel_impl_10
  end; [ cont tt | ].

Ltac hsimpl_find_credits_post tt := 
  try match goal with |- \$ _ ==> \$ _ => apply credits_le end.

(* todo: add a hook to avoid copy-paste *)

Ltac hsimpl_step tt ::=
  match goal with |- ?HL ==> ?HA \* ?HN =>
  match HN with
  | ?H \* _ =>
    match H with
    | \[] => apply hsimpl_empty
    | \[_] => apply hsimpl_prop
    | heap_is_pack _ => hsimpl_extract_exists tt
    | _ \* _ => apply hsimpl_assoc
    | heap_is_single _ _ => hsimpl_try_same tt
    | Group _ _ => hsimpl_try_same tt
    | ?H => check_noevar2 H; hsimpl_find_same H HL 
    | \$ _ => hsimpl_find_credits HL ltac:(hsimpl_find_credits_post)
    | hdata _ ?l => hsimpl_find_data l HL ltac:(hsimpl_find_data_post)
    | ?x ~> Id _ => check_noevar x; apply hsimpl_id
    | ?x ~> _ _ => check_noevar x; apply hsimpl_id_unify
    | _ => apply hsimpl_keep
    end
  | \[] => fail 1
  | _ => apply hsimpl_starify
  end end.


(** Demo *)

Lemma hsimpl_demo_credits : forall x y (H1 H2 H3:hprop) (a b : loc), 
  (y >= 0)%R -> \[] \* H2 \* \$ (x+y) \* a ~> Id b \* H1 ==> H2 \* \$ (x) \* H3.
Proof. intros. hsimpl. simpl_credits. auto. Admitted.



(********************************************************************)
(** Tactic [xskip_credits] *)

Parameter skip_credits : forall x, \$ x = \[].

Hint Rewrite skip_credits : rew_skip_credits.

Ltac xskip_credits_core := 
  autorewrite with rew_skip_credits.

Ltac xskip_credits_base := 
  xskip_credits_core; hsimpl.

Tactic Notation "xskip_credits" := 
  xskip_credits_base.




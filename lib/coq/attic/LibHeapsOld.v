   destruct (f2 x); intuition.
Qed.

Lemma heap_disjoint_union_eq_l : forall h1 h2 h3,
  heap_disjoint (heap_union h2 h3) h1 =
  (heap_disjoint h1 h2 /\ heap_disjoint h1 h3).
Proof.
  intros. rewrite heap_disjoint_comm. 
  apply heap_disjoint_union_eq_r.
Qed.

Definition heap_disjoint_3 h1 h2 h3 :=
  heap_disjoint h1 h2 /\ heap_disjoint h2 h3 /\ heap_disjoint h1 h3.

Notation "\# h1 h2 h3" := (heap_disjoint_3 h1 h2 h3)
  (at level 40, h1 at level 0, h2 at level 0, h3 at level 0, no associativity).

Lemma heap_disjoint_3_unfold : forall h1 h2 h3,
  \# h1 h2 h3 = (\# h1 h2 /\ \# h2 h3 /\ \# h1 h3).
Proof. auto. Qed.

(** Union *)

Lemma heap_union_neutral_l : forall h,
  heap_union heap_empty h = h.
Proof. 
  intros [f F]. unfold heap_union, pfun_union, heap_empty. simpl.
  apply~ heap_eq.
Qed.

Lemma heap_union_neutral_r : forall h,
  heap_union h heap_empty = h.
Proof. 
  intros [f F]. unfold heap_union, pfun_union, heap_empty. simpl.
  apply heap_eq. intros x. destruct~ (f x).
Qed.

Lemma heap_union_comm : forall h1 h2,
  heap_disjoint h1 h2 ->
  heap_union h1 h2 = heap_union h2 h1.
Proof.
  intros [f1 F1] [f2 F2] H. simpls. apply heap_eq. simpl.
  intros. rewrite~ pfun_union_comm.
Qed.

Lemma heap_union_assoc : 
  LibOperation.assoc heap_union.
Proof.
  intros [f1 F1] [f2 F2] [f3 F3]. unfolds heap_union. simpls.
  apply heap_eq. intros x. unfold pfun_union. destruct~ (f1 x).
Qed.

(** Hints and tactics *)

Hint Resolve heap_union_neutral_l heap_union_neutral_r.

Hint Rewrite 
  heap_disjoint_union_eq_l
  heap_disjoint_union_eq_r
  heap_disjoint_3_unfold : rew_disjoint.

Tactic Notation "rew_disjoint" :=
  autorewrite with rew_disjoint in *.
Tactic Notation "rew_disjoint" "*" :=
  rew_disjoint; auto_star.

Set Implicit Arguments.
Require Import LibCore Shared CFHeaps.

Open Scope heap_scope.


(********************************************************************)
(** [xunfold] *)

Axiom Ref : int->loc->hprop.

Definition R' n p := p ~> Ref n.

Definition R n p := Hexists m, p ~> R' (n+m) \* \[m = 0].

Lemma xunfold_repr_at_demo_4 :
  forall (F:hprop->(loc->hprop)->Prop),
  forall p n,
  F (p ~> R n) (fun p' => Hexists n', p' ~> R n').
Proof using.
  intros. dup 2.
  xunfold R at 1. unfold R at 1. xunfold R at 1. admit.
  xunfold R at 2. xunfold R at 1.
Abort.

Lemma xunfold_repr_demo_3 :
  forall (F:hprop->(loc->hprop)->Prop),
  forall p n,
  F (p ~> R n \* p ~> Ref n) (fun p' => Hexists n', p' ~> R n').
Proof using.
  intros.
  xunfold R.
  xunfold R'.
Abort.

Lemma xunfold_clean_demo_2 :
  forall (F:hprop->(loc->hprop)->Prop),
  forall p n,
  F (p ~> R n) (fun p' => Hexists n', p' ~> R n').
Proof using.
  intros. dup 2.
  unfold R at 1. xunfold_clean. (* good *) admit.
  unfold R at 2. xunfold_clean.
    (* does nothing, because p' and n' are bound *)
Abort. (* demo *)

Lemma xunfold_at_demo_1 :
  forall (F:hprop->(loc->hprop)->Prop),
  forall p n,
  F (p ~> R n) (fun p' => Hexists n', p' ~> R n').
Proof using.
  intros.
  xunfold at 1.
  unfold R at 1.
  xunfold at 1.
  unfold R' at 1.
  xunfold at 2.
  unfold R at 1.
  xunfold at 2.
  unfold R' at 1.
Abort. (* demo *)



(********************************************************************)
(** [hsimpl] *)

Lemma hsimpl_demo_1 : forall H1 H2 H3 H4 H5,
  H1 \* H2 \* H3 \* H4 ==> H4 \* H3 \* H5 \* H2.
Proof using.
  intros. dup.
  (* details *)
  hsimpl_setup tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  try hsimpl_step tt.
  hsimpl_cleanup tt.
  skip. (* demo *)
  (* short *)
  hsimpl. skip. (* demo *)
Admitted. (* demo *)

Lemma hsimpl_demo_2 : forall (l1 l2:loc) S1 S2 H1 H2 H3 H',
  (forall X S1' HG, X ==> H1 \* l1 ~> S1' \* H2 \* HG -> X ==> H') ->
  H1 \* l1 ~> S1 \* l2 ~> S2 \* H3 ==> H'.
Proof using.
  intros. dup.
  (* details *)
  eapply H.
  hsimpl_setup tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  try hsimpl_step tt.
  hsimpl_cleanup tt.
  skip. (* demo *)
  (* short *)
  eapply H.
  hsimpl. skip. (* demo *)
Admitted. (* demo *)

Lemma hsimpl_demo_3 : forall (l1 l2:loc) S1 S2 H1 H2 H',
  (forall X S1' HG, X ==> H1 \* l1 ~> S1' \* HG -> HG = HG -> X ==> H') ->
  H1 \* l1 ~> S1 \* l2 ~> S2 \* H2 ==> H'.
Proof using.
  intros. dup.
  (* details *)
  eapply H.
  hsimpl_setup tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  try hsimpl_step tt.
  hsimpl_cleanup tt.
  skip.
  auto.
  (* short *)
  eapply H.
  hsimpl.
  auto.
Admitted. (* demo *)

(*
Require Import CFPrimSpec.
==!!Anomaly: Universe CFML.CFHeaps.771 undefined. Please report.
TODO COQ BUG

Lemma hsimpl_demo_4 : forall x y (n m : int) (My:int->int->hprop),
  x ~> Ref Id n \* y ~> Ref Id n ==> x ~> Ref Id m \* y ~> Ref My m.
Proof using.
  intros. hsimpl. skip. (* demo *)
  (* todo: replace equal with ==> *)
Admitted. (* demo *)

Lemma hsimpl_demo_5 : forall n m J H2 H3 H4,
  n = m + m ->
  H2 \* J m \* H3 \* H4 ==> H4 \* (Hexists y, y ~> Id 2) \* (Hexists x, \[n = x + x] \* J x \* H2) \* H3.
Proof using.
  intros. dup.
  (* details *)
  hsimpl_setup tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  try hsimpl_step tt.
  hsimpl_cleanup tt.
  auto.
  auto.
  auto.
  (* short *)
  (* TEMPORARY this call to [hsimpl] loops,
     and the calls to [auto] above do nothing
  hsimpl.
  auto.
  auto.
  auto.
  *)
Admitted. (* demo *)

(** [hsimpl] *)

Lemma hsimpl_demo_6 : forall H1 H2 H3 H4 H5,
  H1 \* H2 \* H3 \* H4 \* H5 ==> H3 \* H1 \* H2 \* \GC.
Proof using.
  intros. dup.
  (* details *)
  hsimpl_setup tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  hsimpl_step tt.
  try hsimpl_step tt.
  hsimpl_cleanup tt.
  (* short *)
  hsimpl.
Admitted. (* demo *)

(* ** FUTURE WORK: extend hsimpl to handle \[] ==> ?H1 \* ?H2 *)

(*
  Ltac instantiate_right_when_left_empty tt
    ...
  Ltac hsimpl_cleanup tt ::=
    try apply hsimpl_stop;
    try apply hsimpl_stop;
    try apply pred_le_refl;
    try hsimpl_hint_remove tt;
    try remove_empty_heaps_right tt;
    try remove_empty_heaps_left tt;
    try instantiate_right_when_left_empty tt.

  Lemma hsimpl_demo_6 :
    (forall H1 H2, \[] ==> H1 \* H2 -> True) ->
    True.
  Proof using.
    introv M. dup.
    (* details *)
    eapply M.
    hsimpl_setup tt.
    hsimpl_step tt.
    hsimpl_step tt.
    hsimpl_step tt.
    try hsimpl_step tt.
    hsimpl_cleanup tt.

  Admitted.  (* demo *)

*)



(** [hsimpl_hints] *)

Lemma demo_hsimpl_hints : exists n, n = 3.
Proof using.
  hsimpl_hint_put (>> 3 true).
  hsimpl_hint_next ltac:(fun x => exists x).
  hsimpl_hint_remove tt.
Abort.


(********************************************************************)


(** [hclean] *)

Lemma hclean_demo_1 : forall A (x X:A) H1 H2 H3 B (F:~~B) Q,
   is_local F ->
   F (H1 \* \[] \* (H2 \* Hexists y:int, \[y = y]) \* x ~> Id X \* H3) Q.
Proof using.
  intros. dup.
  (* details *)
  hclean_setup tt.
  hclean_step tt.
  hclean_step tt.
  hclean_step tt.
  hclean_step tt.
  hclean_step tt.
  hclean_step tt.
  hclean_step tt.
  hclean_step tt.
  hclean_step tt.
  try hclean_step tt.
  hclean_cleanup tt.
  skip. (* demo *)
  (* short *)
  hclean.
  skip. (* demo *)
Admitted. (* demo *)


(** [hsimpl] on nat credits *)

Lemma hsimpl_credits_nat_demo_1 : forall (x y:nat) (H1 H2 H3:hprop) (a b : loc),
 \[] \* H2 \* \$_nat (x+y)%nat \* a ~> Id b \* H1 ==> H2 \* \$_nat (x) \* H3.
Proof using. intros. hsimpl. omega. demo. Qed.

Lemma hsimpl_credits_nat_demo_2 : forall (x y z:nat) (H1:hprop),
 \[] \* H1 \* \$_nat (x) ==> H1 \* \$_nat (y) \* \$_nat (z).
Proof using. intros. hsimpl. demo. demo. demo. Qed.

Lemma hsimpl_credits_nat_demo_3 : forall (x y z t:nat) (H1:hprop),
 (x >= z + t)%nat ->
 \[] \* H1 \* \$_nat x \* \$_nat y ==> H1 \* \$_nat y \* \$_nat z \* \$_nat t.
Proof using. intros. hsimpl. omega. omega. demo. Qed.


(** [hsimpl] on int credits *)

Lemma hsimpl_credits_int_demo_1 : forall (x y:int) (H1 H2 H3:hprop) (a b : loc),
 x >= 0 -> y >= 0 ->
 \[] \* H2 \* \$ (x+y) \* a ~> Id b \* H1 ==> H2 \* \$ (x) \* H3.
Proof using. intros. hsimpl. math. math. demo. Qed.

Lemma hsimpl_credits_int_demo_2 : forall (x y z:int) (H1:hprop),
 \[] \* H1 \* \$ (x) ==> H1 \* \$ (y) \* \$ (z).
Proof using. intros. hsimpl. demo. demo. demo. demo. demo. Qed.

Lemma hsimpl_credits_int_demo_3 : forall (x y z t:int) (H1:hprop),
 (x >= z + t) -> y >= 0 -> z >= 0 -> t >= 0 ->
 \[] \* H1 \* \$ x \* \$ y ==> H1 \* \$ y \* \$ z \* \$ t.
Proof using. intros. hsimpl; try math. demo. Qed.


(** [csimpl] on int credits *)

Require Import CFLibCredits.

Lemma hsimpl_credits_int_demo_list : forall A (L:list A) x (c:nat),
  \$ c \* \$ (c * LibList.length L) \* \$ c ==>
  \$ c \* \$ (c * LibList.length (x::L)).
Proof using.
  intros. rew_length.
  hsimpl. (* optional, but having it is more efficient since it cancels
             immediately the \$ c. *)
  csimpl. (* same as: hsimpl_credits. csimpl. csimpl. *)
  csimpl.
  (* usually garbage collected *)
  rewrite~ credits_int_zero_eq_prove. simpl_credits. math.
Qed.


(* LATER
Lemma hsimpl_credits_nat_demo_list : forall A (L:list A) x (c:nat),
  \$_nat c \* \$_nat (c * LibList.length L) \* \$_nat c ==>
  \$_nat c \* \$_nat (c * LibList.length (x::L)).
Proof using.
  intros. rew_length.
  dup. credits_join. demo. hsimpl; try mmath.
  admit. (* distrib *)
  rewrite credits_nat_zero_eq_prove. auto. admit. (* distrib *)
Qed.
*)

*)





(********************************************************************)
(** App and Records *)

Require Import CFApp CFTactics.


Definition demo_arglist :=
  forall f (xs:list int) (x y:int) B H (Q:B->hprop),
  app f [ x y ] H Q.
(* Print demo_arglist. *)
(* TODO: find a way that the parentheses are not printed around "app" *)


Module RecordDemo.

Definition f := 0%nat.
Definition g := 1%nat.

Definition demo1 r :=
  r ~> `{ f := 1+1 ; g := 1 }.

Lemma demo_get_1 : forall r,
  app_keep record_get [r f] (r ~> `{ f := 1+1 ; g := 1 }) \[= 2].
Proof using.
  intros.
  xspec_record_get_compute tt. intros G. apply G.
Qed.
  (* details 1:
  match goal with |- app record_get [?r ?f] (?r ~> record_repr ?L) _ =>
    pose (demo_L := L); pose (demo_f := f) end.
  forwards G: (record_get_compute_spec_correct demo_f demo_L);
    [ reflexivity |].
  subst demo_f demo_L.
  apply G.
  *)
  (* details 2:
  let R := eval hnf in (record_get_compute_spec demo_f demo_L) in
  match R with
  | None => fail 2 "field not found for record_get"
  | Some ?P => pose P
  end.
  let G := fresh "HP" in assert (G: P).
  apply (@record_get_compute_spec_correct demo_f demo_L). reflexivity.
  subst P.
  subst demo_f demo_L.
  apply G.
  *)

Lemma demo_get_2 : forall r,
  app_keep record_get [r g] (r ~> `{ f := 1+1 ; g := 1 }) \[= 1].
Proof using.
  intros.
  xspec_record_get_compute tt. intros G. apply G.
Qed.

Lemma demo_get_3 : forall r,
  app_keep record_get [r g] (r ~> `{ g := 1 }) \[= 1].
Proof using.
  intros.
  xspec_record_get_compute tt. intros G. apply G.
Qed.

Lemma demo_set_1 : forall r,
  (forall H',
    app record_set [r f 4] (r ~> `{ f := 1+1 ; g := 1 }) (#H') -> True)
  -> True.
Proof using.
  introv M. applys (rm M).
  xspec_record_set_compute tt. intros G. apply G.
Qed.

End RecordDemo.

(**

This file extends MLSep by "lifting" heap predicates 
and triples so as to express specifications using logical values,
as opposed to deeply-embedded values.

The relationship between the two kind of values is implemented
using encoding functions, called encoders, realized using
typeclasses.

Author: Arthur Charguéraud.
License: MIT.

*)

Set Implicit Arguments.
Require Import LibCore MLSep MLCF.
Generalizable Variables A B.

Open Scope trm_scope.
Open Scope heap_scope.
Open Scope charac.

Ltac auto_star ::= jauto.



(* ********************************************************************** *)
(* * Lifting *)


(* ---------------------------------------------------------------------- *)
(* ** Func type *)

Definition func := val.


(* ---------------------------------------------------------------------- *)
(* ** Encoders *)

Class Enc (A:Type) := 
  make_Enc { enc : A -> val }.

Instance Enc_loc : Enc loc. 
Proof using. constructor. applys val_loc. Defined.

Instance Enc_unit : Enc unit. 
Proof using. constructor. applys (fun (x:unit) => val_unit). Defined.

Instance Enc_int : Enc int. 
Proof using. constructor. applys val_int. Defined.

Instance Enc_func : Enc func. 
Proof using. constructor. applys (fun (x:func) => x). Defined.

Instance Enc_val : Enc val. 
Proof using. constructor. applys (fun (x:val) => x). Defined.

Instance Enc_prim : Enc prim. 
Proof using. constructor. applys (fun (p:prim) => val_prim p). Defined.

Instance Enc_pair : forall A B, Enc A -> Enc B -> Enc (A*B). 
Proof using. 
  constructor. 
  applys (fun (p:A*B) => val_pair (enc (fst p)) (enc (snd p))).
Defined.


(* ---------------------------------------------------------------------- *)
(* ** Representation of values packed with their type and encoder *)

(** Representation of dependent pairs *)

Record dyn := Dyn {
  dyn_type : Type;
  dyn_enc : Enc dyn_type;
  dyn_value : dyn_type }.

Implicit Arguments Dyn [dyn_type [dyn_enc]].

Lemma dyn_inj : forall A `{EA:Enc A} (x y : A),
  Dyn x = Dyn y -> x = y.
Proof using. introv H. inverts~ H. Qed.

Lemma dyn_inj_type : forall A1 `{EA1:Enc A1} A2 `{EA2:Enc A2} (x1:A1) (x2:A2),
  Dyn x1 = Dyn x2 -> A1 = A2.
Proof using. introv H. inverts~ H. Qed.

Lemma dyn_def : forall (d:dyn),
  d = @Dyn _ (dyn_enc d) (dyn_value d).
Proof using. intros. destruct~ d. Qed.

(** Conversion of dyns to values *)

Definition dyn_val (d:dyn) :=
  @enc _ (dyn_enc d) (dyn_value d).

Instance Enc_dyn : Enc dyn. 
Proof using. constructor. applys dyn_val. Defined.

(* paragraphase of definition
Lemma enc_dyn_def : forall (d:dyn), 
  enc d = dyn_val d.
Proof using. auto. Qed.
*)

Lemma enc_dyn_def : forall (d:dyn),
  enc d = @enc _ (dyn_enc d) (dyn_value d).
Proof using. auto. Qed.

(** List of dyns and encoder for lists *)

Definition dyns := list dyn.

Definition encs (ds:dyns) : list val :=
  List.map dyn_val ds.


(* ---------------------------------------------------------------------- *)
(* ** Decoder *)

Fixpoint decode (v:val) : dyn :=
  match v with
  | val_unit => Dyn tt
  | val_int n => Dyn n
  | val_pair v1 v2 => 
      let d1 := decode v1 in
      let d2 := decode v2 in
      @Dyn _ (Enc_pair (dyn_enc d1) (dyn_enc d2))
             (dyn_value d1, dyn_value d2)
  | val_loc l => Dyn l
  | val_prim p => Dyn p
  | val_fix f xs t => Dyn (v:func)
  | val_var x => Dyn v (* not meant to be used *)
  end.

Lemma encode_decode : forall (v:val), 
  let d := decode v in
  @enc _ (dyn_enc d) (dyn_value d) = v.
Proof using.
  intros. induction v; auto.
  { simpls. unfolds dyn_val. simpls. fequals. }
Qed.

Lemma encode_decode' : forall (v:val), 
  enc (decode v) = v.
Proof using.
  intros. induction v; auto.
  { simpls. unfolds dyn_val. simpls. fequals. }
Qed.

Definition decodes (vs:vals) : dyns :=
  List.map decode vs.

Definition encodes_decodes : forall (vs:vals),
  encs (decodes vs) = vs.
Proof using.
  intros. induction vs. 
  { auto. }
  { simpl. fequals. applys encode_decode. }
Qed.


(* ---------------------------------------------------------------------- *)
(* ** Lemmas about encoders *)

Lemma enc_val_fix_eq : forall F f x t1,
  F = val_fix f x t1 ->
  enc F = F.
Proof using. intros. subst~. Qed.


(* ---------------------------------------------------------------------- *)
(* ** Lifting heap predicates *)

Definition hsingle_enc `{EA:Enc A} (l:loc) (f:field) (V:A) := 
  hsingle l f (enc V).

Notation "l `.` f '~~>' V" := (hsingle_enc l f V)
  (at level 32, f at level 0, no associativity,
   format "l `.` f  '~~>'  V") : heap_scope.

(* ** Configure [hcancel] to make it aware of [hsingle] *)

Ltac hcancel_hook H ::= 
  match H with
  | hsingle _ _ => hcancel_try_same tt
  | @hsingle_enc _ _ _ _ _ => hcancel_try_same tt
  end.

(* ---------------------------------------------------------------------- *)
(* ** Record representation *)

(** Representation predicate [r ~> Record L], where [L]
    is an association list from fields to values. *)

Fixpoint Record (L:record_fields) (r:loc) : hprop :=
  match L with
  | nil => \[]
  | (f, V)::L' => (r `.` f ~~> V) \* (r ~> Record L')
  end. 

Lemma hRecord_not_null : forall (r:loc) (L:record_fields),
  L <> nil ->
  (r ~> Record L) ==> (r ~> Record L) \* \[r <> null].
Proof using.
  intros. destruct L as [|(f,v) L']; tryfalse.
  xunfold Record. hchange (hsingle_not_null r). hsimpl~.
Qed.

Global Opaque Record.


(* ---------------------------------------------------------------------- *)
(* ** Lifting of postconditions *)

Definition PostEnc `{Enc A} (Q:A->hprop) : val->hprop :=
  fun v => Hexists V, \[v = enc V] \* Q V.

Lemma PostEnc_himpl : forall `{Enc A} Q Q',
  Q ===> Q' ->
  PostEnc Q ===> PostEnc Q'.
Proof using.
  introv M. unfold PostEnc. intros v. hpull. intros x E.
  subst v. hsimpl*.
Qed.

Lemma PostEnc_star : forall `{Enc A} Q H,
  PostEnc (Q \*+ H) = (PostEnc Q) \*+ H.
Proof using.
  intros. unfold PostEnc. applys func_ext_1. intros v.
  applys himpl_antisym; hsimpl~.
Qed.

Hint Resolve PostEnc_himpl.


(* ---------------------------------------------------------------------- *)
(* ** Lifting of substitution *)

Definition Ctx := list (var * dyn).
 
(* TODO: move *)
Notation "'fun' ''' ( x1 , x2 ) '=>' E" := 
  (fun p => let '(x1,x2) := p in E) 
  (at level 200, format "'fun'  ''' ( x1 , x2 )  '=>'  E").

Definition Ctx_ctx (E:Ctx) :=
  List.map (fun '(x,d) => (x,enc d)) E.

Definition Subst_trm (E:list(var*dyn)) (t:trm) : trm :=
  subst_trm (Ctx_ctx E) t.



(* ********************************************************************** *)
(* * Lifting of triples *)

Definition Triple (t:trm) `{EA:Enc A} (H:hprop) (Q:A->hprop) :=
  triple t H (PostEnc Q).


(* ---------------------------------------------------------------------- *)
(* ** Lifting of structural rules *)

Lemma Rule_extract_hexists : forall t `{Enc A} (J:A->hprop) (Q:A->hprop),
  (forall x, Triple t (J x) Q) ->
  Triple t (hexists J) Q.
Proof using. intros. applys~ rule_extract_hexists. Qed.

Lemma Rule_extract_hprop : forall t (P:Prop) `{Enc A} H (Q:A->hprop),
  (P -> Triple t H Q) ->
  Triple t (\[P] \* H) Q.
Proof using. intros. applys~ rule_extract_hprop. Qed.

Lemma Rule_consequence : forall t H' `{Enc A} (Q':A->hprop) H (Q:A->hprop),
  H ==> H' ->
  Triple t H' Q' ->
  Q' ===> Q ->
  Triple t H Q.
Proof using. introv MH M MQ. applys* rule_consequence MH. Qed.

Lemma Rule_frame : forall t `{Enc A} H (Q:A->hprop) H',
  Triple t H Q ->
  Triple t (H \* H') (Q \*+ H').
Proof using. 
  introv M. unfold Triple. rewrite PostEnc_star. applys* rule_frame. 
Qed.

Lemma Rule_htop_post : forall t H Q,
  Triple t H (Q \*+ \Top) ->
  Triple t H Q.
Proof using.
  introv M. unfolds Triple. rewrite PostEnc_star in M. applys* rule_htop_post.
Qed.

Lemma Rule_htop_pre : forall t H Q,
  Triple t H Q ->
  Triple t (H \* \Top) Q.
Proof using. introv M. applys* rule_htop_pre. Qed.


(* ---------------------------------------------------------------------- *)
(* ** Lifting of term rules *)

Lemma Rule_val : forall A `{EA:Enc A} (V:A) v H (Q:A->hprop),
  v = enc V ->
  H ==> Q V ->
  Triple (trm_val v) H Q.
Proof using.
  introv E M. applys rule_val. subst.
  unfold PostEnc. hsimpl*.
Qed.

Lemma Rule_if_val : forall (V:int) v t1 t2 H A `{EA:Enc A} (Q:A->hprop),
  v = enc V ->
  Triple (If V = 0 then t2 else t1) H Q ->
  Triple (trm_if v t1 t2) H Q.
Proof using.
  introv E M. applys rule_if_val. subst. 
  applys_eq (rm M) 3. tests: (V = 0); simpl; case_if~.
Qed.

Lemma Rule_if : forall t0 t1 t2 H A0 `{EA0:Enc A0} (Q0:A0->hprop) A `{EA:Enc A} (Q:A->hprop),
  Triple t0 H Q0 ->
  (forall (X:A0), Triple (trm_if (trm_val (enc X)) t1 t2) (Q0 X) Q) ->
  Triple (trm_if t0 t1 t2) H Q.
Proof using.
Admitted. (* TODO *)

Lemma Rule_seq : forall t1 t2 H,
  forall A `{EA:Enc A} (Q:A->hprop) (Q1:unit->hprop),
  Triple t1 H Q1 ->
  (Triple t2 (Q1 tt) Q) ->
  Triple (trm_seq t1 t2) H Q.
Proof using.
  introv M1 M2. applys* rule_seq' M1.
  { unfold PostEnc. xpull ;=> V E. destruct V. applys* M2. }
  { intros v. unfold PostEnc. hpull ;=> V E.
    destruct V. subst v. simpl. hsimpl~. }
Qed.

Lemma Rule_let : forall x t1 t2 H,
  forall A `{EA:Enc A} (Q:A->hprop) A1 `{EA1:Enc A1} (Q1:A1->hprop),
  Triple t1 H Q1 ->
  (forall (X:A1), Triple (Subst_trm [(x,Dyn X)] t2) (Q1 X) Q) ->
  Triple (trm_let x t1 t2) H Q.
Proof using.
  introv M1 M2. applys rule_let M1.
  intros v. unfold PostEnc. xpull. intros V E. subst. applys M2.
Qed. (* TODO COQ: why notation for hexists shows hexists? *)

Lemma Rule_let_val_enc : forall A1 `{Enc A1} (V1:A1) x v1 t2 H A `{EA: Enc A} (Q:A->hprop),
  v1 = enc V1 ->
  (forall X, X = V1 -> Triple (Subst_trm [(x,Dyn X)] t2) H Q) ->
  Triple (trm_let x (trm_val v1) t2) H Q.
Proof using.
  introv E M. applys rule_let_val. intros X EX. subst. applys~ M.
Qed.

(* Remark:
Lemma Rule_let_val : forall x v1 t2 H A `{EA: Enc A} (Q:A->hprop),
  (forall `{Enc A1} (X:A1), v1 = enc X -> Triple (Subst_trm x X t2) H Q) ->
  Triple (trm_let x (trm_val v1) t2) H Q.
 => could be proved by exploiting the fact that every value is the encoding of another.
*)

Lemma encs_length : forall (Vs:dyns),
  List.length (encs Vs) = List.length Vs.
Proof using. intros. unfold encs. rewrite~ List.map_length. Qed.


Lemma Subst_to_subst_combine : forall xs vs,
  List.length vs = List.length xs ->
  List.combine xs (encs vs) = Ctx_ctx (List.combine xs vs).
Proof using. 
  intros xs. induction xs as [|x xs']; introv E.
  { auto. }
  { destruct vs as [|v vs']; tryfalse. simpls. 
    inverts E. fequals. applys~ IHxs'. }
Qed.  


Lemma Rule_app : forall f xs F (Vs:dyns) t1 H A `{EA:Enc A} (Q:A->hprop) E,
  F = (val_fix f xs t1) ->
  List.length Vs = List.length xs ->
  (*
  E = List.combine (f::xs) ((Dyn F)::Vs) ->
  Triple (Subst_trm E t1) H Q ->
  *)
  E = List.combine (f :: xs) (Dyn F :: Vs) ->
  Triple (Subst_trm E t1) H Q ->
  Triple (trm_app F (encs Vs)) H Q.
Proof using.
  introv EF EL EE M. applys* rule_app.
  { rewrite~ encs_length. }
  { unfolds Subst_trm. subst E. rewrite~ <- Subst_to_subst_combine in M.
    (* todo: clean up script *) { simpl. fequals. } } 
Qed.

Lemma Rule_app' : forall f xs F (Vs:dyns) t1 H A `{EA:Enc A} (Q:A->hprop) E,
  F = (val_fix f xs t1) ->
  List.length Vs = List.length xs ->
  (*
  E = List.combine (f::xs) ((Dyn F)::Vs) ->
  Triple (Subst_trm E t1) H Q ->
  *)
  E = List.combine (f :: xs) (F :: encs Vs) ->
  Triple (subst_trm E t1) H Q ->
  Triple (trm_app F (encs Vs)) H Q.
Proof using.
  introv EF EL EE M. applys* rule_app.
  { rewrite~ encs_length. }
Qed.

Lemma Rule_let_fix : forall f xs t1 t2 H A `{EA:Enc A} (Q:A->hprop),
  (forall (F:val), 
    (forall A' `{EA:Enc A'} H' (Q':A'->hprop) (Xs:dyns) E, 
      List.length Xs = List.length xs ->
      E = List.combine (f::xs) (Dyn F::Xs) ->
      Triple (Subst_trm E t1) H' Q' ->
      Triple (trm_app F (encs Xs)) H' Q') ->
    Triple (Subst_trm [(f,Dyn F)] t2) H Q) ->
  Triple (trm_let f (trm_val (val_fix f xs t1)) t2) H Q.
Proof using.
  introv M. applys (@Rule_let_val_enc _ _ (val_fix f xs t1)).
  { symmetry. applys* enc_val_fix_eq. }
  intros F EF. applys (rm M). clears H Q.
  intros A2 EA2 H Q Xs E EL EE M. subst E. applys~ Rule_app EF.
Qed.

(*
Lemma Rule_let_fix' : forall f xs t1 t2 H A `{EA:Enc A} (Q:A->hprop),
  (forall (F:val), 
    (forall (Xs:dyns) H' Q' E, 
      List.length Xs = List.length xs ->
      E = List.combine (f::xs) (F::(encs Xs)) ->
      Triple (subst_trm E t1) H' Q' ->
      Triple (trm_app F (encs Xs)) H' Q') ->
    Triple (subst_trm [(f,F)] t2) H Q) ->
  Triple (trm_let f (trm_val (val_fix f xs t1)) t2) H Q.
Proof using.
  introv M. applys (@Rule_let_val_enc _ _ (val_fix f xs t1)).
  { symmetry. applys* enc_val_fix_eq. }
  intros F EF. applys (rm M). clears H Q.
  intros A2 EA2 Q Xs EL EE M.
  applys~ Rule_app EF.
  { subst Xs. applys* M. }
Qed.
*)

(*
Lemma Rule_ref : forall A `{EA:Enc A} (V:A) v,
  v = enc V ->
  Triple (prim_ref v) \[] (fun l => l ~~~> V).
Proof using.
  introv E. applys_eq rule_ref 1. subst~. 
Qed.

Lemma Rule_get : forall A `{EA:Enc A} (V:A) l,
  Triple (prim_get (val_loc l)) (l ~~~> V) (fun x => \[x = V] \* (l ~~~> V)).
Proof using.
  introv. applys rule_consequence. { auto. } { applys rule_get. }
  { intros v. unfold PostEnc. 
    applys himpl_hprop_l. intro_subst.
    applys himpl_hexists_r V.
    applys~ himpl_hprop_r.
    applys~ himpl_hprop_r. }
Qed.

Lemma Rule_set : forall A1 A2 `{EA1:Enc A1} (V1:A1) `{EA2:Enc A2} (V2:A2) l v2,
  v2 = enc V2 ->
  Triple (prim_set (val_pair (val_loc l) v2)) (l ~~~> V1) (fun (r:unit) => l ~~~> V2).
Proof using.
  introv E. applys rule_consequence. { auto. } { applys rule_set. }
  { subst. intros v. unfold PostEnc. 
    applys himpl_hprop_l. intro_subst.
    applys himpl_hexists_r tt.
    applys~ himpl_hprop_r. }
Qed.
*)



(*--------------------------------------------------------*)
(* ** Local *)

Lemma Triple_is_local : forall t A `{EA:Enc A},
  is_local (@Triple t A EA).
Proof using.
  unfold is_local, Triple, triple. intros. 
  applys prop_ext_2. intros H Q. iff M.
  { intros h Hh. forwards (h'&v&N1&N2): M \[] h. { hhsimpl. } 
    exists H \[] Q. splits~. { hhsimpl. } { hsimpl. } }
  { intros H' h Hh. lets (h1&h2&N1&N2&N3&N4): Hh. hnf in M.
    lets (H1&H2&Q1&R1&R2&R3): M N1.
    forwards (h'&v&S1&S2): R2 (H2\*H') h.
    { subst h. rewrite <- hstar_assoc. exists~ h1 h2. }
    exists h' v. splits~. rewrite <- htop_hstar_htop.
    applys himpl_inv S2.
    (* MODIFIED FOR LIFTING *)
    lets R3': PostEnc_himpl R3. 
    do 2 rewrite PostEnc_star in R3'.
    hchange (R3' v). rew_heap.
    rewrite (hstar_comm_assoc \Top H'). hsimpl. }  
Qed.



(**

This file extends MLCF by "lifting" heap predicates like in
MLSepLifted, so as to express specifications using logical values,
as opposed to deeply-embedded values. CF are lifted like triples.

Author: Arthur Charguéraud.
License: MIT.

*)


Set Implicit Arguments.
Require Export LibCore MLSepLifted MLCF.
Generalizable Variables A B.

Open Scope trm_scope.
Open Scope heap_scope.
Open Scope charac.

Ltac auto_star ::= jauto.


(* ********************************************************************** *)
(* CF with Lifting *)


(* ---------------------------------------------------------------------- *)
(* ** Type of a formula *)

(** A formula is a binary relation relating a pre-condition
    and a post-condition. *)

Definition formula := forall `{Enc A}, hprop -> (A -> hprop) -> Prop.

Global Instance Inhab_formula : Inhab formula.
Proof using. apply (prove_Inhab (fun _ _ _ _ => True)). Qed.

Notation "' F H Q" := ((F:formula) _ _ H Q)
  (at level 65, F at level 0, H at level 0, Q at level 0).

(** Lifting of substitution on [Trm] *)

Definition Subst_Trm (E:Ctx) (t:Trm) : Trm :=
  subst_Trm (Ctx_ctx E) t.

(** Lifting of the [local] predicate *)

Definition Local (F:formula) := fun A `{EA:Enc A} H Q =>
   local (F A EA) H Q.

(*
Definition local (F:formula) : formula :=
  fun A `{EA:Enc A} (H:hprop) (Q:A->hprop) =>
    forall h, H h -> exists (H1:hprop) (H2:hprop) (Q1:A->hprop),
       (H1 \* H2) h 
    /\ 'F H1 Q1
    /\ Q1 \*+ H2 ===> Q \*+ \Top.
*)

(** Lifting of the [app] predicate *)

Definition App (f:func) (Vs:dyns) `{Enc A} H (Q:A->hprop) :=
  Triple (trm_app f (encs Vs)) H Q.


(* ---------------------------------------------------------------------- *)
(* ** The [local] predicate *)

Lemma Local_local : forall A `{EA:Enc A} (F:formula),
  local (@Local F A EA) = (@Local F A EA).
Proof using.
  intros. apply prop_ext_2. intros H Q.
  unfold Local. rewrite local_local. split~.
Qed.
  
Lemma Local_is_local : forall A `{EA:Enc A} (F:formula),
  is_local (@Local F A EA).
Proof using. intros. unfolds. rewrite~ Local_local. Qed.

Hint Resolve Local_is_local.

Lemma App_is_local : forall (f:func) (Vs:dyns) A `{EA:Enc A},
  is_local (@App f Vs A EA).
Proof using. intros. unfold App. applys Triple_is_local. Qed.


(* ---------------------------------------------------------------------- *)
(* ** Definition of CF blocks *)

(** These auxiliary definitions give the characteristic formula
    associated with each term construct. *)

Definition Cf_val (v:val) : formula := fun `{Enc A} H (Q:A->hprop) =>
  exists V, v = enc V /\ H ==> Q V.

Definition Cf_if_val (v:val) (F1 F2 : formula) : formula := fun `{Enc A} H (Q:A->hprop) =>
  exists (V:int), v = enc V /\
  (V <> 0 -> 'F1 H Q) /\ (V = 0 -> 'F2 H Q).

Definition Cf_seq (F1 : formula) (F2 : formula) : formula := 
  fun `{Enc A} H (Q:A->hprop) =>
  exists (Q1:unit->hprop), 'F1 H Q1 /\ 'F2 (Q1 tt) Q.

Definition Cf_let (F1 : formula) (F2of : forall `{EA1:Enc A1}, A1 -> formula) : formula := 
  fun `{Enc A} H (Q:A->hprop) =>
  exists (A1:Type) (EA1:Enc A1) (Q1:A1->hprop), 
      'F1 H Q1 
   /\ (forall (X:A1), '(F2of X) (Q1 X) Q).

Definition Cf_if (F0 F1 F2 : formula) : formula := 
  Cf_let F0 (fun A1 EA1 X => Cf_if_val (enc X) F1 F2).

Definition Cf_app (f:val) (vs:vals) : formula := fun `{Enc A} H (Q:A->hprop) =>
  exists Vs, vs = encs Vs /\ App f Vs H Q.

Definition Cf_fix (n:nat) (F1of : func -> dyns -> formula) (F2of : func -> formula) : formula := 
  fun `{Enc A} H (Q:A->hprop) =>
  forall (F:val), 
  (forall (Xs:dyns) H' `{EA':Enc A'} (Q':A'->hprop), 
     List.length Xs = n -> '(F1of F Xs) H' Q' -> App F Xs H' Q') ->
  '(F2of F) H Q.

Definition Cf_fix0 (F1of : func -> formula) 
                   (F2of : func -> formula) : formula := fun `{Enc A} H (Q:A->hprop) =>
  forall (F:func), 
  (forall A' `{EA':Enc A'} H' (Q':A'->hprop), '(F1of F) H' Q' -> App F nil H' Q') -> 
  '(F2of F) H Q.

Definition Cf_fix1 (F1of : forall A `{EA:Enc A}, func -> A -> formula) 
                   (F2of : func -> formula) : formula := fun `{Enc A} H (Q:A->hprop) =>
  forall (F:func), 
  (forall A' `{EA':Enc A'} A1 `{EA1:Enc A1} (X1:A1) H' (Q':A'->hprop), '(F1of _ F X1) H' Q' -> App F [Dyn X1] H' Q') ->
  '(F2of F) H Q.

Definition Cf_fail : formula := fun `{Enc A} H (Q:A->hprop) =>
  False.


(* ---------------------------------------------------------------------- *)
(* ** Instance of [app] for primitive operations *)

(* LATER
Lemma app_ref : forall A `{EA:Enc A} (V:A),
  App prim_ref V \[] (fun (l:loc) => l ~~~> V).
Proof using. intros. applys~ Rule_ref. Qed.

Lemma app_get : forall A `{EA:Enc A} (V:A) l,
  app prim_get (val_loc l) (l ~~~> V) (fun (x:A) => \[x = V] \* (l ~~~> V)).
Proof using. intros. applys~ Rule_get. Qed.

Lemma app_set : forall A1 A2 `{EA1:Enc A1} `{EA2:Enc A2} l (V:A1) (W:A2), 
  app prim_set (val_pair (val_loc l) (enc W)) (l ~~~> V) (fun (_:unit) => l ~~~> W).
Proof using. intros. applys~ Rule_set. Qed.
*)

(* ---------------------------------------------------------------------- *)
(* ** Auxiliary *)

Lemma Trm_size_Subst : forall t E,
  Trm_size (Subst_Trm E t) = Trm_size t.
Proof using.
  intros. unfold Subst_Trm. rewrite~ Trm_size_subst.
Qed.



(* ---------------------------------------------------------------------- *)
(* ** Definition of the CF generator *)

Definition Cf_def Cf (t:Trm) : formula :=
  match t with
  | Trm_val v => Local (Cf_val v)
  | Trm_if_val v t1 t2 => Local (Cf_if_val v (Cf t1) (Cf t2))
  | Trm_if t0 t1 t2 => Local (Cf_if (Cf t0) (Cf t1) (Cf t2))
  | Trm_seq t1 t2 => Local (Cf_seq (Cf t1) (Cf t2))
  | Trm_let x t1 t2 => Local (Cf_let (Cf t1) (fun `{EA:Enc A} (X:A) => Cf (Subst_Trm [(x,Dyn X)] t2)))
  | Trm_let_fix f xs t1 t2 => 
      let G := match xs with
        | nil => Cf_fix0 (fun F => let E := List.combine (f::nil) (Dyn F::nil) in Cf (Subst_Trm E t1))  
        | x::nil => Cf_fix1 (fun A `{EA:Enc A} F (X:A) => let E := List.combine (f::x::nil) (Dyn F::Dyn X::nil) in Cf (Subst_Trm E t1))
        | xs => Cf_fix (List.length xs) (fun F Xs => let E := List.combine (f::xs) (Dyn F::Xs) in Cf (Subst_Trm E t1))
        end in
      Local (G (fun F => Cf (subst_Trm [(f,F)] t2)))
  | Trm_app f vs => Local (Cf_app f vs)
  | Trm_while t1 t2 => Local Cf_fail (* TODO: later *)
  end.

Definition Cf := FixFun Cf_def.

Ltac smath := simpl; math.
Hint Extern 1 (lt _ _) => smath.

Close Scope trm_scope.

Notation "'Dynamic' X" := (@Dyn _ _ X) (at level 32).

Lemma Cf_unfold_iter : forall n t,
  Cf t = func_iter n Cf_def Cf t.
Proof using.
  applys~ (FixFun_fix_iter (measure Trm_size)). auto with wf.
  intros f1 f2 t IH. unfold measure in IH. unfold Cf_def.
  destruct t; fequals.
  { do 2 rewrite~ IH. }
  { do 3 rewrite~ IH. }
  { do 2 rewrite~ IH. }
  { rewrite~ IH. fequals.
    apply func_ext_dep_1. intros A.
    apply func_ext_dep_2. intros EA X.
    (* todo: func_ext_dep_3 *)
    rewrite~ IH. rewrite~ Trm_size_Subst. }
  { applys func_equal_1.
    { rename v0 into xs. destruct xs as [|x [|]]; fequals.
      { applys func_ext_1. intros F. rewrite~ IH. do 2 rewrite~ Trm_size_Subst. } 
      { applys func_ext_dep_1. intros A.
        applys func_ext_3. intros EA1 F X1. rewrite~ IH. do 2 rewrite~ Trm_size_Subst. }
      { applys func_ext_2. intros F Xs. rewrite~ IH. do 2 rewrite~ Trm_size_Subst. } }
    { apply func_ext_1. intros F. rewrite~ IH. rewrite~ Trm_size_subst. } }
Qed.

Lemma Cf_unfold : forall t,
  Cf t = Cf_def Cf t.
Proof using. applys (Cf_unfold_iter 1). Qed.

Ltac simpl_Cf :=
  rewrite Cf_unfold; unfold Cf_def.


(* ********************************************************************** *)
(* ** Soundness proof *)

(* ---------------------------------------------------------------------- *)
(* ** Two substitution lemmas for the soundness proof *)

(** Substitution commutes with the translation from [Trm] to [trm] *)

Lemma Subst_trm_of_Trm : forall (t:Trm) (E:Ctx),
  Subst_trm E (trm_of_Trm t) = trm_of_Trm (Subst_Trm E t).
Proof using. intros. apply~ subst_trm_of_Trm. Qed.

(** The size of a [Trm] is preserved by substitution of 
    a variable by a value. *)

Lemma Trm_size_Subst_Trm_value : forall (t:Trm) (E:Ctx),
  Trm_size (Subst_Trm E t) = Trm_size t.
Proof using. intros. applys Trm_size_subst_Trm_value. Qed.


(* ---------------------------------------------------------------------- *)
(* ** Soundness of the CF generator *)

Lemma Cf_local : forall A `{EA:Enc A} T,
  is_local ((Cf T) A EA).
Proof. intros. simpl_Cf. destruct T; apply Local_is_local. Qed.

Definition Sound_for (t:trm) (F:formula) := 
  forall `{EA:Enc A} H (Q:A->hprop), 'F H Q -> Triple t H Q.

(* todo: move *)
Lemma is_local_Triple : forall A `{EA:Enc A} (t:trm),
  is_local (@Triple t A EA).
(*Proof using. 
  intros. unfolds Triple. lets M: is_local_triple.
  unfolds is_local. applys prop_ext_2. intros H Q.
  iff N.
  applys_eq 1 (H t).
Qed.
*)
Admitted.

Lemma Local_Sound : forall t (F:formula),
  Sound_for t F ->
  Sound_for t (Local F).
Proof using.
  unfold Sound_for. introv SF. intros A EA H Q M.
  rewrite is_local_Triple. applys local_weaken_body M. applys SF.
Qed.

Lemma Cf_sound_induction : forall (t:Trm),
  Sound_for t (Cf t).
Proof using.
  intros t. induction_wf: Trm_size t. 
  rewrite Cf_unfold. destruct t; simpl;
   applys Local_Sound; intros A EA H Q P.
  { destruct P as (V&E&M). applys* Rule_val. }
  { destruct P as (V&E&M1&M2). applys* Rule_if_val. case_if. 
    { applys~ IH. }
    { applys~ IH. } }
  { destruct P as (A1&EA1&Q1&P1&P2). applys Rule_if. 
    { applys* IH. }
    { intros X. specializes P2 X. applys* rule_if_val.
      destruct P2 as (V&E&P3&P4). rewrite E. tests: (V = 0).
      { case_if; simpls; tryfalse. applys* IH. }
      { case_if; simpls; tryfalse. applys* IH. } } }
  { destruct P as (H1&P1&P2). applys Rule_seq H1.
    { applys~ IH. }
    { applys~ IH. } } 
  { destruct P as (A1&EA1&Q1&P1&P2). applys Rule_let Q1.
    { applys~ IH. }
    { intros X. rewrite Subst_trm_of_Trm. 
      applys~ IH. hnf. rewrite~ Trm_size_Subst_Trm_value. } }
  { renames v to f,v0 to xs. applys Rule_let_fix. 
    intros F HF. rewrite Subst_trm_of_Trm. applys IH.
    { hnf. rewrite~ Trm_size_Subst_Trm_value. }
    { destruct xs as [|x1 [|x2 xs']].
      { applys P. introv HB. applys~ HF.
        rewrite Subst_trm_of_Trm. applys~ IH.
       { hnf. rewrite~ Trm_size_Subst_Trm_value. } }
      { applys P. intros X. introv HB. applys~ HF.
        rewrite Subst_trm_of_Trm. applys~ IH.
       { hnf. rewrite~ Trm_size_Subst_Trm_value. } }
      { sets_eq xs: (x1::x2::xs'). 
        applys P. intros Xs. introv HE HB. applys~ HF. auto.
        rewrite Subst_trm_of_Trm. applys~ IH.
       { hnf. rewrite~ Trm_size_Subst_Trm_value. } } } }
  { rename v0 into vs. destruct P as (Vs&E&M). subst vs. applys M. }
  { hnf in P. false. (* LATER: complete *) }
Qed.

Theorem Cf_sound_final : forall (t:Trm) A `{EA:Enc A} H (Q:A->hprop), 
  '(Cf t) H Q ->
  Triple (trm_of_Trm t) H Q.
Proof using. intros. applys* Cf_sound_induction. Qed.


(* ---------------------------------------------------------------------- *)
(* ** Soundness result, practical versions *)

Theorem Cf_sound : forall (t:trm) A `{EA:Enc A} H (Q:A->hprop), 
  '(Cf (Trm_of_trm t)) H Q -> 
  Triple t H Q.
Proof using.
  introv M. rewrite <- (@trm_of_Trm_on_Trm_of_trm t).
  applys~ Cf_sound_final.
Qed.

Theorem Cf_sound_App : forall n F (Vs:dyns) (f:var) (xs:vars) (t:trm) A `{EA:Enc A} H (Q:A->hprop), 
  F = val_fix f xs t ->
  List.length xs = List.length Vs ->
  '(func_iter n Cf_def Cf (Trm_of_trm (Subst_trm (List.combine (f::xs) (Dyn F::Vs)) t))) H Q ->
  App F Vs H Q.
Proof using. 
  introv EF EL M. rewrite <- Cf_unfold_iter in M.
  applys* Rule_app. applys~ Cf_sound.
Qed.

Theorem Cf_sound_App1 : forall n F A1 `{EA1:Enc A1} (V1:A1) (f:var) (x:var) (t:trm) A `{EA:Enc A} H (Q:A->hprop), 
  F = val_fix f [x] t ->
  '(func_iter n Cf_def Cf (Trm_of_trm (Subst_trm (List.combine (f::x::nil) (Dyn F::Dyn V1::nil)) t))) H Q ->
  App F [Dyn V1] H Q.
Proof using. introv EF M. applys* Cf_sound_App. Qed.



(* ********************************************************************** *)
(* * CFLifted tactics *)

Module MLCFLiftedTactics.

Ltac xlocal_core tt ::=
  try first [ applys Local_is_local 
            | applys App_is_local 
            | applys is_local_triple 
            | assumption ].


(* ---------------------------------------------------------------------- *)
(* ** Notation for characteristic formulae *)

Notation "'Val' v" :=
  (Local (Cf_val v))
  (at level 69) : charac.

Notation "'If_' v 'Then' F1 'Else' F2" :=
  (Local (Cf_if_val v F1 F2))
  (at level 69, v at level 0) : charac.

(*
Notation "'If_'' V 'Then' F1 'Else' F2" :=
  (Local (Cf_if_val (enc V) F1 F2))
  (at level 69, V at level 0) : charac.
*)

Notation "'Seq_' F1 ;; F2" :=
  (Local (Cf_seq F1 F2))
  (at level 68, right associativity,
   format "'[v' 'Seq_'  '[' F1 ']'  ;;  '/'  '[' F2 ']' ']'") : charac.

Notation "'Let' x ':=' F1 'in' F2" :=
  (Local (Cf_let F1 (fun _ _ x => F2)))
  (at level 69, x ident, right associativity,
  format "'[v' '[' 'Let'  x  ':='  F1  'in' ']'  '/'  '[' F2 ']' ']'") : charac.

Notation "'Let' [ A EA ] x ':=' F1 'in' F2" :=
  (Local (Cf_let F1 (fun A EA x => F2)))
  (at level 69, A at level 0, EA at level 0, x ident, right associativity,
  format "'[v' '[' 'Let'  [ A  EA ]  x  ':='  F1  'in' ']'  '/'  '[' F2 ']' ']'") : charac.

Notation "'App_' f x1 ;" :=
  (Local (Cf_app f [x1]))
  (at level 68, f at level 0, x1 at level 0) : charac.

Notation "'App_' f x1 x2 ;" :=
  (Local (Cf_app f [x1; x2]))
  (at level 68, f at level 0, x1 at level 0,
   x2 at level 0) : charac.

Notation "'App_' f x1 x2 x3 ;" :=
  (Local (Cf_app f [x1; x2; x3]))
  (at level 68, f at level 0, x1 at level 0, x2 at level 0,
   x3 at level 0) : charac.

Notation "'App_' f x1 x2 x3 x4 ;" :=
  (Local (Cf_app f [x1; x2; x3; x4]))
  (at level 68, f at level 0, x1 at level 0, x2 at level 0,
   x3 at level 0, x4 at level 0) : charac.

(** Record contents *)


(* TODO: notation "f1 ':= x1" := record_entry_lifted f1 x1 := (f1,Dyn x1) *)

Notation "`'{ f1 := x1 }" := 
  ((f1, Dyn x1)::nil)
  (at level 0, f1 at level 0) 
  : charac.
Notation "`'{ f1 := x1 ; f2 := x2 }" :=
  ((f1, Dyn x1)::(f2, Dyn x2)::nil)
  (at level 0, f1 at level 0, f2 at level 0) 
  : charac.
Notation "`'{ f1 := x1 ; f2 := x2 ; f3 := x3 }" :=
  ((f1, Dyn x1)::(f2, Dyn x2)::(f3, Dyn x3)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0) 
  : charac.
Notation "`'{ f1 := x1 ; f2 := x2 ; f3 := x3 ; f4 := x4 }" :=
  ((f1, Dyn x1)::(f2, Dyn x2)::(f3, Dyn x3)::(f4, Dyn x4)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0, f4 at level 0)
  : charac.


Open Scope charac.


(*--------------------------------------------------------*)
(* ** Tactics for conducting proofs *)

Tactic Notation "xcf" :=
  let f := match goal with |- App ?f _ _ _ => constr:(f) end in
  applys Cf_sound_App1 20%nat; 
  [ try reflexivity
  | simpl; unfold dyn_val; simpl; try fold f; try fold dyn_val].

Tactic Notation "xval" :=
  applys local_erase; unfold Cf_val;
  try (
    match goal with |- exists _, ?v = _ /\ _ => exists (dyn_value (decode v)) end;
    simpl enc; simpl dyn_value; split; [ reflexivity | ]).

Tactic Notation "xif" :=
  applys local_erase; unfold Cf_if;
  try esplit; split; [ reflexivity | split ].

Lemma Cf_let_intro : forall A1 (EA1:id Enc A1) (Q1:A1->hprop) (F1 : formula) (F2of : forall A1 `{EA1:Enc A1}, A1 -> formula),
  forall A (EA:id Enc A) H (Q:A->hprop),
  F1 A1 EA1 H Q1 ->
  (forall (X:A1), (@F2of A1 EA1 X) A EA (Q1 X) Q) ->
  (Cf_let F1 (@F2of)) A EA H Q.
Proof using. intros. hnf. exists A1 EA1 Q1. auto. Qed.

Tactic Notation "xlet" :=
  applys local_erase; eapply @Cf_let_intro.
  (* TODO: why does not work?
  applys local_erase; 
  let A := fresh "A" in let EA := fresh "E" A in
  evar (A:Type); evar (EA:Enc A); exists A EA; subst A EA;
  esplit; split. *)

Ltac xapp_setup tt :=
  applys local_erase; unfold Cf_app; 
  match goal with |- exists _, ?L = _ /\ _ => exists (decodes L) end;
  simpl decodes; split; [ reflexivity | ].

Tactic Notation "xapp" constr(E) := 
  xapp_setup tt; xapplys E.

Tactic Notation "xapp" := 
  xapp_setup tt; 
  let G := match goal with |- ?G => constr:(G) end in
  xspec G;
  let H := fresh "TEMP" in intros H; 
  xapplys H;
  clear H.

Tactic Notation "xfail" :=
  applys local_erase; unfold Cf_fail.

Open Scope charac.




End MLCFLiftedTactics.


(* ********************************************************************** *)
(* * Even number axiomatization for demos *)

(* LATER
Fixpoint even_nat (n:nat) :=
  match n with
  | O => true 
  | S n => neg (even_nat n)
  end.

Definition even (n:Z) := even_nat (Z.to_nat n).

Lemma even_minus_two : forall n, even n -> even (n - 2).
Admitted.
*)


(* ********************************************************************** *)
(* * Himpl demos *)

(* LATER
Lemma hsimpl_demo_1 : forall r,
  (r ~~~> 6) ==>
  (Hexists (n:int), (r ~~~> n) \* \[even n]).
Proof using.
  intros. hsimpl. auto. 
Qed.

Lemma hpull_demo_1 : forall r,
  (Hexists (n:int), (r ~~~> n) \* \[even n]) ==>
  (Hexists (m:int), \[even m] \* (r ~~~> (m + 2))).
Proof using.
  intros. hpull. intros n Hn.
  hsimpl (n-2). 
  math_rewrite ((n-2) + 2 = n). auto.
  applys even_minus_two. auto.
Qed.

*)




(* ********************************************************************** *)
(* * DEPRECATED *)

(*

Inductive forced_decode : forall A `{EA:Enc A}, val -> (A -> Prop) -> Prop := 
  | forced_decode_intro : forall (v:val),
      let d := decode v in 
      forall (K : dyn_type d -> Prop),
      K (dyn_value d) ->
      @forced_decode _ (dyn_enc d) v K. 

Implicit Arguments forced_decode [A [EA]].

Definition Cf_if_val v (F1 F2 : formula) : formula := fun `{EA:Enc A} H (Q:A->hprop) =>
  forced_decode v (fun (V:int) =>
    (V <> 0 -> 'F1 H Q) /\ (V = 0 -> 'F2 H Q)).


Definition Cf_val v : formula := fun `{Enc A} H (Q:A->hprop) =>
  forced_decode v (fun (V:A) => H ==> Q V).

Inductive Cf_val'' v : formula :=
  | Cf_val_intro : 
     let d := decode v in
     forall H (Q:(dyn_type d)->hprop),
     @Cf_val' v _ (dyn_enc d) H (fun x => \[x = dyn_value d] \* H).


Definition Cf_app f vs : formula := fun `{Enc A} H (Q:A->hprop) =>
  app f (decodes vs) H Q.

Ltac save_K K := 
  match goal with H: forced_decode ?v ?Kbody |- _ =>
    sets_eq K: (Kbody) end.

Ltac inverts_forced P := 
  let K := fresh "K" in hnf in P; save_K K; destruct P;
  instantiate; subst K.


*)

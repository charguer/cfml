Set Implicit Arguments.
Require Import LibCore Shared ModelLambda.
Open Scope state_scope.


(** Predicate over pairs *)

(* NOT USED
Definition prod_st A B (v:A*B) (P:A->Prop) (Q:B->Prop) := (* todo move to TLC *)
  let (x,y) := v in P x /\ Q y.

Definition prod_st2 A B (P:binary A) (Q:binary B) (v1 v2:A*B) := (* todo move to TLC *)
  let (x1,y1) := v1 in 
  let (x2,y2) := v2 in 
  P x1 x2 /\ Q y1 y2.
*)

Definition prod_func A B (F:A->A->A) (G:B->B->B) (v1 v2:A*B) := (* todo move to TLC *)
  let (x1,y1) := v1 in 
  let (x2,y2) := v2 in 
  (F x1 x2, G y1 y2).



(********************************************************************)
(* ** Heaps *)

(*------------------------------------------------------------------*)
(* ** Representation of heaps *)

(** Representation of heaps *)

Definition heap : Type := (state*state)%type. 

(** Empty heap *)

Definition heap_empty : heap :=
  (state_empty, state_empty).

(** Union of heaps *)

Definition heap_union (h1 h2 : heap) : heap :=
  prod_func state_union state_union h1 h2.

(** Starable heaps: disjoint owned heaps, compatible read-only heaps *)

Definition heap_starable (h1 h2 : heap) : Prop :=
  let '(f1,g1) := h1 in
  let '(f2,g2) := h2 in
     state_compat g1 g2 
  /\ (\# f1 f2 (g1 \+ g2)).


(*------------------------------------------------------------------*)
(* ** Definition of heap predicates *)

(** [hprop] is the type of predicates on heaps *)

Definition hprop := heap -> Prop.

(** Empty heap *)

Definition hprop_empty : hprop := 
  fun '(f,g) => f = state_empty /\ g = state_empty.

(** Lifting of predicates *)

Definition hprop_empty_st (H:Prop) : hprop :=
  fun h => h = heap_empty /\ H.

(** Singleton heap *)

Definition hprop_single (l:loc) (v:val) : hprop := 
  fun '(f,g) => f = state_single l v /\ g = state_empty.

(** Heap union *)

Definition hprop_star (H1 H2 : hprop) : hprop := 
  fun '(f,g) => exists f1 g1 f2 g2, 
       H1 (f1,g1)
    /\ H2 (f2,g2)
    /\ heap_starable (f1,g1) (f2,g2)
    /\ f = f1 \+ f2 
    /\ g = g1 \+ g2.

  (* state_disjoint f1 f2 /\ state_compat g1 g2 *)

(** Lifting of existentials *)

Definition hprop_exists A (Hof : A -> hprop) : hprop := 
  fun hg => exists x, Hof x hg.

(** Garbage collection predicate: [Hexists H, H]. *)

Definition hprop_gc : hprop := 
  hprop_exists (fun H => H).

Global Opaque hprop_empty hprop_empty_st hprop_single 
              hprop_star hprop_exists hprop_gc. 


(*------------------------------------------------------------------*)
(* ** Notation for heap predicates *)

Notation "\[]" := (hprop_empty) 
  (at level 0) : heap_scope.

Notation "\[ L ]" := (hprop_empty_st L) 
  (at level 0, L at level 99) : heap_scope.

Notation "r '~~>' v" := (hprop_single r v)
  (at level 32, no associativity) : heap_scope.

Notation "H1 '\*' H2" := (hprop_star H1 H2)
  (at level 41, right associativity) : heap_scope.

Notation "Q \*+ H" := (fun x => hprop_star (Q x) H)
  (at level 40) : heap_scope.

Notation "'Hexists' x1 , H" := (hprop_exists (fun x1 => H))
  (at level 39, x1 ident, H at level 50) : heap_scope.
Notation "'Hexists' x1 : T1 , H" := (hprop_exists (fun x1:T1 => H))
  (at level 39, x1 ident, H at level 50, only parsing) : heap_scope.
Notation "'Hexists' ( x1 : T1 ) , H" := (hprop_exists (fun x1:T1 => H))
  (at level 39, x1 ident, H at level 50, only parsing) : heap_scope.

Notation "\GC" := (hprop_gc) : heap_scope. 

Open Scope heap_scope.
Bind Scope heap_scope with hprop.
Delimit Scope heap_scope with h.



(********************************************************************)
(* ** Properties *)

(*------------------------------------------------------------------*)
(* ** Properties of [heap_starable] *)

Lemma heap_starable_def : forall f1 g1 f2 g2,
    heap_starable (f1,g1) (f2,g2) 
  = (state_compat g1 g2 /\ (\# f1 f2 (g1 \+ g2))).
Proof using. auto. Qed.

Hint Rewrite heap_starable_def : rew_disjoint.

Lemma heap_starable_sym : forall h1 h2,
  heap_starable h1 h2 -> 
  heap_starable h2 h1.
Proof using.
  intros (f1,g1) (f2,g2) (M1&M2). split.
  { applys~ state_compat_sym. }
  { state_disjoint. }
Qed.


(*------------------------------------------------------------------*)
(* ** Properties of [hprop] and [==>] *)

Global Instance hprop_inhab : Inhab hprop.
Proof using. intros. apply (prove_Inhab hprop_empty). Qed.

Section HeapProp.

Implicit Types H : hprop.
Transparent hprop_empty hprop_empty_st hprop_single hprop_star hprop_gc heap_union.

Lemma hprop_empty_prove : 
  \[] heap_empty.
Proof using. hnfs~. Qed.

Lemma hprop_empty_inv : forall h,
  \[] h -> h = heap_empty.
Proof using. intros [h g] (Mh&Mg). subst*. Qed.

Lemma hprop_empty_inv' : forall f g,
  \[] (f,g) -> f = state_empty /\ g = state_empty.
Proof using. introv (Mh&Mg). subst*. Qed.

Lemma hprop_empty_st_prove : forall (P:Prop),
  P -> 
  \[P] heap_empty.
Proof using. intros. hnfs~. Qed.

Lemma hprop_empty_st_inv : forall h (P:Prop),
  \[P] h -> 
  h = heap_empty /\ P.
Proof using. intros. auto. Qed.

Lemma hprop_empty_st_inv' : forall f g (P:Prop),
  \[P] (f,g) -> f = state_empty /\ g = state_empty /\ P.
Proof using. introv (Mh&Mg). inverts Mh. autos*. Qed.

Lemma hprop_star_comm : forall H1 H2,
   H1 \* H2 = H2 \* H1.
Proof using. 
  intros. unfold hprop, hprop_star. extens. intros (f,g).
  hint state_compat_sym. 
  iff (f1&g1&f2&g2&U1&U2&U3&U4&U5); lets (U3a&U3b): U3.
  { exists f2 g2 f1 g1. splits~. 
    { applys~ heap_starable_sym. }
    { state_eq. }
    { rewrite~ state_union_comm_compat. } }
  { exists f2 g2 f1 g1. splits~. 
    { applys~ heap_starable_sym. }
    { state_eq. }
    { rewrite~ state_union_comm_compat. } }
Qed. (* TODO: exploit symmetry *)

Lemma hprop_star_empty_l : forall H,
   \[] \* H = H.
Proof using.
Admitted.
(*
  intro. unfold hprop, hprop_star. extens. intros h.
  iff (h1&h2&M1&M2&D&U) M. 
  hnf in M1. subst h1 h. rewrite~ heap_union_empty_l.
  exists heap_empty h. hint hprop_empty_prove. splits~. 
Qed.
*)

Lemma hprop_star_empty_r : forall H, 
  H \* \[] = H.
Proof using.
  apply neutral_r_from_comm_neutral_l.
  applys hprop_star_comm. applys hprop_star_empty_l.
Qed.

Lemma hprop_star_assoc : forall H1 H2 H3,
  (H1 \* H2) \* H3 = H1 \* (H2 \* H3).
Proof using. 
Admitted.
(*
  intros H1 H2 H3. unfold hprop, hprop_star. extens. intros h. split.
  { intros (h'&h3&(h1&h2&M3&M4&D'&U')&M2&D&U). subst h'.
    exists h1 (heap_union h2 h3). rewrite heap_disjoint_union_eq_l in D.
    splits.
    { auto. }
    { exists h2 h3. splits*. }
    { rewrite* heap_disjoint_union_eq_r. }
    { rewrite~ <- heap_union_assoc. } }
  { intros (h1&h'&M1&(h2&h3&M3&M4&D'&U')&D&U). subst h'.
    exists (heap_union h1 h2) h3. rewrite heap_disjoint_union_eq_r in D.
    splits.
    { exists h1 h2. splits*. }
    { auto. }
    { rewrite* heap_disjoint_union_eq_l. }
    { rewrite~ heap_union_assoc. } }
Qed. (* later: exploit symmetry in the proof *)
*)

(* NOT NEEDED HERE
Lemma hprop_star_single_same_loc_disjoint : forall (l:loc) (v1 v2:val),
  (hprop_single l v1) \* (hprop_single l v2) ==> \[False].
Proof using.
  intros. unfold hprop_single. intros h (h1&h2&E1&E2&D&E). false.
  subst. applys* state_single_same_loc_disjoint.
Qed.
*)

Lemma hprop_star_prop_elim : forall (P:Prop) H h,
  (\[P] \* H) h -> P /\ H h.
Proof using.
  intros P H (f,g) (f1&g1&f2&g2&(M0&M1)&M2&M3&M4&M5).
  inverts M0. subst. do 2 rewrite~ state_union_empty_l.
Qed.

Lemma himpl_refl : forall H,
  H ==> H.
Proof using. intros h. auto. Qed.

Lemma himpl_trans : forall H1 H2 H3,
  (H1 ==> H2) -> (H2 ==> H3) -> (H1 ==> H3).
Proof using. introv M1 M2. intros h H1h. eauto. Qed.

Lemma himpl_antisym : forall H1 H2,
  (H1 ==> H2) -> (H2 ==> H1) -> (H1 = H2).
Proof using. introv M1 M2. applys prop_ext_1. intros h. iff*. Qed.

Lemma himpl_cancel : forall H1 H2 H2',
  H2 ==> H2' -> (H1 \* H2) ==> (H1 \* H2').
Proof using.
  introv W. intros (f,g) (f1&g1&f2&g2&M1&M2&M3&M4&M5).
  exists* f1 g1 f2 g2.
Qed.

Lemma himpl_extract_prop : forall (P:Prop) H H',
  (P -> H ==> H') -> (\[P] \* H) ==> H'.
Proof using. 
  introv W Hh. lets (?&?): hprop_star_prop_elim Hh. applys* W.
Qed.

Lemma himpl_extract_exists : forall A H (J:A->hprop),
  (forall x, J x ==> H) -> (hprop_exists J) ==> H.
Proof using. introv W. intros h (x&Hh). applys* W. Qed.

Lemma himpl_inst_prop : forall (P:Prop) H H',
  P -> H ==> H' -> H ==> (\[P] \* H').
Proof using.
  introv HP W. intros (f,g) Hh.
  exists state_empty state_empty f g. splits.
  { applys* hprop_empty_st_prove. }
  { applys* W. }
  { split. applys~ state_compat_empty_l. rewrite state_union_empty_l. state_disjoint. }
Qed.
*)

Lemma himpl_inst_exists : forall A (x:A) H J,
  (H ==> J x) -> H ==> (hprop_exists J).
Proof using. introv W h. exists x. apply~ W. Qed.

Lemma himpl_remove_gc : forall H H',
  H ==> H' ->
  H ==> H' \* \GC.
Proof using.
Admitted. (* 
  introv M. intros h Hh. exists h heap_empty. splits*.
  exists \[]. applys hprop_empty_prove.
Qed. *)

End HeapProp.

Hint Resolve hprop_empty_prove hprop_empty_st_prove.





(********************************************************************)
(* ** Definition of the [storable] predicate *)

Definition storable (H:hprop) : Prop := 
  forall h g, H (h,g) -> g = state_empty.

Definition storable' A (Q:A->hprop) :=
  forall x, storable (Q x).

Lemma storable_empty : 
  storable hprop_empty.
Proof using. introv (M1&M2). auto. Qed.

Lemma storable_single : forall l v,
  storable (hprop_single l v).
Proof using. introv (M1&M2). auto. Qed.

Lemma storable_star : forall H1 H2,
  storable H1 ->
  storable H2 ->
  storable (H1 \* H2).
Proof using.
  introv M1 M2 (f1&g1&f2&g2&N1&N2&N3&N4&N5).
  subst g. rewrites (>> M1 N1). rewrites (>> M2 N2).
  rewrite~ state_union_empty_r.
Qed.

Lemma storable_exists : forall A (J:A->hprop),
  storable' J ->
  storable (hprop_exists J).
Proof using. introv M (x&N). rewrites~ (>> M N). Qed.

Lemma storable_himpl : forall H1 H2,
  storable H2 ->
  (H1 ==> H2) ->
  storable H1.
Proof using. introv HS HI M. lets: HI M. applys* HS. Qed.



(********************************************************************)
(* ** Definition of the [Duplicatable] predicate *)

Definition duplicatable (H:hprop) : Prop :=
  H ==> H \* H.


(********************************************************************)
(* ** Definition of the [RO] modifier *)

Definition RO (H:hprop) : hprop :=
  fun '(f,g) => 
      f = state_empty
   /\ exists f1 g1, H (f1,g1)
                 /\ (\# f1 g1)
                 /\ g = f1 \+ g1.

Lemma RO_elim : forall H h g,
  (RO H) (h,g) -> h = state_empty.
Proof using. introv (M1&M2). auto. Qed.

Lemma RO_duplicatable : forall H,
  duplicatable (RO H). 
Proof using.
  intros H (f&g) M.
  lets E: RO_elim M. subst.
  exists state_empty g state_empty g. splits~. 
  { split.
    { apply state_compat_refl. }
    { state_disjoint. } }
  { state_eq. }
  { rewrite~ state_union_idempotent. } 
Qed.

Lemma RO_exists : forall A (J:A->hprop),
    RO (hprop_exists J) 
  = Hexists x, RO (J x).
Proof using.
  intros. apply prop_ext_1. intros (f,g). 
  iff (M1&(f1&g1&(x&N1)&N2&N3)) (x&(E&f1&g1&N1&N2&N3)).
  { exists x. split*. }
  { split*. exists f1 g1. splits*. exists* x. }
Qed.

Lemma RO_star : forall H1 H2,
  RO (H1 \* H2) ==> (RO H1 \* RO H2).
Proof using.
  intros. intros (h,g).
  intros (M1&(f3&g3&(f1&g1&f2&g2&U1&U2&(U3&U5)&U4&U6)&N1&N2)).
  exists state_empty (f1 \+ g1) state_empty (f2 \+ g2).
  subst f3 g3. 
  rewrite state_disjoint_union_eq_r in N1.
  do 2 rewrite state_disjoint_union_eq_l in N1.
  destruct N1 as ((N1a&N1b)&N1c&N1d). 
  splits. 
  { split~. exists___. splits*. }
  { split~. exists___. splits*. }
  { split. 
   { intros l v1 v2 E1 E2.
      unfold state_union, pfun_union in E1,E2. simpls.
      unfold state_disjoint, pfun_disjoint in *.
      asserts U3': (\# f1 f2). state_disjoint.
      destruct (U3' l) as [C|C].
      { rewrite C in E1. cases (state_data f2 l).
        { destruct (N1b l). congruence. congruence. }
        { applys* U3. } }
      { rewrite C in E2. cases (state_data f1 l).
        { destruct (N1c l). congruence. congruence. }
        { applys* U3. } } }
    { state_disjoint. } }
  { state_eq. }
  { state_eq. }
Qed.

(** Equivalence with a direct definition of read-only singleton heap *)

Definition hprop_single_ro (l:loc) (v:val) : hprop := 
  fun '(h,g) => h = state_empty /\ g = state_single l v.

Lemma RO_singe : forall l v,
    (RO (hprop_single l v))
  = hprop_single_ro l v.
Proof using. 
  Transparent hprop_single.
  intros. unfold hprop_single, hprop_single_ro.
  apply prop_ext_1. intros (h,g).
  iff (M1&h1&g1&(M2a&M2b)&M3&M4) (M1&M2).
  { subst. split~. state_eq. }
  { split. auto. exists g h. subst. splits~. state_eq. }
Qed.


(* Remark: "RO (r ~~> v \* r ~~:> v)"  iff \[False] *)


(* NOT TRUE
Lemma RO_star : forall H1 H2,
  RO (H1 \* H2) = (RO H1 \* RO H2).
Proof using.
  intros. apply prop_ext_1. intros (h,g). split. 
  { intros (M1&(h3&g3&(h1&g1&h2&g2&U1&U2&U3&U4&U5&U6)&N1&N2)).
    skip. }
  { intros (h1&g1&h2&g2&(M1a&h11&g11&M1b&M1c&M1d)
             &(M2a&h12&g12&M2b&M2c&M2d)&M3&M4&M5&M6).
    split.
    { subst~. }
    { exists (h11 \+ h12) (g11 \+ g12). splits~.
      { exists h11 g11 h12 g12. splits~. subst. skip. skip. }
    { subst. skip. }
    { subst. skip. } } 
Qed.
*)


(********************************************************************)
(* ** Rules *)

Implicit Types H : hprop.
Implicit Types Q : val -> hprop.

(* DEPRECATEd
Coercion state_of_heap (h:heap) : state :=
  let '(f,g) := h in f \+ g.


Definition triple t (H:hprop) (Q:val->hprop) :=
  forall f1 g1 h2,
  heap_starable (f1,g1) h2 -> 
  H (f1,g1) -> 
  exists f1' f1'' r, 
     (\# f1' f1'' /\ heap_starable ((f1' \+ f1''), g1) h2)
    /\ red (f1 \+ g1 \+ h2) t (f1' \+ f1'' \+ g1 \+ h2) r
    /\ Q r (f1',state_empty).

*)

(** Triples *)

Definition triple t (H:hprop) (Q:val->hprop) :=
  forall f1 g1 f2 g2,
  heap_starable (f1,g1) (f2,g2) -> 
  H (f1,g1) -> 
  exists f1' f1'' r, 
     (\# f1' f1'' /\ heap_starable ((f1' \+ f1''), g1) (f2, g2))
    /\ red (f1 \+ g1 \+ f2 \+ g2) t (f1' \+ f1'' \+ g1 \+ f2 \+ g2) r
    /\ Q r (f1',state_empty). 


(* TODO: move *)



(** Structural rules *)

Lemma rule_extract_prop : forall t (P:Prop) H Q,
  (P -> triple t H Q) ->
  triple t (\[P] \* H) Q.
Proof using. 
Admitted.
(*
  introv M D. 
  intros (h11&g11&h12&g12&N11&N12&Dh1&Uh1&Dg1&Ug1).
  lets (N11a&N11b&HP): hprop_empty_st_inv' N11. subst h11 g11.
  rewrite state_union_empty_l in *. subst h12 g12.
  applys* M.
Qed.
*)

Lemma rule_extract_exists : forall t (J:val->hprop) Q,
  (forall x, triple t (J x) Q) ->
  triple t (hprop_exists J) Q.
Proof using. introv M D (x&Jx). applys* M. Qed.

Lemma rule_consequence_gc : forall t H Q H' Q',
  H ==> H' \* \GC ->
  triple t H' Q' ->
  Q' ===> Q \*+ \GC ->
  triple t H Q.
Proof using. 
Admitted. 
(*
  introv IH M IQ. intros h1 h2 D1 H1. 
  lets H1': IH (rm H1).
  lets (h1a&h1b&H1a&H1b&D1ab&E12): (rm H1').
  lets (h1'&h3&r&Da&Ra&Qa): M (h1b \+ h2) (rm H1a).
  { subst. state_disjoint. }
  lets Qa': IQ (rm Qa).
  lets (h1a'&h1b'&H1a'&H1b'&D1ab'&E12'): (rm Qa').  
  exists h1a' (h1b' \+ h1b \+ h3) r. splits.
  { subst. state_disjoint. }
  { subst. applys_eq Ra 2 4.
    { rewrite~ heap_union_assoc. }
    { rewrite (@heap_union_comm h1b h2).
      repeat rewrite heap_union_assoc. fequals.
      rewrite <- heap_union_comm_assoc.
      rewrite <- heap_union_comm_assoc.
      repeat rewrite heap_union_assoc. fequals. fequals.
      rewrite~ heap_union_comm.
      state_disjoint. state_disjoint. state_disjoint. state_disjoint. } }
  { auto. }
Qed.*)


Lemma rule_consequence : forall H' Q' t H Q,
  H ==> H' ->
  triple t H' Q ->
  Q ===> Q' ->
  triple t H Q'.
Proof using. 
  introv WH M WQ. applys rule_consequence_gc M.
  { applys~ himpl_remove_gc. }
  { intros r. applys~ himpl_remove_gc. }
Qed.


Lemma state_compat_transfer : forall f1 f2 f3,
  state_compat f1 f2 ->
  state_compat (f1 \+ f2) f3 ->
  state_compat f1 (f2 \+ f3).
Proof using.
  introv M1 M2. intros l v1 v2 E1 E2. 
  specializes M1 l v1 v2. specializes M2 l v1 v2.
  simpls. unfolds pfun_union.
  specializes M1 E1. rewrite E1 in M2. specializes~ M2 __.
  clear E1.
  cases (state_data f2 l). 
  { inverts E2. applys* M1. }
  { applys* M2. }
Qed.

Lemma state_compat_transfer' : forall f1 f2 f3,
  state_compat f1 f2 ->
  state_compat (f1 \+ f2) f3 ->
  state_compat f2 (f1 \+ f3).
Proof using.
  hint state_compat_sym.
  introv M1 M2. applys* state_compat_transfer.
  rewrite~ state_union_comm_compat. 
Qed.


(*
Lemma heap_starable_transfer : forall h1 h2 h3,
  heap_starable h1 h2 ->
  heap_starable (heap_union h1 h2) h3 ->
  heap_starable h1 (heap_union h2 h3).



Lemma heap_starable_transfer : forall f1 f2 f3 h,
  heap_starable (f1 \+ f2, f3) h ->
  heap_starable (f1, f3 \+ f2) h.
Proof using. skip. Qed.
*)

Lemma rule_frame_read_only : forall t H1 Q1 H2,
  triple t (H1 \* RO H2) Q1 ->
  storable H2 ->
  triple t (H1 \* H2) (Q1 \*+ H2).
Proof using. 
  introv M1 SH2.
  intros f1 g1 f2 g2 (D1a&D1b).
  intros (f1a&g1a&f1b&g1b&R1&R2&(R3a&R3b)&R4&R5).
  lets E: SH2 R2. subst g1b.
  rewrite state_union_empty_r in R5. subst g1.
  lets (f1'a&f1''a&ra&Da&Ra&Qa): M1 f1a (g1a \+ f1b) f2 g2.
  { subst f1. split. (* applys~ heap_starable_transfer. *)
    { applys state_compat_union_l.
      { auto. }
      { applys~ state_disjoint_compat. state_disjoint. } }
    { state_disjoint. } }
  { exists f1a g1a state_empty f1b. splits.
    { auto. }
    { split.
      { auto. }
      { exists f1b state_empty. splits~. state_eq. } }
    { subst. split.   
      { applys~ state_disjoint_compat. state_disjoint. }
      { state_disjoint. } } 
    { state_eq. }
    { auto. } }
  { simpl in Ra. subst f1. exists (f1'a \+ f1b) f1''a ra. splits.
    { state_disjoint. }
    { simpl. applys_eq Ra 2 4; state_eq. }
    { exists f1'a state_empty f1b state_empty. splits~.
      { split. 
        { applys state_compat_refl. }
        { state_disjoint. } }
      { state_eq. } } }
Qed. (* TODO: more automation for assoc/comm rewrite *)




(* TODO: devrait se prouver avec l'aide de GC-pre 
   (qui n'est plus derivable) ?
Lemma rule_frame_basic : forall t H1 Q1 H2,
  triple t H1 Q1 ->
  storable H2 ->
  triple t (H1 \* H2) (Q1 \*+ H2).
Proof using. 
  introv M1 SH2.
  intros h1 g1 h2 D1 (h1a&g1a&h1b&g1b&R1&R2&R3&R4&R5&R6).
  lets E: SH2 R2. subst g1b.
  lets (h1'a&h3a&ra&Da&Ra&Qa): M1 h1a g1a (h1b \+ h2).
  { subst h1. rew_disjoint. jauto. skip. }
  { auto. }
  { subst h1. rewrite <- state_union_assoc in Ra.
    exists (h1'a \+ h1b) h3a ra. splits.
    { rew_disjoint. jauto. }
    { do 2 rewrite <- state_union_assoc. applys_eq Ra 2 4.
      { subst. skip. }
      { subst. skip. } }
    { exists h1'a state_empty h1b state_empty. splits~.
      { skip. }
      { skip. } }
Qed.
*)
  
  (* Lemma rule_frame : forall t H Q H',
    triple t H Q ->
    triple t (H \* H') (Q \*+ H').
  Proof using. 
    introv M. intros h1 h2 D1 (h1a&h1b&H1a&H1b&D1ab&E12).
    lets (h1'a&h3a&ra&Da&Ra&Qa): M h1a (h1b \+ h2).
    { subst h1. state_disjoint. }
    { auto. }
    { subst h1. rewrite heap_union_assoc in Ra.
      exists (h1'a \+ h1b) h3a ra. splits.
      { state_disjoint. }
      { do 2 rewrite heap_union_assoc. auto. }
      { exists h1'a h1b. splits~. state_disjoint. } }
  Qed. *)


(** Term rules *)

Lemma rule_val : forall v H Q,
  H ==> Q v ->
  storable H -> 
  triple (trm_val v) H Q.
Proof using. 
  introv M HS. intros f1 g1 h2 D H1.
  specializes HS H1. subst g1. 
  exists f1 state_empty v.
  rewrite state_union_empty_r; repeat rewrite state_union_empty_l.
  splits.
  { state_disjoint. }
  { applys red_val. }
  { auto. }
Qed.

Lemma rule_if : forall v t1 t2 H Q,
  triple (If v = val_int 0 then t2 else t1) H Q ->
  triple (trm_if v t1 t2) H Q.
Proof using.
  introv M D H1.
  forwards (h1'&h3&r&R1&R2&R3): M D H1.
  exists h1' h3 r. splits.
  { state_disjoint. }
  { applys~ red_if. }
  { auto. }
Qed.

(* DEPRECATED
Lemma state_union_comm_assoc'' : forall h1 h2 h3,
  \# h1 h2 h3 ->
  h1 \+ h2 \+ h3 = h2 \+ h1 \+ h3.
Proof using. 
  introv M. rewrite <- state_union_comm_assoc.
  rewrite <- state_union_comm_assoc.
  do 2 rewrite~ state_union_assoc. fequals.
  rewrite~ state_union_comm_disjoint. state_disjoint.
  state_disjoint. state_disjoint.
Qed.
*)

(* FALSE
Lemma hprop_decompose : forall H, 
  exists H1 H2, 
     H = H1 \* H2 
  /\ storable H1
  /\ duplicatable H2.
Proof using.
  intros. 
  exists (fun (f:heap) => let '(h,g) := f in = (h,state_empty)).


Lemma hprop_decompose : forall H, 
  H ==> Hexists H1, Hexists H2, H1 \* H2 \* \[ storable H1 ] \* \[ duplicatable H2 ].
Proof using.
  intros H (h,g) M.
  exists (= (h,state_empty)). 
  exists (= (state_empty,g)).
  exists h g state_empty state_empty. splits~.
  exists (fun (f:heap) => let '(h,g) := f in = (h,state_empty)).
*)



(* NOT NEEDED

Lemma heap_starable_sub_union_l : forall f g f' g' h,
  heap_starable (f \+ f', g \+ g') h -> 
  heap_starable (f,g) h.
Proof using.
  introv M. destruct h as [f'' g''].
  unfolds heap_starable. destruct M as (M1&M2). split.
  { intros l v1 v2 E1 E2. specializes M1 l v1 v2.
    applys* M1. simpl. unfold pfun_union. rewrite~ E1. }   
  { state_disjoint. }
Qed.

Lemma heap_starable_sub_union_r : forall f g f' g' h,
  heap_starable (f' \+ f, g' \+ g) h -> 
  state_compat g' g ->
  heap_starable (f,g) h.
Proof using.
  introv M N. destruct h as [f'' g''].
  unfolds heap_starable. destruct M as (M1&M2). split.
  { intros l v1 v2 E1 E2. specializes M1 l. 
    specializes N l.
    cases (state_data g' l) as C.
    { forwards~: N E1. subst. simpls. unfolds pfun_union.
      rewrite C in M1. applys* M1. }
    { simpls. unfolds pfun_union. rewrite C in M1. applys* M1. } }
  { state_disjoint. }
Qed.

Lemma state_of_heap_heap_union_l : forall f g h,
  heap_starable (f, g) h ->
    state_of_heap (heap_union (f, g) h) 
  = f \+ g \+ state_of_heap h.
Proof using.
  intros f g [f' g'] M. simpl. st_eq.
  unfolds in M. state_disjoint.
Qed.

*)



Lemma rule_let : forall x t1 t2 H1 H2 Q Q1,
  triple t1 H1 Q1 ->
  (forall (X:val), triple (subst_trm x X t2) (Q1 X \* H2) Q) ->
  triple (trm_let x t1 t2) (H1 \* H2) Q.
Proof using.
  introv M1 M2.
  intros f1 g1 f2 g2 (D1a&D1b) (f11&g11&f12&g12&R1&R2&(R3a&R3b)&R4&R5).
  lets~ (f1'a&f1''a&ra&(Da1&Da2)&Ra&Qa): M1 f11 g11
     (f12 \+ f2) (g12 \+ g2).
  { subst. split. 
    { applys* state_compat_transfer. }
    { state_disjoint. } }
  simpl in Ra.
  forwards (h1'b&f1''b&rb&Db&Rb&Qb): 
    M2 ra (f1'a \+ f12) g12 (f1''a \+ f2) (g11 \+ g2).
    { subst. split.
      { applys~ state_compat_transfer'. }
      { state_disjoint. } }
    { exists f1'a state_empty f12 g12. splits*.
      { split.
        { applys state_compat_empty_l. }
        { state_disjoint. } }
      { state_eq. } }
  simpl in Rb. exists h1'b (f1''a \+ f1''b) rb. splits.
    { subst. state_disjoint. }
    { applys red_let (f1'a \+ f12 \+ f1''a \+ g1 \+ f2 \+ g2) ra.
      { applys_eq Ra 4 2; state_eq. }
      { subst. rewrite~ (@state_union_comm_compat g11 g12).
        applys_eq Rb 2 4; state_eq. } }
    { auto. }
Qed.



  (* Lemma rule_let : forall x t1 t2 H Q Q1,
    triple t1 H Q1 ->
    (forall (X:val), triple (subst_trm x X t2) (Q1 X) Q) ->
    triple (trm_let x t1 t2) H Q.
  Proof using.
    introv M1 M2. intros h1 h2 D1 H1.
    lets (h1'a&h3a&ra&Da&Ra&Qa): M1 D1 H1.
    rew_disjoint. 
    forwards (h1'b&h3b&rb&Db&Rb&Qb): M2 ra h1'a (h2 \+ h3a).
    { state_disjoint. }
    { auto. }
    { exists h1'b (h3a \+ h3b) rb. splits.
      { state_disjoint. }
      { rewrite heap_union_assoc in Rb. constructors*. }
      { auto. } }
  Qed. *)

Lemma rule_let_val : forall x v1 t2 H Q,
  (forall (X:val), X = v1 -> triple (subst_trm x X t2) H Q) ->
  triple (trm_let x (trm_val v1) t2) H Q.
Proof using. 
  introv M. applys rule_let (fun x => \[x = v1] \* H).
  { applys rule_val. applys~ himpl_inst_prop. }
  { intros X. applys rule_extract_prop. applys M. }
Qed.

Lemma rule_app : forall f x F V t1 H Q,
  F = (val_fix f x t1) ->
  triple (subst_trm f F (subst_trm x V t1)) H Q ->
  triple (trm_app F V) H Q.
Proof using.
  introv EF M. subst F. intros h1 h2 D1 H1.
  lets (h1'a&h3a&ra&Da&Ra&Qa): M D1 H1.
  exists h1'a h3a ra. splits. 
  { state_disjoint. }
  { applys~ red_app. }
  { auto. }
Qed.

Lemma rule_let_fix : forall f x t1 t2 H Q,
  (forall (F:val), 
    (forall X H' Q', 
      triple (subst_trm f F (subst_trm x X t1)) H' Q' ->
      triple (trm_app F X) H' Q') ->
    triple (subst_trm f F t2) H Q) ->
  triple (trm_let f (trm_val (val_fix f x t1)) t2) H Q.
Proof using.
  introv M. applys rule_let_val. intros F EF. 
  applys (rm M). clears H Q. intros X H Q.
  applys rule_app. auto.
Qed.

Lemma rule_new : forall v,
  triple (trm_new v) \[] (fun r => Hexists l, \[r = val_loc l] \* l ~~> v).
Proof using.
  Transparent hprop_single.
  introv D H1. unfolds hprop_single.
  lets: hprop_empty_inv H1. subst h1. rewrite heap_union_empty_l.
  asserts (l&Hl): (exists l, (state_data h2) l = None).
  { skip. } (* infinitely many locations -- TODO *)
  asserts Fr: (state_disjoint h2 (state_single l v)).
  { unfolds state_disjoint, pfun_disjoint, state_single. simpls.
    intros x. case_if~. }
  exists (state_single l v) heap_empty (val_loc l).
  rewrite heap_union_empty_r. splits.
  { rew_disjoint. splits*. }
  { applys~ red_new. }
  { exists l heap_empty (state_single l v). splits~. } 
Qed.

Lemma rule_get : forall v l,
  triple (trm_get (val_loc l)) (l ~~> v) (fun x => \[x = v] \* l ~~> v).
Proof using.
  Transparent hprop_single.
  introv D H1. unfolds hprop_single.  
  exists h1 heap_empty v. rewrite heap_union_empty_r. 
  splits.
  { state_disjoint. }
  { applys~ red_get. }
  { exists heap_empty h1. splits~. }
Qed.

Lemma rule_set : forall l v w,
  triple (trm_set (val_loc l) w) (l ~~> v) (fun r => \[r = val_unit] \* l ~~> w).
Proof using.
  Transparent hprop_single.
  introv D H1. unfolds hprop_single.  
  exists (state_single l w) heap_empty val_unit.
  rewrite heap_union_empty_r. 
  splits.
  { rew_disjoint. splits*. subst h1. 
    unfolds heap_disjoint, state_disjoint, pfun_disjoint, state_single. simpls.
    intros x. specializes D x. case_if; intuition auto_false. }
  { applys~ red_set v. }
  { exists heap_empty (state_single l w). splits~. }
Qed.



(********************************************************************)
(* ** DEPRECATED *)


    (*------------------------------------------------------------------*)
    (* ** Properties of heap empty *)

    (*
    Section HeapEmpty.
    Transparent hprop_empty_st.

    Lemma hprop_empty_prove : 
      \[] hprop_empty.
    Proof using. hnfs~. Qed.

    Lemma hprop_empty_st_prove : forall (P:Prop),
      P -> \[P] hprop_empty.
    Proof using. intros. hnfs~. Qed.

    End HeapEmpty.

    Hint Resolve hprop_empty_prove hprop_empty_st_prove.
    *)

    (*------------------------------------------------------------------*)
    (* ** Properties of [star] and [pack] *)

    Section HeapStar.
    (* Transparent hprop_star heap_union. *)

    (* TODO; move *)
    Axiom state_compat_sym : sym state_compat.

    Lemma star_comm : comm hprop_star. 
    Proof using. 
      intros H1 H2. unfold hprop, hprop_star. extens. intros (h,g).
      iff (h1&g1&h2&g2&M1&M2&M3&M4&M5&M6);
       rewrite state_union_comm_disjoint in M4, M6; auto;
       applys_to M5 state_compat_sym.
       exists* h2 g2 h1 g1. 
    (*lets: disjoint_sym. *) skip.
       exists* h2 g2 h1 g1. 
    (*lets: disjoint_sym. *) skip.
    Qed.

    Lemma star_neutral_l : neutral_l hprop_star \[].
    Proof using.  Admitted.
    (*
      intros H. unfold hprop, hprop_star. extens. intros h.
      iff (h1&h2&M1&M2&D&U) M. 
      hnf in M1. subst h1 h. rewrite~ heap_union_neutral_l.
      exists hprop_empty h. splits~. 
    Qed.*)

    Lemma star_neutral_r : neutral_r hprop_star \[].
    Proof using. Admitted.
    (*
      apply neutral_r_from_comm_neutral_l.
      apply star_comm. apply star_neutral_l.
    Qed.
    *)

    Lemma star_assoc : LibOperation.assoc hprop_star. 
    Proof using.  Admitted.
    (*
      intros H1 H2 H3. unfold hprop, hprop_star. extens. intros h. split.
      intros (h1&h'&M1&(h2&h3&M3&M4&D'&U')&D&U). subst h'.
       exists (heap_union h1 h2) h3. rewrite heap_disjoint_union_eq_r in D.
       splits.
        exists h1 h2. splits*.
        auto.
        rewrite* heap_disjoint_union_eq_l.
        rewrite~ <- heap_union_assoc.
      intros (h'&h3&(h1&h2&M3&M4&D'&U')&M2&D&U). subst h'.
       exists h1 (heap_union h2 h3). rewrite heap_disjoint_union_eq_l in D.
       splits.
        auto.
        exists h2 h3. splits*.
        rewrite* heap_disjoint_union_eq_r.
        rewrite~ heap_union_assoc.
    Qed. (* later: exploit symmetry in the proof *)
    *)

    Lemma star_comm_assoc : comm_assoc hprop_star.
    Proof using. apply comm_assoc_prove. apply star_comm. apply star_assoc. Qed.

    Lemma starpost_neutral : forall B (Q:B->hprop),
      Q \*+ \[] = Q.
    Proof using. extens. intros. (* unfold starpost. *) rewrite~ star_neutral_r. Qed.

    Lemma star_cancel : forall H1 H2 H2',
      H2 ==> H2' -> H1 \* H2 ==> H1 \* H2'.
    (*
    Proof using. introv W (h1&h2&?). exists* h1 h2. Qed.
    *)
    Admitted.

    Lemma star_is_single_same_loc : forall (l:loc) (v1 v2:val),
      (hprop_single l v1) \* (hprop_single l v2) ==> \[False].
    Proof using.
    (*
      Transparent hprop_single state_single.
      intros. intros h ((m1&n1)&(m2&n2)&E1&E2&D&E).
      unfolds heap_disjoint, state_disjoint, prod_st2, pfun_disjoint. 
      specializes D l. rewrite E1, E2 in D.  
      unfold state_single in D. simpls. case_if. destruct D; tryfalse.
    Qed.
    *)
    Admitted.

    Lemma heap_star_prop_elim : forall (P:Prop) H h,
      (\[P] \* H) h -> P /\ H h.
    Proof using.
    Admitted.
    (*
      introv (?&?&N&?&?&?). destruct N. subst. rewrite~ heap_union_neutral_l.
    Qed.
    *)

    Lemma heap_extract_prop : forall (P:Prop) H H',
      (P -> H ==> H') -> (\[P] \* H) ==> H'.
    Proof using. introv W Hh. applys_to Hh heap_star_prop_elim. autos*. Qed.

    Lemma heap_weaken_pack : forall A (x:A) H J,
      H ==> J x -> H ==> (hprop_exists J).
    Proof using. introv W h. exists x. apply~ W. Qed.


    End HeapStar.


    (*------------------------------------------------------------------*)
    (* ** Normalization of [star] *)

    Hint Rewrite 
      star_neutral_l star_neutral_r starpost_neutral : rew_heap.
    Hint Rewrite <- star_assoc : rew_heap.

    Tactic Notation "rew_heap" :=
      autorewrite with rew_heap.
    Tactic Notation "rew_heap" "in" "*" :=
      autorewrite with rew_heap in *.
    Tactic Notation "rew_heap" "in" hyp(H) :=
      autorewrite with rew_heap in H.

    Tactic Notation "rew_heap" "~" :=
      rew_heap; auto_tilde.
    Tactic Notation "rew_heap" "~" "in" "*" :=
      rew_heap in *; auto_tilde.
    Tactic Notation "rew_heap" "~" "in" hyp(H) :=
      rew_heap in H; auto_tilde.

    Tactic Notation "rew_heap" "*" :=
      rew_heap; auto_star.
    Tactic Notation "rew_heap" "*" "in" "*" :=
      rew_heap in *; auto_star.
    Tactic Notation "rew_heap" "*" "in" hyp(H) :=
      rew_heap in H; auto_star.





















(* DEPRECATED
Definition heap_disjoint (f1 f2 : heap) : Prop :=
  let '(h1,g1) := f1 in
  let '(h2,g2) := f2 in
  \# h1 h2 /\ \# h1 g2 /\ \# g1 h2 /\ \# g1 g2.

Lemma heap_disjoint_unfold : forall h1 h2 h3 h4,
  \# h1 h2 h3 h4 = (\# h1 h2 /\ \# h2 h3 /\ \# h1 h3).
Proof using. auto. Qed.
*)


(*------------------------------------------------------------------*)
(* ** Properties of [heap] *)
(* DEPRECATED
(** Disjointness *)

Lemma heap_disjoint_sym : forall h1 h2,
  \# h1 h2 -> \# h2 h1.
Proof using. introv M. applys~ state_disjoint_sym. Qed.

Lemma heap_disjoint_comm : forall h1 h2,
  \# h1 h2 = \# h2 h1.
Proof using. intros. applys~ state_disjoint_comm. Qed.

Lemma heap_disjoint_empty_l : forall h,
  \# heap_empty h.
Proof using. intros. applys~ state_disjoint_empty_l. Qed. 

Lemma heap_disjoint_empty_r : forall h,
  \# h heap_empty.
Proof using. intros. applys~ state_disjoint_empty_r. Qed. 

Lemma heap_disjoint_union_eq_l : forall h1 h2 h3,
  \# (h2 \+ h3) h1 =
  (\# h1 h2 /\ \# h1 h3).
Proof using. intros. applys~ state_disjoint_union_eq_l. Qed.

Lemma heap_disjoint_union_eq_r : forall h1 h2 h3,
  \# h1 (h2 \+ h3) =
  (\# h1 h2 /\ \# h1 h3).
Proof using. intros. applys~ state_disjoint_union_eq_r. Qed.

(** Union *)

Lemma heap_union_empty_l : forall h,
  heap_empty \+ h = h.
Proof using. intros. applys~ state_union_empty_l. Qed.

Lemma heap_union_empty_r : forall h,
  h \+ heap_empty = h.
Proof using. intros. applys~ state_union_empty_r. Qed.

Lemma heap_union_comm : forall h1 h2,
  \# h1 h2 ->
  h1 \+ h2 = h2 \+ h1.
Proof using. intros. applys~ state_union_comm_disjoint. Qed.

Lemma heap_union_assoc : forall h1 h2 h3,
  (h1 \+ h2) \+ h3 = h1 \+ (h2 \+ h3).
  (* LibOperation.assoc heap_union. *)
Proof using. intros. applys~ state_union_assoc. Qed.

(** Disjoint3 *)

Definition heap_disjoint_3 h1 h2 h3 :=
  \# h1 h2 /\ \# h2 h3 /\ \# h1 h3.

Notation "\# h1 h2 h3" := (heap_disjoint_3 h1 h2 h3)
  (at level 40, h1 at level 0, h2 at level 0, h3 at level 0, no associativity).

Lemma heap_disjoint_3_unfold : forall h1 h2 h3,
  \# h1 h2 h3 = (\# h1 h2 /\ \# h2 h3 /\ \# h1 h3).
Proof using. auto. Qed.

(** Hints and tactics *)

Hint Resolve 
   heap_disjoint_sym 
   heap_disjoint_empty_l heap_disjoint_empty_r
   heap_union_empty_l heap_union_empty_r.

Hint Rewrite 
  heap_disjoint_union_eq_l
  heap_disjoint_union_eq_r
  heap_disjoint_3_unfold : rew_disjoint.

Tactic Notation "rew_disjoint" :=
  autorewrite with rew_disjoint in *.
Tactic Notation "rew_disjoint" "*" :=
  rew_disjoint; auto_star.

*)

(* DEPRECATED

  (** Disjointness *)

  Lemma heap_disjoint_sym : forall h1 h2,
    heap_disjoint h1 h2 -> heap_disjoint h2 h1.
  Proof using.
    intros [h1 g1] [h2 g2] H. simpls.
   
    unfolds state_disjoint_4.  rew_disjoint.
    hint state_disjoint_sym. autos*.
  Qed.
  Admitted.

  Lemma heap_disjoint_comm : forall h1 h2,
    \# h1 h2 = \# h2 h1.
  Proof using. 
  (*
    intros [m1 n1] [m2 n2]. simpls.
    hint state_disjoint_sym. extens*.
  Qed. *)
  Admitted.

  Lemma heap_disjoint_empty_l : forall h,
    heap_disjoint hprop_empty h.
  Proof using. Admitted.
  (* intros [m n]. hint state_disjoint_empty_l. simple*. Qed. *)

  Lemma heap_disjoint_empty_r : forall h,
    heap_disjoint h hprop_empty.
  Proof using. Admitted.
  (* intros [m n]. hint state_disjoint_empty_r. simple*. Qed. *)

  Hint Resolve heap_disjoint_sym heap_disjoint_empty_l heap_disjoint_empty_r.

  Lemma heap_disjoint_union_eq_r : forall h1 h2 h3,
    heap_disjoint h1 (heap_union h2 h3) =
    (heap_disjoint h1 h2 /\ heap_disjoint h1 h3).
  Proof using. Admitted.
  (*
    intros [m1 n1] [m2 n2] [m3 n3].
    unfolds heap_disjoint, heap_union. simpls.
    rewrite state_disjoint_union_eq_r. extens*.
  Qed. *)

  Lemma heap_disjoint_union_eq_l : forall h1 h2 h3,
    heap_disjoint (heap_union h2 h3) h1 =
    (heap_disjoint h1 h2 /\ heap_disjoint h1 h3).
  Proof using.
    intros. rewrite heap_disjoint_comm. 
    apply heap_disjoint_union_eq_r.
  Qed.

  Definition heap_disjoint_3 h1 h2 h3 :=
    heap_disjoint h1 h2 /\ heap_disjoint h2 h3 /\ heap_disjoint h1 h3.

  Lemma heap_disjoint_3_unfold : forall h1 h2 h3,
    \# h1 h2 h3 = (\# h1 h2 /\ \# h2 h3 /\ \# h1 h3).
  Proof using. auto. Qed.

  (** Union *)

  Lemma heap_union_neutral_l : forall h,
    heap_union hprop_empty h = h.
  Proof using. Admitted.
  (*
    intros [m n]. unfold heap_union, hprop_empty. simpl.
    fequals. apply~ state_union_neutral_l.
  Qed. *)

  Lemma heap_union_neutral_r : forall h,
    heap_union h hprop_empty = h.
  Proof using. Admitted.
  (*
    intros [m n]. unfold heap_union, hprop_empty. simpl.
    fequals. apply~ state_union_neutral_r. math.
  Qed. *) (*--TODO: comm+neutral_r *)

  Lemma heap_union_comm : forall h1 h2,
    heap_disjoint h1 h2 ->
    heap_union h1 h2 = heap_union h2 h1.
  Proof using. Admitted.
  (*
    intros [m1 n1] [m2 n2] H. simpls. fequals.
    applys* state_union_comm_disjoint.
    math.
  Qed. *)

  Lemma heap_union_assoc : 
    LibOperation.assoc heap_union.
  Proof using.
  Admitted.
  (*
    intros [m1 n1] [m2 n2] [m3 n3]. unfolds heap_union. simpls.
    fequals. applys state_union_assoc. math.
  Qed.*)

  (** Hints and tactics *)

  Hint Resolve heap_union_neutral_l heap_union_neutral_r.

  Hint Rewrite 
    heap_disjoint_union_eq_l
    heap_disjoint_union_eq_r
    heap_disjoint_3_unfold : rew_disjoint.

  Tactic Notation "rew_disjoint" :=
    autorewrite with rew_disjoint in *.
  Tactic Notation "rew_disjoint" "*" :=
    rew_disjoint; auto_star.

*)


(*
Definition triple t (H:hprop) (Q:val->hprop) :=
  forall h1 g1 h2, \# h1 g1 h2 ->
  heap_starable (f1,g1) h2 -> 
  H (h1,g1) -> 
  exists h1' h3 r, 
       (\# h1' g1 /\ \# (h1' \+ g1) h2 h3)
       
    /\ red (f1 \+ g1 \+ h2) t (f1' \+ g1 \+ h2 \+ h3) r
    /\ Q r (f1',state_empty). 


Definition triple t (H:hprop) (Q:val->hprop) :=
  forall f1 g1 f2 h2, 
  heap_disjoint (f1,g1) (f2,g2) -> 
  H (f1,g1) -> 
  exists h1' h3 r, 
       (\# h1' g1 /\ \# (h1' \+ g1) h2 h3)
    /\ red (h1 \+ g1 \+ f2 \+ g2) t (h1' \+ g1 \+ f2 \+ g2 \+ h3) r
    /\ Q r (h1',state_empty). 

  (* heap_disjoint (f1,g1) (f2,g2) *)

state_compat g1 g2 
  /\ (\# f1 f2 (g1 \+ g2)).

*)

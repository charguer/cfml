(* Union-find: pointer-based implementation,
   simplified by omiting the union-by-rank mechanism *)

(*

open NullPointers

type node = node ref
(*{ mutable contents : node }*)

let create () =
  let rec r = ref r in
  r
  (*
  let r = ref null in
  r := r,
  r*)

let rec find x =
  if !x = x then x else begin
    let r = find y in
    x := r;
    r
  end

let same x y =
  find x == find y

let union x y =
  let r = find x in
  let s = find y in
  if r != s then r := s

*)
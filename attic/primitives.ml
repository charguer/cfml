open Mytools

 

(*#########################################################################*)
(* ** Helper function to decompose Coq paths *)




(*#########################################################################*)

(* TEMPORARILY DEPRECATED

   (** List of special modules whose [open] should not lead to the
       generation of an [Require Import] statement. *)

   let is_primitive_module n =
      List.mem n [ "NullPointers"; "StrongPointers" ]
      (* ; "Okasaki"; "OkaStream" *)


   (** [is_primitive_module] Recognizes builtin modules that have a special treatment,
       namely "NullPointers" and "StrongPointers" *)

   val is_primitive_module : string -> bool


*)


(* TEMPORARILY DEPRECATED
   (** List of special top-level definitions that should not lead
       to the generation of a characteristic formula. The definition
       of [!$] as a keyword for forcing lazy expressions is one such
       exception. *)

   let hack_recognize_okasaki_lazy = function
     | "!$" -> true
     | _ -> false

   (** Below is a HACK for the [!$] symbol used in Okasaki's code to mean "lazy" *)

   val hack_recognize_okasaki_lazy : string -> bool

*)


(** DEPRECATED test only base on the last part of the name 

   let rec split_at_dots s pos =
     try
       let dot = String.index_from s pos '.' in
       String.sub s pos (dot - pos) :: split_at_dots s (dot + 1)
     with Not_found ->
       [String.sub s pos (String.length s - pos)]

   let name_of_mlpath s =
      List.hd (List.rev (split_at_dots s 0))



   (* HACK! Same as above, but only checks base on the tail of the name,
      e.g. considering only "+" instead of "Pervasives.+" 
      This hack is only correct if we forbit the rebinding of
      these special primitive names, however this check is not
      yet implemented. *)

   val is_inlined_primitive_hack : string -> int -> bool

   let is_inlined_primitive_hack p arity =
      let p = add_pervasives_prefix_if_needed p in
      match list_assoc_option p Renaming.inlined_primitives_table with
      | None -> false
      | Some (n,y) -> (arity = n)
      (* old: let inlined_primitives_table = List.map (fun (x,(n,y)) -> name_of_mlpath x, (n,y)) inlined_primitives_table in *)

*)

(*------------------------------------------------------------------*)
(* NOT USED

(** A introduction rule to establish [is_local] *)

Lemma is_local_prove : forall B (F:formula),  
  (forall H Q, F H Q <-> local F H Q) -> is_local F.
Proof using.
  intros. unfold is_local. apply func_ext_2.
  intros. applys prop_ext. applys H.
Qed.

(** Weaken and frame properties from [local] *)

Lemma local_frame : forall H1 H2 Q1 (F:formula) H Q,
  is_local F -> 
  F H1 Q1 -> 
  H ==> H1 \* H2 -> 
  Q1 \*+ H2 ===> Q ->
  F H Q.
Proof using.
  introv L M WH WQ. rewrite L. introv Ph.
  exists H1 H2 Q1. splits~.
  ---. (* hchange WQ. hsimpl. *)
Qed.

(** Frame property from [local] *)

Lemma local_frame_direct : forall H' H Q (F:formula),
  is_local F -> 
  F H Q -> 
  F (H \* H') (Q \*+ H').
Proof using. intros. apply* local_frame. Qed.

(** Extraction of existentials below a star from [local] *)

Lemma local_intro_exists : forall B (F:formula) H A (J:A->hprop) Q,
  is_local F -> 
  (forall x, F ((J x) \* H) Q) ->
   F (heap_is_pack J \* H) Q.
Proof using.
  introv L M. rewrite L. introv (h1&h2&(X&HX)&PH2&?&?).
  exists (J X \* H) \[] Q. splits.
  rew_heap~. exists~ h1 h2.
  auto.
  hsimpl.
Qed. 
*)


(*
Inductive val : Type := 
  | val_unit : val
  | val_int : int -> val
  | val_var : var -> val
  | val_loc : loc -> val
  | val_fix : var -> var -> trm -> val (* closed *)

with trm : Type :=
  | trm_val : val -> trm
  | trm_if : val -> trm -> trm -> trm
  | trm_let : var -> trm -> trm -> trm 
  | trm_let_fix : var -> var -> trm -> trm -> trm 
  | trm_app : val -> val -> trm
  | trm_new : val -> trm
  | trm_get : val -> trm
  | trm_set : val -> val -> trm.
*)
(*
Parameter val_of_Val : val -> ModelLambda.val.
Coercion val_of_Val : val >-> ModelLambda.val.
*)







(*--to move --*)



Lemma himpl_extract_prop : forall (P:Prop) H H',
  (P -> H ==> H') -> (\[P] \* H) ==> H'.
Proof using. 
  introv W Hh. lets (?&?): hprop_star_prop_elim Hh. applys* W.
Qed.

Lemma himpl_extract_exists : forall A H (J:A->hprop),
  (forall x, J x ==> H) -> (hprop_exists J) ==> H.
Proof using. introv W. intros h (x&Hh). applys* W. Qed.

Lemma himpl_inst_prop : forall (P:Prop) H H',
  P -> H ==> H' -> H ==> (\[P] \* H').
Proof using.
  introv HP W. intros h M. exists heap_empty h. splits~.
Qed. (* { applys~ hprop_pure_prove. } { rew_heap~. } *)


Lemma himpl_inst_exists : forall A (x:A) H J,
  (H ==> J x) -> H ==> (hprop_exists J).
Proof using. introv W h. exists x. apply~ W. Qed.











(*********************************************
DEPRECATED


  (*------------------------------------------------------------------*)
  (* ** Properties of [heap] *)

  (** Disjointness *)

  Lemma heap_disjoint_sym : forall h1 h2,
    \# h1 h2 -> \# h2 h1.
  Proof using. introv M. applys~ state_disjoint_sym. Qed.

  Lemma heap_disjoint_comm : forall h1 h2,
    \# h1 h2 = \# h2 h1.
  Proof using. intros. applys~ state_disjoint_comm. Qed.

  Lemma heap_disjoint_empty_l : forall h,
    \# heap_empty h.
  Proof using. intros. applys~ state_disjoint_empty_l. Qed. 

  Lemma heap_disjoint_empty_r : forall h,
    \# h heap_empty.
  Proof using. intros. applys~ state_disjoint_empty_r. Qed. 

  Lemma heap_disjoint_union_eq_l : forall h1 h2 h3,
    \# (h2 \+ h3) h1 =
    (\# h1 h2 /\ \# h1 h3).
  Proof using. intros. applys~ state_disjoint_union_eq_l. Qed.

  Lemma heap_disjoint_union_eq_r : forall h1 h2 h3,
    \# h1 (h2 \+ h3) =
    (\# h1 h2 /\ \# h1 h3).
  Proof using. intros. applys~ state_disjoint_union_eq_r. Qed.

  (** Union *)

  Lemma heap_union_empty_l : forall h,
    heap_empty \+ h = h.
  Proof using. intros. applys~ state_union_empty_l. Qed.

  Lemma heap_union_empty_r : forall h,
    h \+ heap_empty = h.
  Proof using. intros. applys~ state_union_empty_r. Qed.

  Lemma heap_union_comm : forall h1 h2,
    \# h1 h2 ->
    h1 \+ h2 = h2 \+ h1.
  Proof using. intros. applys~ state_union_comm_disjoint. Qed.

  Lemma heap_union_assoc : forall h1 h2 h3,
    (h1 \+ h2) \+ h3 = h1 \+ (h2 \+ h3).
    (* LibOperation.assoc heap_union. *)
  Proof using. intros. applys~ state_union_assoc. Qed.

  (** Disjoint3 *)

  Definition heap_disjoint_3 h1 h2 h3 :=
    \# h1 h2 /\ \# h2 h3 /\ \# h1 h3.

  Notation "\# h1 h2 h3" := (heap_disjoint_3 h1 h2 h3)
    (at level 40, h1 at level 0, h2 at level 0, h3 at level 0, no associativity).

  Lemma heap_disjoint_3_unfold : forall h1 h2 h3,
    \# h1 h2 h3 = (\# h1 h2 /\ \# h2 h3 /\ \# h1 h3).
  Proof using. auto. Qed.

  (** Hints and tactics *)

  Hint Resolve 
     heap_disjoint_sym 
     heap_disjoint_empty_l heap_disjoint_empty_r
     heap_union_empty_l heap_union_empty_r.

  Hint Rewrite 
    heap_disjoint_union_eq_l
    heap_disjoint_union_eq_r
    heap_disjoint_3_unfold : rew_disjoint.

  Tactic Notation "rew_disjoint" :=
    autorewrite with rew_disjoint in *.
  Tactic Notation "rew_disjoint" "*" :=
    rew_disjoint; auto_star.

  (** Auxiliary lemma *)

  Lemma heap_union_comm_assoc : forall h1 h2 h3,
    \# h1 h2 h3 ->
    (h1 \+ h2) \+ h3 = h2 \+ (h3 \+ h1).
    (* LibOperation.assoc heap_union. *)
  Proof using. 
    introv M. rewrite (@heap_union_comm h1 h2).
    rewrite~ heap_union_assoc. fequals.
    rewrite~ heap_union_comm. state_disjoint. state_disjoint.
  Qed.


  *)






============

(*
  { forwards N: rule_new v. applys rule_consequence_frame H N. 
    { rewrite~ hprop_star_empty_l. }
    { intros r. applys P r. } }
  { destruct P as (H'&l&w&R1&R2&R3). forwards N: rule_get w l.
    rewrite R1. applys~ rule_consequence_frame H' N. }
  { destruct P as (H'&l&w&R1&R2&R3). forwards N: rule_set l w v0.
    rewrite R1. applys~ rule_consequence_frame H' N. }
*)

(*
  | Trm_new v => local (cf_new v)
  | Trm_get r => local (cf_get r)
  | Trm_set r w => local (cf_set r w)
*)



(*
Definition cf_new v : formula := fun H Q =>
  (fun r => Hexists l, \[r = val_loc l] \* l ~~> v) \*+ H ===> Q.

Definition cf_get vl : formula := fun H Q =>
  exists H' l v,
     vl = val_loc l
  /\ H ==> (l ~~> v) \* H' 
  /\ (fun x => \[x = v] \* l ~~> v) \*+ H' ===> Q. 

Definition cf_set vl w : formula := fun H Q =>
  exists H' l v,
     vl = val_loc l
  /\ H ==> (l ~~> v) \* H' 
  /\ (fun x => \[x = val_unit] \* l ~~> w) \*+ H' ===> Q. 
*)





(*
Definition cf_new (v:val) : formula.
 refine (fun H `{EA:Enc A} (Q:A->hprop) =>
  exists (E : loc = A :> Type) (A1:Type) (EA1:Enc A1) (V:A1), v = enc V :> val /\ _).
Proof using.
subst A. refine ((fun (l:loc) => l ~~~> V) \*+ H ===> Q).
Defined.
Print cf_new.
*)

(*GOOD
Inductive cf_new (v:val) : formula :=
  | cf_new_intro : forall H (Q:loc->hprop) `{EA1:Enc A1} (V:A1),
     v = enc V ->
     ((fun l : loc => l ~~~> V) \*+ H ===> Q) ->
     cf_new v H Enc_loc Q.
*)

(*
Definition cf_new v : formula := fun H `{EA:Enc A} (Q:A->hprop) =>
  exists (E : A = loc) 
    (A1:Type) (EA1:Enc A1) (V:A1), v = enc V /\
    Logic.eq_rect_r (fun A' => Enc A' -> (A' -> hprop) -> Prop)
                    (fun EA' Q' => EA' = Enc_loc /\ (fun l : loc => l ~~~> V) \*+ H ===> Q') 
                    E EA Q.
*)

(*
Definition cf_new v : formula := fun H `{EA:Enc A} (Q:A->hprop) =>
  exists (E : loc = A :> Type) 
    (A1:Type) (EA1:Enc A1) (V:A1), v = enc V /\
    Logic.eq_rect loc (fun A' : Type => Enc A' -> (A' -> hprop) -> Prop)
     (fun (EA' : Enc loc) (Q0 : loc -> hprop) =>
      (fun l : loc => l ~~~> V) \*+ H ===> Q0) A E EA Q.
*)
(*
Definition cf_new v : formula := fun H `{EA1:Enc A1} (Q:A1->hprop) =>
  exists (A1:Type) (EA1:Enc A1) (V:A1), v = enc V /\
  (fun (l:loc) => l ~~~> V) \*+ H ===> Q.

Definition cf_get vl : formula := fun H `{Enc A} (Q:A->hprop) =>
  exists H' l v,
     vl = val_loc l
  /\ H ==> (l ~~~> V) \* H' 
  /\ (fun x => \[x = v] \* l ~~> v) \*+ H' ===> Q. 

Definition cf_set vl w : formula := fun H `{Enc A} (Q:A->hprop) =>
  exists H' l v,
     vl = val_loc l
  /\ H ==> (l ~~> v) \* H' 
  /\ (fun x => \[x = val_unit] \* l ~~> w) \*+ H' ===> Q. 

*)

(*
  | Trm_new v => local (cf_new v)
*)
(*
  | Trm_new v => local (fun _ _ _ _ => False)
*)
(*
  | Trm_get r => local  (fun _ _ _ _ => False)
  | Trm_set r w => local  (fun _ _ _ _ => False)
*)
(**
  | Trm_get r => local (Cf_get r)
  | Trm_set r w => local (Cf_set r w)
*)


(* GOOD
Focus 5.
    inverts P as R _ _. (* TODO: inverts should remove refl proofs *)
    forwards~ N: Rule_new V.
    applys rule_consequence_frame H N. 
    { rewrite~ hprop_star_empty_l. }
    { intros r. unfold PostEnc. rewrite extrude.
    applys himpl_extract_exists. intros l.
     specializes R l. rewrite hprop_star_assoc. 
    applys himpl_extract_prop. intro_subst. 
    applys himpl_trans (rm R).
    applys himpl_inst_exists l.
    applys~ himpl_inst_prop. }
*)

(* GOOD'
  { destruct P as (HA&A1&EA1&V&EV&HV). subst A.
      (* rewrite eq_rect_eq in HV. *)
    asserts (EAQ&R): (EA = Enc_loc /\ (fun x : loc => x ~~~> V \* H) ===> Q). { auto. }
    clear HV. subst. (* TODO: simpl eq_rect *)
    forwards~ N: Rule_new V.
    applys rule_consequence_frame H N. 
    { rewrite~ hprop_star_empty_l. }
    { intros r. unfold PostEnc. rewrite extrude.
    applys himpl_extract_exists. intros l.
     specializes R l. rewrite hprop_star_assoc. 
    applys himpl_extract_prop. intro_subst. 
    applys himpl_trans (rm R).
    applys himpl_inst_exists l.
    applys~ himpl_inst_prop. } }
*)





 

(********************************************************************)
(********************************************************************)
(********************************************************************)
(* EXTRA *)
(*

(*------------------------------------------------------------------*)
(* ** Alternative definitions for operations on references *)

Definition hprop_single_val (vl:val) (v:val) : hprop := 
  Hexists l, \[vl = val_loc l] \* (l ~~> v).

Definition hprop_single_val' (vl:val) (v:val) : hprop := 
  fun h => exists l, vl = val_loc l /\ h = state_single l v.

Notation "vr '~~~>' v" := (hprop_single_val vr v)
  (at level 32, no associativity) : heap_scope.

Definition cf_new' v : formula := fun H Q =>
  (fun r => r ~~~> v) \*+ H ===> Q.

Definition cf_get' r : formula := fun H Q =>
  exists H' v,
     H ==> (r ~~~> v) \* H' 
  /\ (fun x => \[x = v] \* r ~~~> v) \*+ H' ===> Q. 

Definition cf_set' r w : formula := fun H Q =>
  exists H' v,
     H ==> (r ~~~> v) \* H' 
  /\ (fun x => \[x = val_unit] \* r ~~~> w) \*+ H' ===> Q. 

*)



(*
Instance  Enc (A*B)  => val_pair (enc A) (enc B)

App f x y H Q = app f (x,y) H Q
App f x y z H Q = app f (x,y,z) H Q

if computed, then: 
  enc (x,y)
vs
  val_pair (enc x, enc y)


let f x y = t

let f (x,y) = t

let f v = let x = fst v in let y = snd v in t  with x<>y and v\notin t.

f (a,b) --> [y->b][x->a]t.


CF_fix:
  (forall `{EA2:Enc A2} (X:A2) H' Q', '(F1of F X) H' Q' -> app F X H' Q') ->

in proofs, do intros (x,y,z) if X is a tuple of arity 3.
for reasoning about the function, need to provide 
the type of arguments and return type as well.
  xfun (A*B*C->D). (* those types should have encoders *)
  xfun (x:A) (y:B) (z:C) : D. => give the prototype.
  xcf for local functions: split the 


 
*)






(*------------------------------------------------------------------*)
(* ** Lemmas for introduction and elimination of [is_local] *)



(** Weaken and frame and gc property [local] *)

Lemma local_frame_gc : forall (F:formula) H H1 H2 Q1 Q,
  is_local F -> 
  F H1 Q1 -> 
  H ==> H1 \* H2 -> 
  Q1 \*+ H2 ===> Q \*+ \GC ->
  F H Q.
Proof using.
  introv L M WH WQ. rewrite L. introv Ph.
  exists H1 H2 Q1. splits~.
Qed.

(** Weakening on pre and post from [local] *)

Lemma local_weaken : forall H' Q' (F:formula) H Q,
  is_local F -> 
  F H' Q' -> 
  H ==> H' -> 
  Q' ===> Q ->
  F H Q.
Proof using.
  intros. eapply local_frame_gc with (H2 := \[]); eauto.
  rewrite~ hprop_star_empty_r. 
  apply* qimpl_gc_instantiate.
Qed.

(** Weakening on pre from [local] *)

Lemma local_weaken_pre : forall H' (F:formula) H Q,
  is_local F -> 
  F H' Q -> 
  H ==> H' -> 
  F H Q.
Proof using. intros. apply* local_weaken. Qed.

(** Weakening on post from [local] *)

Lemma local_weaken_post : forall Q' (F:formula) H Q,
  is_local F -> 
  F H Q' -> 
  Q' ===> Q -> 
  F H Q.
Proof using. intros. apply* local_weaken. Qed.

(** Garbage collection on precondition from [local] *)

Lemma local_gc_pre : forall H' (F:formula) H Q,
  is_local F -> 
  H ==> H' \* \GC -> 
  F H' Q -> 
  F H Q.
Proof using.
  introv LF H1 H2. applys~ local_frame_gc H2. 
Qed.

(** Variant of the above, useful for tactics to specify 
    the garbage collected part *)

Lemma local_gc_pre_on : forall HG H' (F:formula) H Q,
  is_local F -> 
  H ==> HG \* H' ->
  F H' Q ->
  F H Q.
Proof using.
  introv L M W. rewrite L. introv Ph.
  exists H' HG Q. splits.
  { rewrite hprop_star_comm. apply~ M. }
  { auto. }
  { apply* qimpl_gc_instantiate. }
Qed.

(** Garbage collection on post-condition from [local] *)

Lemma local_gc_post : forall Q' (F:formula) H Q,
  is_local F -> 
  F H Q' ->
  Q' ===> Q \*+ \GC ->
  F H Q.
Proof using.
  introv L M W. rewrite L. introv Ph.
  exists H \[] Q'. splits~.
  { rewrite~ hprop_star_empty_r. }
  { intros x. rewrite~ hprop_star_empty_r. }
Qed.

(** Extraction of pure facts from [local] *)

Lemma local_extract_prop : forall (F:formula) H (P:Prop) Q,
  is_local F -> 
  (P -> F H Q) -> 
  F (\[P] \* H) Q.
Proof using.
  introv L M. rewrite L. introv (h1&h2&(PH1&HP)&PH2&H1&H2).
  lets: hprop_empty_inv HP. subst h1. rew_state. subst h2.
  exists H \[] Q. splits~.
  { rewrite~ hprop_star_empty_r. }
  { apply* qimpl_gc_instantiate. }
Qed.  

(** Extraction of existentials from [local] *)

Lemma local_extract_exists : forall (F:formula) A (J:A->hprop) Q,
  is_local F ->
  (forall x, F (J x) Q) -> 
  F (hprop_exists J) Q.
Proof using.
  introv L M. rewrite L. introv (x&Hx).
  exists (J x) \[] Q. splits~.
  { rewrite~ hprop_star_empty_r. }
  { apply* qimpl_gc_instantiate. }
Qed. 




Lemma qimpl_gc_instantiate : forall (Q Q':val->hprop) (H:hprop),
  Q ===> Q' ->
  Q \*+ H ===> Q' \*+ \GC.
Proof using.
  introv IQ. intros x h (ha&hb&Ha&Hb&Dab&Eab).
  exists ha hb. splits~. 
  { applys~ IQ. }
  { applys hprop_gc_prove. } 
Qed.




Lemma rule_consequence_frame : forall H2 H1 Q1 t H Q,
  H ==> H1 \* H2 ->
  triple t H1 Q1 ->
  Q1 \*+ H2 ===> Q ->
  triple t H Q.
Proof using.
  introv HH Ht HQ. applys rule_consequence HH HQ. applys* rule_frame Ht.
Qed.


(********************************************************************)
(* ** Preliminaries *)

(*------------------------------------------------------------------*)
(** Derived lemmas for heap entailment and structural rules. *)

Lemma himpl_trans_star : forall (H1' H1 H2 H3:hprop),
  H1 ==> H1' ->
  H1' \* H2 ==> H3 ->
  H1 \* H2 ==> H3.
Proof using.
  introv M N. intros h (ha&hb&Ha&Hb&Dab&Eab).
  applys N. lets Ha': M Ha. exists ha hb. splits~.
Qed.



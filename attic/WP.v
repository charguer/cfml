
(* ---------------------------------------------------------------------- *)
(** Weakest precondition, alternative (incomplete) *)

Module WP2.

Require Import LibEpsilon.

Definition wp_spec (t:trm) (Q:val->hprop) (H:hprop) :=
  triple t H Q /\ (forall H', triple t H' Q -> H' ==> H).

Definition wp (t:trm) (Q:val->hprop) : hprop := 
  epsilon (wp_spec t Q).

Lemma wp_exists : forall t Q, exists H, wp_spec t Q H.
Proof using.
Admitted.

Lemma wp_equiv : forall t H Q,
  triple t H Q <-> (H ==> wp t Q).
Proof using.
  intros. unfold wp. iff M.
  { spec_epsilon as H' (M1&M2). applys wp_exists.
    applys~ M2. }
  { spec_epsilon as H' (M1&M2). applys wp_exists.
    applys~ rule_consequence M M1. }
Qed.

End WP2.




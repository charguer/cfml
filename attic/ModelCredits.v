(** 

This file formalizes "Separation Logic with Time Credits",
following the presentation by Arthur Charguéraud and 
François Pottier described in the following papers:


- Machine-Checked Verification of the Correctness and Amortized 
  Complexity of an Efficient Union-Find Implementation (ITP'15)

- Verifying the Correctness and Amortized Complexity of a Union-Find 
  Implementation in Separation Logic with Time Credits
  (Draft journal paper)

Author: Arthur Charguéraud.
License: MIT.

*)


Set Implicit Arguments.
Require Import LibCore Shared ModelLambda.
Open Scope state_scope.


(********************************************************************)
(* ** Semantics, extended *)

(*------------------------------------------------------------------*)
(** Big-step evaluation with counting of the number of beta reductions
    (used by the formalization of Separation Logic with time credits) *)

Section Redn.
Local Open Scope nat_scope.
Local Open Scope state_scope.

Inductive redn : nat -> state -> trm -> state -> val -> Prop :=
  | redn_val : forall m v,
      redn 0 m (trm_val v) m v
  | redn_if : forall n m1 m2 v r t1 t2,
      redn n m1 (If v = val_int 0 then t2 else t1) m2 r ->
      redn n m1 (trm_if v t1 t2) m2 r
  | redn_let : forall n1 n2 m1 m2 m3 x t1 t2 v1 r,
      redn n1 m1 t1 m2 v1 ->
      redn n2 m2 (subst_trm x v1 t2) m3 r ->
      redn (n1+n2) m1 (trm_let x t1 t2) m3 r
  | redn_app : forall n m1 m2 v1 v2 f x t r,
      v1 = val_fix f x t ->
      redn n m1 (subst_trm f v1 (subst_trm x v2 t)) m2 r ->
      redn (S n) m1 (trm_app v1 v2) m2 r
  | redn_new : forall ma mb v l,
      mb = (state_single l v) ->
      \# ma mb ->
      redn 0 ma (prim_new v) (mb \+ ma) (val_loc l)
  | redn_get : forall m l v,
      state_data m l = Some v ->
      redn 0 m (prim_get (val_loc l)) m v
  | redn_set : forall m m' l v,
      m' = state_update m l v ->
      redn 0 m (prim_set (val_pair (val_loc l) v)) m' val_unit.

End Redn.

Ltac state_red_base tt ::=
  match goal with H: redn _ _ ?t _ _ |- redn _ _ ?t _ _ =>
    applys_eq H 2 4; try state_eq end.



(********************************************************************)
(* ** Heaps *)

(*------------------------------------------------------------------*)
(* ** Representation of credits *)

(** Representation of credits *)

Definition credits : Type := nat.

(** Zero and one credits *)

Definition credits_zero : credits := 0%nat.
Definition credits_one : credits := 1%nat.


(*------------------------------------------------------------------*)
(* ** Representation of heaps *)

(** Representation of heaps *)

Definition heap : Type := (state * credits)%type.

(** Empty heap *)

Definition heap_empty : heap :=
  (state_empty, 0%nat). 

(** Projections *)

Coercion heap_state (h:heap) : state :=
  match h with (m,c) => m end.

Definition heap_credits (h:heap) : credits :=
  match h with (m,c) => c end.

Notation "h '^s'" := (heap_state h)
   (at level 9, format "h '^s'") : heap_scope.

Notation "h '^c'" := (heap_credits h)
   (at level 9, format "h '^c'") : heap_scope.

Open Scope heap_scope.

(** Disjoint heaps *)

Definition heap_disjoint (h1 h2 : heap) : Prop :=
  \# (h1^s) (h2^s).

Notation "\# h1 h2" := (heap_disjoint h1 h2)
  (at level 40, h1 at level 0, h2 at level 0, no associativity) : heap_scope.

(** Union of heaps *)

Definition heap_union (h1 h2 : heap) : heap :=
   (h1^s \+ h2^s, (h1^c + h2^c)%nat).

Notation "h1 \u h2" := (heap_union h1 h2)
   (at level 51, right associativity) : heap_scope.


(*------------------------------------------------------------------*)
(* ** Definition of heap predicates *)

(** [hprop] is the type of predicates on heaps *)

Definition hprop := heap -> Prop.

(** Lifting of predicates *)

Definition hprop_pure (P:Prop) : hprop :=
  fun h => h = heap_empty /\ P.

(** Empty heap *)

Definition hprop_empty : hprop := 
  hprop_pure True.

(** Singleton heap *)

Definition hprop_single (l:loc) (v:val) : hprop := 
  fun h => h^s = state_single l v /\ h^c = credits_zero.

(** Heap union *)

Definition hprop_star (H1 H2 : hprop) : hprop := 
  fun h => exists h1 h2, 
              H1 h1 
           /\ H2 h2 
           /\ heap_disjoint h1 h2 
           /\ h = h1 \u h2.

(** Lifting of existentials *)

Definition hprop_exists A (Hof : A -> hprop) : hprop := 
  fun h => exists x, Hof x h.

(** Garbage collection predicate: [Hexists H, H]. *)

Definition hprop_gc : hprop := 
  hprop_exists (fun H => H).

(** Credits *)

Definition hprop_credits (n:nat) : hprop := 
  fun h => h^s = state_empty /\ h^c = n.

  (* DEPRECATED fun h => let (m,n') := h in m = state_empty /\ n' = n. *)

Global Opaque hprop_empty hprop_pure hprop_single 
              hprop_star hprop_exists hprop_gc hprop_credits. 


(*------------------------------------------------------------------*)
(* ** Notation for heap predicates *)

Notation "\[]" := (hprop_empty) 
  (at level 0) : heap_scope.

Notation "\[ P ]" := (hprop_pure P) 
  (at level 0, P at level 99) : heap_scope.

Notation "r '~~>' v" := (hprop_single r v)
  (at level 32, no associativity) : heap_scope.

Notation "H1 '\*' H2" := (hprop_star H1 H2)
  (at level 41, right associativity) : heap_scope.

Notation "Q \*+ H" := (fun x => hprop_star (Q x) H)
  (at level 40) : heap_scope.

Notation "'Hexists' x1 , H" := (hprop_exists (fun x1 => H))
  (at level 39, x1 ident, H at level 50) : heap_scope.
Notation "'Hexists' x1 : T1 , H" := (hprop_exists (fun x1:T1 => H))
  (at level 39, x1 ident, H at level 50, only parsing) : heap_scope.
Notation "'Hexists' ( x1 : T1 ) , H" := (hprop_exists (fun x1:T1 => H))
  (at level 39, x1 ident, H at level 50, only parsing) : heap_scope.

Notation "\GC" := (hprop_gc) : heap_scope. 

Notation "'\$' n" := (hprop_credits n) 
  (at level 40, format "\$ n") : heap_scope.

Open Scope heap_scope.
Bind Scope heap_scope with hprop.
Delimit Scope heap_scope with h.


(********************************************************************)
(* ** Properties *)

(*------------------------------------------------------------------*)
(* ** Tactic for automation *)

Lemma heap_disjoint_def : forall h1 h2,
  heap_disjoint h1 h2 = state_disjoint (h1^s) (h2^s).
Proof using. auto. Qed.

Hint Rewrite heap_disjoint_def : rew_disjoint.

Tactic Notation "state_disjoint_pre" :=
  subst; rew_disjoint; jauto_set.

Hint Extern 1 (\# _ _) => state_disjoint_pre.

Ltac math_0 ::= unfolds credits.


(*------------------------------------------------------------------*)
(* ** Properties of [heap] *)

Lemma heap_eq : forall h1 h2,
  (h1^s = h2^s /\ h1^c = h2^c) -> h1 = h2.
Proof using.
  intros (s1,n1) (s2,n2) (M1&M2). simpls. subst. fequals.
Qed.

Lemma heap_eq_forward : forall h1 h2,
  h1 = h2 ->
  h1^s = h2^s /\ h1^c = h2^c.
Proof using. intros (s1,n1) (s2,n2) M. inverts~ M. Qed.


(*------------------------------------------------------------------*)
(* ** Properties of [heap_disjoint] *)

Lemma heap_disjoint_sym : forall h1 h2,
  \# h1 h2 -> \# h2 h1.
Proof using.
  intros [m1 n1] [m2 n2] H. simpls.
  hint state_disjoint_sym. autos*.
Qed.

Lemma heap_disjoint_comm : forall h1 h2,
  \# h1 h2 = \# h2 h1.
Proof using. 
  intros [m1 n1] [m2 n2]. simpls.
  hint state_disjoint_sym. extens*.
Qed. 

Lemma heap_disjoint_empty_l : forall h,
  \# heap_empty h.
Proof using. intros [m n]. hint state_disjoint_empty_l. simple*. Qed.

Lemma heap_disjoint_empty_r : forall h,
  \# h heap_empty.
Proof using. intros [m n]. hint state_disjoint_empty_r. simple*. Qed.

Lemma heap_disjoint_union_eq_r : forall h1 h2 h3,
  \# h1 (h2 \u h3) = (\# h1 h2 /\ \# h1 h3).
Proof using.
  intros [m1 n1] [m2 n2] [m3 n3].
  unfolds heap_disjoint, heap_union. simpls.
  rewrite state_disjoint_union_eq_r. extens*.
Qed.

Lemma heap_disjoint_union_eq_l : forall h1 h2 h3,
  \# (h2 \u h3) h1 = (\# h1 h2 /\ \# h1 h3).
Proof using.
  intros. rewrite heap_disjoint_comm. 
  apply heap_disjoint_union_eq_r.
Qed.

Hint Resolve 
   heap_disjoint_sym 
   heap_disjoint_empty_l heap_disjoint_empty_r
   heap_disjoint_union_eq_l heap_disjoint_union_eq_r.

Tactic Notation "rew_disjoint" :=
  autorewrite with rew_disjoint in *.
Tactic Notation "rew_disjoint" "*" :=
  rew_disjoint; auto_star.


(*------------------------------------------------------------------*)
(* ** Properties of [heap_union] *)

Lemma heap_union_comm : forall h1 h2,
  \# h1 h2 ->
  h1 \u h2 = h2 \u h1.
Proof using.
  intros [m1 n1] [m2 n2] H. unfold heap_union.
  simpl. fequals. state_eq. math.
Qed.

Lemma heap_union_assoc : forall h1 h2 h3,
  (h1 \u h2) \u h3 = h1 \u (h2 \u h3).
Proof using.
  intros [m1 n1] [m2 n2] [m3 n3]. unfolds heap_union.
  simpl. fequals. state_eq. math.
Qed.

Lemma heap_union_empty_l : forall h,
  heap_empty \u h = h.
Proof using. 
  intros [m n]. unfold heap_union, heap_empty. simpl.
  fequals. apply~ state_union_empty_l.
Qed.

Lemma heap_union_empty_r : forall h,
  h \u heap_empty = h.
Proof using. 
  intros. rewrite~ heap_union_comm. apply~ heap_union_empty_l.
Qed.

Lemma heap_union_state : forall h1 h2,
  heap_state (h1 \u h2) = (heap_state h1) \+ (heap_state h2).
Proof using. intros (m1&n1) (m2&n2). auto. Qed.

Lemma heap_union_credits : forall h1 h2,
  heap_credits (h1 \u h2) = (heap_credits h1 + heap_credits h2)%nat.
Proof using. intros (m1&n1) (m2&n2). auto. Qed.

Hint Resolve heap_union_comm
   heap_union_empty_l heap_union_empty_r.

Hint Rewrite heap_union_state : rew_disjoint.

Hint Rewrite heap_union_state : rew_state.

Hint Rewrite 
  heap_union_empty_l heap_union_empty_r
  heap_union_state heap_union_credits : rew_heap.

Tactic Notation "rew_heap" :=
  autorewrite with rew_heap.
Tactic Notation "rew_heap" "~" :=
  rew_heap; auto_tilde.
Tactic Notation "rew_heap" "in" hyp(H) :=
  autorewrite with rew_heap in H.
Tactic Notation "rew_heap" "~" "in" hyp(H) :=
  rew_heap in H; auto_tilde.
Tactic Notation "rew_heap" "in" "*" :=
  autorewrite with rew_heap in *.
Tactic Notation "rew_heap" "~" "in" "*" :=
  rew_heap in *; auto_tilde.

Ltac heap_eq :=
  solve [ rew_heap; subst; auto ].


(*------------------------------------------------------------------*)
(* ** Properties of [hprop] *)

(* LATER: use rew_heap more often, like RO dev does *)

Global Instance hprop_inhab : Inhab hprop.
Proof using. intros. apply (prove_Inhab hprop_empty). Qed.

Section HeapProp.

Implicit Types H : hprop.
Transparent hprop_empty hprop_pure hprop_single hprop_star hprop_gc heap_union.

Lemma hprop_pure_prove : forall (P:Prop),
  P -> 
  \[P] heap_empty.
Proof using. intros. hnfs~. Qed.

Lemma hprop_pure_inv : forall h (P:Prop),
  \[P] h -> h = heap_empty /\ P.
Proof using. intros. auto. Qed.

Lemma hprop_pure_inv' : forall m n (P:Prop),
  \[P] (m,n) -> m = state_empty /\ n = credits_zero /\ P.
Proof using.
  introv M. lets (E&N): hprop_pure_inv M.
  unfold heap_empty in E. inverts* E.
Qed.

Lemma hprop_empty_prove : 
  \[] heap_empty.
Proof using. hnfs~. Qed.

Lemma hprop_empty_inv : forall h, 
  \[] h -> h = heap_empty.
Proof using. introv M. forwards*: hprop_pure_inv M. Qed.

Lemma hprop_credits_inv : forall h n,
  (\$ n) h -> h = (state_empty,n).
Proof using. intros (m,n') n (M1&M2). simpls. subst*. Qed.

Lemma hprop_gc_prove : forall h, 
  \GC h.
Proof using. intros. exists~ (=h). Qed.

Lemma hprop_gc_merge : 
  \GC \* \GC = \GC.
Proof using.
  unfold hprop. extens. intros h. iff (h1&h2&M1&M2&D&U) M.
  { apply hprop_gc_prove. }
  { exists h heap_empty. splits~. apply hprop_gc_prove. }
Qed.

Lemma hprop_star_comm : forall H1 H2,
   H1 \* H2 = H2 \* H1.
Proof using. 
  intros H1 H2. unfold hprop, hprop_star. extens. intros h.
  iff (h1&h2&M1&M2&D&U); rewrite~ heap_union_comm in U; exists* h2 h1.
Qed.

Lemma hprop_star_empty_l : forall H,
   \[] \* H = H.
Proof using.
  intro. unfold hprop, hprop_star. extens. intros h.
  iff (h1&h2&(M1&E1)&M2&D&U) M. 
  subst h1 h. rewrite~ heap_union_empty_l.
  exists heap_empty h. hint hprop_empty_prove. splits~. 
Qed.

Lemma hprop_star_empty_r : forall H, 
  H \* \[] = H.
Proof using.
  apply neutral_r_from_comm_neutral_l.
  applys hprop_star_comm. applys hprop_star_empty_l.
Qed.

Lemma hprop_star_assoc : forall H1 H2 H3,
  (H1 \* H2) \* H3 = H1 \* (H2 \* H3).
Proof using. 
  intros H1 H2 H3. unfold hprop, hprop_star. extens. intros h. split.
  { intros (h'&h3&(h1&h2&M3&M4&D'&U')&M2&D&U). subst h'.
    exists h1 (h2 \u h3). splits~.
    { exists h2 h3. splits*. }
    { subst. applys heap_eq. split. { state_eq. } { simpl; math. } } }
  { intros (h1&h'&M1&(h2&h3&M3&M4&D'&U')&D&U). subst h'.
    exists (h1 \u h2) h3. splits~.
    { exists h1 h2. splits*. } 
    { subst. applys heap_eq. split. { state_eq. } { simpl; math. } } }
Qed. 

Lemma hprop_star_single_same_loc_disjoint : forall (l:loc) (v1 v2:val),
  (hprop_single l v1) \* (hprop_single l v2) ==> \[False].
Proof using.
  intros. unfold hprop_single. 
  intros h ((m1&n1)&(m2&n2)&(E1&X1)&(E2&X2)&D&E). false.
  subst. applys* state_single_same_loc_disjoint l v1 v2.
  unfolds in D. rewrite <- E1. rewrite <- E2. auto.
Qed.

Lemma hprop_star_prop_elim : forall (P:Prop) H h,
  (\[P] \* H) h -> P /\ H h.
Proof using.
  introv (?&?&N&?&?&?). destruct N. subst. rewrite~ heap_union_empty_l.
Qed.


(*------------------------------------------------------------------*)
(* ** Properties of [himpl] *)

Lemma himpl_forward : forall H1 H2 h,
  (H1 ==> H2) -> (H1 h) -> (H2 h).
Proof using. auto. Qed.

Lemma himpl_refl : forall H,
  H ==> H.
Proof using. intros h. auto. Qed.

Lemma himpl_trans : forall H1 H2 H3,
  (H1 ==> H2) -> (H2 ==> H3) -> (H1 ==> H3).
Proof using. introv M1 M2. intros h H1h. eauto. Qed.

Lemma himpl_antisym : forall H1 H2,
  (H1 ==> H2) -> (H2 ==> H1) -> (H1 = H2).
Proof using. introv M1 M2. applys prop_ext_1. intros h. iff*. Qed.

Lemma himpl_cancel_l : forall H1 H1' H2,
  H1 ==> H1' -> (H1 \* H2) ==> (H1' \* H2).
Proof using. introv W (h1&h2&?). exists* h1 h2. Qed.

Lemma himpl_cancel_r : forall H1 H2 H2',
  H2 ==> H2' -> (H1 \* H2) ==> (H1 \* H2').
Proof using. introv W (h1&h2&?). exists* h1 h2. Qed.

Lemma himpl_extract_prop : forall (P:Prop) H H',
  (P -> H ==> H') -> (\[P] \* H) ==> H'.
Proof using. introv W Hh. lets (?&?): hprop_star_prop_elim Hh. applys* W. Qed.

Lemma himpl_extract_exists : forall A (x:A) H J,
  (H ==> J x) -> H ==> (hprop_exists J).
Proof using. introv W h. exists x. apply~ W. Qed.

Lemma himpl_inst_prop : forall (P:Prop) H H',
  P -> H ==> H' -> H ==> (\[P] \* H').
Proof using. 
  introv HP W. intros h M. hint hprop_pure_prove. exists~ heap_empty h.
Qed.

Lemma himpl_inst_exists : forall A (x:A) H J,
  (H ==> J x) -> H ==> (hprop_exists J).
Proof using. introv W h. exists x. apply~ W. Qed.

Lemma himpl_remove_gc : forall H H',
  H ==> H' ->
  H ==> H' \* \GC.
Proof using.
  introv M. intros h Hh. exists h heap_empty. splits*.
  exists \[]. applys hprop_empty_prove.
Qed.

End HeapProp.

Hint Resolve hprop_empty_prove hprop_pure_prove hprop_gc_prove.


(********************************************************************)
(* ** Properties of credits *)

Section Credits.
Transparent hprop_credits hprop_empty hprop_pure hprop_star 
  heap_union heap_disjoint.

Lemma credits_zero_eq : \$ 0 = \[].
Proof using.
  unfold hprop_credits, hprop_empty, heap_empty. 
  applys prop_ext_1. intros [m n]; simpl. iff [M1 M2] M. 
  { subst*. }
  { lets*: hprop_pure_inv' M. }
Qed.

Lemma credits_split_add : forall (n m : nat),
  \$ (n+m) = \$ n \* \$ m.
Proof using.
  intros c1 c2. unfold hprop_credits, hprop_star, heap_union, heap_disjoint. 
  applys prop_ext_1. intros [m n].
  iff [M1 M2] ([m1 n1]&[m2 n2]&(M1&E1)&(M2&E2)&M3&M4).
  { exists (state_empty,c1) (state_empty,c2). simpls. splits*.
    { fequals. state_eq. } }
  { simpls. inverts M4. subst. split~. state_eq. }  
Qed.

Lemma credits_substract : forall (n m : nat), 
  (n >= m)%nat -> 
  \$ n ==> \$ m \* \$ (n-m).
Proof using.
  introv M. rewrite <- credits_split_add.
  math_rewrite (m + (n-m) = n)%nat. auto.
Qed.

End Credits.


(********************************************************************)
(* ** Rules *)

Implicit Types H : hprop.
Implicit Types Q : val -> hprop.


(*------------------------------------------------------------------*)
(* ** Definition of [on_sub] *)

Definition on_sub H := (H \* \GC).

Lemma on_sub_base : forall H h,
  H h -> 
  on_sub H h.
Proof using. intros H h M. exists~ h heap_empty. Qed.

Lemma on_sub_gc : forall H h,
  on_sub (H \* \GC) h ->
  on_sub H h.
Proof using.
  introv M. unfolds on_sub. applys himpl_forward (rm M).
  rewrite hprop_star_assoc. rewrite~ hprop_gc_merge.
Qed.

Lemma on_sub_union_r : forall H h1 h2,
  on_sub H h1 ->
  \# h1 h2 ->
  on_sub H (h1 \u h2).
Proof using.
  introv M D. unfolds on_sub. rewrite <- hprop_gc_merge.
  rewrite <- hprop_star_assoc. exists~ h1 h2.
Qed.

Lemma on_sub_union : forall H1 H2 h1 h2,
  on_sub H1 h1 ->
  H2 h2 ->
  \# h1 h2 ->
  on_sub (H1 \* H2) (h1 \u h2).
Proof using.
  introv M P2 D. unfolds on_sub. 
  rewrite hprop_star_assoc.
  rewrite (@hprop_star_comm H2 \GC).
  rewrite <- hprop_star_assoc.
  exists~ h1 h2. 
Qed.

Lemma on_sub_weaken : forall Q Q' v h,
  on_sub (Q v) h ->
  Q ===> Q' ->
  on_sub (Q' v) h.
Proof using.
  introv M W. unfolds on_sub. applys himpl_forward (rm M).
  applys~ himpl_cancel_l. 
Qed.


(*------------------------------------------------------------------*)
(* ** Spending of credits *)

Definition pay_one H H' :=
  H ==> (\$ 1%nat) \* H'.


(*------------------------------------------------------------------*)
(** Definition of triples *)

(** Recall that the projection [heap_state : heap >-> state]
   is used as a Coercion, so that we can write [h] where the
   underlying state is expected. *)

Definition triple t H Q :=
  forall h1 h2, 
  \# h1 h2 -> 
  H h1 -> 
  exists n h1' v, 
       \# h1' h2 
    /\ redn n (h1 \u h2) t (h1' \u h2) v
    /\ on_sub (Q v) h1'
    /\ (heap_credits h1 = n + heap_credits h1')%nat. 


(*------------------------------------------------------------------*)
(** Equivalent definitions of triples *)

(** We show that the definition of [triple] is equivalent to the
    following lower-level definition. *)

Definition triple' t H Q :=
  forall m1 c1 m2, 
  state_disjoint m1 m2 -> 
  H (m1,c1) -> 
  exists n m1' c1' v, 
       state_disjoint m1' m2 
    /\ redn n (m1 \+ m2) t (m1' \+ m2) v
    /\ (Q v \* \GC) (m1',c1')
    /\ (c1 = n + c1')%nat.

Lemma triple_eq_triple' : triple = triple'.
Proof using.
  applys func_ext_3. intros t H Q. extens.
  unfold triple, triple'. iff M.
  { introv D P1. forwards~ (n&h1'&v&R1&R2&R3&R4): M (m1,c1) (m2,0%nat).
    simpls. exists n (h1'^s) (h1'^c) v. splits~.
    { applys_eq R3 1. applys~ heap_eq. } }
  { introv D P1. forwards~ (n&m1'&c1'&v&R1&R2&R3&R4): M (h1^s) (h1^c) (h2^s).
    { applys_eq P1 1. applys~ heap_eq. }
    exists n (m1',c1') v. splits~. }  
Qed.


(** We show that the definition of [triple] is equivalent to the
    following higher-level definition. *)

Definition triple'' t H Q :=
  forall H' m c, 
  (H \* H') (m, c) -> 
  exists n m' c' v, 
       redn n m t m' v
    /\ (Q v \* \GC \* H') (m', c')
    /\ (c = n + c')%nat.

Lemma triple_eq_triple'' : triple = triple''.
Proof using.
  applys func_ext_3. intros t H Q. extens.
  unfold triple, triple''. iff M.
  { introv (h1&h2&N1&N2&D&U). inverts U.
    forwards~ (n&h1'&v&R1&R2&R3&R4): M h1 h2.
    exists n (h1'^s \+ h2^s) (h1'^c + h2^c)%nat v. splits~.
    { rewrite <- hprop_star_assoc. exists~ h1' h2. }
    { math. } }
  { introv D P1.
    forwards~ (n&m'&c'&v&R1&R2&R3): M (=h2) (h1 \+ h2) (h1^c + h2^c)%nat.
    { exists~ h1 h2. } 
    rewrite <- hprop_star_assoc in R2.
    destruct R2 as ((m1'&c1')&(m2'&c2')&N0&N1&N2&N3).
    inverts N3. subst h2.
    exists n (m1',c1') v. simpls. splits~. { math. } }
Qed.



(*------------------------------------------------------------------*)
(** Structural rules *)

Lemma rule_extract_prop : forall t (P:Prop) H Q,
  (P -> triple t H Q) ->
  triple t (H \* \[P]) Q.
Proof using. 
  introv M D (h21&h22&N1&(HE&HP)&N3&N4). subst h22 h1.
  rewrite heap_union_empty_r. applys* M. 
Qed.

Lemma rule_extract_exists : forall t A (J:A->hprop) Q,
  (forall x, triple t (J x) Q) ->
  triple t (hprop_exists J) Q.
Proof using. introv M D (x&Jx). applys* M. Qed.

Lemma rule_consequence : forall t H' Q' H Q,
  H ==> H' ->
  triple t H' Q' ->
  Q' ===> Q ->
  triple t H Q.
Proof using. 
  introv MH M MQ. intros h1 h2 D P1. 
  lets P1': (rm MH) (rm P1). 
  forwards~ (n&h1'&v&(N1&N2&N3&N4)): (rm M) h2 (rm P1'). 
  exists n h1' v. splits~. { applys~ on_sub_weaken Q'. }
Qed.

Lemma rule_frame : forall t H Q H',
  triple t H Q ->
  triple t (H \* H') (Q \*+ H').
Proof using. 
  introv M. intros h1 h2 D1 (h1a&h1b&H1a&H1b&D12a&E12).
  lets~ (n&h1'a&v&Da&Ra&Qa&Na): M h1a (h1b \u h2).
  subst h1. exists n (h1'a \u h1b) v. splits.
  { auto. }
  { state_red. }
  { applys~ on_sub_union. }
  { do 2 rewrite heap_union_credits. math. }
Qed.

Lemma rule_gc_post : forall t H Q,
  triple t H (Q \*+ \GC) ->
  triple t H Q.
Proof using.
  introv M. intros h1 h2 D P1. 
  forwards* (n&h1'&v&(N1&N2&N3&N4)): (rm M) h1 h2.
  exists n h1' v. splits~. { applys~ on_sub_gc. }
Qed.

Lemma rule_gc_pre : forall t H Q,
  triple t H Q ->
  triple t (H \* \GC) Q.
Proof using.
  introv M. applys rule_gc_post. applys~ rule_frame.
Qed.


(*------------------------------------------------------------------*)
(** Term rules *)

Lemma rule_val : forall v H Q,
  H ==> Q v ->
  triple (trm_val v) H Q.
Proof using.
  introv M D H1. exists 0%nat h1 v. splits~.
  { applys redn_val. }
  { applys~ on_sub_base. }
Qed.

Lemma rule_if : forall v t1 t2 H Q,
  triple (If v = val_int 0 then t2 else t1) H Q ->
  triple (trm_if v t1 t2) H Q.
Proof using.
  introv M D H1. forwards (n&h1'&v'&Da&Ra&Qa&Na): M D H1.
  exists n h1' v'. splits~.
  { applys~ redn_if. }
Qed.

Lemma rule_let : forall x t1 t2 H Q Q1,
  triple t1 H Q1 ->
  (forall (X:val), triple (subst_trm x X t2) (Q1 X) Q) ->
  triple (trm_let x t1 t2) H Q.
Proof using.
  introv M1 M2. intros h1 h2 D1 H1.
  lets (na&h1'&v1&Da&Ra&Qa&Na): M1 D1 H1.
  lets (h1'a&h1'b&N1&N2&N3&N4): (rm Qa).
  forwards~ (nb&h1''&v2&Db&Rb&Qb&Nb): M2 v1 h1'a (h1'b \u h2).
  exists (na+nb)%nat (h1'' \u h1'b) v2. splits~.
  { applys redn_let Ra. state_red. }
  { applys~ on_sub_union_r. }
  { subst. simpls. math. } 
Qed.

Lemma rule_let_val : forall x v1 t2 H Q,
  (forall (X:val), X = v1 -> triple (subst_trm x X t2) H Q) ->
  triple (trm_let x (trm_val v1) t2) H Q.
Proof using. 
  introv M. forwards~ M': M.
  applys_eq~ (>> rule_let H (fun x => H \* \[x = v1])) 2.
  { applys rule_val. rewrite hprop_star_comm. applys~ himpl_inst_prop. }
  { intros X. applys rule_extract_prop. applys M. }
Qed.

Lemma rule_app : forall f x F V t1 H H' Q,
  F = (val_fix f x t1) ->
  pay_one H H' ->
  triple (subst_trm f F (subst_trm x V t1)) H' Q ->
  triple (trm_app F V) H Q.
Proof using.
  introv EF HP M. subst F. intros h1 h2 D P1.
  lets P1': (rm HP) (rm P1). (* LATER: lemma for these lines *)
  lets (h1a&h1b&V1&P1''&V2&V3): (rm P1').
  lets V1': hprop_credits_inv (rm V1).
  forwards~ (n&h1'&v&N1&N2&N3&N4): (rm M) h2 (rm P1'').
  exists (S n) h1' v. splits~.
  { applys~ redn_app. state_red. }
  { subst. simpl. math. }
Qed.

Lemma rule_let_fix : forall f x t1 t2 H Q,
  (forall (F:val), 
    (forall X H' H'' Q', 
      pay_one H' H'' ->
      triple (subst_trm f F (subst_trm x X t1)) H'' Q' ->
      triple (trm_app F X) H' Q') ->  
    triple (subst_trm f F t2) H Q) ->
  triple (trm_let f (trm_val (val_fix f x t1)) t2) H Q.
Proof using.
  introv M. applys rule_let_val. intros F EF. 
  applys (rm M). clears H Q. intros X H H' Q N.
  applys~ rule_app.
Qed.

Lemma rule_new : forall v,
  triple (prim_new v) \[] (fun r => Hexists l, \[r = val_loc l] \* l ~~> v).
Proof using.
  intros. intros h1 h2 _ P1.
  lets E: hprop_empty_inv P1. subst h1.
  lets (l&Dl): (state_disjoint_new (heap_state h2) v).
  sets h1': (state_single l v, 0%nat).
  exists 0%nat h1' (val_loc l). splits~.
  { rew_state. applys~ redn_new. }
  { applys~ on_sub_base. exists l. 
    applys~ himpl_inst_prop (l ~~> v). split~. }
Qed.

Lemma rule_get : forall v l,
  triple (prim_get (val_loc l)) (l ~~> v) (fun x => \[x = v] \* (l ~~> v)).
Proof using.
  intros. intros h1 h2 D P1. exists 0%nat h1 v. splits~.
  { rew_state. applys redn_get. lets (P1a&P1b): P1.
    rewrite P1a. applys~ state_union_single_read. }
  { exists h1 heap_empty. splits~.
    { exists~ heap_empty h1. } }
Qed.

Lemma rule_set : forall l v w,
  triple (prim_set (val_pair (val_loc l) w)) (l ~~> v) (fun r => \[r = val_unit] \* l ~~> w).
Proof using.
  intros. intros h1 h2 D P1. lets (P1a&P1b): P1.
  sets h1': (state_single l w, 0%nat).
  exists 0%nat h1' val_unit. splits~.
  { applys state_disjoint_single_set v. rewrite~ <- P1a. }
  { rew_state. applys~ redn_set. rewrite P1a. 
    applys~ state_union_single_write v w. }
  { applys~ on_sub_base. applys~ himpl_inst_prop (l ~~> w). split~. }
Qed.



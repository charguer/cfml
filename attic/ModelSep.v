(** 

This file formalizes "Separation Logic", as described in 
Arthur Charguéraud's lecture notes.

We formalize heap predicates, properties of heap entailment,
define the interpretation of triples, and derive the reasoning
rules in the form of lemmas.

Two interpretation of triples are considered: a basic version,
and a refined version that allows for discarding arbitrary
heap predicates. The former version is useful to ensure full recollection
of all allocated data in languages with explicit deallocation,
white the latter version is required for the analysis of programs 
with implicit garbage collection.

Author: Arthur Charguéraud.
License: MIT.

*)

Set Implicit Arguments.
Require Import LibCore Shared ModelLambda.
Open Scope state_scope.

Ltac auto_star ::= jauto.


(********************************************************************)
(* ** Heaps *)

(*------------------------------------------------------------------*)
(* ** Definition of heaps *)

(** Data type for heaps *)

Definition heap : Type := (state)%type.

(** Notation for empty and union *)

Notation "'heap_empty'" := state_empty : heap_scope.

Notation "h1 \u h2" := (state_union h1 h2)
   (at level 51, right associativity) : heap_scope.


(*------------------------------------------------------------------*)
(* ** Definition of heap predicates *)

(** [hprop] is the type of predicates on heaps *)

Definition hprop := heap -> Prop.

Implicit Types H : hprop.
Implicit Types Q : val -> hprop.

(** Empty heap *)

Definition hprop_empty : hprop := 
  fun h => h = state_empty.

(** Lifting of predicates *)

Definition hprop_pure (H:Prop) : hprop :=
  fun h => h = state_empty /\ H.

(** Singleton heap *)

Definition hprop_single (l:loc) (v:val) : hprop := 
  fun h => h = state_single l v.

(** Heap union *)

Definition hprop_star (H1 H2 : hprop) : hprop := 
  fun h => exists h1 h2, H1 h1 
                      /\ H2 h2 
                      /\ (\# h1 h2)
                      /\ h = h1 \+ h2.

(** Lifting of existentials *)

Definition hprop_exists A (Hof : A -> hprop) : hprop := 
  fun h => exists x, Hof x h.

(** Garbage collection predicate: [Hexists H, H]. *)

Definition hprop_gc : hprop := 
  hprop_exists (fun H => H).

Global Opaque hprop_empty hprop_pure hprop_single 
              hprop_star hprop_exists hprop_gc. 


(*------------------------------------------------------------------*)
(* ** Notation for heap predicates *)

Notation "\[]" := (hprop_empty) 
  (at level 0) : heap_scope.

Notation "\[ L ]" := (hprop_pure L) 
  (at level 0, L at level 99) : heap_scope.

Notation "r '~~>' v" := (hprop_single r v)
  (at level 32, no associativity) : heap_scope.

Notation "H1 '\*' H2" := (hprop_star H1 H2)
  (at level 41, right associativity) : heap_scope.

Notation "Q \*+ H" := (fun x => hprop_star (Q x) H)
  (at level 40) : heap_scope.

Notation "'Hexists' x1 , H" := (hprop_exists (fun x1 => H))
  (at level 39, x1 ident, H at level 50) : heap_scope.
Notation "'Hexists' x1 : T1 , H" := (hprop_exists (fun x1:T1 => H))
  (at level 39, x1 ident, H at level 50, only parsing) : heap_scope.
Notation "'Hexists' ( x1 : T1 ) , H" := (hprop_exists (fun x1:T1 => H))
  (at level 39, x1 ident, H at level 50, only parsing) : heap_scope.

Notation "\GC" := (hprop_gc) : heap_scope. 

Open Scope heap_scope.
Bind Scope heap_scope with hprop.
Delimit Scope heap_scope with h.


(********************************************************************)
(* ** Properties *)

(*------------------------------------------------------------------*)
(* ** Tactic for automation *)

Hint Extern 1 (_ = _ :> heap) => state_eq.

Tactic Notation "state_disjoint_pre" :=
  subst; rew_disjoint; jauto_set.

Hint Extern 1 (\# _ _) => state_disjoint_pre.


(*------------------------------------------------------------------*)
(* ** Properties of [hprop] *)

Global Instance hprop_inhab : Inhab hprop.
Proof using. intros. apply (prove_Inhab hprop_empty). Qed.

Section HeapProp.
Transparent hprop_empty hprop_pure hprop_single hprop_star hprop_gc.

Lemma hprop_empty_prove : 
  \[] state_empty.
Proof using. hnfs~. Qed.

Lemma hprop_empty_inv : forall h,
  \[] h -> h = state_empty.
Proof using. auto. Qed.

Lemma hprop_pure_prove : forall (P:Prop),
  P -> 
  \[P] state_empty.
Proof using. intros. hnfs~. Qed.

Lemma hprop_pure_inv : forall h (P:Prop),
  \[P] h -> 
  h = state_empty /\ P.
Proof using. intros. auto. Qed.

Lemma hprop_gc_prove : forall h, 
  \GC h.
Proof using. intros. exists~ (=h). Qed.

Lemma hprop_gc_merge : 
  \GC \* \GC = \GC.
Proof using.
  unfold hprop. extens. intros h. iff (h1&h2&M1&M2&D&U) M.
  { apply hprop_gc_prove. }
  { exists h heap_empty. splits~. apply hprop_gc_prove. }
Qed.

Lemma hprop_star_comm : forall H1 H2,
   H1 \* H2 = H2 \* H1.
Proof using. 
  intros H1 H2. unfold hprop, hprop_star. extens. intros h.
  iff (h1&h2&M1&M2&D&U); 
    rewrite~ state_union_comm_disjoint in U; exists* h2 h1. 
Qed.

Lemma hprop_star_empty_l : forall H,
   \[] \* H = H.
Proof using.
  intro. unfold hprop, hprop_star. extens. intros h.
  iff (h1&h2&M1&M2&D&U) M. 
  hnf in M1. subst h1 h. rewrite~ state_union_empty_l.
  exists state_empty h. hint hprop_empty_prove. auto.
Qed.

Lemma hprop_star_empty_r : forall H, 
  H \* \[] = H.
Proof using.
  apply neutral_r_from_comm_neutral_l.
  applys hprop_star_comm. applys hprop_star_empty_l.
Qed.

Lemma hprop_star_assoc : forall H1 H2 H3,
  (H1 \* H2) \* H3 = H1 \* (H2 \* H3).
Proof using. 
  intros H1 H2 H3. unfold hprop, hprop_star. extens. intros h. split.
  { intros (h'&h3&(h1&h2&M3&M4&D'&U')&M2&D&U). subst h'.
    exists h1 (h2 \+ h3). splits~.
    { exists h2 h3. splits*. } }
  { intros (h1&h'&M1&(h2&h3&M3&M4&D'&U')&D&U). subst h'.
    exists (h1 \+ h2) h3. splits~.
    { exists h1 h2. splits*. } }
Qed. 

Lemma hprop_star_single_same_loc_disjoint : forall (l:loc) (v1 v2:val),
  (hprop_single l v1) \* (hprop_single l v2) ==> \[False].
Proof using.
  intros. unfold hprop_single. intros h (h1&h2&E1&E2&D&E). false.
  subst. applys* state_single_same_loc_disjoint.
Qed.

Lemma hprop_star_prop_elim : forall (P:Prop) H h,
  (\[P] \* H) h -> P /\ H h.
Proof using.
  introv (?&?&N&?&?&?). destruct N. subst. rewrite~ state_union_empty_l.
Qed.

(*------------------------------------------------------------------*)
(* ** Properties of [himpl] *)

(** 

Note that [P ==> Q] is a notation from [pred_le P Q], defined as
[forall x, P x -> Q x].

Note that [P ===> Q] is a notation from [rel_le P Q], defined as
[forall x y, P x y -> Q x y].

*)

Lemma himpl_forward : forall H1 H2 h,
  (H1 ==> H2) -> (H1 h) -> (H2 h).
Proof using. auto. Qed.

Lemma himpl_refl : forall H,
  H ==> H.
Proof using. intros h. auto. Qed.

Lemma himpl_trans : forall H1 H2 H3,
  (H1 ==> H2) -> (H2 ==> H3) -> (H1 ==> H3).
Proof using. introv M1 M2. intros h H1h. eauto. Qed.

Lemma himpl_antisym : forall H1 H2,
  (H1 ==> H2) -> (H2 ==> H1) -> (H1 = H2).
Proof using. introv M1 M2. applys prop_ext_1. intros h. iff*. Qed.

Lemma himpl_cancel_l : forall H1 H1' H2,
  H1 ==> H1' -> (H1 \* H2) ==> (H1' \* H2).
Proof using. introv W (h1&h2&?). exists* h1 h2. Qed.

Lemma himpl_cancel_r : forall H1 H2 H2',
  H2 ==> H2' -> (H1 \* H2) ==> (H1 \* H2').
Proof using. introv W (h1&h2&?). exists* h1 h2. Qed.

Lemma himpl_extract_prop : forall (P:Prop) H H',
  (P -> H ==> H') -> (\[P] \* H) ==> H'.
Proof using. introv W Hh. lets (?&?): hprop_star_prop_elim Hh. applys* W. Qed.

Lemma himpl_extract_exists : forall A H (J:A->hprop),
  (forall x, J x ==> H) -> (hprop_exists J) ==> H.
Proof using. introv W. intros h (x&Hh). applys* W. Qed.

Lemma himpl_inst_prop : forall (P:Prop) H H',
  P -> H ==> H' -> H ==> (\[P] \* H').
Proof using.
  introv HP W. intros h Hh. exists state_empty h.
  splits*. applys* hprop_pure_prove.
Qed.

Lemma himpl_inst_exists : forall A (x:A) H J,
  (H ==> J x) -> H ==> (hprop_exists J).
Proof using. introv W h. exists x. apply~ W. Qed.

Lemma himpl_remove_gc : forall H H',
  H ==> H' ->
  H ==> H' \* \GC.
Proof using.
  introv M. intros h Hh. exists h state_empty. splits*.
  exists \[]. applys hprop_empty_prove.
Qed.

End HeapProp.

Hint Resolve hprop_empty_prove hprop_pure_prove hprop_gc_prove.




(********************************************************************)
(********************************************************************)
(********************************************************************)
(** * Rules without GC *)

Module TripleWithoutGC.


(*------------------------------------------------------------------*)
(** Definition of triples *)

Definition triple t H Q :=
  forall h1 h2, 
  \# h1 h2 -> 
  H h1 -> 
  exists h1' v, 
       \# h1' h2 
    /\ red (h1 \u h2) t (h1' \u h2) v
    /\ Q v h1'.


(*------------------------------------------------------------------*)
(** Structural rules *)

Definition heap_union_empty_r := state_union_empty_r.

Lemma rule_extract_prop : forall t (P:Prop) H Q,
  (P -> triple t H Q) ->
  triple t (H \* \[P]) Q.
Proof using. 
  introv M. intros h1 h2 D (h21&h22&N1&(HE&HP)&N3&N4).
  subst h22 h1. rewrite heap_union_empty_r. applys* M. 
Qed.

Lemma rule_extract_exists : forall t A (J:A->hprop) Q,
  (forall x, triple t (J x) Q) ->
  triple t (hprop_exists J) Q.
Proof using. introv M D (x&Jx). applys* M. Qed.

Lemma rule_consequence : forall t H' Q' H Q,
  H ==> H' ->
  triple t H' Q' ->
  Q' ===> Q ->
  triple t H Q.
Proof using. 
  introv MH M MQ. intros h1 h2 D P1. 
  lets P1': (rm MH) (rm P1). 
  forwards~ (h1'&v&(N1&N2&N3)): (rm M) h2 (rm P1'). 
  exists h1' v. splits~. applys~ MQ.
Qed.

Lemma rule_frame : forall t H Q H',
  triple t H Q ->
  triple t (H \* H') (Q \*+ H').
Proof using. 
  introv M. intros h1 h2 D1 (h1a&h1b&H1a&H1b&D12a&E12).
  lets~ (h1'a&v&Da&Ra&Qa): M h1a (h1b \u h2).
  subst h1. exists (h1'a \u h1b) v. splits~.
  { state_red. }
  { exists~ h1'a h1b. }
Qed.


(*------------------------------------------------------------------*)
(** Term rules *)

Lemma rule_val : forall v H Q,
  H ==> Q v ->
  triple (trm_val v) H Q.
Proof using.
  introv M D H1. exists h1 v. splits~.
  { applys red_val. }
Qed.

Lemma rule_if : forall v t1 t2 H Q,
  triple (If v = val_int 0 then t2 else t1) H Q ->
  triple (trm_if v t1 t2) H Q.
Proof using.
  introv M D H1. forwards (h1'&v'&Da&Ra&Qa): M D H1.
  exists h1' v'. splits~.
  { applys~ red_if. }
Qed.

Lemma rule_let : forall x t1 t2 H Q Q1,
  triple t1 H Q1 ->
  (forall (X:val), triple (subst_trm x X t2) (Q1 X) Q) ->
  triple (trm_let x t1 t2) H Q.
Proof using.
  introv M1 M2. intros h1 h2 D1 H1.
  lets (h1'&v1&Da&Ra&Qa): M1 D1 H1.
  forwards~ (h1''&v2&Db&Rb&Qb): M2 v1 h1' h2.
  exists h1'' v2. splits~.
  { applys red_let Ra. state_red. }
Qed.

Lemma rule_let_val : forall x v1 t2 H Q,
  (forall (X:val), X = v1 -> triple (subst_trm x X t2) H Q) ->
  triple (trm_let x (trm_val v1) t2) H Q.
Proof using. 
  introv M. forwards~ M': M.
  applys_eq~ (>> rule_let H (fun x => H \* \[x = v1])) 2.
  { applys rule_val. rewrite hprop_star_comm. applys~ himpl_inst_prop. }
  { intros X. applys rule_extract_prop. applys M. }
Qed.

Lemma rule_app : forall f x F V t1 H Q,
  F = (val_fix f x t1) ->
  triple (subst_trm f F (subst_trm x V t1)) H Q ->
  triple (trm_app F V) H Q.
Proof using.
  introv EF M. subst F. intros h1 h2 D1 H1.
  lets (h1'a&ra&Da&Ra&Qa): M D1 H1.
  exists h1'a ra. splits~. 
  { applys~ red_app. }
Qed.

Lemma rule_let_fix : forall f x t1 t2 H Q,
  (forall (F:val), 
    (forall X H' Q', 
      triple (subst_trm f F (subst_trm x X t1)) H' Q' ->
      triple (trm_app F X) H' Q') ->
    triple (subst_trm f F t2) H Q) ->
  triple (trm_let f (trm_val (val_fix f x t1)) t2) H Q.
Proof using.
  introv M. applys rule_let_val. intros F EF. 
  applys (rm M). clears H Q. intros X H Q. applys~ rule_app.
Qed.

Lemma rule_new : forall v,
  triple (prim_new v) \[] (fun r => Hexists l, \[r = val_loc l] \* l ~~> v).
Proof using.
  intros. intros h1 h2 _ P1.
  lets E: hprop_empty_inv P1. subst h1.
  lets (l&Dl): (state_disjoint_new h2 v).
  sets h1': (state_single l v).
  exists h1' (val_loc l). splits~.
  { rew_state. applys~ red_new. }
  { exists l. applys~ himpl_inst_prop (l ~~> v). split~. }
Qed.

Lemma rule_get : forall v l,
  triple (prim_get (val_loc l)) (l ~~> v) (fun x => \[x = v] \* (l ~~> v)).
Proof using.
  intros. intros h1 h2 D P1. exists h1 v. splits~.
  { rew_state. applys red_get. applys~ state_union_single_read. }
  { exists~ heap_empty h1. }
Qed.

Lemma rule_set : forall l v w,
  triple (prim_set (val_pair (val_loc l) w)) (l ~~> v) (fun r => \[r = val_unit] \* l ~~> w).
Proof using.
  intros. intros h1 h2 D P1.
  sets h1': (state_single l w).
  exists h1' val_unit. splits~.
  { applys state_disjoint_single_set v. rewrite~ <- P1. }
  { rew_state. applys~ red_set. applys~ state_union_single_write v w. }
  { applys~ himpl_inst_prop (l ~~> w). split~. }
Qed.


End TripleWithoutGC.




(********************************************************************)
(********************************************************************)
(********************************************************************)
(** * Rules with GC *)

Module TripleWithGC.

(*------------------------------------------------------------------*)
(* ** Definition of [on_sub] *)

Definition on_sub H := (H \* \GC).

Lemma on_sub_base : forall H h,
  H h -> 
  on_sub H h.
Proof using. intros H h M. exists~ h heap_empty. Qed.

Lemma on_sub_gc : forall H h,
  on_sub (H \* \GC) h ->
  on_sub H h.
Proof using.
  introv M. unfolds on_sub. applys himpl_forward (rm M).
  rewrite hprop_star_assoc. rewrite~ hprop_gc_merge.
Qed.

Lemma on_sub_union_r : forall H h1 h2,
  on_sub H h1 ->
  \# h1 h2 ->
  on_sub H (h1 \u h2).
Proof using.
  introv M D. unfolds on_sub. rewrite <- hprop_gc_merge.
  rewrite <- hprop_star_assoc. exists~ h1 h2.
Qed.

Lemma on_sub_union : forall H1 H2 h1 h2,
  on_sub H1 h1 ->
  H2 h2 ->
  \# h1 h2 ->
  on_sub (H1 \* H2) (h1 \u h2).
Proof using.
  introv M P2 D. unfolds on_sub. 
  rewrite hprop_star_assoc.
  rewrite (@hprop_star_comm H2 \GC).
  rewrite <- hprop_star_assoc.
  exists~ h1 h2. 
Qed.

Lemma on_sub_weaken : forall Q Q' v h,
  on_sub (Q v) h ->
  Q ===> Q' ->
  on_sub (Q' v) h.
Proof using.
  introv M W. unfolds on_sub. applys himpl_forward (rm M).
  applys~ himpl_cancel_l. 
Qed.


(*------------------------------------------------------------------*)
(** Definition of triples *)

Definition triple t H Q :=
  forall h1 h2, 
  \# h1 h2 -> 
  H h1 -> 
  exists h1' v, 
       \# h1' h2 
    /\ red (h1 \u h2) t (h1' \u h2) v
    /\ on_sub (Q v) h1'.


(*------------------------------------------------------------------*)
(** Structural rules *)

Definition heap_union_empty_r := state_union_empty_r.

Lemma rule_extract_prop : forall t (P:Prop) H Q,
  (P -> triple t H Q) ->
  triple t (H \* \[P]) Q.
Proof using. 
  introv M. intros h1 h2 D (h21&h22&N1&(HE&HP)&N3&N4).
  subst h22 h1. rewrite heap_union_empty_r. applys* M. 
Qed.

Lemma rule_extract_exists : forall t (A:Type) (J:A->hprop) Q,
  (forall x, triple t (J x) Q) ->
  triple t (hprop_exists J) Q.
Proof using. introv M D (x&Jx). applys* M. Qed.

Lemma rule_consequence : forall t H' Q' H Q,
  H ==> H' ->
  triple t H' Q' ->
  Q' ===> Q ->
  triple t H Q.
Proof using. 
  introv MH M MQ. intros h1 h2 D P1. 
  lets P1': (rm MH) (rm P1). 
  forwards~ (h1'&v&(N1&N2&N3)): (rm M) h2 (rm P1'). 
  exists h1' v. splits~. { applys~ on_sub_weaken Q'. }
Qed.

Lemma rule_frame : forall t H Q H',
  triple t H Q ->
  triple t (H \* H') (Q \*+ H').
Proof using. 
  introv M. intros h1 h2 D1 (h1a&h1b&H1a&H1b&D12a&E12).
  lets~ (h1'a&v&Da&Ra&Qa): M h1a (h1b \u h2).
  subst h1. exists (h1'a \u h1b) v. splits~.
  { state_red. }
  { applys~ on_sub_union. }
Qed.

Lemma rule_gc_post : forall t H Q,
  triple t H (Q \*+ \GC) ->
  triple t H Q.
Proof using.
  introv M. intros h1 h2 D P1. 
  forwards* (h1'&v&(N1&N2&N3)): (rm M) h1 h2.
  exists h1' v. splits~. { applys~ on_sub_gc. }
Qed.

Lemma rule_gc_pre : forall t H Q,
  triple t H Q ->
  triple t (H \* \GC) Q.
Proof using.
  introv M. applys rule_gc_post. applys~ rule_frame.
Qed.


(*------------------------------------------------------------------*)
(** Term rules *)

Lemma rule_val : forall v H Q,
  H ==> Q v ->
  triple (trm_val v) H Q.
Proof using.
  introv M D H1. exists h1 v. splits~.
  { applys red_val. }
  { applys~ on_sub_base. }
Qed.

Lemma rule_if : forall v t1 t2 H Q,
  triple (If v = val_int 0 then t2 else t1) H Q ->
  triple (trm_if v t1 t2) H Q.
Proof using.
  introv M D H1. forwards (h1'&v'&Da&Ra&Qa): M D H1.
  exists h1' v'. splits~.
  { applys~ red_if. }
Qed.

Lemma rule_let : forall x t1 t2 H Q Q1,
  triple t1 H Q1 ->
  (forall (X:val), triple (subst_trm x X t2) (Q1 X) Q) ->
  triple (trm_let x t1 t2) H Q.
Proof using.
  introv M1 M2. intros h1 h2 D1 H1.
  lets (h1'&v1&Da&Ra&Qa): M1 D1 H1.
  lets (h1'a&h1'b&N1&N2&N3&N4): (rm Qa).
  forwards~ (h1''&v2&Db&Rb&Qb): M2 v1 h1'a (h1'b \u h2).
  exists (h1'' \u h1'b) v2. splits~.
  { applys red_let Ra. state_red. }
  { applys~ on_sub_union_r. }
Qed.

Lemma rule_let_val : forall x v1 t2 H Q,
  (forall (X:val), X = v1 -> triple (subst_trm x X t2) H Q) ->
  triple (trm_let x (trm_val v1) t2) H Q.
Proof using. 
  introv M. forwards~ M': M.
  applys_eq~ (>> rule_let H (fun x => H \* \[x = v1])) 2.
  { applys rule_val. rewrite hprop_star_comm. applys~ himpl_inst_prop. }
  { intros X. applys rule_extract_prop. applys M. }
Qed.

Lemma rule_app : forall f x F V t1 H Q,
  F = (val_fix f x t1) ->
  triple (subst_trm f F (subst_trm x V t1)) H Q ->
  triple (trm_app F V) H Q.
Proof using.
  introv EF M. subst F. intros h1 h2 D1 H1.
  lets (h1'a&ra&Da&Ra&Qa): M D1 H1.
  exists h1'a ra. splits~. 
  { applys~ red_app. }
Qed.

Lemma rule_let_fix : forall f x t1 t2 H Q,
  (forall (F:val), 
    (forall X H' Q', 
      triple (subst_trm f F (subst_trm x X t1)) H' Q' ->
      triple (trm_app F X) H' Q') ->
    triple (subst_trm f F t2) H Q) ->
  triple (trm_let f (trm_val (val_fix f x t1)) t2) H Q.
Proof using.
  introv M. applys rule_let_val. intros F EF. 
  applys (rm M). clears H Q. intros X H Q. applys~ rule_app.
Qed.

Lemma rule_new : forall v,
  triple (prim_new v) \[] (fun r => Hexists l, \[r = val_loc l] \* l ~~> v).
Proof using.
  intros. intros h1 h2 _ P1.
  lets E: hprop_empty_inv P1. subst h1.
  lets (l&Dl): (state_disjoint_new h2 v).
  sets h1': (state_single l v).
  exists h1' (val_loc l). splits~.
  { rew_state. applys~ red_new. }
  { applys~ on_sub_base. exists l. 
    applys~ himpl_inst_prop (l ~~> v). split~. }
Qed.

Lemma rule_get : forall v l,
  triple (prim_get (val_loc l)) (l ~~> v) (fun x => \[x = v] \* (l ~~> v)).
Proof using.
  intros. intros h1 h2 D P1. exists h1 v. splits~.
  { rew_state. applys red_get. applys~ state_union_single_read. }
  { exists h1 heap_empty. splits~. { exists~ heap_empty h1. } }
Qed.

Lemma rule_set : forall l v w, (* todo: changer order of arguments *)
  triple (prim_set (val_pair (val_loc l) w)) (l ~~> v) (fun r => \[r = val_unit] \* l ~~> w).
Proof using.
  intros. intros h1 h2 D P1.
  sets h1': (state_single l w).
  exists h1' val_unit. splits~.
  { applys state_disjoint_single_set v. rewrite~ <- P1. }
  { rew_state. applys~ red_set. applys~ state_union_single_write v w. }
  { applys~ on_sub_base. applys~ himpl_inst_prop (l ~~> w). split~. }
Qed.

End TripleWithGC.





(********************************************************************)
(********************************************************************)
(********************************************************************)
(** * Triples with lifting *)

Module TripleWithGCAndLifting.

Import TripleWithGC.
Generalizable Variables A B.


Definition func := val.


Class Enc (A:Type) := 
  make_Enc { enc : A -> val }.

Instance Enc_int : Enc int. 
Proof using. constructor. applys val_int. Defined.

Instance Enc_func : Enc func. 
Proof using. constructor. applys (fun x:func => x). Defined.


Definition Subst_trm (x:var) `{Enc A} (X:A) (t:trm) : trm :=
  subst_trm x (enc X) t.

Definition PostEnc `{Enc A} (Q:A->hprop) : val->hprop :=
  fun v => Hexists V, \[v = enc V] \* Q V.

Definition Triple (t:trm) (H:hprop) `{EA:Enc A} (Q:A->hprop) :=
  triple t H (PostEnc Q).

Lemma Rule_val : forall A `{EA:Enc A} (V:A) v H (Q:A->hprop),
  v = enc V ->
  H ==> Q V ->
  Triple (trm_val v) H Q.
Proof using.
  introv E M. applys rule_val.
  unfold PostEnc. subst.
  applys himpl_inst_exists V.
  applys~ himpl_inst_prop.
Qed.

Lemma Rule_if : forall (V:int) v t1 t2 H A `{EA:Enc A} (Q:A->hprop),
  v = enc V ->
  Triple (If V = 0 then t2 else t1) H Q ->
  Triple (trm_if v t1 t2) H Q.
Proof using.
  introv E M. applys rule_if. subst. 
  applys_eq (rm M) 3. tests: (V = 0); simpl; case_if~.
Qed.

Lemma Rule_let : forall x t1 t2 H,
  forall A `{EA:Enc A} (Q:A->hprop) A1 `{EA1:Enc A1} (Q1:A1->hprop),
  Triple t1 H Q1 ->
  (forall (X:A1), Triple (Subst_trm x X t2) (Q1 X) Q) ->
  Triple (trm_let x t1 t2) H Q.
Proof using.
  introv M1 M2. applys rule_let M1.
  intros v. unfold PostEnc.
  applys rule_extract_exists. intros V.
  rewrite hprop_star_comm. applys rule_extract_prop.
  introv E. subst. applys M2.
(* TODO: why notation for hprop_exists shows hexists? *)
Qed.


Lemma Rule_let_val_enc : forall A1 `{Enc A1} (V1:A1) x v1 t2 H A `{EA: Enc A} (Q:A->hprop),
  v1 = enc V1 ->
  (forall X, X = V1 -> Triple (Subst_trm x X t2) H Q) ->
  Triple (trm_let x (trm_val v1) t2) H Q.
Proof using.
  introv E M. applys rule_let_val. intros X EX. subst. applys~ M.
Qed.

(*
Lemma Rule_let_val : forall x v1 t2 H A `{EA: Enc A} (Q:A->hprop),
  (forall `{Enc A1} (X:A1), v1 = enc X -> Triple (Subst_trm x X t2) H Q) ->
  Triple (trm_let x (trm_val v1) t2) H Q.
 => true by exploiting the fact that every value is the encoding of another.
*)

Lemma Rule_app : forall f x (F:func) V t1 H A `{EA:Enc A} (Q:A->hprop),
  F = (val_fix f x t1) ->
  Triple (subst_trm f F (subst_trm x V t1)) H Q ->
  Triple (trm_app F V) H Q.
Proof using.
  introv EF M. applys* rule_app.
Qed.

Lemma enc_val_fix_eq : forall F f x t1,
  F = val_fix f x t1 ->
  enc F = F.
Proof using. intros. subst~. Qed.

Lemma Rule_let_fix : forall f x t1 t2 H A `{EA:Enc A} (Q:A->hprop),
  (forall (F:func), 
    (forall `{EA1:Enc A1} `{EA2:Enc A2} (X:A1) H' (Q':A2->hprop), 
      Triple (Subst_trm f F (Subst_trm x X t1)) H' Q' ->
      Triple (trm_app F (enc X)) H' Q') ->
    Triple (Subst_trm f F t2) H Q) ->
  Triple (trm_let f (trm_val (val_fix f x t1)) t2) H Q.
Proof using.
  introv M. applys (@Rule_let_val_enc _ _ (val_fix f x t1)).
  { symmetry. applys* enc_val_fix_eq. }
  intros F EF. applys (rm M). clears H Q.
  intros A2 EA2 A3 EA3 X H Q M.
  applys~ Rule_app EF.
Qed.

Definition hprop_single_enc `{EA:Enc A} r V := 
  hprop_single r (enc V).

Notation "r '~~~>' V" := (hprop_single_enc r V)
  (at level 32, no associativity) : heap_scope.

Instance Enc_loc : Enc loc. 
Proof using. constructor. applys val_loc. Defined.

Lemma Rule_new : forall A `{EA:Enc A} (V:A) v,
  v = enc V ->
  Triple (prim_new v) \[] (fun l => l ~~~> V).
Proof using.
  introv E. applys_eq rule_new 1. subst~. 
Qed.

Lemma Rule_get : forall A `{EA:Enc A} (V:A) l,
  Triple (prim_get (val_loc l)) (l ~~~> V) (fun x => \[x = V] \* (l ~~~> V)).
Proof using.
  introv. applys rule_consequence. { auto. } { applys rule_get. }
  { intros v. unfold PostEnc. 
    applys himpl_extract_prop. intro_subst.
    applys himpl_inst_exists V.
    applys~ himpl_inst_prop.
    applys~ himpl_inst_prop. }
Qed.


Instance Enc_unit : Enc unit. 
Proof using. constructor. applys (fun x:unit => val_unit). Defined.

Lemma Rule_set : forall A1 A2 `{EA1:Enc A1} (V1:A1) `{EA2:Enc A2} (V2:A2) l v2,
  v2 = enc V2 ->
  Triple (prim_set (val_pair (val_loc l) v2)) (l ~~~> V1) (fun (r:unit) => l ~~~> V2).
Proof using.
  introv E. applys rule_consequence. { auto. } { applys rule_set. }
  { subst. intros v. unfold PostEnc. 
    applys himpl_extract_prop. intro_subst.
    applys himpl_inst_exists tt.
    applys~ himpl_inst_prop. }
Qed.

End TripleWithGCAndLifting.














(********************************************************************)
(********************************************************************)
(********************************************************************)
(* ** * Rules without GC, low-level proof *)

Implicit Types H : hprop.


(*------------------------------------------------------------------*)
(* ** Definition of triples *)

Definition triple t H Q :=
  forall h1 h2, 
  \# h1 h2 -> 
  H h1 -> 
  exists h1' v, 
       \# h1' h2 
    /\ red (h1 \u h2) t (h1' \u h2) v
    /\ Q v h1'.


(*------------------------------------------------------------------*)
(* ** Structural rules *)

Lemma rule_extract_exists : forall t A (J:A->hprop) Q,
  (forall x, triple t (J x) Q) ->
  triple t (hprop_exists J) Q.
Proof using. introv M D (x&Jx). applys* M. Qed.

Lemma rule_extract_prop : forall t (P:Prop) H Q,
  (P -> triple t H Q) ->
  triple t (\[P] \* H) Q.
Proof using.
  intros t. applys (rule_extract_prop_from_extract_exists (triple t)).
  applys rule_extract_exists.
Qed.

Lemma rule_consequence : forall t H' Q' H Q,
  H ==> H' ->
  triple t H' Q' ->
  Q' ===> Q ->
  triple t H Q.
Proof using. 
  introv MH M MQ. intros h1 h2 D P1. 
  lets P1': (rm MH) (rm P1). 
  forwards~ (h1'&v&(N1&N2&N3)): (rm M) h2 (rm P1'). 
  exists h1' v. splits~. applys~ MQ.
Qed.

Lemma rule_frame : forall t H Q H',
  triple t H Q ->
  triple t (H \* H') (Q \*+ H').
Proof using. 
  introv M. intros h1 h2 D1 (h1a&h1b&H1a&H1b&D12a&E12).
  lets~ (h1'a&v&Da&Ra&Qa): M h1a (h1b \u h2).
  subst h1. exists (h1'a \u h1b) v. splits~.
  { state_red. }
  { exists~ h1'a h1b. }
Qed.


(*------------------------------------------------------------------*)
(* ** Term rules *)

Lemma rule_val : forall v H Q,
  H ==> Q v ->
  triple (trm_val v) H Q.
Proof using.
  introv M D H1. exists h1 v. splits~.
  { applys red_val. }
Qed.

Lemma rule_if : forall v t1 t2 H Q,
  triple (If v = val_int 0 then t2 else t1) H Q ->
  triple (trm_if v t1 t2) H Q.
Proof using.
  introv M D H1. forwards (h1'&v'&Da&Ra&Qa): M D H1.
  exists h1' v'. splits~.
  { applys~ red_if. }
Qed.

Lemma rule_let : forall x t1 t2 H Q Q1,
  triple t1 H Q1 ->
  (forall (X:val), triple (subst_trm x X t2) (Q1 X) Q) ->
  triple (trm_let x t1 t2) H Q.
Proof using.
  introv M1 M2. intros h1 h2 D1 H1.
  lets (h1'&v1&Da&Ra&Qa): M1 D1 H1.
  forwards~ (h1''&v2&Db&Rb&Qb): M2 v1 h1' h2.
  exists h1'' v2. splits~.
  { applys red_let Ra. state_red. }
Qed.

Lemma rule_let_val : forall x v1 t2 H Q,
  (forall (X:val), X = v1 -> triple (subst_trm x X t2) H Q) ->
  triple (trm_let x (trm_val v1) t2) H Q.
Proof using. 
  introv M. forwards~ M': M.
  applys_eq~ (>> rule_let H (fun x => \[x = v1] \* H)) 2.
  { applys rule_val. applys~ himpl_inst_prop. }
  { intros X. applys rule_extract_prop. applys M. }
Qed.

Lemma rule_app : forall f x F V t1 H Q,
  F = (val_fix f x t1) ->
  triple (subst_trm f F (subst_trm x V t1)) H Q ->
  triple (trm_app F V) H Q.
Proof using.
  introv EF M. subst F. intros h1 h2 D1 H1.
  lets (h1'a&ra&Da&Ra&Qa): M D1 H1.
  exists h1'a ra. splits~. 
  { applys~ red_app. }
Qed.

Lemma rule_let_fix : forall f x t1 t2 H Q,
  (forall (F:val), 
    (forall X H' Q', 
      triple (subst_trm f F (subst_trm x X t1)) H' Q' ->
      triple (trm_app F X) H' Q') ->
    triple (subst_trm f F t2) H Q) ->
  triple (trm_let f (trm_val (val_fix f x t1)) t2) H Q.
Proof using.
  introv M. applys rule_let_val. intros F EF. 
  applys (rm M). clears H Q. intros X H Q. applys~ rule_app.
Qed.

Section RulesPrimitiveOps.
Transparent hprop_single.

Lemma rule_ref : forall v,
  triple (prim_ref v) \[] (fun r => Hexists l, \[r = val_loc l] \* l ~~> v).
Proof using.
  intros. intros h1 h2 _ P1.
  lets E: hprop_empty_inv P1. subst h1.
  forwards~ (l&Dl&Nl): (state_disjoint_new null h2 v).
  sets h1': (state_single l v).
  exists h1' (val_loc l). splits~.
  { rew_state. applys~ red_ref. }
  { exists l. applys~ himpl_inst_prop (l ~~> v). split~. }
Qed.

Lemma rule_get : forall v l,
  triple (prim_get (val_loc l)) (l ~~> v) (fun x => \[x = v] \* (l ~~> v)).
Proof using.
  intros. intros h1 h2 D P1. exists h1 v. splits~.
  { applys red_get. destruct P1 as (P1a&P1b). applys~ state_union_single_read. }
  { rewrite hprop_star_pure. split~. }
Qed.

Lemma rule_set : forall w l v,
  triple (prim_set (val_pair (val_loc l) w)) (l ~~> v) (fun r => \[r = val_unit] \* l ~~> w).
Proof using.
  intros. intros h1 h2 D (P1a&P1b).
  sets h1': (state_single l w).
  exists h1' val_unit. splits~.
  { applys state_disjoint_single_set v. rewrite~ <- P1a. }
  { rew_state. applys~ red_set. applys~ state_union_single_write v w. }
  { applys~ himpl_inst_prop (l ~~> w). split~. }
Qed.

End RulesPrimitiveOps.






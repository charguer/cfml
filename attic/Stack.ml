
type 'a t = 'a list ref

let create () =
  ref []

let is_empty s =
  !s = []

let push x s =
  s := x::!s

let pop s =
  match !s with
  | [] -> raise Not_found
  | x::t -> s := t; x


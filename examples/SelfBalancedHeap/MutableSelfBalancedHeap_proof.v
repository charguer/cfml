Set Implicit Arguments.
Require Import CFLib LibCore LibMultiset.
Require Import OrderedType_ml OrderedType_proof. 
  (* HeapSig_ml HeapSig_proof *)
Require Import MutableSelfBalancedHeap_ml.



(* -- from HeapSig_proof *)

Generalizable Variables A.

(** Definition of the minimum of a multiset *)

Definition min_of `{Le A} (E:multiset A) (x:A) := 
  x \in E /\ forall_ y \in E, x <= y.

(** Definition of the removal of the minimum from a multiset *)

Definition removed_min `{Le A} (E E':multiset A) :=
  exists x, min_of E x /\ E = \{x} \u E'.



(* -- proof -- *)

Module SelfBalancedHeapSpec (O:MLOrderedType) (OS:OrderedTypeSpec with Module O:=O).
  (* <: HeapSigSpec with Definition H.MLElement.t := O.t. *)

(** instantiations *)

Module Import H (* <: MLHeap *) := MLMutableSelfBalancedHeap O.
Module Import OS := OS.
Definition t := O.t.

Implicit Types x y : t.
Implicit Types q : heap.
Implicit Types E : multiset t.

(** representation *)

Inductive tree :=
  | Empty : tree
  | Node : tree -> t -> tree -> tree.

Instance tree_Inhab : Inhab tree. 
Proof using. apply (prove_Inhab Empty). Qed.

(** Representation predicate as trees
    [p ~> Tree T] *)

Definition ContentsFor Tree (T:tree) v :=
  match T with
  | Empty =>
     \[v = MEmpty]
  | Node T1 x T2 => 
     Hexists t1 t2, \[v = MNode t1 x t2] \* t1 ~> Tree T1 \* t2 ~> Tree T2
  end.

Fixpoint Tree (T:tree) (t:loc) : hprop :=
  Hexists v, t ~~> v \* v ~> ContentsFor Tree T.

Definition Contents := ContentsFor Tree.

(** invariant *)

Definition is_ge (x:t) (y:t) := x <= y.

Definition list_union (Hs:list (multiset t)) := 
  fold_right union \{} Hs.

Inductive inv : tree -> multiset t -> Prop :=
  | inv_empty : 
      inv Empty \{} 
  | inv_node : forall l x r El Er E,
      inv l El ->
      inv r Er ->
      foreach (is_ge x) (El \u Er) ->
      E = \{x} \u El \u Er ->   
      inv (Node l x r) E.

(** Representation predicate for heap trees [p ~> Heap T] *)

Definition Heap (E:multiset t) (t:loc) :=
  Hexists (T:tree), t ~> Tree T \* \[inv T E].


(** model *)

Fixpoint size (q:tree) : nat :=
  match q with
  | Empty => 1%nat
  | Node l x r => (1 + size l + size r)%nat 
  end.

(** automation *)

Hint Extern 1 (_ \in _) => multiset_in.
Hint Extern 1 (_ < _) => simpl; math.
Hint Extern 1 (_ = _ :> multiset _) => permut_simpl : multiset.
Hint Constructors inv.

Ltac auto_tilde ::= eauto.
Ltac auto_star ::= try solve [ intuition (eauto with multiset) ].


(** representation *)

Definition Contents' (T:tree) v :=
   match v with
  | MEmpty => \[T = Empty]
  | MNode t1 x t2 => Hexists T1 T2, \[T = Node T1 x T2] \* t1 ~> Tree T1 \* t2 ~> Tree T2
  end.

(* todo not used *)
Lemma pred_le_antisym : forall A, antisym (@pred_le A).
Proof using. intros_all. applys* prop_ext_1. Qed.


Lemma Contents_Contents' : forall T v,
  v ~> Contents T  =  v ~> Contents' T.
Proof.
  intros. unfold Contents, ContentsFor, Contents'.
  repeat rewrites hunfold_base_eq. applys pred_le_antisym.
  destruct T. 
    xextracts. xsimpl*.
    xextract as t1 t2 E. subst. xsimpl*.
  destruct v.
    xextracts. xsimpl*.
    xextract as T1 T2 E. subst. xsimpl*.
Qed. 

Lemma Contents_focus : forall v T,
  v ~> Contents T ==> 
  match T with
  | Empty =>
     \[v = MEmpty]
  | Node T1 x T2 => 
     Hexists t1 t2, \[v = MNode t1 x t2] \* t1 ~> Tree T1 \* t2 ~> Tree T2
  end.
Proof.
  intros. unfold Contents, ContentsFor. rewrite~ hunfold_base_eq.
Qed.

Lemma Contents_unfocus : forall v T,
  v ~> Contents T ==> 
  match v with
  | MEmpty => \[T = Empty]
  | MNode t1 x t2 => Hexists T1 T2, \[T = Node T1 x T2] \* t1 ~> Tree T1 \* t2 ~> Tree T2
  end.
Proof.
  intros. rewrite Contents_Contents'. unfold Contents'. rewrite~ hunfold_base_eq.
Qed.

Lemma Contents_empty_unfocus : forall T,
  MEmpty ~> Contents T ==> \[T = Empty].
Proof.
  intros. xchange Contents_unfocus. xsimpl. xsimpl*.
  (* intros. unfold Contents, ContentsFor. rewrite~ hunfold_base_eq.
  destruct T; xextract. intros. xsimpl*. *)
Qed.

Lemma Contents_node_unfocus : forall T t1 x t2,
  (MNode t1 x t2) ~> Contents T ==> 
  Hexists T1 T2, \[T = Node T1 x T2] \* t1 ~> Tree T1 \* t2 ~> Tree T2.
Proof.
  intros. xchange Contents_unfocus. xsimpl. xsimpl*.
  (* intros. unfold Contents, ContentsFor. rewrite~ hunfold_base_eq.
  destruct T; xextract. introv E. inverts E. xsimpl*. *)
Qed.

Lemma Tree_def : forall (t:loc) (T:tree),
  t ~> Tree T = Hexists v, t ~~> v \* v ~> Contents T.
Proof using.
  intros. destruct T; simpl; rewrite~ hunfold_base_eq.
Qed.

Lemma Tree_focus : forall (t:loc) (T:tree),
  t ~> Tree T ==> Hexists v, t ~~> v \* v ~> Contents T.
Proof using. intros. rewrite~ Tree_def. Qed.

Lemma Tree_unfocus : forall (t:loc) v (T:tree),
  t ~~> v \* v ~> Contents T ==> t ~> Tree T.
Proof using. intros. rewrite Tree_def. xsimpl. Qed.

Lemma Tree_empty_unfocus : forall (t:loc),
  (t ~~> MEmpty) ==> (t ~> Tree Empty).
Proof using.
  intros. hunfold Tree. hsimpl. unfold ContentsFor.
  rewrite~ hunfold_base_eq. hsimpl*.
Qed.

Lemma Tree_node_unfocus : forall (t:loc) x t1 t2 T1 T2,
  (t ~~> MNode t1 x t2) \* (t1 ~> Tree T1) \* (t2 ~> Tree T2) ==>
  (t ~> Tree (Node T1 x T2)).
Proof using.
  intros. hunfold Tree. hsimpl. unfold ContentsFor.
  repeat rewrite~ hunfold_base_eq. hsimpl*.
  repeat rewrite~ hunfold_base_eq. hsimpl*. (* todo cleanup *)
Qed.


Lemma Heap_focus : forall t E,
  t ~> Heap E ==>
  Hexists (T:tree), t ~> Tree T \* \[inv T E].
Proof using. intros. hunfold Heap. auto. Qed.

Lemma Heap_unfocus : forall t T E,
  t ~> Tree T \* \[inv T E] ==> t ~> Heap E.
Proof using. intros. hunfold Heap. hsimpl*. Qed.

Opaque Tree Heap.

Hint Extern 1 (Register xopen (Tree _)) => Provide Tree_focus.
Hint Extern 1 (Register xclose (Ref Id _)) => Provide Tree_unfocus.
Hint Extern 1 (Register xclose (Ref Id (MNode _ _ _))) => Provide Tree_node_unfocus.
Hint Extern 1 (Register xclose (Ref Id MEmpty)) => Provide Tree_empty_unfocus.

Hint Extern 1 (Register xopen (Heap _)) => Provide Heap_focus.
Hint Extern 1 (Register xclose (Tree _)) => Provide Heap_unfocus.

Hint Extern 1 (Register xopen (Contents _)) => Provide Contents_focus.
Hint Extern 1 (Register xclose (Contents _)) => Provide Contents_unfocus.


(** useful facts *)

Lemma foreach_ge_trans : forall (x y : t) (E : multiset t),
  foreach (is_ge x) E -> y <= x -> foreach (is_ge y) E.
Proof. intros. apply~ foreach_weaken. intros_all. apply* le_trans. Qed.

Hint Resolve foreach_ge_trans.

Lemma min_of_prove : forall (x : t) El Er,
 foreach (is_ge x) (El \u Er) ->
  min_of ('{x} \u El \u Er) x.
Proof using.
  introv H. split~. 
  introv M. destruct (in_union_inv M) as [N|N].
  rewrite (in_single N). apply le_refl.
  destruct (in_union_inv N) as [P|P]; apply~ H.
Qed.

(** verification *)

Lemma empty_spec : Spec empty () |R>> 
  R \[] (fun h => h ~> Heap \{}).
Proof using. xcf. xapp. intros h. xclose2~ h. Qed.

Hint Extern 1 (RegisterSpec empty) => Provide empty_spec.

Lemma is_empty_spec : Spec is_empty h |R>> 
  forall E,
  keep R (h ~> Heap E) (fun b => \[b = isTrue (E = \{})]).
Proof using.
  xcf. intros. xopen h as T IT. xopen h as v. xapps~. xmatch; xret.
  xclose MEmpty. xextracts. inverts IT. xclose2~ h. xsimpl~. 
  xclose v. destruct v; tryfalse. xextract. intros. subst. xclose2~ h.
   xsimpl*. inverts IT.
   { (* todo automate*) 
    introv EQ. asserts~ M:(t0 \in \{t0:t} \u El \u Er).
    rewrite EQ in M. multiset_in M. } 
Qed. 

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.

Lemma merge_spec_ind : Spec merge h1 h2 |R>>
  forall T1 T2 E1 E2, inv T1 E1 -> inv T2 E2 ->
  R (h1 ~> Tree T1 \* h2 ~> Tree T2)
    (fun (_:unit) => Hexists T, Hexists (v:contents), 
       h1 ~> Tree T \* h2 ~~> v \* \[inv T (E1 \u E2)]).
Proof using.
  xinduction_skip.
  xcf. introv I1 I2. xopen h1 as v1. xopen h2 as v2. xapps~. xapps~. xmatch.
  { xret. xclose h1. xclose MEmpty. xextracts. inverts I2. 
    xsimpl. rewrite~ union_empty_r. }
  { xapps. xapps. xclose h1. xclose MEmpty. xextracts. inverts I1.
    xsimpl. rewrite~ union_empty_l. } 
  { xapp as n. introv HC. xif; case_if*.
    { (* todo : simplify by going directly to decomposition *)
      xclose (MNode l1 x1 r1). xextract as T1l T1r EQ1. subst T1.
      inverts I1 as I1l I1r I1h.
      xclose (MNode l2 x2 r2). xextract as T2l T2r EQ2. subst T2.
      asserts M: (foreach (is_ge x1) E2).
        inverts I2 as I2l I2r I2h. rew_foreach in *. repeat splits; autos*.
      xclose h2.
      xapp~. xextract as T3 v I3. xclose h1. xsimpl. constructors~.
        rew_foreach in *. destruct I1h. repeat split; autos*.
      permut_simpl. }
    { applys_to HC gt_to_ge. rewrite ge_as_sle in HC.
      xclose (MNode l2 x2 r2). xextract as T2l T2r EQ2. subst T2.
      inverts I2 as I2l I2r I2h.
      xclose (MNode l1 x1 r1). xextract as T1l T1r EQ1. subst T1.
      asserts M: (foreach (is_ge x2) E1).
        inverts I1 as I1l I1r I1h. rew_foreach in *. repeat splits; autos*.
      xclose h1.
      xapp~. intros T3 v I3. xapps. xapps. xclose h1. xsimpl. constructors~.
        rew_foreach in *. destruct I2h. repeat split; autos*.
      permut_simpl. }
  }
Qed.

Lemma merge_spec : Spec merge h1 h2 |R>>
  forall E1 E2, 
  R (h1 ~> Heap E1 \* h2 ~> Heap E2) (fun (_:unit) => h1 ~> Heap (E1 \u E2)).
Proof using.
  xweaken merge_spec_ind. simpl. intros h1 h2. introv LR HR. intros.
  xopen h1 as T1 I1. xopen h2 as T2 I2.
  forwards~ M: (HR T1 T2). xgc. xapply* (>> HR T1 T2).
  xsimpl. xextract. intros. xclose~ h1. xsimpl~.
Qed.

Hint Extern 1 (RegisterSpec merge) => Provide merge_spec.

Lemma Tree_node_Heaps_unfocus : forall (t:loc) x t1 t2 E1 E2,
  foreach (is_ge x) (E1 \u E2) ->
  (t ~~> MNode t1 x t2) \* (t1 ~> Heap E1) \* (t2 ~> Heap E2) ==>
  (t ~> Heap (\{x} \u E1 \u E2)).
Proof using.   
  introv Hx. xopen t1 as T1 I1. xopen t2 as T2 I2. xclose2~ t0. xsimpl~.
Qed.

Lemma insert_spec : Spec insert (x:t) (h:heap) |R>> 
  forall E, 
  R (h ~> Heap E) (fun (_:unit) => h ~> Heap (\{x} \u E)).
Proof using.
  xcf. intros. xapp~ as hl. xapp~ as hr. xapp~ as hm.
  xchange (@Tree_node_Heaps_unfocus hm x). rew_foreach~.
  xapp~. xsimpl. permut_simpl.
Qed.

Hint Extern 1 (RegisterSpec insert) => Provide insert_spec.


Lemma pop_min_spec : Spec pop_min h |R>>
  forall E, E <> \{} -> 
  R (h ~> Heap E) (fun x => Hexists E', h ~> Heap E' 
                             \* \[min_of E x /\ removed_min E E']).
Proof using.
  hint min_of_prove.
  xcf. introv N. xopen h as T IT. xopen h as v. xapps. xmatch.
  xclose MEmpty. xextracts. xfail. inverts* IT.
  xclose (MNode a x b). xextract as T1 T2 EQ. subst.
   inverts IT as ITa ITb ITc. xapp_spec~ merge_spec_ind.
   intros t3 T3 I3. xopen a as v. xapps. xapps. xret.
   xclose2~ h. xsimpl. splits~. esplit. splits*.
Qed.

Hint Extern 1 (RegisterSpec pop_min) => Provide pop_min_spec.


End SelfBalancedHeapSpec.


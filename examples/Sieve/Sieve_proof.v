Set Implicit Arguments. 
Require Import CFML.CFLib Stdlib Array_proof Sieve_ml.
Require Import TLC.LibListZ.



(********************************************************************)
(** CFML Buffer *)

Lemma while_loop_cf_inv_partial : 
   forall (I:bool->hprop),
   forall (F1:~~bool) (F2:~~unit) H (Q:unit->hprop),
   (exists b, H ==> I b) ->
   (forall b, F1 (I b) (fun b' => I b')) ->
   (F2 (I true) (# Hexists b, (I b))) ->
   (I false ==> Q tt) ->
   (While F1 Do F2 Done_) H Q.
Admitted. (* now proved elsewhere *)

Lemma while_loop_cf_inv_partial_gc : 
   forall (I:bool->hprop),
   forall (F1:~~bool) (F2:~~unit) H (Q:unit->hprop),
   (exists b, H ==> I b) ->
   (forall b, F1 (I b) (fun b' => I b')) ->
   (F2 (I true) (# Hexists b, (I b))) ->
   (I false ==> Q tt \* Hexists H', H') ->
   (While F1 Do F2 Done_) H Q.
Admitted. (* now proved elsewhere *)


(********************************************************************)
(** Specifications *)

Definition divides (k d:int) :=
  exists m, k = m * d.

Definition prime (p:int) := 
  p > 1 /\ forall d, d > 0 -> divides p d -> d = 1 \/ d = p.

Definition returns_array_of_primes f := forall n,
  n > 1 -> 
  App f n; \[] (fun t => Hexists M, t ~> Array M \*
    \[ n = length M /\ forall i, 0 <= i < n -> M[i] = isTrue (prime i)]).
 


(********************************************************************)
(** Invariants *)

(** [prime_candidate i k] asserts that [k] has no proper factor
    smaller than [i], and [k > 1]. *)
 
Definition prime_candidate i k :=
  k > 1 /\ forall d, 1 < d < i -> d < k -> ~ divides k d.
  (* note: d < k could be strengthened to d <> k *)

(** [uncrossed i a b k] asserts that [k] is not a multiple of [i] in
    a particular range, more precisely not of the form [r*i] with
    [a <= r < b]. *)

Definition uncrossed i a b k :=
  forall r, a <= r < b -> k <> r * i.

(** Invariants of the code *)

Definition inv_length (n:int) (M:list bool) := 
  n = length M.

Definition inv_outer (n iv : int) (M:list bool) := 
  (forall k, 0 <= k < n -> M[k] = isTrue (prime_candidate iv k)).

Definition inv_inner (n iv rv : int) (M:list bool) := 
  (forall k, 0 <= k < n -> M[k] = isTrue (prime_candidate iv k 
                                           /\ uncrossed iv iv rv k)).


(********************************************************************)
(** Automation *)

Ltac auto_tilde ::= auto with maths.

Hint Extern 1 (index _ _) => skip.
Hint Extern 1 (inv_length _ _) => hnf; rew_array.



(********************************************************************)
(** Lemmas on the invariants *)

(** Divides *)

Lemma divides_not_elim : forall d k,
  ~ divides k d -> 
  forall m, m * d <> k.
Proof using. introv M E. applys M. exists* m. Qed.

Lemma divides_le : forall k d,
  k > 0 -> 
  d >= 0 ->
  divides k d -> 
  d <= k.
Proof using.
  introv Hk Hd (m&E). subst k. asserts: (m >= 1). skip. skip. 
Qed.

(** Prime candidates *)

Lemma prime_candidate_neg_elim : forall i k,
  ~ prime_candidate i k -> 
  k > 1 ->
  exists d, 1 < d < i /\ d < k /\ divides k d.
Proof using.
  introv N Hk. unfold prime_candidate in N. rew_logic in N. 
  destruct N as [N|(d'&N)]. math. (* bug rew_logic in N.*)
  do 2 rewrite not_impl in N. rewrite not_not in N. eauto.
Qed.

Lemma prime_candidate_le : forall i1 i2 k,
  prime_candidate i1 k -> 
  i2 <= i1 ->
  prime_candidate i2 k.
Proof using. introv (Hk&Hu) Hi. split~. Qed.
 
Lemma prime_candidate_self_then_prime : forall p, 
  p > 1 -> prime_candidate p p -> prime p.
Proof using.
  introv Pp (Hp&Hd). split~. introv Pd Dd.
  absurds. forwards~: divides_le Dd. false~ Hd d. 
Qed.
  
Lemma prime_then_prime_candidate : forall n p, 
  prime p -> prime_candidate n p.
Proof using.
  introv (Bp&Hp). split~. introv Bd1 Bd2 Hd. forwards~ [H|H]: Hp Hd; math.
Qed.

Lemma prime_candidate_2 : forall k,
  prime_candidate 2 k <-> k > 1.
Proof using.
  iff (Hk&M) M. 
  auto. 
  split~. introv Hd Hk Hm. math.
Qed.

Lemma prime_candidate_next : forall i k,
  i > 1 -> 
  ~ prime_candidate i i ->
  prime_candidate i k <-> prime_candidate (i+1) k.
Proof using.
  introv Hi N. lets~ (d'&Hd'&_&(m'&Hm')): (prime_candidate_neg_elim (rm N)).
  iff (Pk&Hk) M.
  { split~. intros d Hd Pd. tests: (d <> i).
      applys~ Hk.
      introv (m&Hm). applys~ Hk d'.
        exists (m * m'). subst k i. ring. }
  { applys~ prime_candidate_le M. }  
Qed.

(** Uncrossed *)

Lemma uncrossed_neg_elim : forall i a b k,
  ~ uncrossed i a b k -> 
  exists d, a <= d < b /\ k = d * i.
Proof using.
  introv N. unfolds uncrossed. rew_logic in N.
  destruct N as (d&N). rewrite not_impl in N.
  rew_logic in N. autos*.
Qed.

Lemma uncrossed_le : forall i a b1 b2 k,
  uncrossed i a b1 k -> 
  b2 <= b1 ->
  uncrossed i a b2 k.
Proof using. introv HU HL Hd. applys~ HU. Qed.

Lemma uncrossed_start : forall i a k,
  uncrossed i a a k.
Proof using. introv Hr. math. Qed.

Lemma uncrossed_neg : forall r i a k,
  k = r * i ->
  r >= a ->
  ~ uncrossed i a (r+1) k.
Proof using. introv E S N. applys~ N r. Qed.

Lemma uncrossed_next_not_multiple : forall r i a k,
  k <> r * i -> 
  (uncrossed i a r k <-> uncrossed i a (r+1) k).
Proof using.
  introv N. iff M.
  intros d Hd. tests: (d<>r). applys~ M. auto.
  applys~ uncrossed_le M.
Qed.

(** Inner loop *)

Lemma inner_loop_end : forall n r i a k,
  k < n ->
  r * i >= n -> 
  1 < a ->
  a <= i ->
  i > 1 ->
      (prime_candidate i k /\ uncrossed i a r k) 
  <-> (prime_candidate (i+1) k).
Proof using.
  introv Hkn Hr Ha Hai Hi. iff ((Hk&Hp)&Hc) M.
  { split~. introv Hdi Hdk. tests: (d <> i).
    applys~ Hp.
    introv (m&Hm). tests: (i <= m).
      false~ (Hc m). split~.
        math M: (k < r * i). subst k. skip. (* divide by i in M *) 
      false (Hp m).
        split~. skip. (* k = m*i  et k > 1 et i > 1 *)
        math.
        exists i. subst k. ring. }
  { split.
    applys~ prime_candidate_le M.
    destruct M as (Hp&M). intros d Hk N. applys~ M i.
      math Hd: (d>1). skip. (* cause k = d * i  and d > 1 and k > 1 *)
      exists~ d. }
Qed.

(** Outer loop *)

Lemma outer_loop_end : forall n k,
  0 <= k < n ->
  (prime_candidate n k <-> prime k).
Proof using.
  introv Hk. tests C: (k > 1).
  { iff M.
      applys~ prime_candidate_self_then_prime.  
       applys~ prime_candidate_le M.
      applys~ prime_then_prime_candidate. }
  { iff (Pk&_); math. }
Qed.


(********************************************************************)
(** Verification very naive version *)


Definition inv_inner' (n iv rv : int) (M:list bool) := 
  (forall k, 0 <= k < n -> M[k] = isTrue (prime_candidate iv k 
                                           /\ uncrossed iv 2 rv k)).


Lemma sieve_spec : returns_array_of_primes sieve_very_naive.
Proof using.
  unfolds. introv Hn. xcf.
  xapp~. intros M HM. xapps~. xapps~.
  xapps. xseq.
  applys (while_loop_cf_inv_partial (fun b =>
    Hexists iv, Hexists (M:list bool),
    \[b = isTrue (iv < n)] \* 
    \[iv > 1 /\ inv_length n M /\ inv_outer n iv M] \*
    t ~> Array M \* i ~~> iv)).
  { (* outer loop init *) 
    esplit. xsimpl; [|eauto]. split~.
    subst M. split~.
      introv Hk. rewrite (prop_ext (prime_candidate_2 k)).
       do 2 rewrite~ @read_update_case. rew_array~.
       repeat case_if; xclean. math. math. math. }
  { (* outer loop conditional *) 
    clears M. intros b. xpull ;=> iv M _ HI. xapps. xrets.
    auto. rew_refl*. }
  { (* outer loop body *) 
    clears M. xpull. intros iv M  Ni (Hiv&HL&HI). xapps. xseq (
      Hexists (M:list bool), \[inv_length n M /\ inv_outer n (iv+1) M]
                             \* t ~> Array M \* i ~~> iv).
    { (* while body *) 
      applys (while_loop_cf_inv_partial_gc (fun b =>
        Hexists rv, Hexists (M:list bool),
        \[b = isTrue (rv * iv < n)] \*
        \[inv_length n M /\ inv_inner' n iv rv M /\ rv > 1]
         \* t ~> Array M \* i ~~> iv \* r ~~> rv)).
       { (* inner loop init *) 
          esplit. hsimpl*. splits~. introv Hk. rewrite~ HI. xclean. 
          lets~ G: (@uncrossed_start iv 2 k).
          rewrite (prop_eq_True_back G). rew_logic*.  }
       { (* inner loop conditional *) 
         intros b. clears M. xpull ;=> rv M _ (HL&HI&HS). xapps. xapps. xret.
         hsimpl. splits~. rew_refl*. } 
       { (* inner loop body *)
         clears M. xpull ;=> rv M HR (HL&HI&HS). xapps. xapps~. xapps~. xapps~. 
         hsimpl; [|eauto]. splits~. introv Hk. rewrite~ @read_update_case. rewrite~ HI.
         case_If as C; xclean. 
         { rew_logic. right. applys~ uncrossed_neg. }
         { forwards~ G: uncrossed_next_not_multiple rv iv 2 k.
           rewrite* (prop_ext G). } }
       { (* inner loop end *) 
         clears M. hpull ;=> rv M HR (HL&HI&HS). xsimpl. split~.
         introv Hk. rewrite~ HI. xclean. applys~ inner_loop_end n. } }
   clears M. intros M (HL&HI). xapps. xsimpl; [|eauto]. splits~. }
  { (* outer loop end *)
    xok. }
  { (* return *) 
    xpull. clears M. intros iv M HN (Hiv&HL&HI). xret. hsimpl. split~.
    clears i. intros k Hk. rewrite~ HI. xclean.
    clears HI. applys~ outer_loop_end. }
Qed.



(********************************************************************)
(** Verification naive version *)

Lemma sieve_spec' : returns_array_of_primes sieve_naive.
Proof using.
  unfolds. introv Hn. xcf.
  xapp~. intros M HM. xapps~. xapps~.
  xapps. xseq.
  applys (while_loop_cf_inv_partial (fun b =>
    Hexists iv, Hexists (M:list bool),
    \[b = isTrue (iv < n)] \* 
    \[iv > 1 /\ inv_length n M /\ inv_outer n iv M] \*
    t ~> Array M \* i ~~> iv)).
  { (* outer loop init *) 
    esplit. xsimpl; [|eauto]. split~.
    subst M. split~.
      introv Hk. rewrite (prop_ext (prime_candidate_2 k)).
       do 2 rewrite~ @read_update_case. rew_array~.
       repeat case_if; xclean. math. math. math. }
  { (* outer loop conditional *) 
    clears M. intros b. xpull ;=> iv M _ HI. xapps. xrets.
    auto. rew_refl*. }
  { (* outer loop body *) 
    clears M. xpull ;=> iv M Ni (Hiv&HL&HI). xapps. xapps. xseq (
      Hexists (M:list bool), \[inv_length n M /\ inv_outer n (iv+1) M]
                             \* t ~> Array M \* i ~~> iv).
    { (* while body *) 
      applys (while_loop_cf_inv_partial_gc (fun b =>
        Hexists rv, Hexists (M:list bool),
        \[b = isTrue (rv * iv < n)] \*
        \[inv_length n M /\ inv_inner n iv rv M /\ iv <= rv]
         \* t ~> Array M \* i ~~> iv \* r ~~> rv)).
       { (* inner loop init *) 
          esplit. hsimpl*. splits~. introv Hk. rewrite~ HI. xclean. 
          rewrite (prop_eq_True_back (@uncrossed_start iv iv k)). rew_logic*.  }
       { (* inner loop conditional *) 
         intros b. clears M. xpull ;=> rv M _ (HL&HI&HS). xapps. xapps. xret.
         hsimpl. splits~. rew_refl*. } 
       { (* inner loop body *)
         clears M. xpull ;=> rv M HR (HL&HI&HS). xapps. xapps~. xapps~. xapps~. 
         hsimpl; [|eauto]. splits~. introv Hk. rewrite~ @read_update_case. rewrite~ HI.
         case_If as C; xclean. 
         { rew_logic. right. applys~ uncrossed_neg. }
         { forwards~ G: uncrossed_next_not_multiple rv iv iv k.
           rewrite* (prop_ext G). } }
       { (* inner loop end *) 
         clears M. hpull ;=> rv M HR (HL&HI&HS). xsimpl. split~.
         introv Hk. rewrite~ HI. xclean. applys~ inner_loop_end n. } }
   clears M. intros M (HL&HI). xapps. xsimpl; [|eauto]. splits~. }
  { (* outer loop end *)
    xok. }
  { (* return *) 
    xpull. clears M. intros iv M HN (Hiv&HL&HI). xret. hsimpl. split~.
    clears i. intros k Hk. rewrite~ HI. xclean.
    clears HI. applys~ outer_loop_end. }
Qed.



(********************************************************************)
(** Verification *)


Lemma sieve_spec'' : returns_array_of_primes sieve.
Proof using.
  unfolds. introv Hn. xcf.
  xapp~. intros M HM. xapps~. xapps~.
  xapps. xseq.
  applys (while_loop_cf_inv_partial (fun b =>
    Hexists iv, Hexists (M:list bool),
    \[b = isTrue (iv < n)] \* 
    \[iv > 1 /\ inv_length n M /\ inv_outer n iv M] \*
    t ~> Array M \* i ~~> iv)).
  { (* outer loop init *) 
    esplit. xsimpl; [|eauto]. split~.
    subst M. split~.
      introv Hk. rewrite (prop_ext (prime_candidate_2 k)).
       do 2 rewrite~ @read_update_case. rew_array~.
       repeat case_if; xclean. math. math. math. }
  { (* outer loop conditional *) 
    clears M. intros b. xpull ;=> iv M _ HI. xapps. xrets.
    auto. rew_refl*. }
  { (* outer loop body *) 
    clears M. xpull ;=> iv M Ni (Hiv&HL&HI). xapps. xapps~. xseq (
      Hexists (M:list bool), \[inv_length n M /\ inv_outer n (iv+1) M]
                             \* t ~> Array M \* i ~~> iv).
    xif.
    { (* then branch *) 
      xapps. xapps. 
      applys (while_loop_cf_inv_partial_gc (fun b =>
        Hexists rv, Hexists (M:list bool),
        \[b = isTrue (rv * iv < n)] \*
        \[inv_length n M /\ inv_inner n iv rv M /\ iv <= rv]
         \* t ~> Array M \* i ~~> iv \* r ~~> rv)).
       { (* inner loop init *) 
          esplit. hsimpl*. splits~. introv Hk. rewrite~ HI. xclean. 
          rewrite (prop_eq_True_back (@uncrossed_start iv iv k)). rew_logic*.  }
       { (* inner loop conditional *) 
         intros b. clears M. xpull ;=> rv M _ (HL&HI&HS). xapps. xapps. xret.
         hsimpl. splits~. rew_refl*. } 
       { (* inner loop body *)
         clears M. xpull ;=> rv M HR (HL&HI&HS). xapps. xapps~. xapps~. xapps~. 
         hsimpl; [|eauto]. splits~. introv Hk. rewrite~ @read_update_case. rewrite~ HI.
         case_If as C; xclean. 
         { rew_logic. right. applys~ uncrossed_neg. }
         { forwards~ G: uncrossed_next_not_multiple rv iv iv k.
           rewrite* (prop_ext G). } }
       { (* inner loop end *) 
         clears M. hpull ;=> rv M HR (HL&HI&HS). xsimpl. split~.
         introv Hk. rewrite~ HI. xclean. applys~ inner_loop_end n. } }
    { (* else branch *)
      xret. xsimpl. split~. forwards~ MI: HI iv. rewrite MI in C.
     (* bug xclean. *) introv Hk. xclean. rewrite~ HI. xclean.  
     applys~ prime_candidate_next. }
   clears M. intros M (HL&HI). xapps. xsimpl; [|eauto]. splits~. }
  { (* outer loop end *)
    xok. }
  { (* return *) 
    xpull. clears M. intros iv M HN (Hiv&HL&HI). xret. hsimpl. split~.
    clears i. intros k Hk. rewrite~ HI. xclean.
    clears HI. applys~ outer_loop_end. }
Qed.




















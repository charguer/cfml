

(* conclusion <-> reachables G (set_of_list r) (blacks C) *)
Evolution:
 (* 1 <-> (blacks C) \c (blacks C')
    2 <-> (grays C) = (grays C') *)

Inv
   (* 2 <-> R \c (blacks C)
      4 <-> reachables G R (blacks C). *)


No black to white false
(* 2 <-> grays C = \{}
   5 <-> i \in blacks C
   6 <-> j \notin white C <-> j \in (blacks C \u grays C) *)

(* NOT USED
Lemma all_black_in_evolution : forall C C' E,
  all_black_in E C ->
  evolution C C' ->
  all_black_in E C'.
Proof using. =>> N (H1&H2) i Hi. auto. Qed. (* trivial *)
*)

(* NOT NEEDED
Lemma evolution_trans' : forall X, trans (evolution' X).
Proof using. (* trivial *)
  intros X. =>> (F1&G1) (F2&G2). unfolds evolution'. splits.
  autos*.
  intros. iff M.
  { rewrite~ <- G2. destruct~ M. left. rewrite~ <- G1. }
  { rewrite~ <- G2 in M. destruct~ M as [M|M]. rewrite~ <- G1 in M. }
Qed.
*)

Lemma evolution_trans' : forall C2 X C1 C3,
  evolution' X C1 C2 ->
  evolution C2 C3 ->
  evolution' X C1 C3.
Proof using. (* trivial *)
  introv. =>> (F1&G1) (F2&G2). splits.
  { autos*. }
  { intros. rewrite~ <- G2. }
Qed.

(* NOT USED
Lemma no_white_in_evolution' : forall C C' X E,
  no_white_in E C ->
  evolution' X C C' ->
  no_white_in E C'.
Proof using.  (* trivial *)
  =>> N (H1&H2) i Hi. cases (C[i]) as Ci.
  { false* N. }
  { forwards~ (H2a&_): H2 i. rewrite~ H2a. auto_false. }
  rewrite~ H1. auto_false.
Qed.
*)
Definition evolution' (X:set int) C C' :=
     (forall i, index C i -> C[i] = Black -> C'[i] = Black)
  /\ (forall i, index C i -> ((C[i] = Gray \/ i \in X) <-> C'[i] = Gray)).
(* EASIER: consider evolution wrt C[i:=Gray] *)



(*
Definition reachables (G:graph) (E F:set int) :=
  forall j, j \in F -> exists i, i \in E /\ reachable G i j.
*)
Lemma reachables_monotone : forall G E1 E2 F1 F2,
  reachables G E1 F1 ->
  E1 \c E2 ->
  F2 \c F1 ->
  reachables G E2 F2.
Proof using.
  introv R HE HF. rewrite incl_in_eq in HE,HF. skip.
Qed.

Lemma reachables_trans : forall G E1 E2 E3,
  reachables G E1 E2 ->
  reachables G E2 E3 ->
  reachables G E1 E3.
Proof using.
  skip.
Qed.

Definition blacks C :=
  set_st (fun i:int => index C i /\ C[i] = Black).
  (* \set{ i:int | index C i /\ C[i] = Black }. *)

Definition grays C :=
  set_st (fun i:int => index C i /\ C[i] = Gray).




Lemma iter_nodes_spec : forall (I:set int->hprop) (G:graph) g f,
  (forall i N, i \in nodes G -> i \notin N ->
     (app f [i] (I N) (# I (\{i}\u N)))) ->
  app iter_nodes [f g]
    PRE (g ~> GraphAdj G \* I \{})
    POST (# g ~> GraphAdj G \* I (nodes G)).
Proof.
  (* -- TODO -- *)
Admitted.

Hint Extern 1 (RegisterSpec iter_nodes) => Provide iter_nodes_spec.



(*
let iter_nodes (f:int->unit) (g:t) =
  for i = 0 to (nb_nodes g)-1 do
    f i
  done
*)

let dfs_main g r =
   let n = Graph.nb_nodes g in
   let c = Array.make n White in
   List.iter (fun i -> c.(i) <- Black)
   Graph.iter_nodes (fun i ->
     if c.(i) = White then
       dfs_from g c i) g



(*************************************************************************)
(** Graph representation by adjacency lists, with info on edges *)

module GraphAdj = struct

type 'a t = ((int*'a) list) array

let nb_nodes (g:'a t) =
  Array.length g

let iter_edges_of (g:'a t) (i:int) (f:int->'a->unit) =
  List.iter (fun (j,w) -> f j w) g.(i)

let iter_edges_target_of (g:'a t) (i:int) (f:int->unit) =
  List.iter (fun (j,_) -> f j) g.(i)

let fold_edges_target_of (g:'a t) (i:int) (a:'b) (f:'b->int->'b) =
  List.fold_left (fun acc (j,_) -> f acc j) a g.(i)

let iter_nodes (g:'a t) (f:int->unit) =
   for i = 0 to (nb_nodes g)-1 do
      f i
   done

let fold_nodes (g:'a t) (a:'b) (f:'b->int->'b) =
   let r = ref a in
   for i = 0 to (nb_nodes g)-1 do
      r := f !r i
   done

end




(*
(*************************************************************************)
(*************************************************************************)
(*************************************************************************)


(*************************************************************************)
(** GraphAdj representation by adjacency matrix --LATER *)

module GraphMat = struct

type 'a t = ('a array) array

let nb_nodes (g:'a t) =
  Array.length g

let get_edge (g:'a t) i j =
   g.(i).(j)

let set_edge (g:'a t) i j w =
   g.(i).(j) <- w

let has_edge (g:bool t) i j =
   get_edge g i j

let add_edge (g:bool t) i j =
   set_edge g i j true

end


(*************************************************************************)
(** Reachability by recursive DFS, three-colored *)

type color = White | Gray | Black

let reachable_recursive g a b =
   let n = GraphAdj.nb_nodes g in
   let c = Array.make n White in
   let rec visit i =
      c.(i) <- Gray;
      GraphAdj.iter_edges_target_of g i (fun j ->
         if c.(j) = White
            then visit j);
      c.(i) <- Black;
      in
   visit a;
   c.(b)


(*************************************************************************)
(** Reachability by imperative DFS, two-colored *)

let reachable_imperative g a b =
   let n = GraphAdj.nb_nodes g in
   let c = Array.make n false in
   let s = Stack.create() in
   c.(a) <- true;
   Stack.push a s;
   while not (Stack.is_empty s) do
      let i = Stack.pop s in
      GraphAdj.iter_edges_target_of g i (fun j ->
         if not c.(j) then begin
           c.(i) <- true;
           Stack.push i s;
         end);
   done;
   c.(b)


(*************************************************************************)
(** Cycle detection by recursive DFS, three-colored *)

(** Note: for simlicity, the current implementation does not exit
    abruptly when detecting a cycle. *)

let cycle_detection g s e =
   let n = GraphAdj.nb_nodes g in
   let c = Array.make n White in
   let r = ref false in
   let rec visit i =
      c.(i) <- Gray;
      GraphAdj.iter_edges_target_of g i (fun j ->
         if c.(j) = White
            then visit j
         else if c.(j) = Gray
            then r := true);
      c.(i) <- Black;
      in
   GraphAdj.iter_nodes g (fun i ->
      if c.(i) = White then visit i);
   !r


(*************************************************************************)
(** Topological sort, without cycle detection *)

(* Note: it would be straightforward to add cycle detection. *)

let topological_sort g s e =
   let n = GraphAdj.nb_nodes g in
   let neverseen = -1 in
   let processed = -2 in
   let c = Array.make n neverseen in
   let k = ref (n-1) in
   let rec visit i =
      c.(i) <- processed;
      GraphAdj.iter_edges_target_of g i (fun j ->
         if c.(j) = neverseen
            then visit j);
      c.(i) <- !k;
      decr k;
      in
   GraphAdj.iter_nodes g (fun i ->
      if c.(i) = neverseen then visit i);
   c


(*************************************************************************)
(** Connected components by recursive DFS, two-colored *)

let connected_recursive g =
   let n = GraphAdj.nb_nodes g in
   let neverseen = -1 in
   let c = Array.make n neverseen in
   let k = ref 0 in
   let rec visit i =
      c.(i) <- !k;
      GraphAdj.iter_edges_target_of g i (fun j ->
         if c.(j) = neverseen
            then visit j)
      in
   GraphAdj.iter_nodes g (fun i ->
      if c.(i) = neverseen then begin
         visit i;
         incr k
      end);
   c

(*************************************************************************)
(** Spanning forest by recursive DFS *)

(** Note: the inductive type is defined in a funny way just to please Coq. *)

type tree = Tree of int * tree list
type forest = tree list

let spanning_forest g =
   let n = GraphAdj.nb_nodes g in
   let c = Array.make n false in
   let rec build_tree i =
      c.(i) <- true;
      let ts = GraphAdj.fold_edges_target_of g i [] harvest in
      Tree (i,ts)
   and harvest acc i =
      if c.(i) then acc else (build_tree i)::acc
   in
   GraphAdj.fold_nodes g [] harvest


(*************************************************************************)
(** Connected components by imperative DFS *)

let connected_imperative g =
   let n = GraphAdj.nb_nodes g in
   let neverseen = -1 in
   let c = Array.make n neverseen in
   let k = ref 0 in
   let s = Stack.create() in
   let find i =
      c.(i) <- !k;
      Stack.push i s
      in
   GraphAdj.iter_nodes g (fun i ->
      if c.(i) = neverseen then begin
         find i;
         while not (Stack.is_empty s) do
            let i = Stack.pop s in
            GraphAdj.iter_edges_target_of g i (fun j ->
               if c.(j) = neverseen
                  then find j)
         done;
         incr k;
      end);
   c


(*************************************************************************)
(** Connected components by warshall-floyd *)

(** Note: implemented by side-effects on the adjacency matrix *)

let connected_warshall_floyd g =
   let n = GraphMat.nb_nodes g in
   for k = 0 to n-1 do
      for i = 0 to n-1 do
         for j = 0 to n-1 do
            if    GraphMat.has_edge g i k
               && GraphMat.has_edge g k j
               then GraphMat.add_edge g i j
         done;
      done;
   done

(* TODO:
   for each node
      if node not marked
         nbcompo++
         foreach neighbor
            mark it

*)


(*************************************************************************)
(** Strongly connected components, Kosaraju's algorithm (2 DFS) *)

(*************************************************************************)
(** Strongly connected components, Tarjan's algorithm *)

(*************************************************************************)
(** Strongly connected components, path-based algorithm *)

(*************************************************************************)
(** Edge cut *)

(*************************************************************************)
(** Vertex cut *)

(*************************************************************************)
(** Eulerian path *)

*)















Set Implicit Arguments.
Require Import CFLib DFS_ml.
Open Scope comp_scope.
Require Import LibArray LibGraph LibMultiset.

Ltac auto_tilde ::= auto with maths.

(********************************************************************)
(********************************************************************)
(* TEMPORARY *)

(********************************************************************)
(* ** Should be generated *)

Instance color_inhab : Inhab color.
Proof. typeclass. Qed.


(********************************************************************)
(* ** TLC List *)

Lemma Forall_weaken : forall A (l:list A) (P P':A->Prop),
  Forall P l -> (forall x, P x -> P' x) -> Forall P' l.
Admitted.


(********************************************************************)
(* ** TLC Bag *)

(* todo: define img as a typeclass, like dom *)
Parameter img : forall A B, map A B -> set B.


(********************************************************************)
(* ** TLC Array *)

Instance array_inhab : forall A, Inhab (array A).
Admitted.

(* todo: rename to keys ; defined as an alias for array dom *)
Parameter array_keys : forall A, array A -> set int.

(* todo: rename to "to_map" *)
Parameter array_to_map : forall A, array A -> map int A.

(********************************************************************)
(* ** TLC Graph *)

Inductive is_path A (g:graph A) : int -> int -> path A -> Prop :=
  | is_path_nil : forall x,
      x \in nodes g ->
      is_path g x x nil
  | is_path_cons : forall x y z w p,
     has_edge g x y w ->
     is_path g y z p ->
     is_path g x z ((x,y,w)::p).


(********************************************************************)
(* ** Stdlib Stack *)

Parameter StackOf : forall a A (T:htype A a) (L:list A) (l:loc), hprop.

Notation "'Stack'" := (StackOf Id).

Parameter ml_stack_create_spec : forall a,
  Spec ml_stack_create (i:unit) |R>>
     R \[] (~> Stack (@nil a)).

Hint Extern 1 (RegisterSpec ml_stack_create) => Provide ml_stack_create_spec.

Parameter ml_stack_is_empty_spec : forall a,
  Spec ml_stack_is_empty (l:loc) |R>>
     forall (L:list a),
     keep R (l ~> Stack L) (fun b => \[b = bool_of (L = nil)]).

Hint Extern 1 (RegisterSpec ml_stack_is_empty) => Provide ml_stack_is_empty_spec.

Parameter ml_stack_push_spec : forall a,
  Spec ml_stack_push (X:a) (l:loc) |R>>
     forall (L:list a),
     R (l ~> Stack L) (# l ~> Stack (X::L)).

Hint Extern 1 (RegisterSpec ml_stack_push_spec) => Provide ml_stack_push_spec.

Parameter ml_stack_pop_spec : forall a,
  Spec ml_stack_pop (l:loc) |R>>
     forall (L:list a), L <> nil ->
     R (l ~> Stack L) (fun X => Hexists L', \[L = X::L'] \* l ~> Stack L').

Hint Extern 1 (RegisterSpec ml_stack_pop) => Provide ml_stack_pop_spec.

(********************************************************************)
(********************************************************************)
(********************************************************************)
(* ** Representation predicate for unweighted graphs
      by adjacency lists *)

(** [nodes_index G n] holds if the nodes in [G] are indexed from
    [0] inclusive to [n] exclusive. *)

Definition nodes_index A (G:graph A) (n:int) :=
  forall i, i \in nodes G = index n i.

(** [g ~> GraphAdjList G] asserts that at pointer [g] is an imperative
    array of pure lists that represents the adjacency lists of [G]. *)

Definition GraphAdjList A (G:graph A) (g:loc) :=
  Hexists N, g ~> Array N
   \* \[nodes_index G (LibArray.length N)
   /\ forall i j w, i \in nodes G ->
        Mem (j,w) (N[i]) = has_edge G i j w].



(********************************************************************)
(* ** Generic hints *)

(** Hints for lists *)

Hint Constructors Forall.
Hint Resolve Forall_last.

(** Hints for indices *)

Lemma graph_adj_index : forall B (G:graph B) n m x,
  nodes_index G n -> x \in nodes G -> n = m -> index m x.
Proof. introv N Nx L. subst. rewrite~ <- N. Qed.

Hint Resolve @index_array_length_eq @index_make @index_update.
Hint Immediate has_edge_in_nodes_l has_edge_in_nodes_r.
Hint Extern 1 (nodes_index _ _) => congruence.
Hint Extern 1 (index ?n ?x) =>
  eapply graph_adj_index;
  [ try eassumption
  | instantiate; try eassumption
  | instantiate; try congruence ].



(********************************************************************)
(** * Specifications of operatiosn on graphs *)

Import MLGraphAdj.

Ltac hdata_simpl_step ::=
  match goal with |- context C [ ?l ~> ?S ] =>
    match S with (fun _ => _) =>
      rewrite (hdata_fun' l)
    end
  end.


Lemma nb_nodes_spec : forall A,
  Spec nb_nodes g |R>>
    forall (G:graph A),
    keep R (g ~> GraphAdjList G) (fun n => \[nodes_index G n]).

Proof.
  xcf. instantiate (1:=A). (* todo: fix instantiation *)
  intros. unfold GraphAdjList. hdata_simpl.
  xextract as N [GI GN]. xapp. hsimpl~.
Admitted. (*faster*)

Hint Extern 1 (RegisterSpec nb_nodes) => Provide nb_nodes_spec.


Parameter out_edges : forall A, graph A -> int -> set (int*A).
Parameter out_edges_target : forall A, graph A -> int -> set int.

Parameter out_edges_target_has_edge : forall (G:graph unit) i j,
  (j \in out_edges_target G i) = (has_edge G i j tt).

(* LATER: use multisets instead of sets to ensure uniqueness
   of the processing of each of the edges *)


Parameter ml_list_iter_spec' : forall a,
  Spec ml_list_iter f l |R>> forall (I:list a->hprop),
    (forall x t, (App f x;) (I t) (# I (t&x))) ->
    R (I nil) (# I l).

Lemma iter_edges_target_of_spec :
  Spec iter_edges_target_of g i f |R>>
    forall (G:graph unit) (I:set int->hprop), i \in nodes G ->
    (forall j js, has_edge G i j tt -> j \notin js ->
      (App f j;) (I js) (# I (\{j} \u js))) ->
    R (g ~> GraphAdjList G \* I \{})
      (# g ~> GraphAdjList G \* I (out_edges_target G i)).
Proof.
(*---TODO---
  xcf. introv Gi Sf. (* todo: fix instantiation *)
  unfold GraphAdjList. hdata_simpl. xextract as N [GI GN].
  xfun_mg. xapps*.
  xapp_spec ml_list_iter_spec' (fun l:list(int*unit) => I (List.map fst l)).
  intros [j w] t. simpls. eapply S_f0.
  intros_all. applys H0. applys H. (* todo: xbody/xisspec *)
  clear j w. intros [j w]. xcase.
  --- TODO ---*)
Admitted.

Hint Extern 1 (RegisterSpec iter_edges_target_of) => Provide iter_edges_target_of_spec.

(* Another similar one talking about remaining edges *)

Lemma iter_edges_target_of_spec' :
  Spec iter_edges_target_of g i f |R>>
    forall (G:graph unit) (I:set int->hprop), i \in nodes G ->
    (forall j js, has_edge G i j tt -> j \notin js ->
      (App f j;) (I (\{j} \u js)) (# I js)) ->
    R (g ~> GraphAdjList G \* I (out_edges_target G i))
      (# g ~> GraphAdjList G \* I \{}).
Admitted.





Lemma iter_nodes_spec :
  Spec iter_nodes g f |R>>
    forall (G:graph unit) (I:set int->hprop),
    (forall i N, i \in nodes G -> i \notin N ->
      (App f i;) (I N) (# I (\{i}\u N))) ->
    R (g ~> GraphAdjList G \* I \{})
      (# g ~> GraphAdjList G \* I (nodes G)).
Proof.
  (* -- TODO -- *)
Admitted.

Hint Extern 1 (RegisterSpec iter_nodes) => Provide iter_nodes_spec.



(********************************************************************)
(** * Generic definitions *)

Implicit Types G : graph unit.

Definition undirected G :=
  forall i j, has_edge G i j tt -> has_edge G j i tt.

Definition reachable G i j :=
  exists p, is_path G i j p.

Definition cyclic G :=
  exists p i, is_path G i i p.

Definition topological_order G L :=
     dom L = nodes G
  /\ img L = range 0 (card (nodes G))
  /\ forall i j, reachable G i j -> L[i] < L[j].

Definition topological_order_one_step G L :=
     dom L = nodes G
  /\ img L = range 0 (card (nodes G))
  /\ forall i j, has_edge G i j tt -> L[i] < L[j].

Definition connected_components G L :=
  exists k,
     dom L = nodes G
  /\ img L = range 0 k
  /\ forall i j, reachable G i j <-> L[i] = L[j].

Definition strongly_connected_components G L :=
  exists k,
     dom L = nodes G
  /\ img L = range 0 k
  /\ forall i j, (reachable G i j /\ reachable G j i) <-> L[i] = L[j].






(********************************************************************)
(** * Reachability by recursive DFS, three-colored
 -- DEPRECATED

Definition reachable_iff_black G s C :=
  forall i, i \in nodes G -> (C[i] = Black <-> reachable G s i).

Definition not_white_reachable G s C :=
  forall i, i \in nodes G -> C[i] <> White -> reachable G s i.

Definition black_no_white_neighbour G C :=
  forall i j, i \in nodes G -> C[i] = Black ->
  has_edge G i j tt -> C[j] <> White.

Record inv G n s C := {
  inv_length_C :
    length C = n;
  inv_not_white_reachable : not_white_reachable G s C;
  inv_black_no_white_neighbour : black_no_white_neighbour G C }.

Definition hinv g c (N:array (list int)) G n s C :=
  g ~> Array N \* c ~> Array C \* [inv G n s C].

Definition same_gray (G:graph unit) C C' :=
  forall i, i \in nodes G -> (C[i] = Gray <-> C'[i] = Gray).

Definition no_more_white (G:graph unit) C C' :=
  forall i, i \in nodes G -> C[i] <> White <-> C'[i] <> White.

Definition fewer_white :=
  binary_map (count (= White)) (downto 0).


Lemma reachable_rec_spec : Spec reachable_rec g s |R>>
   forall G, s \in nodes G ->
  R (g ~> GraphAdjList G)
    (fun (c:loc) => Hexists (C:array color),
      g ~> GraphAdjList G \* c ~> Array C \*
      [reachable_iff_black G s C]).
Proof.
  xcf. introv Gs. unfold GraphAdjList at 1. hdata_simpl. xextract as N Neg Adj.
  xapp. intros Gn. xapps.
  xfun_induction_heap_nointro (fun f => Spec f i |R>> forall C,
    i \in nodes G -> C[i] = White ->
    R (hinv g c N G n s C)
      (# Hexists C', hinv g c N G n s C' \*
         [same_gray G C C' /\ no_more_white G C' C /\ C'[i] = Black]))
    fewer_white.
   unfold hinv. intros i C0 IH Gi Ci. hide IH. xextract as I0.
   lets Cn0: inv_length_C I0.
   xapp*. xfun f. simpls. xapps*.
   xapp_spec ml_list_iter_spec' (fun L =>
     Hexists C, hinv g c N G n s C
      \* [Forall (fun j => j \in nodes G /\ C[j] <> White) L /\ no_more_white G C C0]).
     Focus 1.
     intros j L' Lx. unfold hinv. xextract as C I [HL' F]. lets [Cn I1 I2]: I.
     xapp_body. intro. intro_subst. rewrite* Adj in Lx.
     xapps*. xif.
       xapp*.
         skip. (* decrease *)
         hextract as C'. intros I' (SG&SW&C'j). lets [Cn' I1' I2']: I'.
          hsimpl~. splits*.
            applys* Forall_last.
              applys* Forall_weaken HL'. intros k (Gk&Ck).
               split~. applys* SW. split*. congruence.
               skip. (* transitivity of no_more_while on SW and F *)
       xret*.
  unfold hinv. hsimpl.
  skip. (* turning gray does not break inv *)
  splits.
    constructors.
    skip. (* ok *)
  skip.
  xapp* (make n White).
    skip. (* ok *)
    unfold hinv. hsimpl. skip. (* inv init *)
  introv (SW&?&?). xret. unfold hinv.
   unfold GraphAdjList at 1. hdata_simpl.
   hextract as [_ SW' SG']. hsimpl*.
   unfolds. unfolds in SW'.
   (* todo: utiliser same_gray pour montrer que <>white implique =black *)
  skip.
Admitted.


*)




(********************************************************************)
(** LATER: matrix *)

(** [g ~> GraphMatrix G] asserts that at pointer [g] is an imperative
    array of arrays that represents [G] as a matrix. *)

Definition GraphMatrix (G:graph unit) (g:loc) :=
  Hexists (N:array (array bool)), g ~> Array N
   \* \[exists n, n = LibArray.length N
   /\ (forall (x:int), x \in nodes G -> n = LibArray.length (N[x]))
   /\ (forall (x y:int), x \in nodes G -> y \in nodes G ->
          N[x][y] = isTrue (has_edge G x y tt))].





let reachable_imperative g a b =
   let n = GraphAdj.nb_nodes g in
   let c = Array.make n false in
   let s = Stack.create() in
   c.(a) <- true;
   Stack.push a s;
   while not (Stack.is_empty s) do
      let i = Stack.pop s in
      GraphAdj.iter_edges_target_of g i (fun j ->
         if not c.(j) then begin
           c.(i) <- true;
           Stack.push i s;
         end);
   done;
   c.(b)



(********************************************************************)
(** * Reachability by imperative DFS, two-colored *)

Set Implicit Arguments.
Require Import CFML.CFLib.
Require Import DFS_ml.
Require Import Stdlib.
Require Import TLC.LibListZ.
Require Import Array_proof.
Require Import List_proof.
Require Import DFS_proof.
Open Scope tag_scope.


(*************************************************************************)
(** Verification of a minimal stack *)

Module Import Stack_proof.
Import Stack_ml.

Definition Stack A (L:list A) (p:loc) :=
  p ~~> L.

Lemma affine_Stack : forall A p (L: list A),
  affine (p ~> Stack L).
Proof.
  intros. unfold Stack, hdata. affine.
Qed.

(* Hint Resolve affine_Stack : affine. *)
(* Transparency issues... FIXME? *)

Lemma create_spec : forall A,
  app create [tt]
     PRE \[]
     POST (fun p => p ~> Stack (@nil A)).
Proof using. xcf_go. Qed.

Hint Extern 1 (RegisterSpec create) => Provide create_spec.

Lemma is_empty_spec : forall A (L:list A) (p:loc),
  app is_empty [p]
     INV (p ~> Stack L)
     POST (fun b => \[b = isTrue (L = nil)]).
Proof using. xcf_go*. xpolymorphic_eq. (* todo: automate *) Qed.

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.

Lemma pop_spec : forall A (L:list A) (p:loc),
  L <> nil ->
  app pop [p]
     PRE (p ~> Stack L)
     POST (fun x => Hexists L', \[L = x::L'] \* p ~> Stack L').
Proof using. xcf_go*. Qed.

Hint Extern 1 (RegisterSpec pop) => Provide pop_spec.

Lemma push_spec : forall A (L:list A) x p,
  app push [x p]
     PRE (p ~> Stack L)
     POST (# p ~> Stack (x::L)).
Proof using. xcf_go*. Qed.

Hint Extern 1 (RegisterSpec push) => Provide push_spec.

End Stack_proof.


(*************************************************************************)
(** Proof of DFS with a stack *)

(* Note: [E] describes set of edges left to process in the loop *)

(* - [L] describes the contents of the stack
   - [a] describes the source
   - [n] describes the number of nodes in [G]
   - [E] describes a set of vertices: the neighbors of the currently processed vertex *)

Record inv (G:graph) (n:int) (a:int) (C:list bool) (L:list int) (E:set int) := {
  inv_nodes_index : nodes_index G n;
  inv_length_C : length C = n;
  inv_source_node : a \in nodes G;
  inv_source : C[a] = true;
  inv_stack : forall i, mem i L -> i \in nodes G /\ C[i] = true;
  inv_true_reachable : forall i, i \in nodes G ->
    C[i] = true -> reachable G a i;
  inv_true_edges : forall i j,
    C[i] = true -> has_edge G i j ->
    mem i L \/ C[j] = true \/ j \in E }.

Lemma inv_index : forall G n a C L E i,
  i \in nodes G ->
  inv G n a C L E ->
  index C i.
Proof. introv Hi I. destruct* I. Qed.

Hint Resolve inv_index.

Lemma inv_init : forall G n a C,
  nodes_index G n ->
  C = LibListZ.make n false ->
  a \in nodes G ->
  inv G n a (C[a:=true]) (a :: nil) \{}.
Proof.
  (* Ici il faudrait pousser une version spécialisée du lemme [mem_one_eq] (?),
     qui dit [mem i (a::nil) = (i = a)], à utiliser dans les branches.
     Ou alors, s'appuyer sur [rew_listx] qui devrait faire cette réécriture. *)
  introv Gn EC Ga. constructors~.
  { subst C. rew_array*. destruct* Gn. (* should be auto *) }
  { rew_array*. case_if~. }
  { introv H. inverts H as H.
    { splits~. rew_array*. case_if~.
      (* il faudrait introduire une tactique qui fait
         [rew_array; repeat case_if], mais avec la subtilité
         que ça ne fait [case_if] que sur le [if] introduit
         par les [read_update] sur les tableaux. On peut coder
         ça en utilisant une base de hint différentes que rew_array,
         et une marque identité qu'une variante de [case_if] pourrait
         reconnaitre spécifiquement. On peut, en option, avoir une
         variante qui effectue des [subst] sur les égalités générées
         par [case_if] *) }
    { inverts H. } }
  { introv Gi H.
    assert (i = a) as ->.
    { subst C. rew_array* in H. case_if~.
      (* idem, c'est un [rew_array;case_if] *) }
    applys~ reachable_self. }
  { introv Ci E. assert (i = a) as ->.
    { subst C. rew_array* in Ci. case_if~. }
    autos. }
Qed.

Lemma inv_step_push : forall G n a C L i j js,
  inv G n a C L (js \u \{j}) ->
  C[i] = true ->
  has_edge G i j ->
  inv G n a (C[j:=true]) (j :: L) js.
Proof.
  introv I Ci M. lets I': I. inverts I. constructors~.
  { rew_array~. }
  { rew_array*. case_if~. }
  { intros i' M'. rew_listx in M'. destruct M' as [-> | M'].
    - splits*. rew_array*. case_if~.
    - forwards~ (?&?): inv_stack0 i'. rew_array*. case_if~. }
  { intros k Hk Ck. rew_array* in Ck. case_if~. subst k.
    applys* reachable_trans_edge i. }
  { intros i' j' Ci' E. rew_array*. case_if~. tests~: (i' = j).
    forwards~ [H|[H|H]]: inv_true_edges0 i' j'.
    - rew_array* in Ci'. case_if~.
    - rew_set in H. branches; autos. }
Qed.

Lemma inv_step_skip : forall G n a C L j js,
  inv G n a C L (js \u \{j}) ->
  C[j] = true ->
  inv G n a C L js.
Proof.
  introv I Cj. inverts I. constructors; auto.
  { intros i' j' Ci' E.
    lets [M|[M|M]]: inv_true_edges0 Ci' E; [ eauto | eauto | ].
    rew_set in M. destruct~ M. subst*. (* set_in M devrait faire ça *) }
Qed.

Lemma inv_end : forall G n a C,
  inv G n a C nil \{} ->
  forall j, j \in nodes G ->
  (reachable G a j <-> C[j] = true).
Proof.
  introv I Gj. iff H.
  { destruct H as [p P]. lets PC: inv_source I. gen P PC.
   generalize a as i. intros i P. induction P.
     { auto. }
     { introv Cx. lets [M|[M|M]]: inv_true_edges I Cx H; rew_listx~ in M. } }
  { applys* inv_true_reachable. }
Qed.

Lemma inv_step_pop : forall G n a C i L,
  inv G n a C (i :: L) \{} ->
  inv G n a C L (out_edges G i).
Proof.
  introv I. destruct I. constructors~.
  { intros i' j Ci' E.
    forwards~ [M|[M|M]]: inv_true_edges0 i' j.
    (* il faudrait peut être que je définisse une tactic
       [forwards_branches M: inv_true_edges0 i' j] qui évite
      de se taper le intro_pattern tout moche *)
    rew_listx in M. branches; autos*. }
Qed.

Definition measure C (L: list int) :=
  count (= false) C + length L.

(* FIXME: lemmas about read and update are inconsistent.
   For example:
   - there is LibListZ.read_succ, but not LibListZ.update_succ
     (only LibList.update_succ)
   - there is read_cons_case, but not update_cons_case,
     only update_cons_pos *)
Lemma count_update_remove : forall A `{Inhab A} (P: A -> Prop) L i x,
  index L i ->
  P L[i] ->
  ~ P x ->
  count P L[i:=x] = count P L - 1.
Proof.
  induction L; introv Ii PLi Px.
  - destruct Ii. rew_list* in *.
  - tests: (i = 0).
    { rewrite read_zero in PLi. rewrite update_zero.
      rewrite !count_cons. repeat case_if*. }
    { rewrite read_cons_case in PLi. case_if~.
      rewrite index_eq_inbound, length_cons in Ii.
      rewrite* update_cons_pos. rewrite !count_cons. case_if~.
      { rewrite* IHL. rewrite* index_eq_inbound. }
      { rewrite* IHL. rewrite* index_eq_inbound. } }
Qed.

Lemma measure_step : forall C L i,
  index C i ->
  C[i] = false ->
  measure C[i:=true] (i :: L) = measure C L.
Proof.
  introv Ii Ci. unfold measure. rew_listx.
  erewrite~ count_update_remove. math.
Qed.

Lemma measure_nonneg : forall C L,
  0 <= measure C L.
Proof.
  intros. unfold measure.
  forwards: count_nonneg (= false) C.
  forwards*: length_nonneg L.
Qed.

Lemma reachable_imperative_spec : forall g G a b,
  a \in nodes G ->
  b \in nodes G ->
  app reachable_imperative [g a b]
    INV (g ~> RGraph G)
    POST (fun (r:bool) => \[r = isTrue (reachable G a b)]).
Proof.
  introv Ga Gb. xcf.
  xapp. intros Gn. xapp. { unfold nodes_index in Gn. math. }
  intros C0 HC0.
  xapp. xapp*. xseq. xapp.
  set (hinv := fun E C L =>
       g ~> RGraph G
    \* c ~> Array C
    \* s ~> Stack L
    \* \[inv G n a C L E]).
  set (K := (fun (C: array bool) (L: list int) => bool_of (L <> nil))).
  xseq (# Hexists C, hinv \{} C nil).
  xwhile_inv (fun (b:bool) (m:int) =>
    Hexists C L, hinv \{} C L \*
    \[b = isTrue (L<>nil)] \*
    \[m = measure C L]
  ) (downto 0).
  { unfold hinv. hsimpl~. apply* inv_init. }
  { intros S LS r m HS. unfold hinv at 1. xpull ;=> C L I Er Em.
    (* while condition *)
    xlet. xapps. xret. xpull ;=> E.
    (* todo: simplify E *)
    xif.
    { (* case loop step *)
       xseq (Hexists C' L', hinv \{} C' L' \* \[ measure C' L' < measure C L ]).
       xapp*. intros L' HL'. subst L.
       xfun as f. forwards~ [Gi Ci]: inv_stack I i.
       xapp_spec~ iter_edges_remaining_spec
         (>> (fun E =>
                Hexists C2 L2, hinv E C2 L2 \*
                \[ C2[i] = true ] \*
                \[ measure C2 L2 = measure C L'])
             G).
       { intros L. unfold hinv. applys heap_contains_intro (Hexists C2 L2,
           c ~> Array C2 \* s ~> Stack L2 \*
           \[ inv G n a C2 L2 L] \* \[ C2[i] = true]
           \* \[ measure C2 L2 = measure C L' ]
           ); hsimpl*.
         (* ça on automatisera plus tard avec une tactique *) }
       { introv N Hij. xpull. intros C2 L2 C2i Hm.
         xapp_spec Sf.
         unfold hinv at 1. xpull. intros I'.
         xapps*. xif.
         { xapps*. xapp. intros _. unfold hinv. hsimpl.
           { rewrite* measure_step. rew_bool_eq~. }
           { rew_array*. case_if~. }
           applys~ inv_step_push i. }
         { unfold hinv. xrets~. applys~ inv_step_skip j. } }
       { unfold hinv. hsimpl. apply~ inv_step_pop. }
       { intros ?. xpull;=> C2 L2 C2i Hm. hsimpl. rewrite Hm.
         unfold measure. rew_listx. math. }
       { intros C2 L2 Hm. xapplys~ HS. lets*: measure_nonneg C2 L2. } }
    { (* case loop end *)
      xret. unfold hinv. subst L. hsimpl*. { rew_bool_eq*. } } }
  { intros r. hpull ;=> m C L E Em. rew_bool_eq in *. subst L.
    (* on pourra peut être essayer de patcher des x-tactiques
     pour que le [rew_bool_eq] soit fait automatiquement? *)
    hsimpl C. }
  { unfolds hinv. clear hinv K.
    intro C. xpull. intros I. (* dans cfm2, c'est pratique on peut faire [xpull ;=> C I]
     directement. il faudrait voir pour patcher [xpull] de cfml1 pour qu'il laisse
     les quantificateurs en tête de la même manière *)
    (* Armael: je ne sais pas comment implémenter ça. faire un pose mark /
       gen_until_mark ne marche pas parce que xpull shuffle le contexte *)
    lets: inv_length_C I.
    xapp*. hsimpl. apply affine_Stack. (* à mettre en hint *)
    forwards R: inv_end I Gb. subst r. extens. rew_bool_eq*. }
Qed.
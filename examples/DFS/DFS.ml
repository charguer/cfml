

(*************************************************************************)
(** Graph representation by adjacency lists *)

module Graph = struct

type t = (int list) array

let nb_nodes (g:t) =
  Array.length g

let iter_edges (g:t) (i:int) (f:int->unit) =
  List.iter (fun j -> f j) g.(i)

end


(*************************************************************************)
(** DFS Algorithm, Sedgewick's presentation *)

type color = White | Gray | Black

 let rec dfs_from g c i =
    c.(i) <- Gray;
    Graph.iter_edges g i (fun j ->
       if c.(j) = White
          then dfs_from g c j);
    c.(i) <- Black

let dfs_main g rs =
   let n = Graph.nb_nodes g in
   let c = Array.make n White in
   List.iter (fun i ->
     if c.(i) = White then
       dfs_from g c i) rs;
   c



(*************************************************************************)
(** Minimal stack structure *)

module Stack = struct

type 'a t = ('a list) ref

let create () : 'a t =
  ref []

let is_empty (s : 'a t) =
  !s = []

let pop (s : 'a t) =
  match !s with
  | [] -> assert false
  | x::n -> s := n; x

let push (x : 'a) (s : 'a t) =
  s := x::!s

end


(*************************************************************************)
(** DFS Algorithm, using two colors and a stack *)

let reachable_imperative g a b =
   let n = Graph.nb_nodes g in
   let c = Array.make n false in
   let s = Stack.create() in
   c.(a) <- true;
   Stack.push a s;
   while not (Stack.is_empty s) do
      let i = Stack.pop s in
      Graph.iter_edges g i (fun j ->
         if not c.(j) then begin
           c.(j) <- true;
           Stack.push j s;
         end);
   done;
   c.(b)


type 'a dyn_array = {
   mutable default : 'a;
   mutable size : int;
   mutable data : 'a array;
}

let length_ t =
   t.size

let make def =
 { default = def;
   size = 0;
   data = Array.make 4 def
   }

let transfer src dst nb = 
   for i = 0 to nb - 1 do
      dst.(i) <- src.(i);
   done

let resize t new_capa =
   let old_data = t.data in
   t.data <- Array.make new_capa t.default;
   transfer old_data t.data t.size

let push t x =
   let capa = Array.length t.data in
   if t.size = capa
      then resize t (2 * capa);
   t.data.(t.size) <- x;
   t.size <- t.size + 1

let pop t = 
   if t.size = 0
       then invalid_arg "pop";
   t.size <- t.size - 1;
   let x = t.data.(t.size) in
   let capa = Array.length t.data in
   if (capa > 4) && (t.size <= capa/4)
      then resize t (capa / 2);
   x

let is_empty (t:'a dyn_array) : bool = 
  (length_ t = 0)

let get t i =
  if (i >= 0) && (i < t.size)
  then t.data.(i)
  else invalid_arg "get"

let set t i v =
  if (i >= 0) && (i < t.size)
  then t.data.(i) <- v
  else invalid_arg "set"

(* Demos *)

let demo_compare (t1:int dyn_array) (t2:int dyn_array) =
  let n1 = length_ t1 in
  let n2 = length_ t2 in
  if n1 <> n2 then
    false
  else begin
    let b = ref true in
    let i = ref 0 in
    while (!b && !i < length_ t1) do
      if (get t1 !i) <> (get t2 !i)
        then b := false;
      incr i
    done;
    !b
  end

let demo_rev_append (t1:'a dyn_array) (t2:'a dyn_array) =
  while not (is_empty t2) do
     let x = pop t2 in
     push t1 x
  done

let demo_seq n =
  let t = make 0 in
  for i = 0 to n - 1 do
    push t i
  done;
  for i = 0 to n - 1 do
    let x = pop t in
    if x <> n - 1 - i then assert false
  done


let demo_pushn t x n =
  for i = 0 to n - 1 do
     push t x;
  done

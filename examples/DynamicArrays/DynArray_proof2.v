Set Implicit Arguments.
Require Import CFLibCredits CFLib.
Require Import LibListZ DynArray_ml Facts. (* FactsCredits. *)
Open Scope comp_scope.
Require Import Zpower.

Local Infix "^^" := Zpower (at level 30, right associativity).

Lemma simpl_zero_credits : forall n, n = 0 -> \$ n ==> \[].
Proof. intros. subst. rewrite <-credits_int_zero_eq. hsimpl. Qed.

Lemma group_ineq : forall n m, 0 <= n - m -> m <= n.
Proof. intros. math. Qed.
                                               
(*******************************************************************)

Generalizable Variables A.

(*******************************************************************)
(** * Instantiations *)

Notation "'RecDynArray'" := (Dyn_array Id Id Id).


(*******************************************************************)
(** * Invariants *)

Definition resize_cst := array_make_cst + 1.

Definition op_cst := resize_cst * 4.

Lemma resize_cst_nonneg : resize_cst >= 0.
Proof. unfold resize_cst. 
       simpl_nonneg; math.
Qed.

Lemma op_cst_nonneg : op_cst >= 0.
Proof. unfold op_cst. pose proof resize_cst_nonneg.
       simpl_nonneg; math.
Qed.

Definition potential size n :=
  (Z.abs (size - 2^^(n + 1)) * op_cst).

Lemma potential_nonneg : forall size n, potential size n >= 0.
Proof. intros. unfold potential.
       simpl_nonneg.
       { apply Z.abs_nonneg. }
       { apply op_cst_nonneg. }
Qed.

(* ne marche pas :( *)
(* Hint Resolve resize_cst_nonneg op_cst_nonneg potential_nonneg : simpl_nonneg. *)

Record inv (A:Type) {IA:Inhab A} (L:list A) size (D:list A) n (b_min:bool) :=
build_inv {
  inv_lenL: size = len L;
  inv_items: forall i, index size i -> D[i] = L[i];
  inv_max: len D >= size;
  inv_min: if b_min then (len D > 4 -> size > 2^^n) else True;
  inv_capa: len D = 2^^(n + 2);
  inv_n: n >= 0 }.

Definition hinv (A:Type) {IA:Inhab A} (L:list A) size data (default:A) D n b_min t :=
     t ~> RecDynArray default size data 
  \* data ~> Arr D 
  \* [inv L size D n b_min]
  \* \$ potential size n.

Definition DynArray (A:Type) {IA:Inhab A} (L:list A) (t:dyn_array A) :=
  Hexists size data default D n, t ~> hinv L size data default D n true.

(*******************************************************************)

Lemma pow2_succ' : forall n m: int, n >= 0 -> m = n+1 -> 2 ^^ m = 2 * 2 ^^ n.
Proof. intros. subst. rewrite <-(Z.pow_succ_r 2 n). 
       math_rewrite~ (Z.succ n = n + 1). assumption.
Qed.

Lemma pow_gz_r : forall (p:Z), (0 <= 2 ^^ p).
Proof. intros. apply Z.pow_nonneg. math. Qed.

Lemma pow_gz_l : forall (p:Z), (2 ^^ p >= 0).
Proof. intros. apply ineq_le_to_ge. apply pow_gz_r. Qed.

(* les ajouter à une base de hints ? laquelle ? *)

(*******************************************************************)
(** * Focus/unfocus lemmas *)

Lemma hinv_unfocus : forall t n b_min (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D,
  inv L size D n b_min ->
  (t ~> RecDynArray default size data \* data ~> Arr D
  \* \$ potential size n)
     ==> (t ~> hinv L size data default D n b_min).
Proof.
  introv I. xunfold hinv. xsimpl. auto.
Qed. 

Implicit Arguments hinv_unfocus [ A [IA] ].

Lemma DynArray_unfocus : forall t (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D n,
  (t ~> hinv L size data default D n true)
     ==> (t ~> DynArray L).
Proof.
  introv. xunfold DynArray. xsimpl.
Qed.

Implicit Arguments DynArray_unfocus [ A [IA] ].

Lemma hinv_weaken : forall t n (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D,
  (t ~> hinv L size data default D n true)
     ==> (t ~> hinv L size data default D n false).
Proof.
  intros. xunfold hinv. xextract as IL. xsimpl. destruct IL. constructor~.
Qed.

Implicit Arguments hinv_weaken [ A [IA] ].

Lemma hinv_strengthen : forall t n (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D,
  (len D > 4 -> size > 2^^n) ->
  (t ~> hinv L size data default D n false)
     ==> (t ~> hinv L size data default D n true).
Proof.
  introv Is. xunfold hinv. xextract as IL. xsimpl. destruct IL. constructor~.
Qed.

Implicit Arguments hinv_strengthen [ A [IA] ].

(*******************************************************************)
(** * Verification *)

Hint Resolve inv_lenL.

Lemma length_spec : forall A,
  Spec length_ (t:dyn_array A) |B>>
    forall {IA:Inhab A} (L:list A),
    B (\$ 1 \* t ~> DynArray L) (fun (x:int) => t ~> DynArray L \* [x = len L]).
Proof.
  xcf. introv. 
  xunfold DynArray. xextract as size data default D n.
  xunfold hinv at 1. xextract as IL.
  xpay.
  xapps. intros l. xextracts. xchange* (hinv_unfocus t). hsimpl.
  hsimpl*.
Qed.

Hint Extern 1 (RegisterSpec length_) => Provide length_spec.

Lemma transfer_spec : forall A,
  Spec transfer src dst nb |B>>
    forall {IA:Inhab A} (L1:list A) (L2:list A),
    len L1 >= nb -> len L2 >= nb -> nb >= 0 ->
    B (\$1 \* src ~> Arr L1 \* dst ~> Arr L2 \* \$ nb)
      (# Hexists L2', src ~> Arr L1
      \* dst ~> Arr L2'
      \* [len L2' = len L2] \* [forall k, index nb k -> L2'[k] = L1[k]]).
Proof.
  intros. xcf. introv Nb LL1 LL2.
  xpay.
  xfor. xname_post QF.
  cuts M: (forall i,
    S i (Hexists L2', [0 <= i <= nb] \*
          \$ (nb - i) \*
          src ~> Arr L1 \*
          dst ~> Arr L2' \*
          [len L2' = len L2] \*
          [(forall k, index i k -> L2'[k] = L1[k])])
         QF).
  { xgc. xapply M. hsimpl~. csimpl. math. xok. hsimpl. }

  { intro i. induction_wf IH: (int_upto_wf (nb+1)) i.
    hide IH. apply HS. clear HS. split. clearbody QF.
    introv Inb. xextract as L2' Ii LL2' VL2'.

    xseq. xapps~. xapps~. show IH. simpl.
    xgc. 
    xapply~ IH.
    hsimpl.
      { csimpl. }
      { intro. rew_arr. case_If. subst~. xauto~. }
      { rew_arr~. }
      { math. }
    hsimpl~. 
    introv Inb. xret. xextract as L2' IM EL2' VL2'.
    subst QF. hsimpl~. }
Qed.

Hint Extern 1 (RegisterSpec transfer) => Provide transfer_spec.

Ltac unfold_pow2 n :=
  try rewrite (@pow2_succ' (n+2) (n+3));
  try rewrite (@pow2_succ' (n+1) (n+2));
  auto with zarith;
  try math.

Ltac unfold_pow2' n :=
  try rewrite (@pow2_succ' n (n+1));
  auto with zarith;
  try math.

Ltac unfold_pow2_switch i n :=
  match i with
    1 => unfold_pow2 n
  | 2 => unfold_pow2' n
  end.

Ltac ineq_apply E :=
  match goal with
  | |- ?x <= ?y =>
    match type of E with
    | ?x' <= ?y' => apply E
    | ?x' >= ?y' => apply ineq_ge_to_le; apply E
    end
  | |- ?x >= ?y =>
    match type of E with
    | ?x' >= ?y' => apply E
    | ?x' <= ?y' => apply ineq_le_to_ge; apply E
    end
  end.

(* TODO arthur: rew_list dans LibListZ qui fait ce qui faut *)

Ltac clean_eq_coerce :=
  match goal with
    H: @eq Z (my_Z_of_nat ?a) (my_Z_of_nat ?b) |- _
    =>
    pose proof (@int_nat_eq a b H); clear H
  end.

Ltac clean_eqs_coerce := repeat clean_eq_coerce.

Ltac simpl_nonneg_auto' := simpl_nonneg_auto; try (clear; math).
Tactic Notation "ring_simplify" "~" := ring_simplify; auto.

Ltac crush_ineq i n :=
  match goal with
  | |- 0 <= _ => idtac
  | |- Z.le Z0 _ => idtac
  | |- _ <= _ => apply group_ineq
  end;
  unfold op_cst; unfold resize_cst; unfold_pow2_switch i n; ring_simplify~;
  simpl_nonneg_auto'; try math.

Ltac crush_eq i n :=
  unfold op_cst; unfold resize_cst; unfold_pow2_switch i n; ring_simplify;
  try math.

Ltac csimpl_crush i n :=
  match goal with
  | |- _ ==> \[] => apply simpl_zero_credits; crush_eq i n
  | |- _ ==> _ => csimpl; match goal with
                          | |- _ <= _ => crush_ineq i n
                          | |- _ ==> _ => csimpl_crush i n
                          end
  end.

Parameter ml_array_make_spec : forall A,
  Spec ml_array_make (n:int) (v:A) |R>> 
    forall `{Inhab A}, n >= 0 ->
     R \[] (fun t => Hexists L, t ~> Arr L \* 
     [len L = n /\ (forall i, index n i -> L[i] = v)]).

Hint Extern 1 (RegisterSpecCredits ml_array_make) => Provide ml_array_make_spec.

Lemma resize_spec : forall A,
  Spec resize (t:dyn_array A) (new_capa:int) |B>>
    forall {IA:Inhab A} (L:list A) s d def D n,
       (s = 2^^(n+2) /\ new_capa = 2^^(n+3))
    \/ (s = 2^^n /\ new_capa = 2^^(n+1) /\ new_capa >= 4) ->
    B (\$1 \* \$1 \* t ~> hinv L s d def D n false)
      (# Hexists d' D', Hexists n', t
         ~> hinv L s d' def D' n' true \* [len D' = new_capa]).
Proof.
  xcf. introv Is. xunfold hinv at 1. xextract as R. xpay. xapps. xapps. 
  { destruct Is as [[? ?]|[? [? ?]]]; invert R; intros LL DLi LDc T LD N.
    
    (* Case 1: we extend the array *)
    {
      pose proof (pow_gz_r (n+1)).
      pose proof (pow_gz_r (n+2)).
      pose proof (pow_gz_r (n+3)).

      (* Instantiate the potential for the current values *)
      xchange (\$ potential s n ==> \$ (new_capa * resize_cst)).
      { unfold potential. subst new_capa; subst s; unfold_pow2 n.
        rewrite Z.abs_eq; try (ring_simplify; apply pow_gz_r). unfold op_cst.
        csimpl_crush 1 n. }

      (* Remove unnecessary credits *) 
      subst new_capa; subst s.
      xgc (\$(2 ^^ (n+2))). { subst. csimpl_crush 1 n. }
      
      (* Continue to walk through the program... *)
      { xapp.
        { simpl_ineq~. } 

        intros D' [LD' ID']. repeat xapps; auto.
        { assert (len D' >= len D). {
            rewrite LD. rewrite LD'. unfold_pow2 n.
          } simpl_ineq~. }
        { csimpl_crush 1 n. } 
 
        hextract as L' LL' EL'. 
        hchanges (hinv_unfocus t (n+1)).
        { constructors~.
          { intros i I. rewrite~ EL'. }
          { rewrite LL',LD'. unfold_pow2 n. }
          { intros. forwards ?:pow2_pos (n+1); [math | unfold_pow2 n]. }
          { rewrite LL',LD'. fequals. math. } }
        { csimpl.
          { unfold potential. rewrite Z.abs_eq; math_rewrite ((n+1)+1 = n+2).
            { crush_ineq 1 n. }
            { auto with zarith. } }
          { ineq_apply (potential_nonneg (2^^(n+2)) (n+1)). } }
        { rewrite LL',LD'. auto. } } }
            
    (* Case 2: we shrink the array *)
    {
      pose proof (pow_gz_r (n-1)).
      pose proof (pow_gz_r n).
      pose proof (pow_gz_r (n+1)).

      xchange (\$ potential s n ==> \$ (2 * new_capa * resize_cst)).
      { unfold potential. subst s; subst new_capa.
        rewrite Z.abs_neq. csimpl_crush 2 n. unfold_pow2' n. }

      subst new_capa; subst s.
      (* GC unnecessary credits *)
      xgc (\$ (2 * 2 ^^ (n+1) * array_make_cst + 3 * 2 ^^ n)). { csimpl_crush 2 n. } 

      (* meh *)
      set (X1 := 2 * 2^^(n+1)).
      set (X2 := 3 * 2^^n).
      xapps. math.

      intros D' [LD' _].
      (* GC (d ~> Arr D), but only at the end *)
      xgc.
      repeat xapps; auto.
      { rewrite LD'. unfold_pow2' n. }
      { subst X1 X2. csimpl_crush 2 n. }

      assert (1 <= n). (* laborieux *)
      { apply Zpow_facts.Zpower_le_monotone_inv with 2; auto with zarith.
        rewrite (@pow2_succ' n (n+1)) in H1; auto.
        asserts_rewrite (2^^1 = 2). reflexivity.
        math. } 

      hextract as L' LL' EL'.
      hchanges (hinv_unfocus t (n-1)).
      { apply build_inv; auto. 
        { intros i I. rewrite~ EL'. }
        { rewrite LL',LD'. unfold_pow2' n. }
        { intros. rewrite (@pow2_succ' (n-1) n); auto with zarith; try math.
          match goal with |- ?x > ?y => assert (x - y > 0) end.
          ring_simplify. let H := fresh in lets H: pow2_pos (n-1) ___; math. 
          math. }
        { math_rewrite ((n-1)+2 = n+1). auto with zarith. }
        { math. } }

      { unfold potential. math_rewrite ((n-1)+1 = n). rewrite Z.abs_eq; auto with zarith.
        csimpl_crush 2 n. }
      { auto with zarith. } } }
Qed.

Hint Extern 1 (RegisterSpec resize) => Provide resize_spec.

Parameter ml_div_spec : Spec ml_div (x:int) (y:int) |B>>  
  y <> 0 ->
  B \[] (fun (z:int) => [z = x / y]).

Hint Extern 1 (RegisterSpec ml_div) => Provide ml_div_spec.

Lemma pow2_div : forall x y: int, x >= 1 -> (2 ^^ x) / (2 * y) = 2 ^^ (x-1) / y.
Proof. intros x y ?.
       rewrite (@pow2_succ' (x-1) x); try math.
       rewrite Zdiv_mult_cancel_l; math.
Qed.

Lemma pow2_div_2 : forall x: int, x >= 1 -> 2 ^^ x / 2 = 2 ^^ (x-1).
Proof. intros x H.
       assert (2 = 2 * 1) as A; auto with zarith.
       rewrite A at 2.
       rewrite pow2_div; auto with zarith.
Qed.

Lemma pow2_div_4 : forall n: int, n >= 0 -> 2^^(n+2) / 4 = 2 ^^ n.
Proof. intros. math_rewrite (4 = 2*2).
       rewrite pow2_div; try math.
       rewrite pow2_div_2; try math.
       math_rewrite~ (((n+2)-1)-1 = n).
Qed.

Lemma Zpower_lt_monotone_inv : forall a b c: int, (1 < a)%Z -> (0 <= c) -> (0 < b) ->
                                                  (a ^^ b < a ^^ c) -> (b < c).
Proof. admit. Admitted.
  
  
Lemma pop_spec : forall A,
  Spec pop (t:dyn_array A) |B>>
    forall (L:list A) (x:A) {IA:Inhab A},
    B (t ~> DynArray (L&x) \* \$ op_cst \* \$1 \* \$1 \* \$1) (fun y => [y = x] \* (t ~> DynArray L)).
Proof.
  intros. xcf. intros.
  xpay. 

  xunfold DynArray. 
  xextract as s d def D n. xunfold hinv at 1. xextract as ID. 
  destruct ID as [IDs IDi IDf IDc IDc' IDn]. rew_list in IDs.
  xapps. xname_pre Hcur. xseq Hcur.
  xif as C. { xfail. math. } { xrets. }

  subst Hcur.
  xapps. xapps. xapps. xapps. xapps~. xapps. xapps. xapps. xapps~.
  xchange (hinv_unfocus t n false L).
  { constructor~.
    introv Isi. rewrite IDi. rew_arr. case_If. false. math. auto. math. }
  { unfold potential. subst s.
    csimpl.
    { pose proof (Z.abs_triangle
                    (((1%nat + length L)%I - 1)%I - 2^^(n+1) + 1) (-1))
        as Tri.
      pose proof (int_le_mul_pos_l _ _ op_cst op_cst_nonneg Tri) as Tri'.
      rewrite (Z.abs_neq (-1)) in Tri'; auto with zarith. 
      ring_simplify in Tri'. ring_simplify.

      match type of Tri' with
        (op_cst * Z.abs ?a)%I <=
        ((op_cst * Z.abs ?c)%I + _)%I => 
        match goal with
          |- (Z.abs ?b * op_cst)%I <=
             (((op_cst * Z.abs ?d)%I + _)%I + _)%I =>
          math_rewrite (b = a);
          math_rewrite (d = c)
        end
      end. rewrite Z.mul_comm at 1.
      math. }
    { simpl_nonneg. apply Z.abs_nonneg. apply op_cst_nonneg. } }

  xseq  (# Hexists data' D', Hexists n', t ~> hinv L (s - 1) data' def D' n' true).
  { xif as C.
    { xapps~. 
      assert (s = 2^^n + 1) as Sv.
      { invert C as ID Is.
        specialize (IDc ID). rewrite IDc' in *.
        math_rewrite (4 = 2 * 2) in Is.
        rewrite~ pow2_div_4 in Is. }

      xapps.

      { right. split.
        { rewrite Sv. math. }
        { split.
          { rewrite IDc'. rewrite pow2_div_2; try math.
            math_rewrite ((n+2)-1 = n+1). auto. }
          { destruct C as [C1 C2]. specialize (IDc C1).
            rewrite IDc' in *. rewrite pow2_div_2; try math.
            assert (n >= 1). {
              assert (2 < n+2). {
              forwards U: (@Zpower_lt_monotone_inv 2 2 (n+2));
                auto with zarith; try math.
              } math.
            }
            math_rewrite (4 = 2^^2). reflexivity.
            apply ineq_le_to_ge.
            apply Zpow_facts.Zpower_le_monotone; auto with zarith.
            math. } } }
      
      { csimpl; math_rewrite (my_Z_of_nat (S (length L)) = s); rewrite Sv.
        skip. skip.  (* triangular inequality x2 *)}

      hsimpl. }

    xret. xchange (hinv_strengthen t).
    { intros C2. invert C as C.
      { false. }
      { rewrite IDc' in *. rewrite~ pow2_div_4 in C. } }

    xsimpl. }

  intros data' D' n'.
  xrets. rewrite IDi. rew_arr. case_If. auto. false. math. math.
Qed.
              
Hint Extern 1 (RegisterSpec pop) => Provide pop_spec.

Lemma push_spec : forall A,
  Spec push (t:dyn_array A) (x:A) |B>>
    forall {IA:Inhab A} (L:list A),
    B (t ~> DynArray L \* \$ op_cst \* \$1 \* \$1 \* \$1) (# t ~> DynArray (L&x)).
Proof.
  xcf. intros. xunfold DynArray. sets_eq two: 2. xextract as size data def D n.
  xunfold hinv at 1. xextract. introv IL.
  xpay.
  xapps. xapps. xapps.

  xchange* (hinv_unfocus t).
  { csimpl_crush 1 n. ineq_apply (potential_nonneg size n). }
  xseq (Hexists data' D', Hexists n', t ~> hinv L size data' def D' n' true
         \* \$op_cst \* [len D' > size]). 
  { 
    lets IL2: IL. destruct IL as [InL Ini Inx Inm Inc Inn]. xif.
    { subst two. xchange (hinv_weaken t). xapps~.
      { left. split.
        { math. }
        { rewrite Inc. unfold_pow2 n. } }
      { unfold potential. rewrite C in *. rewrite Inc in *.
        rewrite Z.abs_eq. csimpl_crush 2 n.
        unfold_pow2 n. ring_simplify. apply pow_gz_r. }
      { xextract as d' D' ? LD'. csimpl. apply op_cst_nonneg. rewrite LD'. 
        rewrite C. rewrite Inc. forwards ?: pow2_pos (n+2).
        math. math. } }
    xret. hsimpl. csimpl. apply op_cst_nonneg. math. }

  intros t' D' n' I1. xunfold hinv at 1. xextract as ID'.
  destruct ID' as [IL's IL'i IL'f IL'c IL'v IL'n].
  xapps. xapps. xseq. xapps~. xapps. xapps.
  xchange (hinv_unfocus t n'); [ | | xsimpl ]. 

 constructor~.
    rew_list~.
    introv Ii. rew_arr. case_If; case_If; rew_arr~. 
    rew_arr~.
    rew_arr~. intros H0. specializes~ IL'c H0.
    rew_arr~.
    { unfold potential. csimpl.
      { skip. (* triangular inequality *) }
      { simpl_nonneg. apply Z.abs_nonneg. apply op_cst_nonneg. } }
Qed.

Hint Extern 1 (RegisterSpec push) => Provide push_spec.

Lemma is_empty_spec : forall A,
  Spec is_empty t |B>>
    forall {IA:Inhab A} (L:list A),
    B (t ~> DynArray L \* \$2) (fun (x:bool) => t ~> DynArray L \* [x = isTrue (L = nil)]).
Proof.
  xcf. introv. xpay. csimpl. xapps. csimpl. xrets. apply length_nil_eq.
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.

Lemma get_spec : forall A,
  Spec get (t:dyn_array A) (i:int) |B>>
    forall {IA:Inhab A} (L:list A), index (len L) i ->
    B (t ~> DynArray L \* \$1)
      (fun y:A => t ~> DynArray L \* [y = L[i]]).
Proof.
  (* lets: get_cf. ==> alt+maj+A *)
  xcf. introv Hi. xunfold DynArray. xextract as s d def D n. hunfold hinv at 1. 
  xextract as IL.
  xpay. xapps. xif.
  xapps. xapps. invert~ IL. intros r. xextract. intros.
   xchange* (@hinv_unfocus t). csimpl. 
   xsimpl. subst r. destruct IL as [ELs ELD IDs ID]. apply~ ELD.
  xfail. invert Hi as a c. invert IL.  intros. invert C; intros; math.
Admitted. (* faster *)

Lemma set_spec : forall A,
    Spec set (t:dyn_array A) (i: int) (v: A) |B>>
      forall {IA:Inhab A} (L:list A), index (len L) i ->
      B (t ~> DynArray L \* \$1)
        (# Hexists L', t ~> DynArray L' \* [L' = L[i := v]]).
Proof.
  xcf. introv Hi. xunfold DynArray at 1. xextract as s d def D n. hunfold hinv at 1.
  xextract as IL.
  xpay. xapps. xif.
  { xapps. xapps.
    { invert~ IL. }
    { intros r. xextract. intros.
      xchange* (@hinv_unfocus t).
      { invert IL; intros IL Ii Ima Imi Ic In.
        apply (@build_inv _ _ L[i:=v] s D[i:=v] n true).
        { rew_arr~. }
        { intros j J. rew_arr. rewrite~ Ii. }
        { rew_arr~. }
        { rew_arr~. }
        { rew_arr~. }
        { auto. } }
      { hsimpl. }
      { hchange DynArray_unfocus; hsimpl. rew_arr~. } } }
  { xfail. invert IL. intros. invert C; intros; math. }
Qed.



Set Implicit Arguments.
Require Import CFLibCredits DynArray_ml FactsCredits.
Open Scope comp_scope.
Require Import Zpower.




(*******************************************************************)
(** * todo: bouger dans facts *)


Local Infix "^^" := Zpower (at level 30, right associativity).



(*
Ltac lmath :=
  try rew_length in *; math.
Hint Extern 1 ((_ < _)) => lmath.
Hint Extern 1 ((_ > _)) => lmath.
Hint Extern 1 ((_ >= _)) => lmath
Hint Extern 1 ((_ <= _)) => lmath.
Hint Extern 1 ((_ < _)%Z) => lmath.
Hint Extern 1 ((_ > _)%Z) => lmath.
Hint Extern 1 ((_ >= _)%Z) => lmath.
Hint Extern 1 ((_ <= _)%Z) => lmath.
*)

(* Hint Extern 10 (_ = _ :> list _) => subst; rew_list. *)



(********************************************************************)
(** Tactic [rew_real_int] *)

Section Rlemmas2.
Implicit Types a b c : R.
Open Scope R_scope.


End Rlemmas2.




(*******************************************************************)
(** * Missing lemmas for R *)


Section Rlemmas.
Implicit Types a b c : R.
Open Scope R_scope.


Axiom Rabs_opp : forall a:R, Rabs (-a) = Rabs a.
Axiom Rabs_pos : forall a:R, a >= 0 -> Rabs a = a.
Axiom Rabs_neg : forall a:R, a <= 0 -> Rabs a = -a.
Axiom Rabs_opp_pos : forall a:R, a >= 0 -> Rabs (-a) = a.


Axiom R_sub_zero : forall a,
  a - 0 = a.


Axiom R_mul_zero_r : forall a,
  a*0 = 0.



Axiom R_distrib_add_mul_r : forall a b c,
  c*(a+b) = c*a + c*b.
Axiom R_distrib_sub_mul_r : forall a b c,
  c*(a-b) = c*a - c*b.

Axiom R_mul_one_r : forall a,
  a*1 = a.

Axiom R_minus_same : forall b,
  b-b = 0.
Axiom R_plus_minus_same : forall a b,
  a+b-b = a.
Axiom R_minus_plus_same : forall a b,
  a-b+b = a.

Axiom R_eq_le_ge : forall a b,
  a <= b ->
  b <= a ->
  a = b.



End Rlemmas.

Hint Rewrite R_sub_zero R_mul_zero_r R_mul_one_r R_minus_same
             R_distrib_add_mul_r R_distrib_sub_mul_r : rew_real.

Tactic Notation "simpl_eq" := 
  solve [ apply R_eq_le_ge; [ simpl_ineq | simpl_ineq ]].


(*******************************************************************)

Generalizable Variables A.

(*******************************************************************)
(** * Instantiations *)

Notation "'RecDynArray'" := (Dyn_array Id Id Id).


(*******************************************************************)
(** * Invariants *)

Definition resize_cst := (pay_cst + array_make_cst)%R.

Definition op_cst := (4 * resize_cst)%R.

Definition potential (size:int) (n:int) :=
  (Rabs (size - 2^^(n + 1)) * op_cst)%R.
  
Record inv (A:Type) {IA:Inhab A} (L:list A) size (D:list A) (n:int) (b_min:bool) :=
build_inv {
  inv_lenL: size = len L;
  inv_items: forall i, index size i -> D[i] = L[i];
  inv_max: len D >= size;
  inv_min: if b_min then (len D > 4 -> size > 2^^n) else True;
  inv_capa: len D = 2^^(n + 2);
  inv_n: n >= 0 }.

Definition hinv (A:Type) {IA:Inhab A} (L:list A) size data (default:A) D n b_min t :=
     t ~> RecDynArray default size data 
  \* data ~> Arr D 
  \* [inv L size D n b_min]
  \* $ potential size n.

Definition DynArray (A:Type) {IA:Inhab A} (L:list A) (t:dyn_array A) :=
  Hexists size data default D n, t ~> hinv L size data default D n true.


(*******************************************************************)
(** * Lemmas on constants *)

Lemma resize_cst_pos : (resize_cst >= 0)%R.
Proof. unfold resize_cst. credits_nonneg. Qed.
Hint Resolve resize_cst_pos : credits_nonneg.

Lemma op_cst_pos : (op_cst >= 0)%R.
Proof. unfold op_cst. credits_nonneg. Qed.
Hint Resolve op_cst_pos : credits_nonneg.


(*******************************************************************)
(** * Focus/unfocus lemmas *)

Lemma hinv_unfocus : forall t n b_min (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D,
  inv L size D n b_min ->
  (t ~> RecDynArray default size data \* data ~> Arr D \* $ potential size n)
     ==> (t ~> hinv L size data default D n b_min).
Proof.
  introv I. xunfold hinv. xsimpl. auto.
Admitted. (* faster *)

Implicit Arguments hinv_unfocus [ A [IA] ].

Lemma DynArray_unfocus : forall t (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D n,
  (t ~> hinv L size data default D n true)
     ==> (t ~> DynArray L).
Proof.
  introv. xunfold DynArray. xsimpl.
Admitted.

Implicit Arguments DynArray_unfocus [ A [IA] ].

Lemma hinv_weaken : forall t n (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D,
  (t ~> hinv L size data default D n true)
     ==> (t ~> hinv L size data default D n false).
Proof.
  intros. xunfold hinv. xextract as IL. xsimpl. destruct IL. constructor~.
Qed.

Implicit Arguments hinv_weaken [ A [IA] ].

Lemma hinv_strengthen : forall t n (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D,
  (len D > 4 -> size > 2^^n) ->
  (t ~> hinv L size data default D n false)
     ==> (t ~> hinv L size data default D n true).
Proof.
  introv Is. xunfold hinv. xextract as IL. xsimpl. destruct IL. constructor~.
Qed.

Implicit Arguments hinv_strengthen [ A [IA] ].


(*******************************************************************)
(** * Tactics *)

Ltac pot_simpl := 
  unfold potential; rew_length; try simpl_credits.


(*******************************************************************)
(** * Verification *)

Lemma length_spec : forall A,
  Spec length_ (t:dyn_array A) |B>>
    forall {IA:Inhab A} (L:list A),
    keep B (t ~> DynArray L) (fun (x:int) => [x = len L]).
Proof.
  xcf. introv.
  xunfold DynArray. xextract as size data default D n.
  xunfold hinv at 1. xextract as IL.
  xapps. intros l. xextracts. xchange* (hinv_unfocus t). simpl_ineq.
  xsimpl. 
   (*unfold inv in IL.*) inverts IL. auto.
Admitted. (* faster *)
Print length_spec.

Hint Extern 1 (RegisterSpec length_) => Provide length_spec.


Lemma transfer_spec : forall A,
  Spec transfer src dst nb |B>>
    forall {IA:Inhab A} (L1:list A) (L2:list A),
    len L1 >= nb -> len L2 >= nb -> nb >= 0 ->
    B (src ~> Arr L1 \* dst ~> Arr L2 \* $ (nb * pay_cst))
      (# Hexists L2', src ~> Arr L1
      \* dst ~> Arr L2'
      \* [len L2' = len L2] \* [forall k, index nb k -> L2'[k] = L1[k]]).
Proof.
  intros. xcf. introv LL1 LL2 Nb.
  xfor. xname_post QF.
  cuts M: (forall i,
    S i (Hexists L2', [0 <= i <= nb] \*
          $ ((nb - i) * pay_cst) \*
          src ~> Arr L1 \*
          dst ~> Arr L2' \*
          [len L2' = len L2] \*
          [(forall k, index i k -> L2'[k] = L1[k])])
        QF).
   xapply M. xsimpl. pot_simpl. math. auto. auto~. xsimpl.
   intros i. induction_wf IH: (int_upto_wf (nb+1)) i.
   hide IH. apply HS. clear HS. split. clearbody QF.  
     introv Inb. xextract as L2' Ii LL2' VL2'.
      asserts_rewrite ((nb - i) * pay_cst = ((nb - (i + 1)%Z) * pay_cst) + pay_cst)%R.
      rew_real_int. rew_real. rewrite R_minus_plus_same. auto. credits_split.
      xpay_simulate.
      Focus 2. apply credits_nonneg_mul. rew_real_to_int. math. credits_nonneg. 
      xseq.
      xapps~. xapps~. show IH. xapply IH.
        auto~.
        xsimpl.
          introv Ik. rew_arr. case_If. subst~. auto~.
          rew_arr~.
          auto~.
       xsimpl.
     introv Inb. xret. xextract as IM L2' EL2' VL2'.
      subst QF. xsimpl. (* auto~. ==> introv G. apply~ VL2'. *)
        asserts_rewrite~ (nb = i).
        auto.
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec transfer) => Provide transfer_spec.

Lemma pow_gz : forall (p:Z), (2 ^^ p >= 0)%Z. Admitted.

Lemma pow_two : forall (p:Z), (Rabs (2 ^^ (p + 1) - 2 ^^ p)  = 2 ^^ p). Admitted.

Lemma pow_le : forall p q, p <= q -> 2 ^^ p <= 2 ^^ q. Admitted.

Lemma pow_ge : forall p q, p >= q -> 2 ^^ p >= 2 ^^ q. Admitted.

Lemma pow_gt : forall p q, p > q -> 2 ^^ p > 2 ^^ q. Admitted.

Lemma pow_gt_inv : forall p q, 2 ^^ p > 2 ^^ q -> p > q. Admitted.

Lemma pow_eq_inv : forall p q, 2 ^^ p = 2 ^^ q -> p = q.
Admitted.

Lemma pow_gt_eq : forall p q, (2 ^^ p > 2 ^^ q) = (p > q). Admitted.

(* TODO: faire le ménage avec les lemmes de puissances *)

Axiom R_mul_comm : forall a b : R,
       ((a * b) = (b * a))%R.
Axiom R_mul_assoc_comm : forall a b c : R,
       ((a * b * c) = (b * a * c))%R.

(* Lemma potential_zero : p = 0 quand size = capa/2 *)

Axiom R_mul_zero : forall (a b:R), a = 0 -> (a * b = 0)%R.


Lemma pow_gz_R : forall (p:Z), (2 ^^ p >= 0)%R. Admitted.
Hint Resolve pow_gz_R : credits_nonneg.



Lemma resize_spec : forall A,
  Spec resize (t:dyn_array A) (new_capa:int) |B>>
    forall {IA:Inhab A} (L:list A) s d def D n,
       (s = 2^^(n+2) /\ new_capa = 2^^(n+3))
    \/ (s = 2^^n /\ new_capa = 2^^(n+1) /\ new_capa > 4) ->
    B (t ~> hinv L s d def D n false)
      (# Hexists d' D', Hexists n', t
         ~> hinv L s d' def D' n' true \* [len D' = new_capa]).
 (* todo; fix Hexists3 *)
Proof. 
(*

  xcf. introv Is. xunfold hinv at 1. xextract as R. xapps. xapps. 
  xchange ($(potential s n) ==> $( new_capa * array_make_cst + s * pay_cst))%R.
    xsimpl. unfold potential. unfold op_cst. unfold resize_cst. invert Is.
      intros (E1&E2). subst.
       skip_rewrite (2 ^^ (n + 2) - 2 ^^ (n + 1) = 2 ^^ (n + 1))%R. 
       rewrite Rabs_pos. Focus 2. rew_real_to_int. apply pow_gz.
       rewrite R_mul_assoc.
       skip_rewrite (2 ^^ (n + 1) * 4 = 2 ^^ (n + 3))%R.
       rewrite R_distrib_add_mul_r. simpl_ineq.
       apply R_le_mul_pos_r. credits_nonneg. rew_real_to_int. apply~ pow_le.
      intros (E1&E2). invert E2 as E2 I3. subst.
       skip_rewrite (2 ^^ n - 2 ^^ (n + 1) = - 2 ^^ n)%R. 
       rewrite Rabs_opp_pos. Focus 2. rew_real_to_int. apply pow_gz.
       rewrite R_mul_assoc. skip_rewrite (2 ^^ n * 4 = 2 ^^ (n + 2))%R.
       rewrite R_distrib_add_mul_r.
       asserts In: (2 ^^ (n + 1) * array_make_cst <= 2 ^^ (n + 2) * array_make_cst)%R.
       apply R_le_mul_pos_r. credits_nonneg. rew_real_to_int. apply~ pow_le.
       rew_ineq In. simpl_ineq. 
       apply R_le_mul_pos_r. credits_nonneg. rew_real_to_int. apply~ pow_le.

  credits_split.
    sets foo: ($(s * pay_cst)).
    xapps~. invert Is.
      intros (E1&E2). subst new_capa. apply pow_gz.
      intros (E1&E2&E3). subst new_capa. apply pow_gz.
    rename _x2 into d'. intros D' (LD'&_). xapps. xapps. xapps. 

    invert R. introv Rs Ri Rf Rc Rc' Rn. subst foo. (* destruct Is. *)
     cuts: (exists H, ((#Hexists L2', 
      d ~> Arr D \*
      d' ~> Arr L2' \*
      [length L2' = length D'] \* [(forall k : int, index s k -> L2'[k] = D[k])] \*
      t ~> RecDynArray def s d' \* \[]) ===>
      #Hexists d'0 D'0 , Hexists n',
      t ~> hinv L s d'0 def D'0 n' true \* [length D'0 = new_capa :> Z] \* H)).
      skip.
    esplit. xextract as L2' EL2' eL2'.
    asserts U: (If s = 2^^n
                then (new_capa = 2 ^^ (n + 1)  /\ new_capa > 4)
                else s = 2 ^^ (n + 2) /\ new_capa = 2 ^^ (n + 3)).
      case_If.
        destruct Is.
          invert H as E1 E2. rewrite E2. false. rewrite E1 in C.
           lets: pow_eq_inv C. math.
          invert H as E1 E2. invert E2 as E2 E3. split.
            rewrite E2. auto.
            auto.
        destruct Is.
          auto.
          auto~.
    clear Is.
    xchange (hinv_unfocus t (If s = 2 ^^ n then n - 1 else n + 1) true L).
      case_If.
        invert U as U1 U2. constructor~.
          intros. rewrite~ eL2'.
          subst. rewrite C. rewrite EL2'. rewrite U1. apply~ pow_ge.
          intros. rewrite C. apply pow_gt. auto~.
          rewrite EL2'. rewrite LD'. rewrite U1.
           (* math_rewrite (n - 1 + 2 = n + 1). *)
           fequals~.
          rewrite U1 in U2. skip_rewrite (4 = 2 ^^ 2) in U2. rewrite pow_gt_eq in U2.
           math. (* lemme math *)
        invert U as U1 U2. constructor~.
          introv E. rewrite~ eL2'.
          rewrite EL2', LD', U2, U1. apply~ pow_ge.
          intros. rewrite U1. apply~ pow_gt.
          skip_rewrite (n + 1 + 2 = n + 3). congruence.
      xsimpl. xsimpl. rewrite <- credits_zero. xsimpl.
       unfold potential. case_If.
         rewrite C. math_rewrite (n - 1 + 1 = n). rew_real.
          rewrite Rabs_pos. rew_real. simpl_ineq. simpl_ineq.
         invert U as Es Ec. rewrite Es. math_rewrite (n + 1 + 1 = n + 2).
          rew_real. rewrite Rabs_pos. rew_real. simpl_ineq. simpl_ineq.
    xsimpl. auto~. 
  destruct Is.
    invert H as E1 E2. subst new_capa. credits_nonneg.
    invert H as E1 E2. invert E2 as E2 E3. subst new_capa. credits_nonneg.
  invert Is; intros (E1&E2); subst s; credits_nonneg.
*)
Admitted. (* faster *)




 (* todo; fix Hexists3 *)

Hint Extern 1 (RegisterSpec resize) => Provide resize_spec.

Lemma potential_pop_one : forall size n,
  (potential (size - 1) n <= potential size n + op_cst)%R.
Admitted.


Parameter ml_div_spec : Spec ml_div (x:int) (y:int) |B>>  
  y <> 0 ->
  B \[] (fun (z:int) => [(z = x / y :> R)%R] ).

Hint Extern 1 (RegisterSpec ml_div) => Provide ml_div_spec.


Axiom R_le_Z_back : forall p q : Z,
  (p <= q)%R ->
  (p <= q)%Z.

Axiom R_eq_Z_back : forall p q : Z,
  (p = q :> R)%R ->
  (p = q :> Z)%Z.



Lemma pop_spec : forall A,
  Spec pop (t:dyn_array A) |B>>
    forall (L:list A) (x:A) {IA:Inhab A},
    B (t ~> DynArray (L&x) \* $ op_cst) (fun y => [y = x] \* (t ~> DynArray L)).
Proof.
(*
  intros. xcf. intros. xunfold DynArray. sets_eq four: 4.
  xextract as s d def D n. xunfold hinv at 1. xextract as ID. 
  destruct ID as [IDs IDi IDf IDc IDc' IDn]. rew_list in IDs. 
  xapps. xname_pre Hcur. xseq Hcur.
  xif as C.
    xfail. math.
    xrets. 
  subst Hcur.
  auto~.
  xapps. xapps. xapps. xapps. xapps~. xapps. xapps. xapps. xapps~.
    intros E. rename _x29 into quarter. rewrite IDc' in E. rewrite EQfour in E.
     skip_rewrite ((2 ^^ (n + 2) / 4%Z = 2 ^^ n)%R) in E. applys_to E R_eq_Z_back.
      subst quarter. 
  credits_join.
  xchange (hinv_unfocus t n false L).
    constructor~.
      introv Isi. rewrite IDi. rew_arr. case_If. false. math. auto. auto~.
      apply potential_pop_one.
  xseq  (# Hexists data' D', Hexists n', t ~> hinv L (s - 1) data' def D' n' true).
  xif as C.
    subst four. xapps~. intros f. xapps.
      right. split.
        invert C as ID Is. 
         applys_to Is R_le_Z.  
         applys_to Is R_le_Z_back. specializes IDc ID. math.
        split.
          apply R_eq_Z_back. rewrite f. rewrite IDc'.
           skip_rewrite ((2 ^^ (n + 2) / 2)%R = 2 ^^ (n + 1))%R. auto.
          skip. (* maths *)
      xsimpl.
    xret. xchange (hinv_strengthen t). intros C2. invert C as C. false. math.
         xsimpl.
  intros data' D' n'.
  xrets. rewrite IDi. rew_arr. case_If. auto. false. math. auto~.
*)
Admitted. (* faster *)

(* todo: xchanges *)

(* 
let pop t = 
   if t.size = 0
       then invalid_arg "pop";
   t.size <- t.size - 1;
   let x = t.data.(t.size) in
   let capa = Array.length t.data in
   if (capa >= 4) && (t.size <= capa/4)
      then resize t (capa / 2);
   x

*)

Hint Extern 1 (RegisterSpec pop) => Provide pop_spec.

(*
Axiom bar: forall b c, Rabs (b + 1 - c) <= Rabs (b - c) + 1.
*)

Lemma potential_push_one : forall size n,
  (potential (size + 1) n <= potential size n + op_cst)%R.
Admitted.

Axiom pow_plus : forall p, 2 * 2 ^^ p = 2 ^^ (p + 1).


Lemma push_spec : forall A,
  Spec push (t:dyn_array A) (x:A) |B>>
    forall {IA:Inhab A} (L:list A),
    B (t ~> DynArray L \* $ op_cst) (# t ~> DynArray (L&x)).
Proof.
(*
  xcf. intros. xunfold DynArray. sets_eq two: 2. xextract as size data def D n.
  xunfold hinv at 1. xextract. introv IL. xapps. xapps. xapps. 
  sets foo: ($ op_cst).
  xchange* (hinv_unfocus t). pot_simpl. 
  xseq (Hexists data' D', Hexists n', t ~> hinv L size data' def D' n' true
         \* foo \* [len D' > size]). 
    lets IL2: IL. destruct IL as [InL Ini Inx Inm Inc Inn]. xif.
      subst two. xchange (hinv_weaken t). xapps~. left. split. auto~. rewrite Inc.
       math_rewrite (n + 3 = n + 2 + 1). apply pow_plus.
       (* TODO: faire un lemme *)
       xsimpl~. skip: (2 ^^ (n+2) > 0). math.
      xret. xsimpl~.
    intros t' D' n' I1. xunfold hinv at 1. xextract as ID'.
    destruct ID' as [IL's IL'i IL'f IL'c IL'v IL'n].
    xapps. xapps. xseq. xapps~. xapps. xapps. subst foo. credits_join.
    xchange (hinv_unfocus t n'); [ | | xsimpl ]. 

 constructor~.
    rew_list~.
    introv Ii. rew_arr. case_If; case_If; auto~.
    rew_arr~.
    rew_arr~. intros. specializes~ IL'c H.
    rew_arr~.
    apply potential_push_one.
*)
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec push) => Provide push_spec.


Lemma pushn_spec : forall A,
  Spec pushn (t:dyn_array A) (x:A) (n:int) |B>>
    forall {IA:Inhab A} (L L':list A),
    B (t ~> DynArray L \* $ op_cst)
      (# t ~> DynArray L'
       \* [forall i, index (len L) i -> L[i] = L'[i]]
       \* [forall i, index (n) i -> L'[i + len L] = x]
       \* [len L' = len L + n]).
Proof.
  xcf. intros. xfor.
Admitted.


(*
  Spec ml_array_make (n:int) (v:A) |R>> 
    forall {IA:Inhab A}, n >= 0 ->
     R \[] (fun t => Hexists L, t ~> Arr L \* 
     [len L = n /\ (forall i, index n i -> L[i] = v)]).
*)

Section Rlemmas3.
Implicit Types a b c : R.
Open Scope R_scope.

Axiom R_int_six :
  (6%Z) = 6 :> R.

End Rlemmas3.



Lemma make_spec : forall A,
  Spec make (def:A) |B>>
    forall {IA:Inhab A},
    B ($ (4 * array_make_cst + 2 * op_cst)) (fun t => t ~> DynArray nil).
Proof.
(*
  xcf. intros. credits_split. sets foo: ($ (2 * op_cst)). xapps.
    auto~. rewrite <- R_int_four. simpl_ineq.
  intros D (LD&VD).
  xapps. intros t. lets (X&D'&E): get_last D.
    intros E. lets LD': LibList.length_nil A. subst D. math.
  subst foo. xchange~ (hinv_unfocus t 0 true nil).
    subst D. constructor~.
      introv Ii. false. math. 
    unfold potential. asserts_rewrite (0%Z - 2 ^^ (0 + 1) = - 2)%R.
     rew_real_to_int. auto.
     rewrite Rabs_opp_pos.
       simpl_ineq. rew_real_to_int. auto~.
  xchange (DynArray_unfocus t). xsimpl.
*)
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec make) => Provide make_spec.

(*
Lemma hinv_unfocus : forall t n b_min (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D,
  inv L size D n b_min ->
  (t ~> RecDynArray default size data \* data ~> Arr D \* $ potential size n)
     ==> (t ~> hinv L size data default D n b_min).

Record inv (A:Type) {IA:Inhab A} (L:list A) size (D:list A) (n:int) (b_min:bool) :=
build_inv {
  inv_lenL: size = len L;
  inv_items: forall i, index size i -> D[i] = L[i];
  inv_max: len D >= size;
  inv_min: if b_min then (len D > 4 -> size > 2^^n) else True;
  inv_capa: len D = 2^^(n + 2);
  inv_n: n >= 0 }.

*)

Lemma is_empty_spec : forall A,
  Spec is_empty t |B>>
    forall {IA:Inhab A} (L:list A),
    keep B (t ~> DynArray L) (fun (x:bool) => [x = isTrue (L = nil)]).
Proof.
  xcf. introv. xapps. xrets. apply length_nil_eq.
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.

Lemma get_spec : forall A,
  Spec get (t:dyn_array A) (i:int) |B>>
    forall {IA:Inhab A} (L:list A), index (len L) i ->
    keep B (t ~> DynArray L)
      (fun y:A => [y = L[i]]).
Proof.
  (* lets: get_cf. ==> alt+maj+A *)
  xcf. introv Hi. xunfold DynArray. xextract as s d def D n. hunfold hinv at 1. 
  xextract as IL. xapps. xif.
  xapps. xapps. invert~ IL. intros r. xextract. intros.
   xchange* (@hinv_unfocus t). simpl_ineq.
   xsimpl. subst r. destruct IL as [ELs ELD IDs ID]. apply~ ELD.
  xfail. invert Hi as a c. invert IL.  intros. invert C; intros; math.
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec get) => Provide get_spec.

(* Hint Resolve list_neq_len. *)

(* problème: le code n'est pas polymorphe
Lemma compare_spec : forall A,
  Spec compare (t1:dyn_array A) (t2:dyn_array A) |B>>
    forall {IA:Anhab A} (L1 L2:list A), (* len L1 = len L2 -> *)
    keep B (t1 ~> DynArray L1 \* t2 ~> DynArray L2)
      (fun b:bool => [b = isTrue (L1 = L2)]).
*)

(* TOD0 moyen facile(4) : spec de seq, et énoncé des invariants de boucles 

(xframe (nb * $ op_cst) juste après le xseq qui sépare les 2 boucles for,
et juste après le credits_split (nb*op_cst + nb*opcst))
*)

Axiom R_mul_two : forall a,
  2* a = a + a.

Axiom R_eq_Z_eq : forall p q : Z,
  (p = q :> Z)%Z = 
  (p = q :> R)%R.

Hint Rewrite <- R_eq_Z_eq : rew_real_to_int.


Lemma seq_spec :
  Spec seq (n:int) |B>>
    B ($ (2 * (n + 1) * op_cst + 4 * array_make_cst)) (#\[]).
Proof.
  xcf. intros.
  rewrite R_distrib_add_mul. rewrite R_mul_one.
  math_rewrite ((n + 1 + (n + 1))%R = 2 * n + 2). rew_real_to_int. math.
  rew_real_int. rewrite R_distrib_add_mul.
  skip: (n >=0)%R.
  credits_split. sets foo: ($(2 * n * op_cst))%R. credits_join.
  xapps; try credits_nonneg. subst foo.
  xseq. skip_rewrite (2 * n * op_cst = n * op_cst + n * op_cst)%R.
  credits_split. xframe ($ (n * op_cst)).
  xfor.
  cuts M: (forall i,
    S i (Hexists L, [0 <= i <= n] \*
          $ ((n - i) * op_cst) \*
          t ~> DynArray L \*
          [len L = i] \*
          [(forall k, index i k -> L[k] = k)])
        (# Hexists L,
          t ~> DynArray L \*
          [len L = n] \*
          [(forall k, index n k -> L[k] = k)])).
    xapply M.
      xsimpl.
        rew_real_int. rew_real. simpl_ineq.
        introv Ik. invert Ik. introv Ik1 Ik2. math.
        rew_list~.
        rew_real_to_int in H. math.
      xok.
    intros. induction_wf IH: (int_upto_wf (n+1)) i. apply HS. split.
      introv Iin. xextract as L Iin' EL InvL.
       asserts_rewrite ((n - i) * op_cst = (n - i - 1) * op_cst + op_cst)%R.
         sets a: (n-i-1)%R.
         asserts_rewrite (n - i = (n - i - 1) + 1)%R.
           rew_real_to_int. math.
         subst a. rew_real. auto. 
      credits_split.
      xapp.
        xapply IH.
          unfold upto. auto~. 
          xsimpl.
            rew_real_int. apply R_le_mul_pos_r. credits_nonneg.
              rew_real_to_int. math.
            introv Iik. rew_arr. case_If. auto~. apply InvL. auto~. 
            rew_list~.
            math.
          xsimpl.
            introv Ink. apply~ H2.
            auto.
            
       (* todo: *)
       asserts: (n - i - 1 >= 0)%R. rew_real_to_int. math. credits_nonneg.
     
    (* todo: finir *)
       introv Iin. xrets. rename x into L. introv Ink. apply H3.
        inverts Ink. auto~. auto~.
    xok.
  (* c'est similiaire à la boucle d'avant *)
  xextract as L ELn Ink.
  xfor.
  cuts M: (forall i,
    S i (Hexists L, [0 <= i <= n] \*
          $ ((n - i) * op_cst) \*
          t ~> DynArray L \*
          [len L = n - i] \*
          [(forall k, index (n - i) k -> L[k] = k)])
        (# \[])
        ).
    xapply M.
      xsimpl~. asserts_rewrite ((n - 0%Z)%R = n%R). rew_real_int. rew_real~. simpl_ineq.
      xsimpl.
      (* au début mon invariant était :
        cuts M: (forall i,
          S i (Hexists L, [0 <= i <= n] \*
              $ ((n - i) * op_cst) \*
              t ~> DynArray L \*
              [len L = n - i] \*
              [(forall k, index (n - i) k -> L[k] = k)])
            (# t ~> DynArray nil)
            ).
         mais j'avais un but de cette forme : #t ~> DynArray nil \*+ \[] ===> #\[]
         parce que dans la spec de seq, l'état final est vide et que je ne peux pas y
         mentionner t. Donc j'ai corrigé mon invariant.
      *)
    intros. induction_wf IH: (int_upto_wf (n+1)) i. apply HS. split.
      introv Iin. xextract as L' Iin' EL'.
       asserts_rewrite ((n - i) * op_cst = (n - i - 1) * op_cst + op_cst)%R.
         rew_real.
          asserts_rewrite ((n * op_cst - i * op_cst - op_cst + op_cst)%R
                     = (n * op_cst - i * op_cst)%R).  rew_real. skip. (* pb preuve math *) auto.
         credits_split. introv Ink'. lets (X&L''&E): get_last L'.
          unfolds. introv Lnil. lets Lem: (LibList.length_nil int).
          rewrites <- Lnil in Lem. math. rewrite E. 
         xseq.
           xapps. xif.
             xfail.
              specializes Ink' (n - 1 - i). asserts Ini: (index (n - i) (n - 1 - i)).
              auto~. specializes Ink' Ini.
              rewrite E in Ink'. rew_arr in Ink'.
              case_if in Ink'. rewrite E in EL'. rew_list in EL'. math.
             xrets.
           xapply IH.
             auto~.
             xsimpl.
               rew_real_int. rew_real. simpl_ineq.
               intros. rewrite E in Ink'. lets M: Ink' k __.
                 math. rew_arr in M. case_if in M.
                   false. asserts: (length L'' + 1 = length L'). 
                      subst L'. rew_list. math. math.
                   auto.
                (* TODO voir ci dessus comment j'ai fait --> j'aimerais appliquer rew_arr sur Ink' mais je ne sais pas comment *)
               rewrite E in EL'. rew_list~ in EL'.
               auto~.
             xok.
           asserts Iin'': (((n - i - 1) >= 0)%R). rew_real_to_int. auto~.
            credits_nonneg.
      intros. xret. hcancel.
      (* bloqué, et je ne comprends pas le problème 
         --> c'était encore le problème que les variables ?XXX
             ne s'unifie pas lorsque leur contexte a grandie. 
             j'ai utilisé une astuce mais c'est pas propre
              (et tu ne pouvais pas deviner, c'est sûr). *)



(*

let seq n =
  let t = make 0 in
  for i = 0 to n - 1 do
    push t i
  done;
  for i = 0 to n - 1 do
    let x = pop t in
    if x <> n - 1 - i then assert false
  done

*)

Admitted.

Lemma compare_spec : 
  Spec compare (t1:dyn_array int) (t2:dyn_array int) |B>>
    forall (L1 L2:list int), (* len L1 = len L2 -> *)
    keep B (t1 ~> DynArray L1 \* t2 ~> DynArray L2)
      (fun b:bool => [b = isTrue (L1 = L2)]).
Proof.
  xcf. intros. xapps. xapps. xif. xrets. xclean. apply~ list_neq_len.
  xapps. xapps. xseq. xwhile.
    cuts M: (forall vi vb,
     R ([0 <= vi <= len L1
         /\ vb = isTrue (forall k, index vi k -> L1[k] = L2[k])] \*
        i ~~> vi \*
        b ~~> vb \*
        t1 ~> DynArray L1 \*
        t2 ~> DynArray L2) 
       (# b ~~> isTrue (L1 = L2) \*
        t1 ~> DynArray L1 \*
        t2 ~> DynArray L2
      )).
    xapply M. hsimpl. split~. xclean. introv Ik. invert Ik. introv Ik1 Ik2.
     false. math. xok.
    skip_goal.
    intros. applys (rm HR). xextract as (Hi&Hb).
(* TODO moins facile(5): généraliser les invariants   puis (7)    xpay_simulate. *)
    xlet. xapps. xapps. xapps. xret.
    simpl. xextracts. xif.
      xseq (Hexists vi vb, 
        [0 <= vi <= len L1 /\ vb = isTrue (forall k, index vi k -> L1[k] = L2[k])] \*
          i ~~> vi \*
          b ~~> vb \*
          t1 ~> DynArray L1 \*
          t2 ~> DynArray L2) .
        xseq. xapps. intros i2. xextract as Ei. subst i2. xapps. constructor~.
        xapps. xapps. rewrite C in Hi. constructor~.
          xseq (Hexists vb, [vb = isTrue (forall k, index (vi + 1) k -> L1[k] = L2[k])]
                             \* i ~~> vi
                             \* b ~~> vb
                             \* t1 ~> DynArray L1
                             \* t2 ~> DynArray L2).
          xif. xapps. hsimpl. xclean. intros f. lets F': f vi __. math. false. 
          xrets. destruct C0. rewrite H in *. xclean. introv Ivi. tests Ek: (k = vi).
           auto. apply Hb. math.
          intros vb' Ivb. xapps. hsimpl. split. math. auto. 
        intros. xapply IH. hsimpl. auto. hsimpl.
      xrets. subst vb. extens. iff M. applys~ isTrue_eq_list. invert C0 as N. xclean.
       intros. false. asserts_rewrite~ (len L1 = vi). subst~.
  xapps. hsimpl~.
Admitted. (* faster *)

(* TODO: (1000): faire une version de compare avec une précondition 
  de crédits égaux au nb de cases identiques aux deux tableaux
    L1 L2 n
   (If len L1 <> len L2 then n = 0
    else 
       (forall i, index n i -> L1[i] = L2 [i])
    /\ (n = len L1 \/ (0 <= n < len L1 /\ L1[n] <> L2[n])))
*)

(* TODO (6): 
  coût de l'opération : t2.size * (pay_cst + 2 * op_cst) 
ajouter le xpay_simulate.. juste après applys (rm HR).
(éventuellment après un xextract *)

Lemma rev_append_spec : forall A,
  Spec rev_append (t1:dyn_array A) (t2:dyn_array A) |B>>
    forall {IA:Inhab A} (L1 L2:list A), 
    B (t1 ~> DynArray L1 \* t2 ~> DynArray L2)
      (# t1 ~> DynArray (L1 ++ rev L2)).
Proof. (*
  xcf. introv. xwhile. xname_post QF.
  cuts M: (forall L1' L2',  L1 ++ rev L2 = L1' ++ rev L2' ->
         R (t1 ~> DynArray L1' \* t2 ~> DynArray L2') QF).
  xapply M; [ | xsimpl | xsimpl ]. auto~.
  skip_goal.
  introv ELs. applys (rm HR). xlet. xapps. xrets. xextract as E. xif.
  subst QF. lets (X&L2''&EL2'): get_last L2'. auto~. subst.
  xapps. intro_subst. xapps. xapply IH; [ | xsimpl| ]. rew_list in *. auto. xsimpl. 
  xrets. subst QF. xsimpl. subst L2'. rew_list~ in *. 
(* cuts M: (R (Hexists L3, t1 ~> DynArray (L1 ++ rev L3)) QF). *)
(* je savais pas trop quoi faire... j'ai essayé de m'inspirer
   des deux spec ci-dessous *)


(* todo: faire un dessin décrivant la situation à une étape arbitraire de la boucle,
   à laquelle on trouve des L1' et des L2' dans t1 et t2, et  
   et exprimer des égalités bien senties entre : L1, L2, L2' et L2'.

   L'énoncé à prouver est de la forme: 
     cuts M: (forall L1' L2',  L1 ++ rev L2 = L1' ++ rev L2' ->
         (t1 ~> DynArray L1' \* t2 ~> DynArray L2') QF).
*) *)
Admitted. (* faster *)


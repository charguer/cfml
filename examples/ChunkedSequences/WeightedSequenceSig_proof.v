Set Implicit Arguments.
Require Import CFLib Weighted_ml Weighted_proof WeightedSequenceSig_ml LibCore.
Require Import Common_proof.
Open Scope heap_scope_advanced.

Module Type WeightedSequenceSigSpec.

Declare Module Q : WeightedSequenceSig_ml.MLS.
Import Q.

Definition list_sum (A:Type) (W:A->int) (L:list A) : int :=
  LibList.fold_left (fun x acc => acc + W x) 0 L.

Definition witem (a:Type) := loc.

Parameter Seq : forall (A a : Type),
  (A->int) -> (A->a->hprop) -> list A -> Q.t a -> hprop.

Parameter weight_spec : forall a A,
  forall (W:A->int) (R:A->a->hprop), forall L,
  Spec weight (q:Q.t a) |B>> 
     keep B (q ~> Seq W R L) (\= list_sum W L).

Parameter create_spec : forall a A,
  Spec create () |B>> 
    forall (W:A->int) (R:A->a->hprop),
     B \[] (fun q => q ~> Seq W R nil).

Parameter is_empty_spec : forall a A,
  Spec is_empty (q:Q.t a) |B>>  
     forall (W:A->int) (R:A->a->hprop), forall L,
     keep B (q ~> Seq W R L) (\= isTrue (L = nil)).

Parameter push_front_spec : forall a A,
  Spec push_front (x:witem a) (q:Q.t a) |B>>  
     forall (W:A->int) (R:A->a->hprop), forall X L,
     B (q ~> Seq W R L \* x ~> Weighted W R X) (# q ~> Seq W R (X::L)).

Parameter push_back_spec : forall a A,
  Spec push_back (x:witem a) (q:Q.t a) |B>>  
     forall (W:A->int) (R:A->a->hprop), forall X L,
     B (q ~> Seq W R L \* x ~> Weighted W R X) (# q ~> Seq W R (L&X)).

Parameter pop_front_spec : forall a A, 
  Spec pop_front (q:Q.t a) |B>>  
     forall (W:A->int) (R:A->a->hprop), forall X L,
     B (q ~> Seq W R (X::L)) (fun x => x ~> Weighted W R X \* q ~> Seq W R L).

Parameter pop_back_spec : forall a A, 
  Spec pop_back (q:Q.t a) |B>>  
     forall (W:A->int) (R:A->a->hprop), forall X L,
     B (q ~> Seq W R (L&X)) (fun x => x ~> Weighted W R X \* q ~> Seq W R L).

Parameter append_spec : forall a A, 
  Spec append (q1:Q.t a) (q2:Q.t a) |B>>  
     forall (W:A->int) (R:A->a->hprop), forall L1 L2,
     B (q1 ~> Seq W R L1 \* q2 ~> Seq W R L2) (# q1 ~> Seq W R (L1 ++ L2)).

Parameter carve_back_at_spec : forall a A,  
  Spec carve_back_at (w:int) (q:Q.t a) |B>>  
     forall (W:A->int) (R:A->a->hprop), forall L, 0 <= w <= list_sum W L ->
     B (q ~> Seq W R L) (fun p => let '(x,q2) := p in
       Hexists L1 L2, Hexists (X:A),
       \[L = L1 ++ X :: L2] \* \[list_sum W L1 < w <= list_sum W (L1&X)] 
       \* q ~> Seq W R L1 \* x ~> Weighted W R X \* q2 ~> Seq W R L2).

Parameter iter_spec : forall a A,
  Spec iter (f:func) (q:Q.t a) |B>>  
    forall L (W:A->int) (R:A->a->hprop) (I:list A->hprop),
     (forall x X K, Mem X L -> App f x; (I K \* x ~> R X) (# I (K&X) \* x ~> R X)) ->
     B (q ~> Seq W R L \* I nil) (# q ~> Seq W R L \* I L).

Parameter front_spec : forall a A, 
  Spec front (q:Q.t a) |B>>  
     forall (W:A->int) (R:A->a->hprop), forall X L,
     B (q ~> Seq W R (X::L)) (fun x => x ~> Weighted W R X \*==>* q ~> Seq W R (X::L)).

Parameter back_spec : forall a A, 
  Spec back (q:Q.t a) |B>>  
     forall (W:A->int) (R:A->a->hprop), forall X L,
     B (q ~> Seq W R (L&X)) (fun x => x ~> Weighted W R X \*==>* q ~> Seq W R (L&X)).


Hint Extern 1 (RegisterSpec create) => Provide create_spec.
Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.
Hint Extern 1 (RegisterSpec weight) => Provide weight_spec.
Hint Extern 1 (RegisterSpec push_front) => Provide push_front_spec.
Hint Extern 1 (RegisterSpec push_back) => Provide push_back_spec.
Hint Extern 1 (RegisterSpec pop_front) => Provide pop_front_spec.
Hint Extern 1 (RegisterSpec pop_back) => Provide pop_back_spec.
Hint Extern 1 (RegisterSpec append) => Provide append_spec.
Hint Extern 1 (RegisterSpec carve_back_at) => Provide carve_back_at_spec.
Hint Extern 1 (RegisterSpec iter) => Provide iter_spec.
Hint Extern 1 (RegisterSpec back) => Provide back_spec.
Hint Extern 1 (RegisterSpec front) => Provide front_spec.

End WeightedSequenceSigSpec.

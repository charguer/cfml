Set Implicit Arguments.
Require Export Rbase Rfunctions.
Require Import CFLib LibSet LibMap LibArray LibInt.
Require Import CapacityParamSig_ml.
Require Import SequenceSig_ml SequenceSig_proof.
Require Import ChunkedSeq_ml.
Require Import Common_proof.

Open Scope R_scope.
Open Scope Z_scope.
Open Scope list_scope.
Open Scope heap_scope_advanced.
Require Import LibOrder.

(*------------------------------------------------------*)
(** instantiations *)

Module MakeSpec
   (Chunk : SequenceSig_ml.MLFixedCapacityS)
   (ChunkSpec : SequenceSigFixedCapacitySpec with Module Q':=Chunk)
   (Middle : SequenceSig_ml.MLS) 
   (MiddleSpec : SequenceSigSpec with Module Q:=Middle).

Module Import Q := MLMake Middle Chunk.

Module MiddleSpecOwn := SequenceSigOwnSpec_from_SigSpec (Middle)(MiddleSpec).


(*------------------------------------------------------*)
(** representation predicates *)

Notation "'RChunk'" := (@ChunkSpec.Seq _).

Notation "'RMiddle'" := (MiddleSpecOwn.Seq RChunk).

Notation "'TF' fo fi mid bi bo" :=
  (Q.T RChunk RChunk RMiddle RChunk RChunk fo fi mid bi bo)
  (fo at level 0, fi at level 0, mid at level 0, bi at level 0, bo at level 0, at level 30).

Notation "'TU_'" := (T Id Id Id Id Id).

Notation "'length'" := (fun L => my_Z_of_nat (LibList.length L)).

(*------------------------------------------------------*)
(** invariants *)

Definition capa := Chunk.capacity.

Definition full (A:Type) (L:list A) :=
  length L = capa. 

Definition middle_conseq_ok (A:Type) (mid:list(list A)) :=
  ForallConseq (fun c1 c2 => length c1 + length c2 > capa) mid.

Definition chunk_ok {A:Type} (L:list A) :=
  0 < length L <= capa.

Definition middle_chunks_ok (A:Type) (mid:list(list A)) :=
  Forall chunk_ok mid.

Record inv (A : Type) (L fo fi : list A) mid bi bo := build_inv {
  inv_items : L = fo ++ fi ++ concat mid ++ bi ++ bo;
  inv_front_fill : fi = nil \/ full fi;
  inv_back_fill : bi = nil \/ full bi;
  inv_middle_chunks : middle_chunks_ok mid;
  inv_middle_conseq : middle_conseq_ok mid }.

Definition item_potential := 
  ((ChunkSpec.create_cst + MiddleSpecOwn.op_cst) / capa)%R.

Definition side_potential (A:Type) (ko ki : list A) : R :=
  If ki = nil then 0%R else (length ko * item_potential)%R.

Definition potential (A:Type) (fo fi bi bo : list A) :=
  (side_potential fo fi + side_potential bo bi)%R.

Definition Seq (A : Type) (L : list A) (q : loc) :=
  Hexists fo fi mid bi bo,
    q ~> TF fo fi mid bi bo \* 
    $(potential fo fi bi bo) \*
    \[inv L fo fi mid bi bo].


(*------------------------------------------------------*)
(** tactics *)

Ltac pot_simpl := 
  unfold potential; try simpl_credits_core; unfold side_potential; 
  repeat case_if; rew_length; try comp_simpl.

Ltac lmath :=
  unfolds chunk_ok, capa; try rew_length in *; math.

Hint Unfold middle_chunks_ok middle_conseq_ok.
Hint Constructors Forall ForallConseq.
Hint Resolve Forall_last Forall_app.
Hint Extern 10 (_ = _ :> list _) => subst; rew_list.
Hint Extern 1 ((_ > _)) => lmath.
Hint Extern 1 ((_ >= _)) => lmath.
Hint Extern 1 ((_ <= _)) => lmath.
Hint Extern 1 ((_ > _)%Z) => lmath.
Hint Extern 1 ((_ >= _)%Z) => lmath.
Hint Extern 1 ((_ <= _)%Z) => lmath.
Hint Extern 1 (chunk_ok _) => lmath.


(*------------------------------------------------------*)
(** auxiliary definitions *)

Definition merge_cost :=  
  (MiddleSpecOwn.op_cst + MiddleSpecOwn.op_cst + ChunkSpec.append_cst)%R.

Definition inv_middle A L (mid:list (list A)) :=
  concat mid = L /\ middle_chunks_ok mid /\ middle_conseq_ok mid.


(*------------------------------------------------------*)
(** lemmas *)

Lemma inv_middle_from_inv : forall A (L:list A) fo fi mid bi bo,
  inv L fo fi mid bi bo -> inv_middle (concat mid) mid.
Proof. introv [MI MF MB MN MC]. splits~. Qed.

Hint Resolve inv_middle_from_inv.

Ltac math_0 ::= hint ChunkSpec.capacity_spec. (* capa > 0 *)

Lemma not_full_nil : forall A,
  ~ full (@nil A).
Proof. introv H. unfolds in H. rew_list in *. lmath. Qed.

Hint Resolve not_full_nil.

Lemma item_potential_nonneg : (item_potential >= 0)%R.
Proof.
  unfolds item_potential. apply R_div_pos.
  ineq_rew <- ChunkSpec.create_cst_nonneg. 
  ineq_rew <- MiddleSpec.op_cst_nonneg.
  ineq_simpl. rewrite <- R_int_zero. apply~ R_gt_Z.
Qed.

Hint Resolve item_potential_nonneg.

Lemma side_potential_nonneg : forall A (ko ki : list A),
  (side_potential ko ki >= 0)%R.
Proof. intros. unfold side_potential. case_if; comp_simpl. Qed.

Hint Resolve side_potential_nonneg : credits_nonneg.

Lemma potential_nonneg : forall A (fo fi bi bo : list A),
  (potential fo fi bi bo >= 0)%R.
Proof. intros. unfold potential. credits_nonneg. Qed.

Hint Resolve potential_nonneg : credits_nonneg.

Lemma concat_middle_eq_nil_inv : forall A (mid:list (list A)),
  concat mid = nil -> middle_chunks_ok mid -> mid = nil.
Proof.
  introv E N. unfolds in N. destruct mid as [|L mid']. auto.
  rew_concat in E. forwards [E1 E2]: app_eq_nil_inv (rm E).
  subst. inverts N. lmath. 
Qed.

Lemma middle_conseq_push_front_full : forall A (fi:list A) mid,
  middle_conseq_ok mid -> 
  middle_chunks_ok mid ->
  full fi -> 
  middle_conseq_ok (fi :: mid).
Proof. 
  introv M K N. unfolds in M N K.
  destruct mid as [|f mid]; inverts K; constructors~.  
Qed.

Lemma middle_conseq_push_back_full : forall A (fi:list A) mid,
  middle_conseq_ok mid -> 
  middle_chunks_ok mid ->
  full fi -> 
  middle_conseq_ok (mid & fi).
Proof. 
  introv M K N. unfolds in M N K.
  destruct (case_last mid) as [|(LB&mid'&Emid)]; subst.
    rew_list~.
    lets (?&?): Forall_last_inv K. applys* ForallConseq_last.
Qed.

Lemma middle_conseq_cons : forall A (LC:list A) mid,
  middle_conseq_ok (LC::mid) -> 
  middle_conseq_ok mid.
Proof. 
  introv M. inverts M as. constructors.
  introv N M. inverts M; constructors*.
Qed.

Lemma middle_conseq_tail : forall A (LC:list A) mid,
  middle_conseq_ok (mid&LC) -> 
  middle_conseq_ok mid.
Proof. 
  introv M. destruct (case_last mid) as [|(LB&mid'&Emid)]; subst.
  constructors.
  lets (?&E): ForallConseq_last_inv M. applys E.
Qed.

Hint Resolve middle_conseq_cons middle_conseq_tail.

Lemma middle_conseq_push_front_merged : forall A mid (L1 L2:list A),
  middle_conseq_ok (L2 :: mid) ->
  length L1 + length L2 <= capa ->
  middle_conseq_ok ((L1 ++ L2) :: mid).
Proof.
  introv M HC. inverts M as E M; constructors~.
Qed.

Lemma middle_conseq_push_back_merged : forall A mid (L1 L2:list A),
  middle_conseq_ok (mid & L1) ->
  length L1 + length L2 <= capa ->
  middle_conseq_ok (mid & (L1 ++ L2)).
Proof.
  introv M HC. destruct (case_last mid) as [|(LB&mid'&Emid)]; subst.
  constructors.
  lets (?&?): ForallConseq_last_inv M.
  applys~ ForallConseq_last. 
Qed.

Lemma inv_middle_push_front_full : forall A mid (ci:list A),
  inv_middle (concat mid) mid ->
  full ci ->
  inv_middle (ci ++ concat mid) (ci :: mid).
Proof. 
  introv (M1&M2&M3) Fi. unfolds full. splits~.
    applys* middle_conseq_push_front_full.
Qed.

Lemma inv_middle_push_front_full_and_nonempty : forall A mid ci (co:list A),
  inv_middle (concat mid) mid ->
  full ci -> 0 < length co <= capa ->
  inv_middle (co ++ ci ++ concat mid) (co :: ci :: mid).
Proof.
  introv (M1&M2&M3) Fi Lo. unfolds full. splits~.
    forwards*: middle_conseq_push_front_full M3 Fi.
Qed.

Lemma inv_middle_push_back_full : forall A mid (ci:list A),
  inv_middle (concat mid) mid ->
  full ci ->
  inv_middle (concat mid ++ ci) (mid & ci).
Proof. 
  introv (M1&M2&M3) Fi. unfolds full. splits~.
    applys* middle_conseq_push_back_full.
Qed.

Lemma inv_middle_push_back_full_and_nonempty : forall A mid ci (co:list A),
  inv_middle (concat mid) mid ->
  full ci -> 0 < length co <= capa ->
  inv_middle (concat mid ++ ci ++ co) (mid & ci & co).
Proof.
  introv (M1&M2&M3) Fi Lo. unfolds full. splits~.
    forwards*: middle_conseq_push_back_full M3 Fi.
     applys~ ForallConseq_last.
Qed.

Lemma list_not_nil_length_pos : forall A (L:list A),
  L <> nil -> length L > 0.
Proof. intros. destruct L; rew_length. false. math. Qed.

Lemma potential_shift_front : forall (A:Type) (fo fi bo bi : list A),
  fi <> nil -> full fo ->
  (potential fo fi bi bo >=
   potential nil fo bi bo + ChunkSpec.create_cst + MiddleSpecOwn.op_cst)%R.
Proof.
  introv Fi Fo. unfold potential. 
  asserts_rewrite (side_potential nil fo = 0). pot_simpl; auto.
  comp_simpl. unfold side_potential, item_potential. rewrite Fo. pot_simpl.
Qed.

Lemma potential_shift_back : forall (A:Type) (fo fi bo bi : list A),
  bi <> nil -> full bo ->
  (potential fo fi bi bo >=
   potential fo fi bo nil + ChunkSpec.create_cst + MiddleSpecOwn.op_cst)%R.
Proof.
  introv Bi Bo. unfold potential. 
  asserts_rewrite (side_potential nil bo = 0). pot_simpl; auto.
  comp_simpl. unfold side_potential, item_potential. rewrite Bo. pot_simpl.
Qed.


(*------------------------------------------------------*)
(** verification *)

Lemma create_spec : forall A,
  Spec create () |B>> 
     B ($ (5%nat * ChunkSpec.create_cst)) (fun q => q ~> Seq (@nil A)).
Proof.
  xcf. do 4 rewrite R_succ_nat. rewrite R_mul_one_nat.
  credits_split. xapp. xapp. xapp. xapp. xapp. xapp.
  intros r. hchange (@T_unfocus r).
  hunfold Seq. hsimpl. pot_simpl. constructor*.
Qed.

Lemma is_empty_spec : forall A,
  Spec is_empty (q:Q.t A) |B>>  
     forall (L:list A),
     keep B (q ~> Seq L) (\= isTrue (L = nil)).
Proof.
  xcf. intros. hunfold Seq.
  xextract as fo fi mid bi bo Inv.
  xchange (@T_focus s) as pbi pbo pfi pfo pmid.
  xapps. xapps. xapps. xapps. xapps. xapps. 
  xapps. xapps. xapps. xapps. 
  xchange (@T_unfocus s). xret. hsimpl. auto.
  extens; rew_refl. 
  destruct Inv as [MI MF MB MN MC]. split.
  intros (E1&E2&E3&E4&E5). subst. rew_list~. intros M. subst. 
  list_eq_nil_inv M. forwards*: concat_middle_eq_nil_inv.
Qed.

Lemma push_front_spec : forall A,
  Spec push_front (X:A) (q:Q.t A) |B>>  
     forall L,
     B (q ~> Seq L \* $ (ChunkSpec.op_cst + item_potential))
       (# q ~> Seq (X::L)).
Proof.
  xcf. intros. hunfold Seq at 1. xextract as fo fi mid bi bo Inv.
  xapp_by focus. xapps. 
  xseq. xframe ($ (ChunkSpec.op_cst + item_potential)).
  xif (# Hexists (fo2 fi2 : list A), Hexists mid2,
    s ~> TF fo2 fi2 mid2 bi bo 
    \* \[inv L fo2 fi2 mid2 bi bo]
    \* $ potential fo2 fi2 bi bo 
    \* \[~ full fo2]).
  (* case full *) 
  xapp_by focus. xapp_by unfocus. xapps. xif.
  (* case inner empty *)
  xapp_by unfocus. hsimpl~. pot_simpl. destruct Inv. constructor~.
  (* case inner full *)
  tryfalse. xapp_by focus. 
  destruct Inv as [MI MF MB MN MC]. destruct MF; tryfalse.
  forwards Po: potential_shift_front;  
   [ | | xchange (credits_ge Po); [ try apply R_ge_refl | ] ];
   auto~; clear Po.
  credits_split. xapp~. xapp. xapp_by unfocus.
  hchange (@_unfocus_middle s). hsimpl~. constructor~.
    unfolds_hd in *. unfolds full. constructors~.
    applys* middle_conseq_push_front_full.
  (* case not full *)
  xret. hchange (@_unfocus_front_outer s). hsimpl~. 
  (* push front *)
  xok. clears Inv. clears. xextract as fo fi mid Inv Nfo.
  credits_split. xapp_by focus. xapp.
  hchange (@_unfocus_front_outer s). hunfold Seq.
  credits_join. hsimpl. pot_simpl. destruct Inv. constructor~. 
Qed.

Lemma push_back_spec : forall A,
  Spec push_back (X:A) (q:Q.t A) |B>>  
     forall L,
     B (q ~> Seq L \* $ (ChunkSpec.op_cst + item_potential))
       (# q ~> Seq (L&X)).
Proof.
  xcf. intros. hunfold Seq at 1. xextract as fo fi mid bi bo Inv.
  xapp_by focus. xapps. 
  xseq. xframe ($ (ChunkSpec.op_cst + item_potential)).
  xif (# Hexists (bo2 bi2 : list A), Hexists mid2,
    s ~> TF fo fi mid2 bi2 bo2 
    \* \[inv L fo fi mid2 bi2 bo2]
    \* $ potential fo fi bi2 bo2 
    \* \[~ full bo2]).
  (* case full *) 
  xapp_by focus. xapp_by unfocus. xapps. xif.
  (* case inner empty *)
  xapp_by unfocus. hsimpl~. pot_simpl. destruct Inv. constructor~.
  (* case inner full *)
  tryfalse. xapp_by focus. 
  destruct Inv as [MI MF MB MN MC]. destruct MB; tryfalse.
  forwards Po: potential_shift_back;  
   [ | | xchange (credits_ge Po); [ try apply R_ge_refl | ] ];
   auto~; clear Po.
  credits_split. xapp~. xapp. xapp_by unfocus.
  hchange (@_unfocus_middle s). hsimpl~. constructor~.
    unfolds_hd in *. unfolds full. applys~ Forall_last.
    applys* middle_conseq_push_back_full.
  (* case not full *)
  xret. hchange (@_unfocus_back_outer s). hsimpl~. 
  (* push back *)
  xok. clears Inv. clears. xextract as bo bi mid Inv Nfo.
  credits_split. xapp_by focus. xapp.
  hchange (@_unfocus_back_outer s). hunfold Seq.
  credits_join. hsimpl. pot_simpl. destruct Inv. constructor~.
Qed.

Lemma pop_front_spec : forall A,
  Spec pop_front (q:Q.t A) |B>>  
     forall (X:A) L,
     B (q ~> Seq (X::L)) (fun r => \[r = X] \* q ~> Seq L).
Proof.   
  Hint Resolve build_inv.
  xcf. intros. hunfold Seq at 1. xextract as fo fi mid bi bo Inv.
  xapp_by focus. xapps. destruct Inv as [MI MF MB MN MC]. xif. 
  (* case co *)
  destruct fo as [|Y fo2]; tryfalse. rew_list in MI. inverts MI. 
  xapp. hchange (@_unfocus_front_outer s). hunfolds~ Seq. pot_simpl. 
  (* case ci *)
  xapp_by focus. xapp_by focus. xapps. xif.
  xchange (@_unfocus_middle s).
  destruct fi as [|Y fi2]; tryfalse. rew_list in MI. inverts MI. 
  xapp. xapp_by unfocus. xapps. xapp.
  hchange (@_unfocus_front_outer s). hunfolds~ Seq. pot_simpl.
  (* case mid *) 
  xapps. xif. xchange (@_unfocus_front_inner s).
  destruct mid as [|LC mid2]; tryfalse.
  inverts MN. destruct LC as [|Y LC2]. lmath.
  rew_list in MI. inverts MI. xapp. xapp. xapps. xapp.
  hchange (@_unfocus_middle s) (@_unfocus_front_outer s).
  hunfolds* Seq. pot_simpl.
  (* case bo *)
  xchange (@_unfocus_front_inner s). xchange (@_unfocus_middle s).
  xapp_by focus. xapps. xif.
  destruct bi as [|Y bi2]; tryfalse. rew_list in MI. inverts MI. 
  xapps. xapp. xapp_by unfocus. xapps. xapp.
  hchange (@_unfocus_front_outer s). hunfold Seq. hsimpl~. pot_simpl. 
  (* case bi *)
  rew_list in MI. subst bo.
  xchange (@_unfocus_back_inner s). xchange (@_unfocus_front_outer s).
  xapp_by focus. xapp. hchange (@_unfocus_back_outer s).
  hunfold Seq. hsimpl~. pot_simpl.
Qed.

Lemma merge_cost_nonneg : (merge_cost >= 0)%R.
Proof. unfold merge_cost. comp_simpl. Qed.

Hint Resolve merge_cost_nonneg : credits_nonneg.

Lemma middle_merge_back_spec : forall A,
  Spec middle_merge_back (m:Middle.t (Chunk.t A)) (c:Chunk.t A) |B>>  
     forall mid L LC, inv_middle L mid -> 
     B (m ~> RMiddle mid \* c ~> RChunk LC \* $ merge_cost) 
       (# Hexists mid', m ~> RMiddle mid' \* [inv_middle (L++LC) mid']).
Proof.
  xcf. unfold merge_cost. introv (ME&MC&MQ). xapps. xif.
  xchange (ChunkSpec.Seq_le_capacity c) as LCC. 
  asserts OC: (chunk_ok LC). lmath.
  xapps. credits_split. xif.
  (* case empty middle *)
  xapp. hsimpl. rew_list. splits~.
  (* case non-empty middle *)
  destruct~ (get_last mid) as (LB&mid'&Emid). xapp. xapps. xif.
  (* subcase keep *)
  xapp. xapp. hsimpl. subst mid. splits~. applys~ ForallConseq_last.
  (* subcase merge *)
  xapp. xapp. hsimpl. subst mid. lets (?&?): Forall_last_inv MC.
   splits~. apply~ middle_conseq_push_back_merged. 
  (* case empty chunk *)
  asserts: (LC = nil). destruct LC. auto. lmath.
  xret. hsimpl. splits~. 
Qed.

Hint Extern 1 (RegisterSpec middle_merge_back) =>
  Provide middle_merge_back_spec.

Lemma middle_merge_front_spec : forall A,
  Spec middle_merge_front (m:Middle.t (Chunk.t A)) (c:Chunk.t A) |B>>  
     forall mid L LC, inv_middle L mid -> 
     B (m ~> RMiddle mid \* c ~> RChunk LC \* $ merge_cost) 
       (# Hexists mid', m ~> RMiddle mid' \* \[inv_middle (LC++L) mid']).
Proof.
  xcf. unfold merge_cost. introv (ME&MC&MQ). xapps. xif.
  xchange (ChunkSpec.Seq_le_capacity c) as LCC. 
  asserts OC: (chunk_ok LC). lmath.
  xapps. credits_split. xif.
  (* case empty middle *)
  xapp. hsimpl. rew_list. splits~.
  (* case non-empty middle *)
  destruct mid as [|LB mid']; tryfalse. xapp. xapps. xif.
  (* subcase keep *)
  xapp. xapp. hsimpl. splits~. 
  (* subcase merge *)
  xapp. xapp. hsimpl. inverts MC.
   splits~. apply~ middle_conseq_push_front_merged.
  (* case empty chunk *)
  asserts: (LC = nil). destruct LC. auto. lmath.
  xret. hsimpl. splits~.
Qed.

Hint Extern 1 (RegisterSpec middle_merge_front) =>
  Provide middle_merge_front_spec.

Lemma append_spec : forall A, 
  Spec append (q1:Q.t A) (q2:Q.t A) |B>>  
     forall (L1 L2 : list A),
     B (q1 ~> Seq L1 \* q2 ~> Seq L2 \*
        $ (3%nat * merge_cost + MiddleSpecOwn.append_cst))
      (# q1 ~> Seq (L1 ++ L2)).
Proof.
  xcf. intros. norm_credits. credits_split. 
  (* empty the back of s1 *)
  hunfold Seq at 1. xextract as fo1 fi1 mid1 bi1 bo1 Inv1.
  xapp_by focus. xapp_by focus. xapp_by focus. xapps. 
  lets IB1: (inv_back_fill Inv1).
  xseq. xframes s1 s2. xframe ($ merge_cost \* $ merge_cost 
    \* $ MiddleSpecOwn.append_cst \* $ potential fo1 fi1 bi1 bo1).
  xpost (# Hexists mid1', 
   m1 ~> RMiddle mid1' \* \[inv_middle (concat mid1 ++ bi1 ++ bo1) mid1']).
   unfold merge_cost. xchange (ChunkSpec.Seq_le_capacity co) as LCo. xif. 
     xapp*. ineq_simpl. hsimpl. rew_list~.
     credits_split. xapp. xapps. xif.
       xapp. hsimpl. apply* inv_middle_push_back_full_and_nonempty. 
         lets: list_not_nil_length_pos C0. lmath. 
       xret. hsimpl. rew_list~. applys* inv_middle_push_back_full.
  xok. xok. xextract as mid1' Imid1'. clears. clears IB1.
  (* empty the back of s2 *)
  hunfold Seq at 1. xextract as fo2 fi2 mid2 bi2 bo2 Inv2.
  xapp_by focus. xapp_by focus. xapp_by focus. xapps. 
  lets IF2: (inv_front_fill Inv2).
  xseq. xframes s1. xframes m1. xframes s2. xframe ($ merge_cost 
   \* $ MiddleSpecOwn.append_cst \* $ potential fo1 fi1 bi1 bo1 \* $ potential fo2 fi2 bi2 bo2).
  xpost (# Hexists mid2', 
    m2 ~> RMiddle mid2' \* \[inv_middle (fo2 ++ fi2 ++ concat mid2) mid2']).
    unfold merge_cost. xchange (ChunkSpec.Seq_le_capacity fo) as Lfo. xif. 
     xapp*. ineq_simpl. hsimpl. rew_list~.
     credits_split. xapp. xapps. xif.
       xapp. hsimpl. apply* inv_middle_push_front_full_and_nonempty. 
         lets: list_not_nil_length_pos C0. lmath. 
       xret. hsimpl. rew_list~. applys* inv_middle_push_front_full.
  xok. xok. xok. xok. xextract as mid2' Imid2'. clears. clears IF2.
  (* bring back of s2 to back of s1 *)
  xapp_by focus. xapp_by unfocus.
  xapp_by focus. xapp_by unfocus.
  xapps. xapps. xseq. 
  (* try to merge back of s1.middle with front of s2.middle *)
  xframes s1 s2. xframe ($ MiddleSpecOwn.append_cst \* 
    $ potential fo1 fi1 bi1 bo1 \* $ potential fo2 fi2 bi2 bo2).
  xpost (# Hexists mid1f mid2f, 
    m1 ~> RMiddle mid1f \* m2 ~> RMiddle mid2f 
   \* \[inv_middle (concat (mid1' ++ mid2')) (mid1f ++ mid2f)]). 
    lets (CI1&CK1&CO1): Imid1'. lets (CI2&CK2&CO2): Imid2'. xif as C.
      destruct C as [C1 C2].
      destruct~ (get_last mid1') as (B1&Mi1&E1); subst mid1'; clear C1.
      destruct mid2' as [|F2 Mi2]; tryfalse; clear C2. 
      xapp. xapps. xapp. xapps. clears. unfold merge_cost. credits_split. 
      xif.
        xapp. xapp. hsimpl. splits~. applys* ForallConseq_concat.
        xapp. xapp. hsimpl. splits~.
         inverts CK2. forwards*: Forall_last_inv CK1.
         applys* ForallConseq_concat_fuse CO1 CO2.
      xret. hsimpl. splits~. destruct C; subst; rew_list~. 
  xok. xok. xextract as mid1f mid2f (HI&HM&HC). 
  (* merge the middle sequences and conclude *)
  xapp. hchange (_unfocus_middle s1). credits_join.
  hunfold Seq. hsimpl. unfold potential. comp_simpl.
  constructors~.
    rewrite HI. rew_concat. rewrite (proj31 Imid1'). rewrite (proj31 Imid2').
     rewrite (inv_items Inv1). rewrite~ (inv_items Inv2).
    apply (inv_front_fill Inv1).
    apply (inv_back_fill Inv2).

Qed.


End MakeSpec.




(* TODO

Lemma pop_back_spec : forall A,
  Spec pop_back (q:Q.t A) |B>>  
     forall (X:A) L,
     B (q ~> Seq (L&X)) (fun r => [r = X] \* q ~> Seq L).
Proof.
  Hint Resolve build_inv.
  xcf. intros. hunfold Seq at 1. xextract as fo fi mid bi bo Inv.
  xapp_by focus. xapps. destruct Inv as [MI MF MB MN MC]. xif. 
  (* case co *)
  destruct bo as [|Y bo2]; tryfalse. rew_list in MI. inverts MI. 
  xapp. hchange (@_unfocus_back_outer s). hunfolds~ Seq. pot_simpl. 
  (* case ci *)
  xapp_by focus. xapp_by focus. xapps. xif.
  xchange (@_unfocus_middle s).
  destruct fi as [|Y fi2]; tryfalse. rew_list in MI. inverts MI. 
  xapp. xapp_by unfocus. xapps. xapp.
  hchange (@_unfocus_back_outer s). hunfolds~ Seq. pot_simpl.
  (* case mid *) 
  xapps. xif. xchange (@_unfocus_back_inner s).
  destruct mid as [|LC mid2]; tryfalse.
  inverts MN. destruct LC as [|Y LC2]. lmath.
  rew_list in MI. inverts MI. xapp. xapp. xapps. xapp.
  hchange (@_unfocus_middle s) (@_unfocus_back_outer s).
  hunfolds* Seq. pot_simpl.
  (* case bo *)
  xchange (@_unfocus_back_inner s). xchange (@_unfocus_middle s).
  xapp_by focus. xapps. xif.
  destruct bi as [|Y bi2]; tryfalse. rew_list in MI. inverts MI. 
  xapps. xapp. xapp_by unfocus. xapps. xapp.
  hchange (@_unfocus_back_outer s). hunfold Seq. hsimpl~. pot_simpl. 
  (* case bi *)
  rew_list in MI. subst bo.
  xchange (@_unfocus_front_inner s). xchange (@_unfocus_back_outer s).
  xapp_by focus. xapp. hchange (@_unfocus_front_outer s).
  hunfold Seq. hsimpl~. pot_simpl.
Qed.

*)
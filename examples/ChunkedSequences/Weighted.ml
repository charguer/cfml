
type weight_type = int

type 'a t = { 
   mutable weight : weight_type;
   mutable value : 'a }

let make w v =
   { weight = w; value = v }

let to_pair wx =
   (wx.weight, wx.value)

let get_value wx =
   wx.value 

let get_weight wx =
   wx.weight 

let swap x y =
   let (w,v) = (x.weight, x.value) in
   x.weight <- y.weight;
   x.value <- y.value;
   y.weight <- w;
   y.value <- v

let add_weight w wx =
   wx.weight <- wx.weight + w

let rem_weight w wx =
   wx.weight <- wx.weight - w

let add_weight_of y wx =
   wx.weight <- wx.weight + y.weight

let rem_weight_of y wx =
   wx.weight <- wx.weight - y.weight


Set Implicit Arguments.
Require Import CFLib LibSet LibMap.
Require Import ReducerSig_ml ReducerSig_proof FingerTree_ml.

Module MakeSpec (R:MLReducer) (RS:ReducerSigSpec with Module R:=R).

(** instantiations *)

Module Import F := MLMake R.
Import RS.

Notation "'reduce'" := (list_reduce Red).

Definition Items := list Item.


Parameter combine_spec : 
  Spec R.combine (m1:meas) (m2:meas) |R>>
    R \[] (fun m => \[m = monoid_oper Red m1 m2]).

Hint Extern 1 (RegisterSpec R.combine) => Provide (*TODO: RS.*) combine_spec.
Hint Extern 1 (RegisterSpec R.measure) => Provide RS.measure_spec.
Hint Resolve Red_Reducer.



(****************************************************)
(** Invariants *)

(** Invariants on trees *)

Inductive treestr_inv : nat -> treestr -> Items -> Prop :=
  | treestr_inv_leaf : forall V v,
      Rep V v ->
      treestr_inv 0 (Tree_leaf v) (V::nil)
  | treestr_inv_node2 : forall n t1 t2 L1 L2,
      tree_inv n t1 L1 ->
      tree_inv n t2 L2 ->
      treestr_inv (S n) (Tree_node2 t1 t2) (L1 ++ L2)
  | treestr_inv_node3 : forall n t1 t2 t3 L1 L2 L3,
      tree_inv n t1 L1 ->
      tree_inv n t2 L2 ->
      tree_inv n t3 L3 ->
      treestr_inv (S n) (Tree_node3 t1 t2 t3) (L1 ++ L2 ++ L3)

with tree_inv : nat -> tree -> Items -> Prop :=
  | tree_inv_intro : forall n m t L,
      treestr_inv n t L ->
      m = reduce L ->
      tree_inv n (Tree_intro m t) L.

(** Invariants on digits *)

Inductive digitstr_inv_zero : bool -> nat -> digitstr -> Items -> Prop :=
  | digitstr_inv_zero_0 : forall n,
      digitstr_inv_zero true n Digit0 nil
  | digitstr_inv_zero_1 : forall b n t1 L1,
      tree_inv n t1 L1 ->
      digitstr_inv_zero b n (Digit1 t1) L1 
  | digitstr_inv_zero_2 : forall b n t1 t2 L1 L2,
      tree_inv n t1 L1 ->
      tree_inv n t2 L2 ->
      digitstr_inv_zero b n (Digit2 t1 t2) (L1 ++ L2)
  | digitstr_inv_zero_3 : forall b n t1 t2 t3 L1 L2 L3,
      tree_inv n t1 L1 ->
      tree_inv n t2 L2 ->
      tree_inv n t3 L3 ->
      digitstr_inv_zero b n (Digit3 t1 t2 t3) (L1 ++ L2 ++ L3)
  | digitstr_inv_zero_4 : forall b n t1 t2 t3 t4 L1 L2 L3 L4,
      tree_inv n t1 L1 ->
      tree_inv n t2 L2 ->
      tree_inv n t3 L3 ->
      tree_inv n t4 L4 ->
      digitstr_inv_zero b n (Digit4 t1 t2 t3 t4) (L1 ++ L2 ++ L3 ++ L4)

with digit_inv_zero : bool -> nat -> digit -> Items -> Prop :=
  | digit_inv_zero_intro : forall b n m d L,
      digitstr_inv_zero b n d L ->
      m = reduce L ->
      digit_inv_zero b n (Digit_intro m d) L.

Definition digit_inv := digit_inv_zero false.
Definition digit_inv0 := digit_inv_zero true.
Definition digitstr_inv := digitstr_inv_zero false.
Definition digitstr_inv0 := digitstr_inv_zero true.

(** Invariants on finger trees *)

Inductive ftreestr_inv : nat -> ftreestr -> Items -> Prop :=
  | ftreestr_inv_empty : forall n,
      ftreestr_inv n Ftree_empty nil
  | ftreestr_inv_single : forall n t L,
      tree_inv n t L ->
      ftreestr_inv n (Ftree_single t) L
  | ftreestr_inv_deep : forall n dl fm dr Ll Lr Lm,
      digit_inv n dl Ll ->
      ftree_inv (S n) fm Lm ->
      digit_inv n dr Lr ->
      ftreestr_inv n (Ftree_deep dl fm dr) (Ll ++ Lm ++ Lr)

with ftree_inv : nat -> ftree -> Items -> Prop :=
  | ftree_inv_intro : forall n m f L,
      ftreestr_inv n f L ->
      m = reduce L ->
      ftree_inv n (Ftree_intro m f) L.

Definition fftree_inv := 
  ftree_inv 0.


(****************************************************)
(** Auxiliary *)

(* -- TOFIX: induction principle

(** Well-founded order on trees *)

Inductive tree_sub (A:Type) : binary (tree A) :=
  | Tree_node2_1 : forall T1 T2,
      tree_sub T1 (Tree_node2 T1 T2)
  | Tree_node2_2 : forall T1 T2,
      tree_sub T2 (Tree_node2 T1 T2)
  | Tree_node3_1 : forall T1 T2 T3,
      tree_sub T1 (Tree_node3 T1 T2 T3)
  | Tree_node3_2 : forall T1 T2 T3,
      tree_sub T2 (Tree_node3 T1 T2 T3)
  | Tree_node3_3 : forall T1 T2 T3,
      tree_sub T3 (Tree_node3 T1 T2 T3).

Lemma tree_sub_wf : forall A, wf (@tree_sub A).
Proof using.
  intros A T. induction T; constructor; intros t' H; inversions~ H.
Qed.

Hint Resolve tree_sub_wf : wf.

(** Well-founded order on ftrees *)

Inductive ftree_sub (A:Type) : binary (ftree A) :=
  | ftree_sub_intro : forall dl sub dr,
      ftree_sub sub (Ftree_deep dl sub dr).

Lemma ftree_sub_wf : forall A, wf (@ftree_sub A).
Proof using.
  intros A T. induction T; constructor; intros t' H; inversions~ H.
Qed.

Hint Resolve ftree_sub_wf : wf.

--*)

(****************************************************)
(** Automation *)

Hint Constructors treestr_inv tree_inv digitstr_inv_zero digit_inv_zero ftree_inv Forall2.
Hint Unfold digit_inv digit_inv0 digitstr_inv digitstr_inv0.

Lemma ftreestr_inv_deep' : forall n dl fm dr Ll Lr Lm (L:Items),
  digit_inv n dl Ll ->
  ftree_inv (S n) fm Lm ->
  digit_inv n dr Lr ->
  L = (Ll ++ Lm ++ Lr) ->
  ftreestr_inv n (Ftree_deep dl fm dr) L.
Proof using. intros. subst. constructors*. Qed.

Hint Resolve ftreestr_inv_deep'.

Ltac auto_star ::= 
  try solve [ jauto | check_noevar_goal; rew_list; jauto ].

Hint Resolve zero_spec. 


(****************************************************)
(** Auxiliary functions *)

Definition Tree_cached (t:tree) :=
  let (m,_) := t in m.

Definition Tree_str (t:tree) :=
  let (_,ts) := t in ts.

Definition Digit_cached (d:digit) :=
  let (m,_) := d in m.

Definition Digit_str (d:digit) :=
  let (_,ds) := d in ds.

Definition Ftree_cached (f:ftree) :=
  let (m,_) := f in m.

Definition Ftree_str (f:ftree) :=
  let (_,fs) := f in fs.


(****************************************************)
(** Auxiliary specifications *)

Inductive digit_full : digit -> Prop :=
  | digit_full_intro : forall m x1 x2 x3 x4,
      digit_full (Digit_intro m (Digit4 x1 x2 x3 x4)).

Hint Constructors digit_full.

Definition tree_not_leaf (t:tree) :=
  forall x, Tree_str t <> Tree_leaf x.

Definition digit_not_one (d:digit) :=
  forall x, Digit_str d <> Digit1 x.

Hint Resolve ftreestr_inv_empty ftreestr_inv_single. 


(****************************************************)
(** Auxiliary lemmas *)

Lemma contradict : forall (P:Prop),  (*todo: move to LibLogic *)
  P -> ~ P -> False.
Proof using. auto. Qed.

Lemma tree_inv_S_not_leaf : forall n t L,
  tree_inv (S n) t L -> tree_not_leaf t.
Proof using.
  introv Inv. unfolds. inverts Inv as M.
  inverts M; simpls; congruence.
Qed.

Hint Immediate tree_inv_S_not_leaf.

Lemma tree_inv_not_nil : forall n t L,
  tree_inv n t L -> L <> nil.
Proof using.
  intros n. induction n using peano_induction. introv M.
  inverts M as M. inverts M as. 
  intros. congruence.
  introv M1 M2. apply app_not_empty_l. applys H M1. math.
  introv M1 M2 M3. apply app_not_empty_l. applys H M1. math.
Qed.
 
Lemma digit_inv_not_nil : forall n d L,
  digit_inv n d L -> L <> nil.
Proof using.
  hint tree_inv_not_nil, app_not_empty_l.
  introv M. inverts M as M. inverts* M.
Qed.

Lemma ftree_inv_nil_empty : forall n f,
  ftree_inv n f nil -> Ftree_str f = Ftree_empty.
Proof using.
  introv M. inverts M as M. inverts M as; simpl.
  auto.
  introv N. false~ tree_inv_not_nil N.
  introv D _ _ E. forwards (F&_): app_eq_nil_inv E.
   subst. false digit_inv_not_nil D.
  auto.
Qed.

Lemma Tree_cached_inv : forall n t L,
  tree_inv n t L -> Tree_cached t = reduce L.
Proof using. introv H. inverts~ H. Qed.

Lemma Digit_cached_inv : forall n d L,
  digit_inv n d L -> Digit_cached d = reduce L.
Proof using. introv H. inverts~ H. Qed.

Lemma Ftree_cached_inv : forall n f L,
  ftree_inv n f L -> Ftree_cached f = reduce L.
Proof using. introv H. inverts~ H. Qed.


(****************************************************)
(** Verification of [combine2] *)

Lemma combine2_spec : 
  Spec combine2 (m1:meas) (m2:meas) |R>>
     R \[] (fun m => \[m = monoid_oper Red m1 m2]).
Proof using. xgo*. Qed.

Hint Extern 1 (RegisterSpec combine2) => Provide combine2_spec.

Lemma combine3_spec : 
  Spec combine3 (m1:meas) (m2:meas) (m3:meas) |R>>
     R \[] (fun m => \[m = monoid_oper Red m1 (monoid_oper Red m2 m3)]).
Proof using. xcf. intros. xapps. xapp*. xsimpl*.  (* TODO: fix xgo on subst *)
Admitted. (*faster*) 

Hint Extern 1 (RegisterSpec combine3) => Provide combine3_spec.

Lemma combine4_spec : 
  Spec combine4 (m1:meas) (m2:meas) (m3:meas) (m4:meas) |R>>
     R \[] (fun m => \[m = monoid_oper Red m1 (monoid_oper Red m2 (monoid_oper Red m3 m4))]).
Proof using. xcf. intros. xapps. xapp*. xsimpl*. Qed.

Hint Extern 1 (RegisterSpec combine4) => Provide combine4_spec.


(****************************************************)
(** Verification of [*_cached] and [*_str] *)

Lemma tree_cached_spec : 
  Spec tree_cached (t:tree) |R>>
     R \[] (fun m => \[m = Tree_cached t]).
Proof using. xgo*. Qed.

Hint Extern 1 (RegisterSpec tree_cached) => Provide tree_cached_spec.

Lemma tree_str_spec : 
  Spec tree_str (t:tree) |R>>
     R \[] (fun ts => \[ts = Tree_str t]).
Proof using. xgo*. Qed.

Hint Extern 1 (RegisterSpec tree_str) => Provide tree_str_spec.

Lemma digit_cached_spec : 
  Spec digit_cached (d:digit) |R>>
     R \[] (fun m => \[m = Digit_cached d]).
Proof using. xgo*. Qed.

Hint Extern 1 (RegisterSpec digit_cached) => Provide digit_cached_spec.

Lemma digit_str_spec : 
  Spec digit_str (d:digit) |R>>
     R \[] (fun ds => \[ds = Digit_str d]).
Proof using. xgo*. Qed.

Hint Extern 1 (RegisterSpec digit_str) => Provide digit_str_spec.

Lemma ftree_cached_spec : 
  Spec ftree_cached (f:ftree) |R>>
     R \[] (fun m => \[m = Ftree_cached f]).
Proof using. xgo*. Qed.

Hint Extern 1 (RegisterSpec ftree_cached) => Provide ftree_cached_spec.

Lemma ftree_str_spec : 
  Spec ftree_str (f:ftree) |R>>
     R \[] (fun fs => \[fs = Ftree_str f]).
Proof using. xgo*. Qed.

Hint Extern 1 (RegisterSpec ftree_str) => Provide ftree_str_spec.


(****************************************************)
(** Verification of smart constructors *)

Lemma tree_leaf_spec : 
  Spec tree_leaf x |R>>
     forall X, Rep X x -> 
     R \[] (fun f => \[tree_inv 0 f (X::nil)]).
Proof using.
  xcf. introv RX. xapps*. xret*. xsimpl.
  constructors*. rewrite~ list_reduce_one.
Qed.

Hint Extern 1 (RegisterSpec tree_leaf) => Provide tree_leaf_spec.

Lemma tree_node2_spec : 
  Spec tree_node2 t1 t2 |R>>
     forall n L1 L2, tree_inv n t1 L1 -> tree_inv n t2 L2 -> 
     R \[] (fun f => \[tree_inv (S n) f (L1++L2)]).
Proof using.
  xcf. introv I1 I2. xvals. xapps*. xapps*. xapps*. xret*. xsimpl.
  constructors*. rewrite~ list_reduce_app. do 2 erewrite* Tree_cached_inv.
Qed.

Hint Extern 1 (RegisterSpec tree_node2) => Provide tree_node2_spec.

Lemma tree_node3_spec : 
  Spec tree_node3 t1 t2 t3 |R>>
     forall n L1 L2 L3, tree_inv n t1 L1 -> tree_inv n t2 L2 -> tree_inv n t3 L3 -> 
     R \[] (fun f => \[tree_inv (S n) f (L1++L2++L3)]).
Proof using.
  xcf. introv I1 I2 I3. xvals. xapps*. xapps*. xapps*. xapps*. xret*. xsimpl.
  constructors*. do 2 rewrite~ list_reduce_app. do 3 erewrite* Tree_cached_inv. (* tactic *)
Qed.

Hint Extern 1 (RegisterSpec tree_node3) => Provide tree_node3_spec.

Lemma digit0_spec : forall n,
  digit_inv0 n digit0 nil.
Proof using.
  intros. rewrite digit0_cf. (*todo xcf *) constructors*.
  rewrite~ list_reduce_nil.
Qed.

Hint Resolve digit0_spec.

Lemma digit1_spec : 
  Spec digit1 t1 |R>>
     forall n L1, tree_inv n t1 L1 -> 
     R \[] (fun d => \[digit_inv n d L1]).
Proof using.
  xcf. introv I1. xvals. xapps*. xret*. xsimpl.
  constructors*. erewrite* Tree_cached_inv.
Qed.

Hint Extern 1 (RegisterSpec digit1) => Provide digit1_spec.

Lemma digit2_spec : 
  Spec digit2 t1 t2 |R>>
     forall n L1 L2, tree_inv n t1 L1 -> tree_inv n t2 L2 -> 
     R \[] (fun d => \[digit_inv n d (L1++L2)]).
Proof using.
  xcf. introv I1 I2. xvals. xapps*. xapps*. xapps*. xret*. xsimpl.
  constructors*. rewrite~ list_reduce_app. do 2 erewrite* Tree_cached_inv.
Qed.

Hint Extern 1 (RegisterSpec digit2) => Provide digit2_spec.

Lemma digit3_spec : 
  Spec digit3 t1 t2 t3 |R>>
     forall n L1 L2 L3, tree_inv n t1 L1 -> tree_inv n t2 L2 -> 
	    tree_inv n t3 L3 -> 
     R \[] (fun d => \[digit_inv n d (L1++L2++L3)]).
Proof using.
  xcf. introv I1 I2 I3. xvals. xapps*. xapps*. xapps*. xapps*. xret*. xsimpl.
  constructors*. do 2 rewrite~ list_reduce_app. do 3 erewrite* Tree_cached_inv.
Qed.

Hint Extern 1 (RegisterSpec digit3) => Provide digit3_spec.

Lemma digit4_spec : 
  Spec digit4 t1 t2 t3 t4 |R>>
     forall n L1 L2 L3 L4, tree_inv n t1 L1 -> tree_inv n t2 L2 -> 
	    tree_inv n t3 L3 -> tree_inv n t4 L4 -> 
     R \[] (fun d => \[digit_inv n d (L1++L2++L3++L4)]).
Proof using.
  xcf. introv I1 I2 I3 I4. xvals. xapps*. xapps*. 
  xapps*. xapps*. xapps*. xret*. xsimpl.
  constructors*. do 3 rewrite~ list_reduce_app. do 4 erewrite* Tree_cached_inv.
Qed.

Hint Extern 1 (RegisterSpec digit4) => Provide digit4_spec.

Lemma ftree_empty_spec : forall n,
  ftree_inv n ftree_empty nil.
Proof using.
  intros. rewrite ftree_empty_cf. (*todo xcf *) constructors*.
  rewrite~ list_reduce_nil.
Qed.

Hint Resolve ftree_empty_spec.

Lemma ftree_single_spec : 
  Spec ftree_single t1 |R>>
     forall n L1, tree_inv n t1 L1 -> 
     R \[] (fun f => \[ftree_inv n f L1]).
Proof using.
  xcf. introv I1. xapps*. xret*. xsimpl.
  constructors*. erewrite* Tree_cached_inv.
Qed.

Hint Extern 1 (RegisterSpec ftree_single) => Provide ftree_single_spec.

Lemma ftree_deep_spec : 
  Spec ftree_deep dl sub dr |R>>
     forall n L1 L2 L3, digit_inv n dl L1 ->
       ftree_inv (S n) sub L2 -> digit_inv n dr L3 ->
     R \[] (fun f => \[ftree_inv n f (L1++L2++L3)]).
Proof using.
  xcf. introv I1 I2 I3. xapps*. xapps*. xapps*. xapps*. xret*. xsimpl.
  constructors*. do 2 rewrite~ list_reduce_app.
  do 2 erewrite* Digit_cached_inv. erewrite* Ftree_cached_inv.
Qed.

Hint Extern 1 (RegisterSpec ftree_deep) => Provide ftree_deep_spec.


(****************************************************)
(** Verification of digit-tree conversions *)

Lemma ftree_of_digit_spec : 
  Spec ftree_of_digit (d:digit) |R>>
     forall n L, digit_inv0 n d L -> 
     R \[] (fun f => \[ftree_inv n f L]).
Proof using.
  xcf. introv ID. inverts ID as ID.
  xapps*. xmatch; inverts ID as; simpl.
  xret*. (* xsimpl*. *)
  intros. xapp*. xsimpl*.
  intros. xapp*. intros. xapp*. intros. xapp*. xsimpl*.
  intros. xapp*. intros. xapp*. intros. xapp*. rew_list. xsimpl*.
  intros. xapp*. intros. xapp*. intros. xapp*. rew_list. xsimpl*.
Admitted. (* for faster compilation *)

Hint Extern 1 (RegisterSpec ftree_of_digit) => Provide ftree_of_digit_spec.

Lemma digit_of_tree_spec : 
  Spec digit_of_tree (t:tree) |R>>
     forall n L, tree_inv (S n) t L -> tree_not_leaf t ->
     R \[] (fun d => \[digit_inv n d L]).
Proof using.
  xcf. introv IT N. inverts IT as IT. xapps*.
  xmatch; inverts IT as.
  intros. xapp*. xsimpl*.
  intros. xapp*. xsimpl*.
Admitted. (* for faster compilation *)

Hint Extern 1 (RegisterSpec digit_of_tree) => Provide digit_of_tree_spec.


(****************************************************)
(** Verification of push front *)

Lemma digit_push_front_spec : 
  Spec digit_push_front (t0:tree) (d:digit) |R>>
     forall n L L0, digit_inv n d L -> tree_inv n t0 L0 -> ~ digit_full d ->
     R \[] (fun d' => \[digit_inv n d' (L0 ++ L)]).
Proof using.
  xcf. introv ID IT Nfull. inverts ID as ID. xapps*.
  xmatch; inverts ID as.
  intros. xapp*. xsimpl*.
  intros. xapp*. xsimpl*.
  intros. xapp*. xsimpl*.
  intros. xfail. false* Nfull.
Admitted. (* for faster compilation *)

Hint Extern 1 (RegisterSpec digit_push_front) => Provide digit_push_front_spec.

Lemma ftree_push_front_spec :
  Spec ftree_push_front (t0:tree) (f:ftree) |R>>
     forall n L L0, ftree_inv n f L -> tree_inv n t0 L0 ->
     R \[] (fun f' => \[ftree_inv n f' (L0 ++ L)]).
Proof using.
  intros. (* xinduction (unproj22 (tree A) (@ftree_sub A)).*) skip_goal IH.
  xcf. introv (*IH*) JF IT. inverts JF as JF. xapps*. xmatch; inverts JF as.
  xapp*. rew_list. xsimpl*.
  introv IT2. xapp*. introv IL. xapp*. introv IR. xapp*. xsimpl*. 
  introv IDl ISub IDr. xapps. xmatch.
    inverts IDl as IDl. simpls. subst d. inverts IDl as M1 M2 M3 M4.
     clear C C0 H. (* TODO: why clear?*) 
       xapp*. introv ISub2. xapp*. introv InvF'.
       xapp*. introv InvL. xapp*. rew_list. xsimpl*.
    xapp*. intros N. inverts N. false* C1.
     introv ID2. xapps*. rew_list. xsimpl*.
Admitted. (* for faster compilation *)

Hint Extern 1 (RegisterSpec ftree_push_front) => Provide ftree_push_front_spec.

Lemma push_front_spec : 
  Spec push_front (x:R.item) (f:ftree) |R>>
     forall X L, Rep X x -> fftree_inv f L -> 
     R \[] (fun f' => \[fftree_inv f' (X::L)]).
Proof using.
  xcf. introv IX IT. xapp*. introv IT'. xapp*. rew_list. xsimpl*.
Admitted. (*faster*)

Hint Extern 1 (RegisterSpec push_front) => Provide push_front_spec.


(****************************************************)
(** Verification of push back --TODO: symmetric *)

Axiom ftree_push_back_spec :
  Spec ftree_push_back (t0:tree) (f:ftree) |R>>
     forall n L L0, ftree_inv n f L -> tree_inv n t0 L0 ->
     R \[] (fun f' => \[ftree_inv n f' (L ++ L0)]).

Hint Extern 1 (RegisterSpec ftree_push_back) => Provide ftree_push_back_spec.


(****************************************************)
(** Verification of pop front *)


Lemma digit_inv_digit_inv_0 : forall n d L,
  digit_inv n d L -> digit_inv0 n d L.
Proof using. introv H. inverts H as M. constructors~. inverts~ M. Qed.

Hint Resolve digit_inv_digit_inv_0.

Lemma digit_inv_0_digit_inv : forall n d L,
  digit_inv0 n d L -> Digit_str d <> Digit0 -> digit_inv n d L.
Proof using. introv H N. inverts H as M. simpls. inverts~ M. false. Qed.


Lemma digit_pop_front_spec : 
  Spec digit_pop_front (d:digit) |R>>
     forall n L, digit_inv n d L -> 
     R \[] (fun p => let '(t,d') := p in 
           Hexists L1 L2, \[tree_inv n t L1 /\ digit_inv0 n d' L2 /\ L = L1 ++ L2]).
Proof using.
  xcf. introv ID. inverts ID as ID. xapps*. xmatch; inverts ID as.
  intros. xret*. hsimpl* L (nil:Items).
  intros. xapp*. intros. xret*. 
  intros. xapp*. intros. xret*. 
  intros. xapp*. intros. xret*. 
Admitted. (* for faster compilation *)

Hint Extern 1 (RegisterSpec digit_pop_front) => Provide digit_pop_front_spec.

Definition ftree_pop_front_spec_def :=
  Spec ftree_pop_front (f:ftree) |R>>
     forall n L, ftree_inv n f L -> 
     R \[] (fun o => 
        [match o with
        | None => L = nil
        | Some (t,f') => 
            exists L1 L2, tree_inv n t L1 /\ ftree_inv n f' L2 /\ L = L1 ++ L2
        end]).

Definition ftree_fix_front_spec_def :=
  Spec ftree_fix_front (dl:digit) (fm:ftree) (dr:digit) |R>>
     forall n L1 L2 L3, 
     digit_inv0 n dl L1 -> 
     ftree_inv (S n) fm L2 -> 
     digit_inv n dr L3 -> 
     R \[] (fun f => \[ftree_inv n f (L1 ++ L2 ++ L3)]).

Lemma ftree_pop_and_fix_front_spec : 
  ftree_pop_front_spec_def /\ ftree_fix_front_spec_def.
Proof using.
  skip_goal IH. destruct IH as [IH1 IH2]. split. (* todo: skip_goal [IH1 IH2] *)
  (* ftree_pop_front *)
  unfolds. xcf. (* todo: unfold on xcf *)
  introv IT. inverts IT as IT. xapps. xmatch; inverts IT as.
  xret*.
  intros. xret*. hsimpl. exists* L (nil:Items).
  intros. xapp*. xmatch. xextract as L1 L2 (Inv1&Inv2&Eq).
   xapp_spec* IH2. introv InvM. xret*.
  (* ftree_fix_front *)
  unfolds. xcf. introv IDl IFm IDr. xapps. xmatch.
  xlet as p. xapp_spec* IH1. 
   asserts: (L1 = nil). (* TODO: asserts_subst *)
    inverts IDl as M. simpls. subst. inverts~ M.
   xextract as M. xmatch; xextract.
     inverts IDl as M. xapp*. xsimpl*.
     destruct M as (Lt2&Lfm2&(IT2&IFM2&EQ)). xapp*.
      introv ID2. xapp*. xsimpl*.
  xapp*. applys* digit_inv_0_digit_inv. xsimpl*.
Qed.

Definition ftree_pop_front_spec := proj1 ftree_pop_and_fix_front_spec.

Hint Extern 1 (RegisterSpec ftree_pop_front) => Provide ftree_pop_front_spec.

Definition ftree_fix_front_spec := proj2 ftree_pop_and_fix_front_spec.

Hint Extern 1 (RegisterSpec ftree_fix_front) => Provide ftree_fix_front_spec.

Lemma pop_front_spec : 
  Spec pop_front (f:ftree) |R>>
     forall L, fftree_inv f L -> L <> nil ->
     R \[] (fun p => let '(x,f') := p in Hexists X L',
           \[Rep X x /\ fftree_inv f' L' /\ L = X::L']).
Proof using.
  xcf. introv IT N. xapp* as p. intros M. xmatch.
  xfail*.
  destruct M as (L1&L2&(M1&M2&M3)). inverts M1 as M0.
   inverts M0. simpl. xapps*. xmatch. xret*.
Qed.

Hint Extern 1 (RegisterSpec pop_front) => Provide pop_front_spec.


(****************************************************)
(** Verification of pop back --TODO: symmetric *)

Axiom ftree_fix_back_spec :
  Spec ftree_fix_back (dl:digit) (fm:ftree) (dr:digit) |R>>
     forall n L1 L2 L3, 
     digit_inv n dl L1 -> 
     ftree_inv (S n) fm L2 -> 
     digit_inv0 n dr L3 -> 
     R \[] (fun f => \[ftree_inv n f (L1 ++ L2 ++ L3)]).

Hint Extern 1 (RegisterSpec ftree_fix_back) => Provide ftree_fix_back_spec.




(****************************************************)
(** Verification of split *)



Infix "**" := (monoid_oper Red) (at level 60).

Definition splitter_pred (P:meas->Prop) :=
  forall i1 i2, P i1 -> P (i1 ** i2).

Definition splitter_func (p:func) (P:meas->Prop) :=
  Spec p (i:meas) |R>> R \[] (fun b:bool => \[b = isTrue (P i)]).

Instance red_assoc : @Monoid_assoc R.meas Red.
  eapply @Monoid_Monoid_assoc.
  eapply Red_Reducer.
Qed.

Instance red_neutral_l : @Monoid_neutral_l R.meas Red.
  eapply @Monoid_Monoid_neutral_l.
  eapply Red_Reducer.
Qed.

Instance red_neutral_r : @Monoid_neutral_r R.meas Red.
  eapply @Monoid_Monoid_neutral_r.
  eapply Red_Reducer.
Qed.


Lemma digit_split_spec : 
  Spec digit_split (p:func) (i:meas) (d:digit) |R>>
     forall P n L, digit_inv n d L -> 
     splitter_pred P -> splitter_func p P -> ~ (P i) -> P (i ** reduce L) ->
     R \[] (fun p => [let '(dl,tm,dr) := p in exists (L1 L2 L3:Items),
           digit_inv0 n dl L1 /\ tree_inv n tm L2 /\ digit_inv0 n dr L3 
            /\ L = L1++L2++L3 /\ ~ P (i ** reduce L1) /\ P (i ** reduce (L1++L2))]).
Proof using.
  xcf. introv ID PM Specp PN PL. unfolds in Specp.
  inverts ID as ID. xapps. xmatch; inverts ID as.
  introv IT1. xret. xsimpl*. exists___. splits*.
    rewrite* list_reduce_nil. rewrite* monoid_neutral_r. 
  introv IT1 IT2. xapps*. xapps. xapp. intro_subst. xif.
    xapp*. intros. xret*. xsimpl*. exists___. splits*.
      rewrite* list_reduce_nil. rewrite* monoid_neutral_r.     
      rew_list. inverts~ IT1. (* TODO:  erewrite Tree_cached_inv in *; eauto. OU erewrite* <- Tree_cached_inv *)
    xapp*. intros. xret. xsimpl*. exists___. splits*. 
      inverts~ IT1.
  introv IT1 IT2 IT3. xapps*. xapps*. xapp. intro_subst. xif.
    xapp*. intros. xret. hsimpl* (nil:Items) L1 (L2++L3). exists___. splits*.
      rewrite* list_reduce_nil. rewrite* monoid_neutral_r.  (* todo: factorize *)
       rew_list. inverts~ IT1.
    xapps*. xapps. xapp. intro_subst. xif.
      xapp*. intros. xapp*. intros. xret. xsimpl*. exists___. splits*.
        inverts~ IT1.
        rewrite~ list_reduce_app. rewrite monoid_assoc. inverts~ IT1. inverts~ IT2.
      xapp*. intros. xret. hsimpl* (L1++L2) L3 (nil:Items). exists___. splits*.
        rewrite~ list_reduce_app. rewrite monoid_assoc. inverts~ IT1. inverts~ IT2.
  introv IT1 IT2 IT3 IT4. xapps*. xapps*. xapp. intro_subst. xif.
    xapp*. intros. xret. hsimpl* (nil:Items) L1 (L2++L3++L4). exists___. splits*.
      rewrite* list_reduce_nil. rewrite* monoid_neutral_r.     
      rew_list. inverts~ IT1.
    xapps*. xapps. xapp. intro_subst. xif.
      xapp*. intros. xapp*. intros. xret. hsimpl* L1 L2 (L3++L4). exists___. splits*.
        inverts~ IT1. 
        rewrite~ list_reduce_app. rewrite monoid_assoc. inverts~ IT1. inverts~ IT2.
      xapps*. xapps. xapp. intro_subst. xif.
        xapp*. intros. xapp*. intros. xret. hsimpl* (L1++L2) L3 L4. exists___. splits*.
          rewrite~ list_reduce_app. rewrite monoid_assoc. inverts~ IT1. inverts~ IT2.
          do 2 rewrite~ list_reduce_app. do 2 rewrite monoid_assoc.
           inverts~ IT1. inverts~ IT2. inverts~ IT3.
        xapp*. intros. xret. hsimpl* (L1++L2++L3) L4 (nil:Items). exists___. splits*.
          do 2 rewrite~ list_reduce_app. do 2 rewrite monoid_assoc.
           inverts~ IT1. inverts~ IT2. inverts~ IT3.
Admitted. (*faster*)

Hint Extern 1 (RegisterSpec digit_split) => Provide digit_split_spec.

Lemma ftree_split_spec : 
  Spec ftree_split (p:func) (i:meas) (f:ftree) |R>>
     forall P n L, ftree_inv n f L -> L <> nil ->
     splitter_pred P -> splitter_func p P -> ~ (P i) -> P (i ** reduce L) ->
     R \[] (fun p => \[let '(fl,tm,fr) := p in exists (L1 L2 L3:Items),
           ftree_inv n fl L1 /\ tree_inv n tm L2 /\ ftree_inv n fr L3 
            /\ L = L1++L2++L3 /\ ~ P (i ** reduce L1) /\ P (i ** reduce (L1++L2))]).
Proof using.
  skip_goal IH. (*todo*)
  xcf. introv ID N PM Specp PN PL. unfolds in Specp.
  inverts ID as ID. xapps. xmatch; inverts ID as.
  xfail. false* N.
  introv IT1. xret*. xsimpl*. exists___. splits*.
     rewrite* list_reduce_nil. rewrite* monoid_neutral_r. 
  introv IL IM IR. xapps*. xapp*. intro_subst. xapp. intro_subst. xif.
    xapp*. inverts~ IL.
     intros M. xmatch. destruct M as (Lll&Llm&Llr&(ILl&ILm&ILr&EQ&P1&P2)).
     xapp_spec* ftree_of_digit_spec. introv ILl'.
     xapp*. introv IR'. xret. xsimpl. subst. exists___. splits*.
    xapp*. intro_subst. xapp*. intro_subst. xapp. intro_subst. xif.
      xapp*. 
         intro_subst. lets: ftree_inv_nil_empty IM. inverts IM. simpls.
          subst. applys contradict (rm C2). rewrite~ list_reduce_nil.
          rewrite monoid_neutral_r. apply C1.
         inverts~ IM.
       intros M. xmatch. destruct M as (Lml&Lmm&Lmr&(ILml&ILmm&ILmr&EQ&P1&P2)).
       xapp*. introv IFmmd. xapp*. intro_subst. xapp*. intro_subst. (* todo: why slower *)
       xapp*.
         inverts~ ILml.
         rewrite~ list_reduce_app in P2. rewrite monoid_assoc in P2. inverts~ ILml.
       intros M. xmatch. destruct M as (Lfmml&Lfmmm&Lfmmr&(ILfmml&ILfmmm&ILfmmr&EQ'&P3&P4)).
       xapp*. introv IL'. xapp*. introv IR'. xret. xsimpl. subst. exists___. splits*.
          do 2 rewrite~ list_reduce_app. do 2 rewrite~ monoid_assoc.
           inverts ILml. inverts~ IL.
          do 3 rewrite~ list_reduce_app. do 3 rewrite~ monoid_assoc.
           rewrite~ list_reduce_app in P4. rewrite~ monoid_assoc in P4. 
           inverts IL. inverts~ ILml.            
      xapp*. do 2 rewrite~ list_reduce_app in PL. do 2 rewrite monoid_assoc in PL.
       inverts~ IM. inverts~ IL.
       intros M. xmatch. destruct M as (Lll&Llm&Llr&(ILl&ILm&ILr&EQ&P1&P2)).
       (* todo: remove xmatch on let-pair by adding let_pair construct *)
       xapp*. introv ILr'. xapp_spec* ftree_of_digit_spec. introv IL'.
        xret. xsimpl. subst. exists___. splits*.
          do 2 rewrite~ list_reduce_app. do 2 rewrite monoid_assoc.
           inverts IL. inverts~ IM.
          rew_list. do 3 rewrite~ list_reduce_app. do 3 rewrite~ monoid_assoc.
           rewrite~ list_reduce_app in P2. rewrite~ monoid_assoc in P2. 
           inverts IL. inverts~ IM.             
Qed.
(* todo: fix name conventions *)

Hint Extern 1 (RegisterSpec ftree_split) => Provide ftree_split_spec.

Lemma split_spec : 
  Spec split (p:func) (ff:ftree) |R>>
     forall P L, fftree_inv ff L -> L <> nil ->
     splitter_pred P -> splitter_func p P -> ~ (P R.zero) -> P (reduce L) ->
     R \[] (fun p => \[let '(ffl,x,ffr) := p in exists (L1 L2:Items) X,
           fftree_inv ffl L1 /\ Rep X x /\ fftree_inv ffr L2
            /\ L = L1++X::L2 /\ ~ P (reduce L1) /\ P (reduce (L1&X))]).
Proof using.
  xcf. introv ID N PM Specp PN PL.
  xapp*. rewrite zero_spec. rewrite* monoid_neutral_l.
  intros M. xmatch. destruct M as (L1&L2&L3&E&M1&M2&M3&M4&M5).
  xapps. inverts ID as M. inverts M1 as M0. simpls. inverts M0.
  xmatch. xret. hsimpl.  
  rewrite zero_spec in M4,M5. rewrite monoid_neutral_l in M4,M5.
  exists___*.
Qed.


(****************************************************)
(** Verification of merge *)

Notation "'len'" := LibList.length.

Definition tree_list_inv n ts L :=
  exists Ls,   Forall2 (tree_inv n) ts Ls
            /\ L = LibList.concat Ls.

Lemma tree_list_inv_nil : forall n,
  tree_list_inv n nil nil.
Proof using. intros. esplit; eauto. Qed. (* todo: esplit* *)

Lemma merge_digit_list_spec0 : 
  Spec merge_digit_list (d:digit) (acc:list tree) |R>>
     forall n L1 L2, digit_inv0 n d L1 -> tree_list_inv n acc L2 ->
     R \[] (fun ts => 
         \[tree_list_inv n ts (L1++L2)
          /\ (len acc <= len ts <= 4 + len acc)%Z]).
Proof using.
  xcf. introv Id (Ls'&FL2&EL2). subst L2. inverts Id as Id.
  xapps*. xmatch; xret; xsimpl; inverts Id as; 
    intros; rew_list; (split; [ exists* __ | math]).
Qed. 

Lemma merge_digit_list_spec : 
  Spec merge_digit_list (d:digit) (acc:list tree) |R>>
     forall n L1 L2, digit_inv n d L1 -> tree_list_inv n acc L2 ->
     R \[] (fun ts => 
         \[tree_list_inv n ts (L1++L2)
          /\ (1 + len acc <= len ts <= 4 + len acc)%Z]).
Proof using.
  xcf. introv Id (Ls'&FL2&EL2). subst L2. inverts Id as Id.
  xapps*. xmatch; xret; xsimpl; inverts Id as; 
    intros; rew_list; (split; [ exists* __ | math]).
Qed. 

Hint Extern 1 (RegisterSpec merge_digit_list) => Provide merge_digit_list_spec.

(* todo: factorize *)


Lemma digit_from_treestr_list_spec : 
  Spec digit_from_treestr_list (ts:list tree) |R>>
     forall n L, tree_list_inv n ts L -> 
     (1 <= LibList.length ts <= 4)%Z -> 
     R \[] (fun d => \[digit_inv n d L]).
Proof using.
  xcf. introv (Ls&M&ELs) Len. subst L.
  xmatch; rew_length in Len.
  xfail. math.
  inverts M as I1 M. inverts M. rew_list. xapp*. xsimpl*.
  inverts M as I1 M. inverts M as I2 M. inverts M. 
   rew_list. xapp*. xsimpl*.
  inverts M as I1 M. inverts M as I2 M. inverts M as I3 M. inverts M. 
   rew_list. xapp*. xsimpl*.
  inverts M as I1 M. inverts M as I2 M. inverts M as I3 M. inverts M as I4 M. inverts M. 
   rew_list. xapp*. xsimpl*.
  xfail. do 5 (destruct ts as [|? ts]; [ false* | ]). rew_list in Len. math.
Qed. 
Hint Extern 1 (RegisterSpec digit_from_treestr_list) => Provide digit_from_treestr_list_spec.

Lemma ftree_digit_push_front_spec : 
  Spec ftree_digit_push_front d f |R>>
     forall n L1 L2, digit_inv0 n d L1 -> ftree_inv n f L2 -> 
     R \[] (fun f' => \[ftree_inv n f' (L1++L2)]).
Proof using.
 (* TODO *)
Admitted.

Hint Extern 1 (RegisterSpec ftree_digit_push_front) => Provide ftree_digit_push_front_spec.

Lemma ftree_digit_push_back_spec : 
  Spec ftree_digit_push_back d f |R>>
     forall n L1 L2, ftree_inv n f L1 -> digit_inv0 n d L2 -> 
     R \[] (fun f' => \[ftree_inv n f' (L1++L2)]).
Proof using.
 (* TODO *)
Admitted.

Hint Extern 1 (RegisterSpec ftree_digit_push_back) => Provide ftree_digit_push_back_spec.

(** [xfun_induction_skip S IH] is similar to [xfun_induction]
    except that an induction hypothesis IH is provided without
    a proof of termination. *)

Tactic Notation "xfun_induction_skip" constr(S) ident(IH) :=
  xfun_core S ltac:(fun _ => 
    let H := fresh in
    intro H; unfolds_to_spec tt; skip_goal IH; move H after IH; xbody).

Tactic Notation "xfun_induction_nointro_skip" constr(S) ident(IH) :=
  xfun_core S ltac:(fun _ => 
    let H := fresh in
    intro H; unfolds_to_spec tt; skip_goal IH; move H after IH; xbody_nointro).

Hint Resolve  tree_list_inv_nil.

Lemma merge_digits_spec : 
  Spec merge_digits d1 d2 d3 |R>>
     forall n L1 L2 L3, digit_inv n d1 L1 -> digit_inv0 n d2 L2 -> digit_inv n d3 L3 -> 
     R \[] (fun d => \[digit_inv (S n) d (L1++L2++L3)]).
Proof using.
  xcf. introv I1 I2 I3.
  xfun_induction_nointro_skip (fun f => Spec f ts |R>> 
      forall L, tree_list_inv n ts L -> (len ts >= 2)%Z ->
      R \[] (fun ts' => \[tree_list_inv (S n) ts' L 
                     /\ (len ts' >= 1)%Z 
                     /\ (len ts' * 3 <= len ts + 2)%Z ])) IH.
    gen IH. clears_all. intros IH ts L (Ls&M&ELs) Gt. subst L. xmatch; rew_list in Gt.
    xfail. math.
    xfail. math.
    inverts M as I1 M. inverts M as I2 M. inverts M. 
     xapp*. intros. xret. xsimpl. rew_list. splits; [|math|math]. exists* ((x2++x0)::nil).
    inverts M as I1 M. inverts M as I2 M. inverts M as I3 M.
     inverts M. xapp*. intros. xret. xsimpl. rew_list. 
     splits; [|math|math]. exists* ((x2++x0++x3)::nil). 
    inverts M as I1 M. inverts M as I2 M. inverts M as I3 M.
     inverts M as I4 M. inverts M. xapp*. intros. xapp*. intros. xret. 
     xsimpl. rew_list. splits; [|math|math]. exists* ((x2++x0)::(x3++x4)::nil).
    inverts M as I1 M. inverts M as I2 M. inverts M as I3 M.
     xapp*. intros. xapp*. exists* __.
     do 2 (destruct ts0 as [|? ts0]; [ false* | ]). rew_list. math.
     introv ((Lr&?&ELr)&Len'). xret. xsimpl. rew_list. splits; [|math|math].
     exists ((x2++x0++x3)::Lr). rew_list. rewrite <- ELr. split*.
  xvals.
  xapp*. intros (?&?&?).
  xapp_spec* merge_digit_list_spec0. intros (?&?&?).
  xapp*. intros (?&?&?).
  xapp*. math.
  intros (?&?&?).
  xapp*. rew_list in *. math.
  rew_list. xsimpl*. 
Qed. 

Hint Extern 1 (RegisterSpec merge_digits) => Provide merge_digits_spec.

Lemma ftree_inv_empty_nil : forall L n f,
  ftree_inv n f L -> Ftree_str f = Ftree_empty -> L = nil.
Proof using. introv IT E. inverts IT as M. inverts M; simpl; inverts~ E. Qed.

Lemma ftree_inv_single_one : forall L n f t,
  ftree_inv n f L -> Ftree_str f = Ftree_single t -> tree_inv n t L.
Proof using. introv IT E. inverts IT as M. inverts M; simpls; inverts~ E. Qed.

Lemma ftree_inv_deep_inv : forall L n f dl fm dr,
  ftree_inv n f L -> Ftree_str f = Ftree_deep dl fm dr -> 
   exists L1 L2 L3,
   digit_inv n dl L1 /\ ftree_inv (S n) fm L2 /\ digit_inv n dr L3 /\ L = L1 ++ L2 ++ L3.
Proof using. introv IT E. inverts IT as M. inverts M; simpls; inverts~ E. exists___*. Qed.



Lemma merge_with_digit_spec : 
  Spec merge_with_digit fl d fr |R>>
     forall n L1 L2 L3, ftree_inv n fl L1 -> digit_inv0 n d L2 -> ftree_inv n fr L3 -> 
     R \[] (fun f => \[ftree_inv n f (L1++L2++L3)]).
Proof using.
  skip_goal IH.
  xcf. introv I1 I2 I3.
  xapps*. xapps*. xmatch.
  xapp*. xsimpl*. rewrite* (ftree_inv_empty_nil I1).
  xapp*. xsimpl*. rewrite* (ftree_inv_empty_nil I3).
  xapp*. intros. forwards*: ftree_inv_single_one I1. xapp*. xsimpl*.
  xapp*. intros. forwards*: ftree_inv_single_one I3. xapp*. rew_list. xsimpl*.
  forwards* (?&?&?&?&?&?&?): ftree_inv_deep_inv I1.
   forwards* (?&?&?&?&?&?&?): ftree_inv_deep_inv I3. subst L1 L3.
   clear I1 I3 H0 H1. clear C0 C1 C2. (*todo: clearpat *)
   xapp*. intros. xapp*. intros. xapp*. rew_list. xsimpl*.
Qed. 

Hint Extern 1 (RegisterSpec merge_with_digit) => Provide merge_with_digit_spec.

Lemma merge_spec : 
  Spec merge ff1 ff2 |R>>
     forall L1 L2, fftree_inv ff1 L1 -> fftree_inv ff2 L2 -> 
     R \[] (fun ff => \[fftree_inv ff (L1++L2)]).
Proof using.
  xcf. introv I1 I2. xapp*. rew_list. xsimpl*.
Qed. 

Hint Extern 1 (RegisterSpec merge) => Provide merge_spec.






(****************************************************)
(** Imperative versions *)

Parameter Fingertree : Items -> ftree -> hprop.

Axiom empty_spec :
  \[] ==> (ftree_empty ~> Fingertree nil).

Axiom is_empty_spec : 
  Spec is_empty (f:ftree) |R>>
     forall L,
     R (f ~> Fingertree L) (fun b => \[b = isTrue (L = nil)] \* f ~> Fingertree L).

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.

Axiom push_front_spec' : 
  Spec push_front (x:item) (f:ftree) |R>>
     forall X L, Rep X x -> 
     R (f ~> Fingertree L) (fun f' => f' ~> Fingertree (X::L)).

Hint Extern 1 (RegisterSpec push_front) => Provide push_front_spec'.

Axiom push_back_spec' : 
  Spec push_back (x:item) (f:ftree) |R>>
     forall X L, Rep X x -> 
     R (f ~> Fingertree L) (fun f' => f' ~> Fingertree (L&X)).

Hint Extern 1 (RegisterSpec push_back) => Provide push_back_spec'.

Axiom pop_front_spec' : 
  Spec pop_front (f:ftree) |R>>
     forall L, L <> nil ->
     R (f ~> Fingertree L) (fun p => let '(x,f') := p in Hexists X L',
          f' ~> Fingertree L' \* \[Rep X x /\ L = X::L']).

Hint Extern 1 (RegisterSpec pop_front) => Provide pop_front_spec'.

Axiom pop_back_spec' : 
  Spec pop_back (f:ftree) |R>>
     forall L, L <> nil ->
     R (f ~> Fingertree L) (fun p => let '(x,f') := p in Hexists X L',
          f' ~> Fingertree L' \* \[Rep X x /\ L = L'&X]).

Hint Extern 1 (RegisterSpec pop_back) => Provide pop_back_spec'.


End MakeSpec.
 
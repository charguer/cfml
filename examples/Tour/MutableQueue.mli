
type 'a t

val create : unit -> 'a t

val length : 'a t -> int

val is_empty : 'a t -> bool

val push : 'a -> 'a t -> unit

val pop : 'a t -> 'a

val transfer : 'a t -> 'a t -> unit

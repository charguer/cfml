(** Mutable queue, represented with two lists and a size field.

    The items stored in the queue [q] are described by the
    list [q.front ++ rev q.back]. *)

type 'a t = {
   mutable front : 'a list;
   mutable back : 'a list;
   mutable size : int }

let create () =
  { front = [];
    back = [];
    size = 0 }

let push x q =
  q.back <- x :: q.back;
  q.size <- q.size + 1

let pop q =
  if q.front = [] then begin
    q.front <- List.rev q.back;
    q.back <- [];
  end;
  match q.front with
  | [] -> raise Not_found
  | x::f ->
      q.front <- f;
      q.size <- q.size - 1;
      x

let length q =
  q.size

let is_empty q =
  q.size = 0

(** [transfer q1 q2] adds all of [q1]'s elements at the end
    of the queue [q2], then clears [q1] *)

let transfer q1 q2 =
  while not (is_empty q1) do
    push (pop q1) q2
  done

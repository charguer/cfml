Set Implicit Arguments.
Require Import UF.
Require Import UFPointerRank_ml.
Import RefGroupSpecs.


(*******************************************************************)
(** Types *)

Implicit Type D : set loc.
Implicit Type M : map loc contents_.
Implicit Type R : loc->loc.
Implicit Type x y z r s : loc.

Global Instance Inhab_contents_ : Inhab contents_.
Proof using. apply (Inhab_of_val (Root 0)). Qed.


(*******************************************************************)
(** Representation predicate *)


(** [parent M x c] maps a root to itself and a link to its destination. *)

Definition parent (x:loc) (c:contents_) : loc :=
  match c with
  | Root _ => x
  | Link y => y
  end.

(** [no_self_link D M] asserts that the target of a [Link]
    always differs from the node itself. *)

Definition no_self_link (D:set loc) (M:map loc contents_) : Prop :=
  forall x y, x \in D -> M[x] = Link y -> x <> y.

(** [inv D R M] captures the invariants of the structure *)

Definition inv (D:set loc) (R:loc->loc) (M:map loc contents_) : Prop :=
     D = dom M
  /\ roots (LibMaps.map_ parent M) R
  /\ (forall y, y \notin D -> R y = y)
  /\ no_self_link D M.

(** [UF D R] is a heap predicate that describes a pool of references,
    whose locations belong do the domain [D], representing an instance
    of the union-find data structure with respect to the root
    function [R]. *)

Definition UF (D:set loc) (R:loc->loc) : hprop :=
  Hexists (M:map loc contents_), Group Ref M \* \[inv D R M].


(*******************************************************************)
(** Hints *)

Lemma index_map_parent_eq : forall M x,
  index (LibMaps.map_ parent M) x = x \indom M.
Proof using.
 intros. rewrite index_eq_indom. rewrite* LibMaps.dom_map.
Qed.

Lemma indom_root : forall D R M x,
  inv D R M ->
  x \in D ->
  (R x) \in D.
Proof using.
  introv (HD&HR&HN&HL) Hx. subst D.
  rewrites* <- index_map_parent_eq.
  applys* index_R. rewrite* index_map_parent_eq.
Qed.

Hint Extern 1 (index (LibMaps.map_ parent ?L) ?x) =>
  rewrite index_map_parent_eq.
Hint Extern 1 (?x \in dom ?L) => congruence.
Hint Resolve indom_root.


(*******************************************************************)
(** Create *)

(** [roots] is preserved by creation of a fresh root. *)

Lemma roots_create : forall M R x,
  roots (LibMaps.map_ parent M) R ->
  x \notindom M ->
  R x = x ->
  roots (LibMaps.map_ parent M[x:=Root 0]) R.
Proof using.
  introv HR Hx Rx. unfolds roots.
  intros y Hy.
  rewrite index_eq_indom in Hy. rewrite LibMaps.dom_map in Hy.
  rewrite dom_update in Hy. rewrite in_union_eq in Hy. destruct Hy.
  { forwards* Ry: HR y. rewrite LibMaps.map_update.
    applys* repr_extends.
    { rewrite* LibMaps.dom_map. }
    { rewrite* LibMaps.dom_map. } }
  { rewrite* @in_single_eq in H. subst y. rewrite LibMaps.map_update.
    rewrite Rx. applys repr_root.
    { rewrite index_eq_indom. 
      rewrite LibMap.dom_update, LibMaps.dom_map. rewrite in_union_eq.
      right. rewrite* @in_single_eq. typeclass. }
    { rewrite* LibMap.read_update_same. }
    { typeclass. } }
Qed.

(** [inv] is preserved by creation of a fresh root. *)

Lemma inv_create : forall M R D x,
  inv D R M ->
  x \notin D ->
  inv (D \u '{x}) R M[x:=Root 0].
Proof using.
  introv (HD&HR&HN&HL) Hx. splits.
  { rewrite* dom_update. }
  { subst D. applys* roots_create. }
  { intros y Hy. rewrite notin_union_eq in Hy. applys* HN. }
  { intros y z Hy Hz. rewrite in_union_eq, in_single_eq in Hy.
    destruct Hy.
    { rewrite read_update in Hz. case_if. applys* HL. }
    { subst y. rewrite read_update in Hz. case_if. } }
Qed.

(** [create] is correct. *)

Lemma create_spec : forall D R,
  app create [tt]
    PRE (UF D R)
    POST (fun x => UF (D \u \{x}) R \* \[x \notin D]).
Proof using.
  xcf. xunfold UF. xpull. intros M HI. lets (HD&HR&HN&HL): HI.
  xapp. intros x. xsimpl*. applys* inv_create.
Qed.

Hint Extern 1 (RegisterSpec create) => Provide create_spec.


(*******************************************************************)
(** Find *)

(** [inv] is preserved by path compression *)

Lemma inv_compress : forall D R M M' x r,
  inv D R M ->
  x \in D ->
  x <> R x ->
  M' = M[x := Link (R x)] ->
  inv D R M'.
Proof using.
  introv (HD&HR&HN&HL) Hx Nx ->. splits.
  { rewrite* dom_update_at_index. rewrite* index_eq_indom. }
  { applys* roots_compress.
    { rewrite* LibMaps.map_update. }
    { rewrite* index_map_parent_eq. } }
  { auto. }
  { intros a b Ha Hb. rewrite read_update in Hb. case_if.
    { inverts* Hb. } { applys* HL. } }
Qed.

(** [find] is correct. *)

Lemma find_spec : forall D R x,
  x \in D ->
  app find [x]
    INV (UF D R)
    POST (fun r => \[r = R x]).
Proof using.
  introv Ix. xunfold UF. xpull. intros M HI. lets (HD&HR&HN&HL): HI.
  forwards* Hx: HR x.
  forwards (d&Hx'): reprn_of_repr Hx. clear Hx.
  gen M x. induction_wf IH: lt_wf d. hide IH. intros.
  xcf. xapps*. xmatch_no_intros.
  { intros n HLx. xrets*. forwards* Ex: roots_inv_R_root x R.
    rewrite* LibMaps.read_map. rewrite* HLx. }
  { intros y Hy. sets M':(LibMaps.map_ parent M). inverts Hx' as.
    { intros IRx E1 E2. forwards* HLx: roots_inv_L x HR.
      { subst M'. rewrite* index_map_parent_eq. }
      subst M'. rewrite* LibMaps.read_map in E1. rewrite <- E2 in E1.
      rewrite Hy in E1. simpls. unfolds in HL. false* (>> HL x y). }
    { intros Ix' N Hy'. rename d0 into d.
      sets z: (M'[x]). asserts EQ: (z = y).
      { subst z M'. rewrite* LibMaps.read_map. rewrite* Hy. }
      clearbody z. subst z.
      forwards* ER: roots_inv_R y (R x) HR.
      { applys* repr_of_reprn. }
      xapp_spec* (IH d); try solve [ subst M'; autos* ].
      { subst M'. lets: repr_of_reprn Hy'. subst D.
        rewrite <- index_map_parent_eq. applys* repr_inv_index_l. }
      asserts Ryx: (R y <> x).
      { intros E. forwards* HL'x: roots_inv_L x HR. subst M'.
        rewrite* LibMaps.read_map in HL'x. rewrite Hy in HL'x.
        simpls. false. }
      clears M M'. clear M HN. intros Hr M HI. lets (HD&HR&HN&HL): HI.
      xapp*. xrets*. applys* inv_compress. } }
Qed.

Hint Extern 1 (RegisterSpec find) => Provide find_spec.


(*******************************************************************)
(** Same *)

(** [same] is correct. *)

Lemma same_spec : forall D R x y,
  x \in D ->
  y \in D ->
  app same [x y]
    INV (UF D R)
    POST (fun b => \[b = isTrue (R x = R y)]).
Proof using.
  introv Ix Iy. xcf. xapps*. xapps*. xapps. xsimpl*.
Qed.

Hint Extern 1 (RegisterSpec same) => Provide same_spec.


(*******************************************************************)
(** Union *)

(** Invariants are preserved by a link operation. *)

Lemma inv_link : forall D R R' M M' x y,
  R' = link R x y (R y) ->
  M' = M[(R x) := Link (R y)] ->
  inv D R M ->
  x \in D ->
  y \in D ->
  R x <> R y ->
  inv D R' M'.
Proof using.
  introv ER' EL' HI Hx Hy Hxy. lets (HD&HR&HN&HL): HI.
  forwards* HRx: indom_root x HI. forwards* HRy: indom_root y HI.
  splits.
  { subst D M'. rewrite* dom_update_at_index. }
  { subst M'. applys* roots_link R x y. rewrite* LibMaps.map_update. }
  { intros z Hz. subst R'. unfold link. specializes HN Hz. case_if as Cz.
    { destruct Cz. { false; congruence. } { congruence. } } { auto. } }
  { subst M'. intros a b Ha Hb. rewrite read_update in Hb. case_if.
    { inverts* Hb. } { applys* HL. } }
Qed.

(** Additional lemma to prove that if a node [R x] is necessarily
    represented as [Root n] for some [n]. *)

Lemma roots_inv_L_Root : forall x D M R,
  inv D R M ->
  x \in D ->
  (R x) \in D ->
  exists n, M[R x] = Root n.
Proof using.
  introv (HD&HR&HN&HL) Hx HRx. forwards* EQ: roots_inv_L_root x HR.
  rewrite* LibMaps.read_map in EQ. cases (M[R x]) as ERx.
  { eauto. } { simpls. false* HL (R x). }
Qed.

(** [inv] is preserved by updating the rank information. *)

Lemma inv_rerank : forall x D M M' R n,
  M' = M[R x := Root n] ->
  inv D R M ->
  x \in D ->
  inv D R M'.
Proof using.
  introv Hd HI EL'. lets (HD&HR&HN&HL): HI.
  asserts ED: (dom M = dom M').
  { subst M'. rewrite* dom_update_at_index.
    rewrite* index_eq_indom. rewrite* <- HD. }
  forwards* HRx: indom_root x HI. splits.
  { subst M'. rewrite* dom_update_at_index. rewrite* index_eq_indom. }
  { asserts_rewrite* (LibMaps.map_ parent M' = LibMaps.map_ parent M).
    { applys LibMaps.extens. { repeat rewrite LibMaps.dom_map. auto. }
      { intros a Ha. rewrite LibMaps.dom_map in Ha.
        rewrite* LibMaps.read_map.
        rewrite* LibMaps.read_map; [| rewrite* ED].
        subst M'. rewrite read_update. case_if.
        { forwards* (m&Hm): roots_inv_L_Root x HI.
          subst a. rewrite Hm. simple*. }
        { auto. } } } }
  { auto. }
  { subst M'. unfolds no_self_link. intros a b Ha HLa.
   rewrite read_update in HLa. case_if. applys* HL. }
Qed.

Lemma union_spec : forall D R x y,
  x \in D ->
  y \in D ->
  app union [x y]
    (UF D R)
    (fun z => Hexists R', UF D R' \* \[R' = link R x y z]).
Proof using.
  introv Ix Iy. xcf. xapps*. xapps*. xapps. xif.
  { unfold UF.
    xpull. intros M HI. lets (HD&HR&HN&HL): HI.
    xapps*. xapps*. xmatch.
    { xif.
      { xapps*. xrets*. applys* inv_link. }
      { xif.
        { xapp*. xrets*. rewrite link_sym.
          applys* inv_link. }
        { xapp*. sets M': (M[R x:=Root (n + 1)%I]).
          asserts ED: (dom M = dom M').
          { unfold M'. rewrite* dom_update_at_index.
            rewrite* index_eq_indom. rewrite* <- HD. }
          forwards* HRy: indom_root y HI.
          xapp*. xrets*. rewrite link_sym.
          applys* inv_link.
          subst M'. applys* inv_rerank. } } }
   { xfail. (* false C0 *)
      forwards* (n&En): roots_inv_L_Root x D.
      forwards* (m&Em): roots_inv_L_Root y D. } }
  { xrets. rewrite link_sym. rewrite* link_related. }
Qed.

Hint Extern 1 (RegisterSpec union) => Provide union_spec.



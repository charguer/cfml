Set Implicit Arguments.
Require Import UF.
Import ArrayMap.
Require Import UFArray_ml.

Implicit Type P : map int int.
Implicit Type R : int->int.
Implicit Type x y z r s : int.
Implicit Types t : loc.


(*******************************************************************)
(** Representation predicate *)

(** [t ~> UF n R] is a heap predicate that describes an array [t]
    of size [n] representing an instance of the union-find data
    structure with respect to the root function [R], with
    elements indexed in the range [0...(n-1)]. *)

Definition UF (n:int) (R:int->int) (t:loc) : hprop :=
  Hexists (P:map int int), t ~> ArrayMap P
    \* \[conseq_dom n P /\ roots P R].

(** Note: [conseq_dom n P] asserts that [dom P] is equal to
    the range from [0...(n-1)]. *)


(*******************************************************************)
(** Create *)

Lemma create_spec : forall n,
  n >= 0 ->
  app create [n]
    PRE \[]
    POST (fun t => Hexists t R, t ~> UF n R \*
                   \[forall x, index n x -> R x = x]).
Proof using.
  introv Hn. xcf. xfun. xapp (fun (i:int) => i).
  { math. }
  { intros i Hi. xapp. xrets*. }
  { intros t. xpull. intros P HL HR. xunfold UF. xsimpl t (fun x => x) P.
    { auto. } { split*. applys* roots_init. } }
Qed.

Hint Extern 1 (RegisterSpec create) => Provide create_spec.


(*******************************************************************)
(** Find *)

Lemma find_spec : forall n R t x,
  index n x ->
  app find [t x]
    PRE (t ~> UF n R)
    POST (fun r => \[r = R x] \* t ~> UF n R).

(* Syntactic sugar to avoid repeating the precondition
   in the postcondition:
[[
  app find [t x]
    INV (t ~> UF n R)
    POST (fun r => \[r = R x]).
]]
*)

Proof using.
  introv Ix. xunfold UF. xpull. intros P (Hn&HR).
  forwards* Hx: HR x.
  forwards (d&Hx'): reprn_of_repr Hx.
  gen P x. induction_wf IH: lt_wf d. hide IH. intros.
  xcf. xapps*. xrets. xif.
  { xrets*. forwards* Ex: roots_inv_R_root x P R. }
  { inverts Hx' as. { intros. false. } intros Ix' N Hy. rename d0 into d.
    sets y: (P[x]). lets Ry: repr_of_reprn Hy.
    forwards* ER: roots_inv_R y (R x) HR. xapp_spec* (IH d).
    { subst. rewrites (>> conseq_dom_index_eq Hn). applys* index_L. }
    clearbody y. clears P. intros Hr P (Hn&HR).
    xapp*. xrets*. splits*.
    { applys* conseq_dom_update. }
    { rewrite Hr,ER. applys* roots_compress. } }
Qed.

Hint Extern 1 (RegisterSpec find) => Provide find_spec.


(*******************************************************************)
(** Same *)

Lemma same_spec : forall n R t x y,
  index n x ->
  index n y ->
  app same [t x y]
    INV (t ~> UF n R)
    POST (fun b => \[b = isTrue (R x = R y)]).
Proof using.
  introv Ix Iy. xcf. xapps*. xapps*. xrets.
Qed.

Hint Extern 1 (RegisterSpec same) => Provide same_spec.


(*******************************************************************)
(** Union *)

Lemma union_spec : forall n R t x y,
  index n x ->
  index n y ->
  app union [t x y]
    PRE (t ~> UF n R)
    POST (fun z => Hexists R', t ~> UF n R' \* \[R' = link R x y z]).
Proof using.
  introv Ix Iy. xcf. xapps*. xapps*. xrets. xif.
  { xunfold UF. xpull. intros P (Hn&HR).
    forwards* IRx: index_R x HR.
    xapp*. xrets*. splits.
    { applys* conseq_dom_update.
      rewrites* (>> conseq_dom_index_eq Hn) in *. }
    { applys* roots_link. } }
  { xrets. rewrite* link_related. }
Qed.

Hint Extern 1 (RegisterSpec union) => Provide union_spec.













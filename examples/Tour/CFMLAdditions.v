Set Implicit Arguments.
Require Export CFML.CFLib.
Require Export Stdlib.
Require Export Array_proof.
Require Import TLC.LibListZ.
Require TLC.LibSet.

Generalizable Variables A B.

(* TODO: local setup *)

Ltac auto_typeclass :=
  match goal with |- Inhab _ => typeclass end.

Ltac auto_star ::=
  try solve [ jauto_set; solve [ eauto | congruence | math | auto_typeclass ] ].


(* TODO: update TLC *)

Lemma lengthZ_zero_eq_eq_nil : forall A (l:list A),
  (LibListZ.length l = 0) = (l = nil).
Proof using.
  intros. unfolds length. rewrite <- LibList.length_zero_eq_eq_nil. math.
Qed.

Lemma index_update_inv : forall A `{Inhab B} (L:map A B) (a x:A) (b:B),
  index L a ->
  index (L[a:=b]) x ->
  index L x.
Proof using.
  introv IB Ia Ix. rewrite index_eq_indom in *.
  rewrite* dom_update_at_index in Ix.
Qed.

(* TODO: update CFML *)

Module ArrayMap.


Parameter ArrayMap : forall A, map int A -> loc -> hprop.

Import TLC.LibSet.
Open Scope set_scope.

Definition conseq_dom A (n:int) (M:map int A) : Prop :=
  dom M = \set{ i | 0 <= i < n }.

Parameter ArrayMap_conseq_dom : forall A (M:map int A) p,
  p ~> ArrayMap M ==+> Hexists n, \[conseq_dom n M].

Lemma conseq_dom_index_eq : forall A n (L:map int A),
  conseq_dom n L ->
  index n = index L.  (* i.e. [index L x <> index n x] *)
Proof using.
  introv HD. extens. intros x. rewrite int_index_eq.
  rewrite index_eq_indom. unfolds in HD. rewrite HD.
  rewrite* in_set_st_eq.
Qed.

Lemma conseq_dom_index_L : forall A n (L:map int A) x,
  conseq_dom n L ->
  index n x ->
  index L x.
Proof. introv Hd Hx. rewrites* <- (>> conseq_dom_index_eq Hd). Qed.

Hint Resolve conseq_dom_index_L.

Lemma conseq_dom_update : forall A n (L:map int A) x y,
  conseq_dom n L ->
  index n x ->
  conseq_dom n (L[x:=y]).
Proof using.
  introv HD Hx. unfolds conseq_dom.
  rewrite* dom_update_at_index. (* typeclass. *)
Qed.



(* TODO: ArrayMap domain *)

(* -------------------------------------------------------------------------- *)

(* [Array.length].

Parameter length_spec :
  curried 1%nat Array_ml.length /\
  forall A (xs:list A) t,
  app Array_ml.length [t]
    INV (t ~> Array xs)
    POST (fun n => \[n = length xs]).

Hint Extern 1 (RegisterSpec Array_ml.length) => Provide length_spec.
*)

(* -------------------------------------------------------------------------- *)

(* [Array.get]. *)

Parameter get_spec :
  curried 2%nat Array_ml.get /\
  forall A `{Inhab A} (xs:map int A) t i,
  index xs i ->
  app Array_ml.get [t i]
    INV (t ~> ArrayMap xs)
    POST \[= xs[i] ].

Hint Extern 1 (RegisterSpec Array_ml.get) => Provide get_spec.

(* -------------------------------------------------------------------------- *)

(* [Array.set]. *)

Parameter set_spec :
  curried 2%nat Array_ml.set /\
  forall A (xs:map int A) t (i:int) (x:A),
  index xs i ->
  app Array_ml.set [t i x]
    PRE  (  t ~> ArrayMap xs)
    POST (# t ~> ArrayMap (xs[i:=x])).

Hint Extern 1 (RegisterSpec Array_ml.set) => Provide set_spec.

(* -------------------------------------------------------------------------- *)

(* [Array.make].

Parameter make_spec :
  curried 2%nat Array_ml.make /\
  forall A n (x:A),
  0 <= n ->
  app Array_ml.make [n x]
    PRE  \[]
    POST (fun t => Hexists xs, t ~> ArrayMap xs \* \[xs = make n x]).

Hint Extern 1 (RegisterSpec Array_ml.make) => Provide make_spec.
*)

(* -------------------------------------------------------------------------- *)

(* [Array.init]. *)

Axiom init_spec :
  forall A (IA:Inhab A) (F : int -> A) (n : int) (f : func),
  0 <= n ->
  (forall (i : int),
      index n i ->
      app f [i]
        PRE  \[]  (* simplified spec for now: no state carried through *)
        POST (fun (x:A) => \[x = F i])) ->
  app Array_ml.init [n f]
    PRE  \[]
    POST (fun t =>
           Hexists (xs:map int A), t ~> ArrayMap xs \* \[conseq_dom n xs]
                   \* \[forall i, index xs i -> xs[i] = F i]).

Hint Extern 1 (RegisterSpec Array_ml.init) => Provide init_spec.


End ArrayMap.


(* TODO: update TLC *)

Module LibMaps.
Section Map.
Transparent map.

Definition map_ A `{Inhab B1} `{Inhab B2} (f:A->B1->B2) (M:map A B1) : map A B2 :=
  fun (x:A) => match M x with
    | None => None
    | Some y => Some (f x y)
    end.

Transparent update update_inst bag_update_as_union_single single_bind single_bind_inst LibMap.union_inst LibContainer.union.

Lemma map_update : forall A `{Inhab B1} `{Inhab B2} (f:A->B1->B2) (M:map A B1) x y,
  map_ f (M[x:=y]) = (map_ f M)[x:=f x y].
Proof using.
  intros. extens. intros a. unfold map_. simpl. unfold bag_update_as_union_single.
  unfold single_bind. simpl. unfold single_bind_impl. unfold LibContainer.union. simpl.
  unfold LibMap.union_impl.
  case_if. autos*. destruct* (M a). (* TODO: cleanup *)
Qed.

Transparent dom dom_inst.

Lemma dom_map : forall A `{Inhab B1} `{Inhab B2} (f:A->B1->B2) (M:map A B1),
  dom (map_ f M) = dom M.
Proof using.
  intros. unfold map_. simpl. unfold dom_impl.
  fequal. extens. intros x. destruct (M x); auto_false.
Qed.

Transparent read LibMap.read_inst.

Lemma read_map : forall A `{Inhab B1} `{Inhab B2} (f:A->B1->B2) (M:map A B1) x,
  x \indom M ->
  (map_ f M)[x] = f x (M[x]).
Proof using.
  introv Hx. unfold map_. simpl. unfold read, LibMap.read_inst. unfold LibMap.read_impl.
  unfold dom, LibMap.dom_inst, dom_impl in Hx. rewrite in_set_st_eq in Hx.
  destruct (M x); auto_false.
Qed.

Axiom extens : forall A `{Inhab B} (M1 M2:map A B),
  dom M1 = dom M2 ->
  (forall (x:A), x \indom M1 -> M1[x] = M2[x]) ->
  M1 = M2.

End Map.
End LibMaps.


(* TODO: update CFML *)

Module RefGroupSpecs.
Hint Extern 1 (RegisterSpec ref) => Provide ref_spec_group.
Hint Extern 1 (RegisterSpec infix_emark__) => Provide infix_emark_spec_group.
Hint Extern 1 (RegisterSpec infix_colon_eq__) => Provide infix_colon_eq_spec_group.
End RefGroupSpecs.


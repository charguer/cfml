Set Implicit Arguments.
Require Import CFML.CFLib Stdlib CFMLAdditions.
Require Import MutableQueue_ml.
Require Import TLC.LibListZ.

Implicit Types n : int.
Implicit Types q : loc.


(*******************************************************************)
(** Representation Predicate *)

(** [q ~> Queue L] is a notation of [Queue L q], a heap 
    predicate that describes a mutable queue at location [q]
    with items represented by the list [L]. *)

Definition Queue A (L:list A) (q:t_ A) : hprop :=
  Hexists (f:list A) (b:list A) (n:int),
      q ~> `{ front' := f; back' := b; size' := n }
   \* \[ L = f ++ rev b /\ n = length L ].

(** Note: OCaml features different namespaces for values,
    types, and record fields, but all fall in the same 
    namespace in Coq. To avoid clashes,
    - types are suffixed with a underscore symbol
    - field names are suffixed with a quote symbol. *)

(** Note: [q] has type ['a t] is OCaml, translated as [t_ A]
    in Coq. The type [t_ A] is defined as [loc], which is 
    the type of memory locations in CFML. *)


(*******************************************************************)
(** Unfolding and folding of the representation predicate *)

Lemma Queue_open : forall q A (L:list A),
  q ~> Queue L ==>
  Hexists f b n, q ~> `{ front' := f; back' := b; size' := n }
     \* \[ L = f ++ rev b /\ n = length L ].
Proof using.
  intros. xunfolds* Queue.
Qed.

Lemma Queue_close : forall q A (f b:list A) n,
  n = length (f ++ rev b) ->
  q ~> `{ front' := f; back' := b; size' := n } ==>
  q ~> Queue (f ++ rev b).
Proof using. intros. xunfolds* Queue. Qed.

Arguments Queue_close : clear implicits.

(** Customization of [xopen] and [xclose] tactics for [Queue]. *)

Hint Extern 1 (RegisterOpen (Queue _)) =>
  Provide Queue_open.
Hint Extern 1 (RegisterClose (record_repr _)) =>
  Provide Queue_close.


(*******************************************************************)
(** Create *)

(** Cusom notation for specifications:
[[
   app f [x1 x2] 
    PRE H 
    POST (fun r => H')
]]
*)

Lemma create_spec : forall A,
  app create [tt]
    PRE \[]
    POST (fun q => q ~> Queue (@nil A)).
Proof using.
  xcf. xapp. intros q. xclose q.
  { rew_list. auto. } 
  { xsimpl. }
Qed.

Hint Extern 1 (RegisterSpec create) => Provide create_spec.


(*******************************************************************)
(** Push *)

(** 
[[
  let push x q =
    q.back <- x :: q.back;
    q.size <- q.size + 1
]]
   The code is A-normalized by CFML as:
[[
  let push x q =
    let x0__ = q.back in
    q.back <- x :: x0__;
    let x1__ = q.size in
    q.size <- x1__ + 1
]]
*)

Lemma push_spec : forall A (L:list A) q x,
  app push [x q]
    PRE (q ~> Queue L)
    POST (fun (_:unit) => q ~> Queue (L ++ x::nil)).
Proof using.
  xcf. xopen q. xpull. intros f b n (IL&In).
  xapps. xapp. xapps. xapp. intros _.
  xclose q. { subst. rew_list. math. }
  { xsimpl. subst. rew_list. auto. } 
Qed.

Hint Extern 1 (RegisterSpec push) => Provide push_spec.


(*******************************************************************)
(** Pop *)

(** 
[[
  let pop q =
    if q.front = [] then begin
      q.front <- List.rev q.back;
      q.back <- [];
    end;
    match q.front with
    | [] -> raise Not_found
    | x::f ->
        q.front <- f;
        q.size <- q.size - 1;
        x
]]
*)

Lemma pop_spec : forall A (L:list A) q,
  L <> nil ->
  app pop [q]
    PRE (q ~> Queue L)
    POST (fun x => Hexists L', \[L = x::L'] \* q ~> Queue L').
Proof using.
  introv N. xcf. xopen q. xpull. intros f b n (I1&I2).
  xapps. xapps. xpolymorphic_eq.
  xseq (fun (_:unit) => Hexists f' b',
      q ~> `{ front' := f'; back' := b'; size' := n }
      \* \[ L = f' ++ rev b' ] \* \[ f' <> nil ]).
  { xif.
    { xapps. xrets. xapps. xapps. xsimpl. 
      { subst*. } { subst. rew_list*. } }
    { xrets*. } }
  { clears f b. intros f b I1 I3. xapps. xmatch.
    { xapps. xapps. xapps. xret.
      xclose q. { subst L. rew_list in *. math. }
      xsimpl. subst L. rew_list*. } }
Qed.

Hint Extern 1 (RegisterSpec pop) => Provide pop_spec.


(*******************************************************************)
(** Length *)

Lemma length_spec : forall (A:Type) (L:list A) (q:loc),
(* app MutableQueue_ml.length [s]
     PRE (q ~> Queue L)
     POST (fun n => \[n = length L] \* q ~> Queue L). 
*)
  app MutableQueue_ml.length [q]
    INV (q ~> Queue L)
    POST (fun n => \[n = length L]).
Proof using.
  xcf.
  xopen q. xpull. intros f b n (I1&I2).
  xapp. xpull. intros ->. 
  xclose q. { subst*. }
  xsimpl*.
  (* Here we have an issue because Coq is unable to infer a type. *)
  Unshelve. solve_type.
Qed.

Hint Extern 1 (RegisterSpec length) => Provide length_spec.


(*******************************************************************)
(** Is_empty *)

Lemma is_empty_spec : forall A (L:list A) q,
  app is_empty [q]
    INV (q ~> Queue L)
    POST (fun b => \[b = isTrue (L = nil)]).
Proof using.
  xcf. xopen q. xpull. intros f b n (I1&I2).  
  xapps. xclose* q. xrets.
  subst n. rewrite lengthZ_zero_eq_eq_nil. tauto.
  Unshelve. solve_type.
Qed.

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.


(*******************************************************************)
(** Transfer *)

Lemma transfer_spec : forall A (L1 L2:list A) q1 q2,
  app transfer [q1 q2]
    PRE (q1 ~> Queue L1 \* q2 ~> Queue L2)
    POST (fun (_:unit) => q1 ~> Queue (@nil A) \* q2 ~> Queue (L2 ++ L1)).
Proof using.
  xcf. xwhile; [|xok]. intros R LR HR. revert L2.
  induction_wf IH: (@list_sub A) L1. hide IH.
  intros. applys (rm HR).
  xlet (fun b => \[b = isTrue (L1 <> nil)] 
        \* q1 ~> Queue L1 \* q2 ~> Queue L2).
  { xapps. xrets. rew_isTrue*. }
  intros ->. xif.
  { destruct L1 as [|x L1']; tryfalse. 
    xseq. { xapps*. intros L1'' E. inverts E. xapps. }
    show IH. xpull.
    xapplys* IH. rew_list*. } 
  { xret_no_gc. xsimpl. subst. rew_list*. }
Qed.

Hint Extern 1 (RegisterSpec transfer) => Provide transfer_spec.



(* Union-find: pointer-based implementation
   including the union-by-rank mechanism *)

type contents = Root of int | Link of contents ref

type node = contents ref

let create () =
  ref (Root 0)

let rec find x =
  match !x with
  | Root _ -> x
  | Link y ->
      let r = find y in
      x := Link r;
      r

let same x y =
  find x == find y

let union x y =
  let r = find x in
  let s = find y in
  if r != s then begin
    match !r, !s with
    | Root n, Root m ->
       if n < m then begin
         r := Link s;
         s
       end else if n > m then begin
         s := Link r;
         r
       end else begin
         r := Root (n+1);
         s := Link r;
         r
       end
    | _ -> assert false
  end else r



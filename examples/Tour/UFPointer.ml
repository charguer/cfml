(* Union-find: pointer-based implementation,
   without the union-by-rank mechanism *)

type contents = Root | Link of contents ref

type node = contents ref

let create () =
  ref Root

let rec find x =
  match !x with
  | Root -> x
  | Link y ->
      let r = find y in
      x := Link r;
      r

let same x y =
  find x == find y

let union x y =
  let r = find x in
  let s = find y in
  if r != s
    then (r := Link s; s)
    else s



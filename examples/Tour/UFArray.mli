

type node = int

type t

val create : int -> t

val find : t -> node -> node

val union : t -> node -> node -> node

val same : t -> node -> node -> bool



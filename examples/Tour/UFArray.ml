(* Union-find: array-based implementation,
   without the union-by-rank mechanism *)

type node = int

type t = node array

let create n =
  Array.init n (fun i -> i)

let rec find t x =
  let p = t.(x) in
  if p = x then p else begin
    let r = find t p in
    t.(x) <- r;
    r
  end

let same t x y =
  find t x = find t y

let union t x y =
  let r = find t x in
  let s = find t y in
  if r <> s
    then (t.(r) <- s; s)
    else s


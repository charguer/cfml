
type node

val create : unit -> node

val find : node -> node

val union : node -> node -> node

val same : node -> node -> bool


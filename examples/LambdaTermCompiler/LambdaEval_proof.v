(**************************************************************************
* Proving functional programs through a deep embedding                    *
* Lambda-term evaluator : verification                                    *
***************************************************************************)

Set Implicit Arguments.
Require Import CFLib LambdaEval_ml.
Open Local Scope heap_scope_advanced.


(*==========================================================*)
(* * Definitions at the logical level *)
(*==========================================================*)

(************************************************************)
(* ** Specification of substitution *)

Fixpoint Subst (x : int) (v : term) (t : term) : term :=
  match t with
  | Tvar y => if x '= y then v else t
  | Tint n => Tint n
  | Tfun y b => Tfun y (if x '= y then b else Subst x v b)
  | Tapp t1 t2 => Tapp (Subst x v t1) (Subst x v t2)  
  end.


(************************************************************)
(* ** Specification of big-step reduction *)

Inductive Reds : term -> term -> Prop :=
  | Reds_int : forall n,
      Reds (Tint n) (Tint n)
  | Reds_fun : forall y b,
      Reds (Tfun y b) (Tfun y b)
  | Reds_app : forall x b v2 t1 t2 v,
      Reds t1 (Tfun x b) ->
      Reds t2 v2 ->
      Reds (Subst x v2 b) v ->
      Reds (Tapp t1 t2) v.

(************************************************************)
(* ** Subterm relationship, to argue for termination *)

Inductive term_sub : term -> term -> Prop :=
  | term_sub_fun : forall x t,
     term_sub t (Tfun x t)
  | term_sub_app_1 : forall t1 t2,
     term_sub t1 (Tapp t1 t2) 
  | term_sub_app_2 : forall t1 t2,
     term_sub t2 (Tapp t1 t2).
  
Hint Constructors term_sub.

Lemma term_sub_wf : wf term_sub.
Proof.
  intros t. induction t; apply Acc_intro;
    introv H; inverts H; auto.
Qed.

Hint Resolve term_sub_wf : wf.


(*==========================================================*)
(* * Optimized tactics under development *)
(*==========================================================*)


(*todo:remove*)
Ltac xapps_core spec args solver ::=
  let cont1 tt :=
    xapp_with spec args solver in
  let cont2 tt :=
    instantiate; xextract; try intro_subst in    
  match ltac_get_tag tt with
  | tag_let_trm => xlet; [ cont1 tt | cont2 tt ]
  | tag_seq =>     xseq; [ cont1 tt | cont2 tt ]
  end.

Ltac refl :=
  fold_bool; rew_refl in *.

Lemma app_weaken_1 : forall B H1 Q1 H2 A1 (x1:A1) f H (Q:B->hprop),
  app_1 f x1 H1 Q1 -> 
  H ==> H1 \* H2 -> 
  Q1 \*+ H2 ===> Q ->
  app_1 f x1 H Q.
Proof. intros. applys* local_wframe. Qed.

Lemma app_weaken_2 : forall B H1 Q1 H2 A1 A2 (x1:A1) (x2:A2) f H (Q:B->hprop),
  app_2 f x1 x2 H1 Q1 -> 
  H ==> H1 \* H2 -> 
  Q1 \*+ H2 ===> Q ->
  app_2 f x1 x2 H Q.
Proof. intros. applys* local_wframe. Qed.

Lemma app_weaken_3 : forall B H1 Q1 H2 A1 A2 A3 (x1:A1) (x2:A2) (x3:A3) f H (Q:B->hprop),
  app_3 f x1 x2 x3 H1 Q1 -> 
  H ==> H1 \* H2 -> 
  Q1 \*+ H2 ===> Q ->
  app_3 f x1 x2 x3 H Q.
Proof. intros. applys* local_wframe. Qed.

Lemma app_weaken_4 : forall B H1 Q1 H2 A1 A2 A3 A4 (x1:A1) (x2:A2) (x3:A3) (x4:A4) f H (Q:B->hprop),
  app_4 f x1 x2 x3 x4 H1 Q1 -> 
  H ==> H1 \* H2 -> 
  Q1 \*+ H2 ===> Q ->
  app_4 f x1 x2 x3 x4 H Q.
Proof. intros. applys* local_wframe. Qed.

Ltac get_app_weaken_x x :=
  match x with
     | 1%nat => constr:(app_weaken_1)
     | 2%nat => constr:(app_weaken_2)
     | 3%nat => constr:(app_weaken_3)
     | 4%nat => constr:(app_weaken_4)
  end.
        
Ltac try_app_hyp f cont :=
  match goal with
  | H: context [ app_1 f _ _ _ ] |- _ => cont (H)
  | H: context [ app_2 f _ _ _ _ ] |- _ => cont (H)
  | H: context [ app_3 f _ _ _ _ _ ] |- _ => cont (H) 
  | H: context [ app_4 f _ _ _ _ _ _ ] |- _ => cont (H) 
  end.

Tactic Notation "xapp'" :=
  xuntag tag_apply; 
  let f := spec_goal_fun tt in 
  try_app_hyp f ltac:(fun H =>
  (*let n := spec_term_arity H in*)
  let n := spec_goal_arity tt in
  let W := get_app_weaken_x n in
  eapply W; [ apply H | hsimpl | try apply rel_le_refl ]).

Tactic Notation "xapps'" :=
  xlet; [ xapp' | try xextract; try intro_subst ].


(*==========================================================*)
(* * Verification of the code *)
(*==========================================================*)


Lemma subst_spec :
  Spec subst x v t |R>> 
    pure R (= Subst x v t).
Proof.
  xinduction (unproj33 int term term_sub). 
skip.
  xintros x v t. intros IH. 
  apply app_spec_3. xcf. intro_subst_arity 3%nat.  (* todo : tactic *)
  xmatch; simpl. (* destruct t as [ y | n | y b | t1 t2] *)
  destruct (x '= y); xif; xrets~. (* todo: better *) 
  xrets~.
  xlets (fun z => \[z = (if x '= y then b else Subst x v b) ]). (* todo: auto *)
    xif.
      xrets. case_if~. refl; false. (* was: false *)
      xlet. xapp_spec~ IH. (* was: xapps~. *)
       xrets~. case_if~. refl; false. 
   xrets~.
  xapp_spec~ IH. intro_subst. (* was xapps~. *) 
  xapp_spec~ IH. intro_subst. (* was xapps~. *) 
  xrets~.
Qed.

Hint Extern 1 (RegisterSpec subst) => Provide subst_spec.

Lemma eval_spec :
  Spec eval t |R>> forall v, Reds t v -> R \[] (fun x => \[x = v]).
Proof.
  xintros t. introv Red. induction Red;
  apply app_spec_1; xcf; intro_subst_arity 1%nat; 
  xmatch.  (* todo : tactic *)
  xrets~.
  xrets~.
  xapps'. xmatch; [ | false* C0 ]. (* tactic todo *)
  xapps'.
  xapps. xapp'. xsimpl~.
Qed.

Inductive Stuck : term -> Prop :=
  | Stuck_var : forall x,
      Stuck (Tvar x)
  | Stuck_app : forall t1 t2,
      (forall y b, t1 <> Tapp y b) ->
      Stuck (Tapp t1 t2).


Lemma eval_spec_partial :
  Spec eval t |R>> R \[] (fun v => \[Reds t v]).
  (* todo: add maydiverge as precondition *)
Proof.
  skip_goal IH. (* todo: fix_partial *)
  xintros t. xcf_app. (* was:  apply app_spec_1. xcf. intro_subst_arity 1%nat; *)
  xmatch.
  xfail. skip. (* needs to take closed terms / welltyped terms only *)
  xapp. introv R1. xmatch.
    xapp. introv R2. xapps.
      xapp. hsimpl. constructors*.
    skip. (* needs typing / or fails to reduce *)
  xapps'.
  xapps. xapp'. xsimpl~.
Qed.






(*==========================================================*)
(* * Definitions for adding simple types *)

(* ==> needs var to be exported as int...

Require Import LibEnv.

Notation "x ~~ a" := (single x a)
  (at level 27, left associativity) : env_scope.

(************************************************************)
(* ** Grammar of simple types *)

CoInductive typ :=
  | typ_int : typ
  | typ_arrow : typ -> typ -> typ.


(************************************************************)
(* ** Typing judgment *)

Inductive typing : env typ -> term -> typ -> Prop :=
  | typing_int : forall E k,
      ok E ->
      typing E (Tint k) typ_int
  | typing_var : forall E x T,
      ok E ->
      binds x T E ->
      typing E (Tvar x) T
  | typing_abs : forall x E U T t1,
      typing (E & x ~~ U) t1 T ->
      typing E (Tfun x t1) (typ_arrow U T)
  | typing_app : forall T1 T2 E t1 t2,
      typing E t1 (typ_arrow T1 T2) -> 
      typing E t2 T1 ->
      typing E (Tapp t1 t2) T2.

(************************************************************)
(* ** Proof of type soundness *)

(** Hints *)

Hint Constructors one typing.
Hint Extern 1 (_ < _) => math.
Hint Extern 1 (~ one _ -> exists _, _) => 
  let H := fresh in intros H; false H.

(** Regularity lemma *)

Lemma typing_ok : forall E t T,
  typing E t T -> ok E.
Proof. introv H. induction* H. Qed.

(** Weakening for values *)

Lemma typing_val_weaken : forall E v T,
  typing empty v T -> ok E -> typing E v T.
Proof. introv H. inverts* H. Qed.

(** Substitution lemma *)

Lemma typing_subst : forall E x T U t v,
  typing empty v U ->
  typing (empty & single x U & E) t T ->
  typing E (subst x v t) T.
Proof.
  introv Tv Tt. inductions Tt; simpl.
  constructors*.
  constructors*.
  case_if. 
    binds_cases H0.
      false* binds_empty_inv. 
      subst. applys* typing_val_weaken.
      lets (?&?): (ok_middle_inv H).
       false~ (binds_fresh_inv B0).
    binds_cases H0.
      false* binds_empty_inv.
      constructors~.
  case_if.
    false. lets: typing_ok Tt.
     rewrite <- concat_assoc in H.
     lets (?&M): (ok_middle_inv H).
     simpl_dom. notin_false.
    forwards*: IHTt. rewrite* concat_assoc. 
  constructors*.
Qed.



*)
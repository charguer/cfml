

type 'a queue = 'a list * 'a list

val empty : 'a queue

val is_empty : 'a queue -> bool

val push : 'a queue -> 'a -> 'a queue

val pop : 'a queue -> 'a * 'a queue


(** Functional queue represented as a pair of lists.

    The pair (front,back) represents the list [front ++ rev back].
    The front list is maintained nonempty for nonempty queues. *)

type 'a queue = 'a list * 'a list

let empty : 'a queue = ([],[])

let is_empty q =
 match (q : 'a queue) with
 | [], _  -> true
 | _ -> false

let check = function
 | [], b -> (List.rev b, [])
 | q -> q

let push (f,b) x =
  check (f, x::b)

let pop = function
 | [], _ -> raise Not_found
 | x::f, b -> (x, check (f,b))


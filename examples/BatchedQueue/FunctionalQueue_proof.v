Set Implicit Arguments.
Require Import CFML.CFLib Stdlib.
Require Import FunctionalQueue_ml.


(*******************************************************************)
(** Representation Predicate *)

(** [inv L (f,b)] asserts that [L] describes the items stored in 
    the queue represented as [(f,b)], from front to back. *)

Definition inv A (L:list A) (q:queue_ A) : Prop :=
  let (f,b) := q in
     (L = f ++ rev b)
  /\ (f = nil -> b = nil).


(*******************************************************************)
(** Empty *)

Lemma empty_spec : forall A,
  inv (@nil A) (@empty A).
Proof.
  intros. rewrite empty__cf. unfolds*.
Qed.

Hint Extern 1 (RegisterSpec empty) => Provide empty_spec.


(*******************************************************************)
(** IsEmpty *)

Lemma is_empty_spec : forall A (L:list A) q,
  inv L q ->
  app is_empty [q]
    PRE \[]
    POST (fun b => \[b = isTrue (L = nil)]).
Proof.
  intros A L (f,b) [I1 I2]. xcf. xmatch; xrets.
  { rewrite* I2 in I1. }
  { subst L. applys* app_not_empty_l. intros ->. false* C. }
Qed.

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.


(*******************************************************************)
(** Check *)

Lemma check_spec : forall A (f b:list A),
  app check [(f,b)]
    PRE \[]
    POST (fun q => \[inv (f ++ rev b) q]).
Proof.
  xcf. xmatch.
  { xrets. xrets. unfolds. rew_list*. }
  { xrets. split. { auto. } { intros ->. false* C. } }
Qed.

Hint Extern 1 (RegisterSpec check) => Provide check_spec.


(*******************************************************************)
(** Push *)

(** Source code desugared as 
[[
  let push q x =
    match q with (f,b) -> check (f, x::b)
]]
*)

Lemma push_spec : forall A (L:list A) q x,
  inv L q ->
  app push [q x]
    PRE \[]
    POST (fun q' => \[inv (L ++ x::nil) q']).
Proof.
  intros A L [f b] x [I1 I2]. xcf. xmatch. xapp.
  xpull. intros I'. xsimpl. subst L. rew_list in *. auto.
Qed.

(* Note: [(L ++ x::nil)] gets displayed as [L & x]. *)

Hint Extern 1 (RegisterSpec push) => Provide push_spec.


(*******************************************************************)
(** Pop *)

(** Source code desugared as 
[[
    let pop q = 
     match q with
     | [], _ -> raise Not_found
     | x::f, b -> let x1__ = check (f,b) in (x, x1__)
]]
*)

Lemma pop_spec : forall A (L:list A) q,
  inv L q ->
  L <> nil ->
  app pop [q]
    PRE \[]
    POST (fun '(x, q') => \[exists L', L = x::L' /\ inv L' q']).
Proof.
  intros A L [f b] [I1 I2] N. xcf. xmatch.
  { xfail. rewrite* I2 in I1. }
  { xapp. intros I'. xrets. rew_list in *. eauto. }
Qed.

Hint Extern 1 (RegisterSpec pop) => Provide pop_spec.



Set Implicit Arguments. 
Require Import CFLib BST_ml Buffer.


(**********************************************************)
(** Search function *)

(*--------------------------------------------------------*)

Inductive tree :=
  | Leaf : tree
  | Node : tree -> int -> tree -> tree.

Instance tree_Inhab : Inhab tree. 
Proof using. apply (prove_Inhab Leaf). Qed.

(** Representation predicate as trees
    [p ~> Tree T] *)

Definition ContentsFor Tree (T:tree) v :=
  match T with
  | Leaf =>
     \[v = MLeaf]
  | Node T1 x T2 => 
     Hexists t1 t2, \[v = MNode t1 x t2] 
     \* t1 ~> Tree T1 \* t2 ~> Tree T2
  end.

Fixpoint Tree (T:tree) (t:loc) : hprop :=
  Hexists v, t ~~> v \* v ~> ContentsFor Tree T.

Definition Contents := ContentsFor Tree.

(** Characterization of binary search trees *)

Definition is_lt (X Y:int) := Y < X.
Definition is_gt (X Y:int) := X < Y.

Inductive stree : tree -> LibSet.set int -> Prop :=
  | stree_Leaf : 
      stree Leaf \{}
  | stree_Node : forall y T1 T2 E1 E2 E,
      stree T1 E1 ->
      stree T2 E2 -> 
      foreach (is_lt y) E1 -> 
      foreach (is_gt y) E2 ->
      E =' (\{y} \u E1 \u E2) ->
      stree (Node T1 y T2) E.

(** Representation predicate for binary search trees
    [p ~> Mset T] *)

Definition Mset (E:set int) (t:loc) :=
  Hexists (T:tree), t ~> Tree T \* \[stree T E].


(*--------------------------------------------------------*)
(** automation *)

Ltac xsimpl_core ::= hsimpl; xclean.

Hint Extern 1 (_ = _ :> LibSet.set _) => permut_simpl : set.

Ltac math_0 ::= unfolds is_lt, is_gt.
Ltac auto_tilde ::= eauto.
Ltac auto_star ::= try solve [ intuition (eauto with set) ].

Implicit Types E : LibSet.set int.

Lemma foreach_gt_notin : forall E X Y,
  foreach (is_gt Y) E -> lt X Y -> X \notin E.
Proof using. introv F L N. lets: (F _ N). apply* lt_slt_false. Qed.

Lemma foreach_lt_notin : forall E X Y,
  foreach (is_lt Y) E -> lt Y X -> X \notin E.
Proof using. introv F L N. lets: (F _ N). apply* lt_slt_false. Qed.


(*--------------------------------------------------------*)

(* shared *)

Lemma Tree_def : forall (t:loc) (T:tree),
  t ~> Tree T = Hexists v, t ~~> v \* v ~> Contents T.
Proof using.
  intros. destruct T; simpl; rewrite~ hunfold_base_eq.
Qed.

(* focus contents *)

Lemma Tree_focus : forall (t:loc) (T:tree),
  t ~> Tree T ==> Hexists v, t ~~> v \* v ~> Contents T.
Proof using. intros. rewrite~ Tree_def. Qed.

(* focus contents based on the value *)

Lemma Contents_value_focus : forall v T,
  v ~> Contents T ==> 
  match v with
  | MLeaf => \[T = Leaf]
  | MNode t1 x t2 => Hexists T1 T2, \[T = Node T1 x T2] 
                     \* t1 ~> Tree T1 \* t2 ~> Tree T2
  end.
Proof.
  intros. unfold Contents, ContentsFor.
  rewrite hunfold_base_eq. destruct T. 
  { xextracts. xsimpl*. }
  { xextract as t1 t2 E. subst. xsimpl*. }
Qed.

Lemma Contents_mleaf_focus : forall T,
  MLeaf ~> Contents T ==> \[T = Leaf].
Proof.
  intros. xchange Contents_value_focus. xsimpl. xsimpl*.
Qed.

Lemma Contents_mnode_focus : forall t1 x t2 T,
  (MNode t1 x t2) ~> Contents T ==> 
  Hexists T1 T2, \[T = Node T1 x T2] \* t1 ~> Tree T1 \* t2 ~> Tree T2.
Proof.
  intros. xchange Contents_value_focus. xsimpl. xsimpl*.
Qed.

(* focus contents based on the tree *)

Lemma Contents_tree_focus : forall v T,
  v ~> Contents T ==> 
  match T with
  | Leaf =>
     \[v = MLeaf]
  | Node T1 x T2 => 
     Hexists t1 t2, \[v = MNode t1 x t2] \* t1 ~> Tree T1 \* t2 ~> Tree T2
  end.
Proof using.
  intros. destruct T; simpl; 
   rewrite~ hunfold_base_eq; hsimpl; subst; hsimpl*. (* todo clean *)
Qed.

(* unfocus *)

Lemma Tree_unfocus : forall (t:loc) v (T:tree),
  t ~~> v \* v ~> Contents T ==> t ~> Tree T.
Proof using. intros. rewrite Tree_def. xsimpl. Qed.

Implicit Arguments Tree_unfocus [].

Lemma Tree_mleaf_unfocus : forall (t:loc),
  (t ~~> MLeaf) ==> (t ~> Tree Leaf).
Proof using. 
 intros. hunfold Tree. hsimpl*. hunfold ContentsFor. hsimpl~.
Qed.

Lemma Tree_mnode_unfocus : forall (t:loc) x t1 t2 T1 T2,
  (t ~~> MNode t1 x t2) \* (t1 ~> Tree T1) \* (t2 ~> Tree T2) ==>
  (t ~> Tree (Node T1 x T2)).
Proof using. 
  intros. hunfold Tree. hsimpl*.
  hunfold ContentsFor. hsimpl~. 
Qed.

(* Mset focus/unfocus *)

Lemma Mset_focus : forall t E,
  t ~> Mset E ==>
  Hexists (T:tree), t ~> Tree T \* \[stree T E].
Proof using. intros. hunfold Mset. auto. Qed.

Lemma Mset_unfocus : forall t T E,
  t ~> Tree T \* \[stree T E] ==> t ~> Mset E.
Proof using. intros. hunfold Mset. hsimpl*. Qed.

Opaque Tree Mset.


(*--------------------------------------------------------*)

Hint Extern 1 (Register xopen (Tree _)) => 
  Provide Tree_focus.
Hint Extern 1 (Register xclose (Ref Id (MNode _ _ _))) =>  
  Provide Tree_mnode_unfocus.
Hint Extern 1 (Register xclose (Ref Id MLeaf)) => 
  Provide Tree_mleaf_unfocus.
Hint Extern 1 (Register xopen (Contents _)) => 
  Provide Contents_value_focus.
Hint Extern 1 (Register xclose (Tree _)) => 
  Provide Mset_unfocus.
Hint Extern 1 (Register xopen (Mset _)) => 
  Provide Mset_focus.

Hint Constructors stree.

(*--------------------------------------------------------*)
(** Well-founded order on trees *)

Inductive tree_sub : binary (tree) :=
  | tree_sub_1 : forall x T1 T2,
      tree_sub T1 (Node T1 x T2)
  | tree_sub_2 : forall x T1 T2,
      tree_sub T2 (Node T1 x T2).

Lemma tree_sub_wf : wf tree_sub.
Proof using.
  intros T. induction T; constructor; intros t' H; inversions~ H.
Qed.

Hint Constructors tree_sub.
Hint Resolve tree_sub_wf : wf.


(*--------------------------------------------------------*)
(** Search *)

(** Tactics:
   - [xapp], [xapps], [xif], [xret], [xsimpl]
   - [math], [inverts as H1 .. HN], [subst], [assert (H)]
   - [xextract], or [xextract as x1 .. xN], to do the intros
   - [xopen t], or [xopen t as v], to do the intros
   - [xclose t]
   - add [~] to the tactic for [eauto]
   - add [*] to the tactic for [intuition eauto with sets] 
*)

Lemma insert_spec_ind_exercise :
  Spec insert (x:int) (t:loc) |R>> 
     forall T E, stree T E ->
     R (t ~> Tree T)
       (fun (_:unit) => Hexists T', 
        \[stree T' (E \u \{x})] \* t ~> Tree T').
Proof using. 
  (* xcf. intros x t T E IE. *)
  xinduction_heap (tree_sub). xcf. intros x t T IH E IE.
  xopen t as v. xopen v. xapps. xmatch_clean.
  { xextract as HT. subst. inverts IE. 
    xapp as t1. xapp as t2. xapps. 
    intros _. xclose t1 t2 t. xsimpl*.
    (* details:
    constructors. eauto. eauto. eauto. eauto. 
    intuition eauto with set. (* auto_star. *)
    *)  }
  (* begin exercise *)
  { admit. }
  (* end exercise *)
Qed.


(*--------------------------------------------------------*)
(** Search *)

Lemma search_spec_ind :
  Spec search (x:int) (t:loc) |R>> 
     forall T E, stree T E ->
     R (t ~> Tree T)
       (fun b => \[b = isTrue (x \in E)] \* t ~> Tree T).
Proof using. 
  xinduction_heap (tree_sub). xcf.
  intros x t T IH E IE.
  xopen t as v. xopen v. xapps. xmatch_clean.
  { xextract as HT. subst. inverts IE. xret. xclose t.
    xsimpl. applys* @notin_empty. typeclass. }
  { xextract as T1 T2 HT. subst. inverts IE as IE1 IE2 F1 F2 EQ. xif.
   { xapps*. intros b. xextracts. xclose t. xsimpl. subst E. iff M.
     { autos. }
     { set_in M. math. auto. false* foreach_gt_notin F2. } }
   { xif. 
     { xapps*. intros b. xextracts. xclose t. xsimpl. subst E. iff M.
       { applys~ in_union_3. }
       { set_in M. math. false* foreach_lt_notin F1. math. auto. } }
     { xret. xclose t. xsimpl.
       asserts_rewrite (x = y). math. subst E. auto. } } } 
Qed. 

Lemma search_spec :
  Spec search (x:int) (t:loc) |R>> 
     forall E,
     R (t ~> Mset E)
       (fun b => \[b = isTrue (x \in E)] \* t ~> Mset E).
Proof using.
  xweaken search_spec_ind. simpl. intros x t R HR S1 E. 
  xopen t. xextract as T1 HT1.
  xapply* S1. xsimpl. intros b.
  xextract as Hb. xclose* t. xsimpl*.
Qed.

(*--------------------------------------------------------*)
(** Delete *)

Lemma extract_max_spec_ind :
  Spec extract_max (t:loc) |R>> 
     forall T E, stree T E -> T <> Leaf ->
     R (t ~> Tree T)
       (fun (m:int) => Hexists T', t ~> Tree T' \* 
          \[m \in E /\ foreach (is_lt m) (E \- \{m}) /\ stree T' (E \- \{m})] ).
Proof using.
  xinduction_heap tree_sub. xcf. intros t T. introv IH IE NE.
  xopen t as v. xopen v. xapps. xmatch_clean.
  { xextracts. xfail. false. }
  { xextract as T1 T2 EQ. subst T. inverts IE as IE1 IE2 F1 F2 EQE.
    xopen t2 as v2. xopen v2. xapps. xmatch.
    { xextracts. inverts IE2. xopen t1 as v1. xapps. xapps. xret. 
      xchange (Tree_unfocus t). xsimpl. subst E. splits.
      { autos*. }
      { rewrite remove_union. rewrite remove_self.
        rewrite union_empty_r. rewrite union_empty_l.
        applys foreach_remove_simple F1. }
      { applys_eq IE1 1. rewrite union_empty_r.
        rewrite remove_union. rewrite remove_self.
        rewrite union_empty_l. rewrite~ remove_notin.
        applys foreach_notin_prove F1. math. } }
    { destruct v2; tryfalse. xextract as T21 T22 EQ2.
      xclose t2. xapps~ IE2; clear IH. subst; auto_false*.
      intros m. xextract as T2' (P1&P2&P3). xclose t. xsimpl.
      subst E.
      asserts G: (m > x). { forwards~: F2 m. }
      asserts M: (m \notin E1). { intros I. forwards~: F1 m. math. }
      splits.
      { apply in_union_r. apply in_union_r. auto. (* todo fix auto *) }
      { do 2 rewrite remove_union. rew_foreach. splits.
        { applys foreach_remove_simple. rew_foreach~. }
        { applys foreach_weaken. applys foreach_remove_simple F1.
          intros_all. math. }
        { auto. } }
      { constructors~.
        { applys~ foreach_remove_simple. }
        { repeat rewrite remove_union. sets_eq E2': (E2 \-- m).
          repeat rewrite~ remove_notin. rewrite notin_single_eq. math. } } } }
Qed.

Hint Extern 1 (RegisterSpec extract_max) => Provide extract_max_spec_ind.

Lemma delete_spec_ind :
  Spec delete (x:int) (t:loc) |R>> 
     forall T E, stree T E ->
     R (t ~> Tree T)
       (fun (_:unit) => Hexists T', \[stree T' (E \- \{x})] \* t ~> Tree T').
Proof using. 
  xinduction_heap tree_sub. xcf. intros x t T IH E IE.
  xopen t as v. xopen v. xapps. xmatch_clean.
  { xextract as HT. subst. inverts IE. xret. xclose t. xsimpl*. 
    rewrite remove_empty. constructors~. }
  { xextract as T1 T2 EHT. subst T. inverts IE as IE1 IE2 F1 F2 EQ.
   (* todo: check notation *)
   xif.
   { xapps*. xextract as T' HT'. xclose t. xsimpl*.
     constructors~. applys foreach_remove_simple F1.
     subst E. repeat rewrite remove_union.
     sets_eq E1': (E1 \- \{x}). repeat rewrite~ remove_notin.
       intros N. forwards~: F2 N. math.
       rewrite notin_single_eq. math. }
   { xif.
     { xapps*. xextract as T' HT'. xclose t. xsimpl*.
       constructors~. applys foreach_remove_simple F2.
       subst E. repeat rewrite remove_union.
       sets_eq E1': (E2 \- \{x}).
       repeat rewrite~ remove_notin.
       intros N. forwards~: F1 N.
       rewrite notin_single_eq. math. }
     { asserts Exy: (x = y). math. subst y. xopen t1 as v1. xopen v1. 
       xapps. xmatch. 
       { xextracts. inverts IE1.
         xopen t2 as v2. xapps. xapps.
         xchange (Tree_unfocus t). xsimpl.
         subst E. applys_eq IE2 1. rewrite union_empty_l. 
         rewrite remove_union. rewrite remove_self.
         rewrite union_empty_l. rewrite~ remove_notin. intros N. forwards~: F2 N. }
       { destruct v1; tryfalse. xextract as T11 T12 EQ1.
         xclose t1. xapps IE1. { intros. subst; auto_false*. }
         intros T1' (M1&LM1&IE1'). xapps. xclose t. xsimpl.
         constructors~.
         { intros w Hw. forwards: F2 Hw. unfolds is_gt. forwards~: F1 m. math. }
         { subst E. rewrite remove_union. rewrite remove_self. rewrite union_empty_l.
           rewrite remove_notin. rewrite union_assoc. fequals.
            rewrite~ remove_then_add_same. rewrite notin_union_eq. split.
            { applys foreach_notin_prove F1. math. }
            { applys foreach_notin_prove F2. math. } } } } } }
Qed.

Lemma delete_spec :
  Spec delete (x:int) (t:loc) |R>> 
     forall E,
     R (t ~> Mset E)
       (fun (_:unit) => t ~> Mset (E \- \{x})).
Proof using. 
  xweaken delete_spec_ind. simpl. intros x t R HR S1 E.
  xopen t as T1 HT1. xapply* S1. xsimpl.
  xextract as T' HT'. xclose* t. xsimpl*.
Qed.


(*--------------------------------------------------------*)
(** Is_empty *)

Lemma is_empty_spec :
  Spec is_empty (t:loc) |R>> 
     forall E,
     R (t ~> Mset E)
       (fun (b:bool) => t ~> Mset E \* \[b = isTrue(E = \{})]).
Proof using. 
  xcf. intros. 
  xopen t. xextract as T HT1. xopen t as v. xopen v. xapps. xmatch.
  { xextracts. xret. xclose t. xclose* t. xsimpl. inverts~ HT1. }
  { xextract as T1 T2 ET. xret. subst T. xclose t. xclose* t. xsimpl.
    inverts HT1. subst. applys~ @is_nonempty_prove y. typeclass. }
Qed.


(*--------------------------------------------------------*)
(** Insert *)

Lemma insert_spec_ind :
  Spec insert (x:int) (t:loc) |R>> 
     forall T E, stree T E ->
     R (t ~> Tree T)
       (fun (_:unit) => Hexists T', 
        \[stree T' (E \u \{x})] \* t ~> Tree T').
Proof using. 
  xinduction_heap (tree_sub). xcf. intros x t T IH E IE.
  xopen t as v. xopen v. xapps. xmatch_clean.
  { xextract as HT. subst. inverts IE. 
    xapp as t1. xapp as t2. xapps. 
    intros _. xclose t1 t2 t. xsimpl*. }
  { xextract as T1 T2 HT. subst T. 
    inverts IE as IE1 IE2 F1 F2 EQ. xif.
    { xapps*. xextract as T' HT'. xclose t. xsimpl*. }
    { xif.
      { xapps*. xextract as T' HT'. 
        xclose t. xsimpl*. }
      { xret. xclose t. asserts_rewrite (x = y). 
        math. xsimpl*. } } }
Qed.

Lemma insert_spec :
  Spec insert (x:int) (t:loc) |R>> 
     forall E,
     R (t ~> Mset E)
       (fun (_:unit) => t ~> Mset (E \u \{x})).
Proof using.
  xweaken insert_spec_ind. simpl. intros x t R HR S1 E.
  xopen t as T1 HT1. xapply* S1. xsimpl.
  xextract as T' HT'. xclose* t. xsimpl*.
Qed.



Set Implicit Arguments. 
Require Import CFLib.
Require Import LibListZ.


(*--------------------------------------------------------*)
(** Product of a list of integers *)

Definition list_mul (xs:list int) :=
  LibList.fold_right Z.mul 1 xs.

Lemma list_mul_nil :  
 list_mul nil = 1.
Proof using. intros. unfold list_mul. rew_list~. Qed.

Lemma list_mul_cons : forall x xs, 
 list_mul (x::xs) = x * list_mul xs.
Proof using. intros. unfold list_mul. rew_list~. Qed.

Lemma list_mul_ind : forall xs a,
  fold_right Z.mul a xs = a * fold_right Z.mul 1 xs.
Proof using.
  intros xs. induction xs; intros; rew_list.
  ring. rewrite IHxs. ring.
Qed.

Lemma list_mul_app : forall xs1 xs2,
 list_mul (xs1 ++ xs2) = list_mul xs1 * list_mul xs2.
Proof using.
  intros. unfold list_mul. rew_list.
  rewrite~ list_mul_ind. ring. 
Qed.

Lemma list_mul_last : forall x xs,
 list_mul (xs&x) = x * list_mul xs.
Proof using.
  intros. rewrite list_mul_app.
  unfold list_mul. rew_list. ring. 
Qed.

Hint Rewrite list_mul_nil list_mul_cons list_mul_last list_mul_app : rew_list_mul.
Tactic Notation "rew_list_mul" := autorewrite with rew_list_mul.
Tactic Notation "rew_list_mul" "~" := rew_list_mul; auto_tilde.
Tactic Notation "rew_list_mul" "*" := rew_list_mul; auto_star.



(*----------------------------------------------------------*)
(* Hints for index and length *)

(* For development, use:
   Hint Extern 1 (index _ _) => skip. 
*)

Hint Resolve length_nonneg : maths.
Hint Extern 1 (index ?M _) => subst M : maths.
Hint Resolve index_update : maths.
Hint Resolve index_zmake : maths.
Hint Resolve index_bounds_impl : maths.
Hint Resolve index_length_unfold int_index_prove : maths.
Hint Extern 1 (length (?l[?i:=?v]) = _) => rewrite length_update.
Hint Resolve length_make : maths.
Hint Extern 1 (length ?M = _) => subst M : maths.


(*----------------------------------------------------------*)
(* Hints for lists *)

Hint Constructors Forall.

(*----------------------------------------------------------*)
(* Controlling simpl *)

Global Opaque Z.mul. 


(*----------------------------------------------------------*)
(* Notations *)

Notation "r '~->' n" :=
  (@hdata loc (@Ref _ _ (@Id _) n) r) (at level 32).

Notation "l '~~>' v" := (hdata (@Ref _ _ (@Id _) v) l)
  (at level 32, no associativity).



(*--------------------------------------------------------*)
(* ** [sequence of consecutive integers] *)

(* LATER: realize *)

Axiom seg : int -> int -> set int.
Axiom seg_first : forall a b, a <= b ->
  seg a b = \{a} \u (seg (a+1) b).

Axiom filter : forall A, (A->Prop) -> set A -> set A.
Axiom filter_single : forall A (P:A->Prop) x,
  filter P \{x} = If P x then \{x} else \{}.
Axiom filter_empty : forall A (P:A->Prop),
  filter P \{} = \{}.
Axiom filter_union : forall A (P:A->Prop) E1 E2,
  filter P (E1 \u E2) = filter P E1 \u filter P E2.

Hint Rewrite filter_single filter_empty filter_union : rew_filter.


(* ---------------------------------------------------------------------- *)
(** ** Reduce on sets *)

(* LATER: realize *)

Require Import LibStruct.
Section Reduce.
Variables (A B : Type).
Parameter reduce : monoid_def B -> (A -> B) -> set A -> B.
Parameter reduce_empty : forall m f, Monoid m ->
  reduce m f \{} = monoid_neutral m.
Parameter reduce_single : forall x m f, Monoid m ->
  reduce m f \{x} = f x.
Parameter reduce_union : forall E1 E2 m f, Monoid m ->
  reduce m f (E1 \u E2) = (monoid_oper m) (reduce m f E1) (reduce m f E2).
End Reduce.

Instance monoid_add_zero:
  Monoid (monoid_ Z.add 0).
Proof using.
  constructor; repeat intro; omega.
Qed.


(*----------------------------------------------------------*)
(* Basic set properties, to add as classes in tlc/LibBag *)

Axiom remove_union : forall A (E F G: set A),
  (E \u F) \- G = (E \- G) \u (F \- G).
Axiom remove_self : forall A (x:A), \{x} \- \{x} = \{}.
Axiom remove_notin : forall A (x:A) (E:set A), x \notin E -> E \- \{x} = E.
Axiom remove_then_add_same : forall A (x:A) (E: set A),
  x \in E -> \{x} \u (E \- \{x}) = E.
Axiom remove_empty : forall (E:set int), \{} \- E = \{} :> set int. 

(* LATER: find out why set_in tactic does not work *)

Lemma in_union_3 : forall A x (E1 E2 E3: set A),  
  x \in E3 -> x \in E1 \u E2 \u E3.
Proof using.
  introv H. rewrite <- for_set_union_empty_r.
  repeat rewrite <- for_set_union_assoc.
  apply in_union_get_3. assumption. 
Qed.




(*--------------------------------------------------------*)
(* [math_dia] tactic *)


Definition Zdiv_hyp (P:Prop) := P.

Lemma Z_div_mod' : forall a b : int,
  Zdiv_hyp ((b > 0)%Z) ->
  let (q, r) := Z.div_eucl a b in
  a = (b * q)%I + r /\ (0 <= r < b)%Z.
Proof using. applys Z_div_mod. Qed.

Ltac Zdiv_eliminate_step tt :=
  match goal with |- context[ Z.div_eucl ?X ?Y ] =>   
     generalize (@Z_div_mod' X Y);
     destruct (Z.div_eucl X Y)
  end.

Ltac math_dia_generalize_all_prop tt :=
  repeat match goal with H: ?T |- _ => 
    match type of T with Prop => gen H end end.

Ltac Zdiv_eliminate tt :=
  math_dia_generalize_all_prop tt;
  unfold Z.div; 
  repeat (Zdiv_eliminate_step tt).

(* todo: deal differently with iterated divisions,
   in order to avoid blow up *)

Ltac Zdiv_instantiate_hyp_steps tt :=
  match goal with H: Zdiv_hyp ?P -> _ |- _ =>
    specializes H __; 
    [ idtac
    | try Zdiv_instantiate_hyp_steps tt ]  
  end.

Ltac Zdiv_instantiate_hyp tt :=
  Zdiv_instantiate_hyp_steps tt.

Ltac math_dia_setup :=
  math_0; math_1; math_2; math_3; Zdiv_eliminate tt; 
  intros; try Zdiv_instantiate_hyp_steps tt; unfolds Zdiv_hyp.

Tactic Notation "math_dia" :=
  math_dia_setup; math_nia.





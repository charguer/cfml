

let example_let n =
  let a = n+1 in  
  let b = n-1 in
  a + b

let example_incr r =
  r := !r + 1

let example_incr_2 n =
  let i = ref 0 in 
  let r = ref n in 
  decr r;
  incr i;
  r := !i + !r;
  !i + !r

let example_array () = 
  let t = Array.make 3 true in
  t.(2) <- false;
  t.(1) <- t.(2);
  t

let example_loop n =
  let i = ref 0 in  
  let r = ref 0 in
  while !i < n do
    incr i;
    incr r;  
  done;
  r

let list_product xs =
  let n = ref 1 in
  let q = ref xs in
  while !q <> [] do
    match !q with
    | [] -> assert false
    | x::t -> q := t; n := !n * x
  done;
  !n

let list_product_iter xs =
  let n = ref 1 in
  List.iter (fun x -> n := !n * x) xs;
  !n

let decompose n t =
  let r = ref n in
  let q = ref [] in
  while t.(!r) <> 1 do
    let d = t.(!r) in
    q := d :: !q;
    r := !r / d;
  done;
  !q


  
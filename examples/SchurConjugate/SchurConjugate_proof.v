Set Implicit Arguments.
Require Import CFLib LibSet LibMap LibArray SchurConjugate_ml.




(****************************************************)

(* Recall that [A[i]] is the notation for reading in an array
   and that [index A i] is defined as [0 <= i < length A]. *)

Notation "'index' A i" := (0 <= i < length A) 
  (A ident, at level 60).

(****************************************************)
(** Facts about matrices *)

Generalizable Variable A.

Parameter matrix : Type -> Type.

Parameter matrix_read_impl : forall `{Inhab A}, matrix A -> (int * int) -> A.

Global Instance read_inst : forall `{Inhab A}, BagRead (int*int) A (matrix A).
constructor. rapply (@matrix_read_impl A). Defined.
Global Opaque read_inst.

Notation "m \( x , y )" := (read m (x,y))
  (at level 33, format "m \( x , y )") : container_scope.

Parameter matrix_size : forall A, matrix A -> int * int.

Parameter matrix_tr : forall A, matrix A -> matrix A.

Parameter matrix_tr_spec : forall `{Inhab A} (m:matrix A) (i j:int),
  (matrix_tr m)\(i,j) = m\(j,i).

Parameter matrix_tr_size : forall `{Inhab A} (m:matrix A) (i j:int),
  matrix_size m = (i,j) -> matrix_size (matrix_tr m) = (j,i).

Parameter matrix_init : forall A, (int * int) -> (int -> int -> A) -> matrix A.

Parameter matrix_init_spec : forall `{Inhab A}, 
  forall f m n i j, 0 <= i < m -> 0 <= j < n ->
    (matrix_init (m,n) f) \(i,j) = f i j.

Parameter matrix_init_size : forall `{Inhab A}, 
  forall (f:int->int->A) p, matrix_size (matrix_init p f) = p.


(****************************************************)
(** Specification *)

Notation "'tab'" := (array int) (at level 0).

(** [partition a] asserts that [a] is an array that 
    contains decreasing values and whose last element is 0. *)

Record partition (A:tab) := {
  partition_pos : length A > 0;
  partition_last : A\(length A - 1) = 0;
  partition_ord : forall i j, index A i -> index A j -> i < j -> A\(i) < A\(j) }.

(** [asmatrix A M] asserts that the i-th row of the boolean matrix 
    [M] starts with A[i] times the value "true", and the rest of the
    row is completed with the value "false". The matrix has size [m] by [m],
    where [m] is the size of array [A]. *)

Record asmatrix (A:tab) (M:matrix bool) := {
  asmatrix_par : partition A;
  asmatrix_dim : matrix_size M = (length A, length A);
  asmatrix_val : forall i j, index A i -> index A j -> M\(i,j) = isTrue (j <= A\(i)) }.

(** [isconjugate A B] asserts that [A] and [B] are conjugate of one 
    another, meaning that they correspond to transposed matrices. *)

Definition isconjugate (A B:tab) :=
  exists M, asmatrix A M /\ asmatrix B (matrix_tr M).


(****************************************************)
(** Lemmas *)

Lemma partition_asmatrix : forall A,
  partition A -> exists M, asmatrix A M.
Proof.
  introv P. lets [Pp Pl Po]: P. sets m: (length A). 
  exists (matrix_init (m,m) (fun i j => isTrue (j <= A\(i)))).
  constructor.
    auto.
    apply matrix_init_size.
    apply matrix_init_spec.   
Qed.

Definition loopminv m M B j :=
  forall i' j', 0 <= i' < m -> j < j' < m ->
  M\(i',j') = isTrue (i' <= B\(j')).

Definition loopbinv i j m b M :=
  Hexists B, \[0 <= i < m] \* \[-1 <= j < m] \*
  b ~> Array B \* \[length B = m] \* \[loopminv m M B j].
  (* missing: B sorted *)
  (* tofix: [i] can reach [m] at the last iteration *)
  (* todo: move [0 <= i < m] below *)

Definition loophinv a b li lj m M (A:tab) := fun p =>
  let '(i,j) := p in
  a ~> Array A \* li ~~> i \* lj ~~> j \* loopbinv i j m b M.



(****************************************************)
(** Temp -- to move -- *)


Notation "'_While' Q1 '_Do' Q2 '_Done'" :=
  (!While (fun H Q => forall R:~~unit, is_local R ->
        (forall H Q, (If_ Q1 Then (Q2 ;; R) Else (Ret tt)) H Q -> R H Q)
        -> R H Q))
  (at level 69, Q2 at level 68, only parsing) : charac.
Open Scope charac.

Lemma while_loop_cf_to_inv_post : 
   forall (A:Type) (I:A->hprop) (B:A->bool->Prop) (lt:binary A) (W:wf lt),
   forall (F1:~~bool) (F2:~~unit) H,
    (exists X0, H ==> (I X0)) ->
   (forall X, F1 (I X) (fun b => \[B X b] \* I X)
              /\ F2 (\[B X true] \* I X) (# Hexists Y, (I Y) \* \[lt Y X])) ->
  (_While F1 _Do F2 _Done) H (# Hexists X, \[B X false] \* I X).
Admitted.

Ltac xwhile_inv_core W I B ::=
  match type of W with
  | wf _ => eapply (@while_loop_cf_to_inv _ I B _ W)
  | _ -> nat => eapply (@while_loop_cf_to_inv _ I B (measure W)); [ try prove_wf | | ]
  | _ =>  (* todo -> interaction with xseq *)
    first [
          eapply (@while_loop_cf_to_inv_post _ I B W); [ try prove_wf | | ] |
      eapply (@while_loop_cf_to_inv _ I B W); [ try prove_wf | | ]
      ]
  end.


Ltac esplit2 :=
  match goal with |- ex (fun _:?A*?B => _) =>
    let x1 := fresh in let x2 := fresh in 
  evar (x1:A); evar (x2:B); exists (x1,x2); subst x1 x2 end.

Ltac fixboolof := fix_bool_of_known tt.



(****************************************************)
(** Verification *)

Lemma conjugate_spec : forall a A,
  App conjugate a; 
     (a ~> Array A \* \[partition A]) 
     (fun b => Hexists B, a ~> Array A \* b ~> Array B \* \[isconjugate A B]).
Proof.
  intros. xcf_app. xextract as P. lets (M&Ms): (partition_asmatrix P).
  lets [_ D V]: Ms. lets [Pp Pl Po]: P. xapp. intros Em.
  xapps. xapp as li. xapp as lj.
  (* external loop *)
  xwhile_inv (lexico2 (upto m) (downto 0)) (loophinv a b li lj m M A) (fun p:int*int =>
   let '(i,j) := p in bool_of (j >= 0)).
    esplit2. unfold loophinv, loopbinv. hsimpl.
      intros_all. false. math.
      apply length_make.
      math.
      math.
    intros (i,j0). splits.
      unfolds loophinv. xapps. xret. hsimpl*. fixboolof. auto*.
      xextract as M1. sets ai: (A[i]).
      (* internal loop *)
      xwhile_inv (downto ai) (fun j => loophinv a b li lj m M A (i,j))
                              (fun j => bool_of (j >= A[i])).
       esplit. unfold loophinv. hsimpl.
       intros j. splits.
       unfold loophinv. xapps. xapps. xapps. skip. (* todo index A i *)
        xret. hsimpl. fixboolof. auto*.
       (* body of inner loop *)
       xextract as Bs. xapps. xapps.
       unfold loopbinv. xextract as B B1 B2 B3 B4.
       xapps. skip. (* j index in B *)
       xapps. hsimpl.
         skip. (* decrease measure *)
         skip. (* show minv preserved *)
         rewrite~ length_update.
         skip. (* show -1 <= (j - 1)%I < m *)
         auto.
       (* increment i *)
       xextract as j Bs. xapp. unfolds loophinv, loopbinv. hsimpl (i+1,j).
         skip. (* decrease measure *)
         auto.
         auto.
         auto.
         skip. (* show i+1 <= m *)
  (* return *)
  xextract as (i,j) E. xret. unfolds loophinv, loopbinv.
  hextract as B N1 N2 Bl L. hsimpl. exists M. split~. 
  constructor.
    constructor.
      math.
      skip. (* show B\((length B - 1)%I) = 0 *)
      skip. (* show B sorted *)
      rewrite Bl. rewrite <- Em in D. apply matrix_tr_size. auto.
    rewrite Bl. introv B1 B2. rewrite matrix_tr_spec. rewrite~ L. math.
Qed.





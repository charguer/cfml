Set Implicit Arguments.
Require Import CFML.CFLib CFML.CFLibCredits.
(* Require Import RandomAccessListSig_ml. *)
Require Import BinaryRandomAccessList_ml.
Require Import TLC.LibListZ.
Require TLC.LibInt.

Module BinaryRandomAccessListSpec (* <: RandomAccessListSigSpec *).

Import BinaryRandomAccessList_ml.

(** invariant *)

Section Polymorphic.
Variables (a : Type).

Inductive btree : int -> tree_ a -> list a -> Prop :=
  | btree_nil : forall x,
      btree 0 (Leaf x) (x::nil)
  | btree_cons : forall p p' n t1 t2 L1 L2 L',
      btree p t1 L1 ->
      btree p t2 L2 ->
      p' =' p+1 ->
      n =' 2^p' ->
      L' =' L1 ++ L2 ->
      btree p' (Node n t1 t2) L'.

Inductive inv : int -> rlist_ a -> list a -> Prop :=
  | inv_nil : forall p,
      p >= 0 ->
      inv p nil nil
  | inv_cons : forall p (t: tree_ a) ts d L L' T,
      inv (p+1) ts L ->
      L' <> nil ->
      p >= 0 ->
      (match d with
       | Zero => L = L'
       | One t => btree p t T /\ L' = T ++ L
       end) ->
      inv p (d :: ts) L'.

Definition Rlist (s: rlist_ a) (L: list a) :=
  inv 0 s L.

End Polymorphic.

Implicit Arguments btree [a].
Implicit Arguments inv [a].

(** automation *)

(* Let [math] know that [length] is in fact a nat,
   this way, we automatically have [0 <= length L] for all [L]. *)
Ltac math_0 ::= unfold length in *.
(* Use [eauto with maths] in ~-suffixed tactics *)
Ltac auto_tilde ::= eauto with maths.

(* Do not try to unfold Z.mul, e.g. in [2 * n] *)
Opaque Z.mul.

Hint Constructors btree inv.
Hint Extern 1 (@lt nat _ _ _) => rew_list; math.
Hint Resolve ZNth_zero ZUpdate_here ZUpdate_not_nil.
Hint Resolve app_not_empty_l app_not_empty_r.

(** useful facts *)

Fixpoint tree_size {a: Type} (t:tree_ a) : nat :=
  match t with
  | Leaf _ => 0%nat
  | Node _ t1 t2 => (1 + tree_size t1 + tree_size t2)%nat
  end.

Definition Size {a: Type} (t:tree_ a) :=
  match t with
  | Leaf _ => 1
  | Node w _ _ => w
  end.

Lemma btree_size_correct : forall a p t L,
  @btree a p t L -> Size t = 2^p.
Proof. introv Rt. inverts~ Rt. Qed.
Hint Resolve btree_size_correct.

Lemma btree_p_pos : forall a p t L,
  @btree a p t L -> p >= 0.
Proof. introv Rt. inductions Rt; math. Qed.

Hint Resolve btree_p_pos.

Lemma length_correct : forall a t p L,
  @btree a p t L -> length L = 2^p :> int.
Proof.
  introv Rt. induction Rt. auto.
  unfolds eq'. subst. rew_length. rewrite~ pow2_succ.
Qed.

Lemma btree_size_pos : forall a p t L,
  @btree a p t L -> p >= 0.
Proof. introv Rt. induction Rt; unfolds eq'; math. Qed.

Hint Resolve btree_size_pos.

Lemma to_empty : forall a p L,
  @inv a p nil L -> L = nil.
Proof. introv RL. inverts~ RL. Qed.

Lemma from_empty : forall a p ts,
  @inv a p ts nil -> ts = nil.
Proof. introv RL. inverts RL; auto_false. Qed.

Lemma btree_not_empty : forall a p t L,
  @btree a p t L -> L <> nil.
Proof.
  introv Rt. lets: (length_correct Rt). intro_subst_hyp.
  rew_length in H. forwards~: (@pow2_pos p). math.
Qed.

Hint Resolve btree_not_empty.

Lemma group_ineq : forall n m, 0 <= n - m -> m <= n.
Proof. intros. math. Qed.

(* Useless *)
Lemma inv_length_correct : forall a ts p L,
    @inv a p ts L ->
    If ts = nil then
      length L = 0
    else
      (2 ^ p <= length L <= 2 ^ (length ts + p) - 2 ^ p).
Proof.
  induction ts.
  - intros. case_if. rewrites~ (>> to_empty ___).
  - { rename a0 into d. case_if; intros p L Rdts.
      - { inverts Rdts as. intros L0 T _t Rts Lnnil ?. forwards IH: IHts Rts.
          forwards~: pow2_pos p.
          destruct d; intros; subst.
          - { assert (ts <> nil). { intro TS. apply Lnnil. subst. applys~ to_empty. }
              case_If. destruct IH as [IH1 IH2]. split.
              - rew_ineq <- IH1. rew_pow~ 2 p.
              - rewrite~ length_cons. rew_ineq IH2. rew_pow~ 2 p. }
          - { case_If.
              - { unpack; subst. forwards~: (>> to_empty ___); subst. split.
                  - forwards~: (>> length_correct ___). rew_list~.
                  - rew_length~. forwards~ Tlen: (>> length_correct ___). rew_pow~ 2 p. }
              - { destruct IH as [IH1 IH2]; unpack; subst.
                  split; forwards~ Tlen: (>> length_correct ___); rew_length~.
                  rew_ineq IH2. rewrite Tlen. rew_pow~ 2 p. } } } }
Qed.

Lemma inv_ts_len : forall A ts p (L:list A),
  inv p ts L ->
  ts <> nil ->
  2 ^ (p + (length ts) - 1) <= length L.
Proof.
  induction ts as [|d ts].
  - intros. auto_false.
  - { intros p L Rdts. inverts Rdts as. intros L0 T _t Rts Lnnil ? ? ?.
      destruct ts.
      - { (* ts = nil // interesting case *)
          destruct d; intros; subst. (* [d] must be a [One t] for the invariant to hold. *)
          - forwards~: to_empty. auto_false.
          - unpack; subst. forwards~: to_empty; subst.
            forwards~ lenT: length_correct.
            rew_length. rew_pow~ 2 p. }
      - { forwards IH: IHts Rts. auto_false.
          destruct d; intros; unpack; subst; rew_length in *;
          rew_pow~ 2 p in IH; rew_pow~ 2 p. } }
Qed.

(* move to tlc? *)
Lemma Zdiv_le : forall a b: int, (0 <= a) -> (1 <= b) -> (Zdiv a b <= a).
Proof. admit. Qed.

Lemma ts_bound : forall a ts p L,
    @inv a p ts L -> 2 ^ (length ts) <= 2 * (length L) + 1.
Proof.
  destruct ts; intros.
  - { forwards~: (>> to_empty ___). subst; rew_length. reflexivity. }
  - { forwards I: inv_ts_len H. congruence.
      (* TODO: rew_pow 2 p in I. puis diviser des deux côtés avec un lemme *)
      assert (2 ^ length (d::ts) <= 2 * (length L) / 2 ^ p). {
        forwards~ I': int_le_mul_pos_l 2 I.
        forwards~ I'': Z_div_le (2^p) I'. inverts H. forwards~: (>> pow2_pos ___).
        rewrite <-pow2_succ in I''. rewrites~ <-Z.pow_sub_r in I''.
        math_rewrite~ ((((p + length (d :: ts)) - 1) + 1) - p = length (d :: ts)) in I''.
        inverts~ H. rew_length. inverts~ H.
      }
      inverts H. forwards~: pow2_pos.
      forwards~: Zdiv_le (2 * length L) (2^p). } (* SMT *)
Qed.

Lemma ts_bound_log : forall a ts p L,
    @inv a p ts L -> length ts <= Z.log2 (2 * (length L) + 1).
Proof.
  intros. forwards~: ts_bound. forwards~: Z.log2_le_mono. rewrites~ Z.log2_pow2 in *.
Qed.

(* Useless *)
Lemma p_bound_log : forall a ts p L,
    @inv a p ts L -> If ts <> nil then p <= Z.log2 (length L) else True.
Proof.
  introv Rts. forwards~ inv_bounds: inv_length_correct. case_if.
  - case_if. destruct inv_bounds as [I1 _].
    forwards~: Z.log2_le_mono. rewrites~ Z.log2_pow2 in *.
    inverts~ Rts.
  - auto.
Qed.

(** verification *)

(* todo move *)
(* TODO: lemma to rewrite ÷ with / under assumption that args > 0
   and tactic rew_div pour optimiser. *)

Lemma pow2_succ_div : forall p, p >= 0 -> 2 ^ (p+1) ÷ 2 = 2 ^ p.
Proof.
  intros. rewrite~ pow2_succ. rewrites~ Z.mul_comm. rewrites~ Z.quot_mul.
Qed.

(* todo move *)
Lemma simpl_zero_credits : forall n, n = 0 -> \$ n ==> \[].
Proof. intros. subst. rewrite <-credits_int_zero_eq. hsimpl. Qed.

Lemma empty_spec :
  forall a, Rlist (@empty a) (@nil a).
Proof. intros. rewrite (empty__cf a). constructors~. Qed.

Hint Extern 1 (RegisterSpec empty) => Provide empty_spec.

Lemma is_empty_spec : forall A (l: rlist_ A), (* style *)
  app is_empty [l]
    PRE \[]
    POST (fun (b: bool) => \[b = isTrue (l = nil)]).
Proof. xcf. xgo~. Qed.

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.

Lemma size_spec :
  forall a (t: tree_ a),
  app size [t]
    PRE \[]
    POST (fun (n: int) => \[n = Size t]).
Proof. xcf. xgo~. Qed.

Hint Extern 1 (RegisterSpec size) => Provide size_spec.

Lemma link_spec :
  forall a (t1 t2: tree_ a) p L1 L2,
  btree p t1 L1 -> btree p t2 L2 ->
  app link [t1 t2]
    PRE \[]
    POST (fun t' => \[btree (p+1) t' (L1 ++ L2)]).
Proof.
  xcf. intros. repeat xapps. xret. hsimpl. constructors~.
  do 2 (erewrite btree_size_correct; eauto).
  rewrite~ pow2_succ.
Qed.

Hint Extern 1 (RegisterSpec link) => Provide link_spec.

Lemma cons_tree_spec :
  forall a (t: tree_ a) (ts: rlist_ a) p T L,
  btree p t T -> inv p ts L ->
  app cons_tree [t ts]
    PRE \[]
    POST (fun ts' => \[inv p ts' (T ++ L)]).
Proof.
  intros a t ts. revert t.
  induction ts; introv Rt Rts; inverts Rts; xcf.
  - xgo~. constructors~.
  - xmatch; [| unpack; xapps~; intros; xapps~; intros];
    xret; hsimpl; constructors~; subst~; rew_list~.
Qed.

Hint Extern 1 (RegisterSpec cons_tree) => Provide cons_tree_spec.


(* test *)
Notation "F 'PURE' P" :=
  (tag tag_goal F \[] (fun x => \[P x]))
  (at level 69, only parsing) : charac.

Lemma cons_spec' :
  forall a (x: a) (l: rlist_ a) L,
  Rlist l L ->
  app cons [x l] PURE (fun l' => Rlist l' (x::L)).
(* TODO: Rlist dans l'autre sens permet de faire ça : *)
(*  app cons [x l] PURE (Rlist (x::L)). *)
Proof. simpl. xcf. xapp~. Qed.



Lemma cons_spec :
  forall a (x: a) (l: rlist_ a) L,
  Rlist l L ->
  app cons [x l]
    PRE \[]
    POST (fun l' => \[Rlist l' (x::L)]).
Proof. xcf. xapp~. Qed.

Hint Extern 1 (RegisterSpec cons) => Provide cons_spec.

Lemma uncons_tree_spec : forall A (ts: rlist_ A) p L,
  inv p ts L ->
  ts <> nil ->
  app uncons_tree [ts]
    PRE \[]
    POST (fun '(t', ts') =>
      \[exists T' L', btree p t' T' /\ inv p ts' L' /\ L = T' ++ L']).
Proof.
  induction ts as [| t ts']; introv Rts Ne; inverts Rts as.
  - xcf; xgo.
  - { introv ? I. intros. xcf. xmatch.
      - intros. xgo. unpack; subst. forwards~: to_empty. subst. rew_list.
        exists___. splits~. rew_list~.
      - xgo. unpack; subst. exists___. splits; [eauto| |eauto]. constructors~.
         (* TODO: arthur one day: figure out why eauto won't work *)
        inverts I; auto_false*.
      - { xapp~ as [t l].
          - inverts~ I. discriminate.
          - { intros; unpack; subst. xgo; inverts H.
              - subst. maths (p0 = p); subst. exists___. splits; eauto. rew_list~.
              - math.
              - subst. auto_false*. } } }
Qed.

Hint Extern 1 (RegisterSpec uncons_tree) => Provide uncons_tree_spec.

Lemma head_spec :
  forall a (l: rlist_ a) L,
  Rlist l L ->
  l <> nil ->
  app head [l]
    PRE \[]
    POST (fun x => \[is_head L x]).
Proof.
  introv Rts NE. xcf. xapp~ as [t ts]. intros (?&?&BT&I&?); subst. xmatch.
  - xret. hsimpl. inverts BT. rew_list~.
  - { xfail. inverts~ BT.
      - auto_false*.
      - subst. forwards~: btree_size_pos. math. }
Qed.

Hint Extern 1 (RegisterSpec head) => Provide head_spec.

Lemma tail_spec :
  forall a (l: rlist_ a) L,
  Rlist l L -> l <> nil ->
  app tail [l]
    PRE \[]
    POST (fun tl => \[exists TL, Rlist tl TL /\ is_tail L TL]).
Proof.
  introv Rts NE. xcf. xapp~ as [t ts].
  intros (?&?&BT&I&?). subst. xgo.
  inverts BT.
  - rew_list~ in *.
  - forwards~: btree_size_pos. math.
Qed.

Hint Extern 1 (RegisterSpec tail) => Provide tail_spec.

Import TLC.LibInt.

Lemma lookup_tree_spec :
  forall a i (t: tree_ a) p L,
  btree p t L ->
  ZInbound i L -> (* index L i *)
  app lookup_tree [i t]
    PRE \[]
    POST (fun x => \[ZNth i L x]). (* L[i] *)
Proof.
  intros a i t. revert i.
  induction t; introv BT Bi; inverts BT; xcf.
  - xgo. subst. apply~ ZNth_zero. apply~ ZInbound_one_pos_inv.
  - { xmatch. subst. forwards~: length_correct L1. forwards~: btree_size_pos.
      xif; rewrites~ pow2_succ_div in *.
      - (* TODO  ARTHUR : xapp_spec_no_simpl IHt1. *)
        xapp_spec~ IHt1 L1.
        apply~ ZInbound_app_l_inv. hsimpl. apply~ ZNth_app_l.
      - xapps~. apply~ ZInbound_app_r_inv. hsimpl. apply~ ZNth_app_r. }
Qed.

Hint Extern 1 (RegisterSpec lookup_tree) => Provide lookup_tree_spec.

Lemma update_tree_spec :
  forall a i (x: a) (t: tree_ a) p L,
  btree p t L ->
  ZInbound i L ->
  app update_tree [i x t]
    PRE \[]
    POST (fun t' => \[exists L', btree p t' L' /\ ZUpdate i x L L']).
Proof.  (* exists L', L'= L[i:=x] /\ btree p t' L' *)
        (* Let L' := L[i:=x] in btree p t' L' *)
        (* btree p t' (L[i:=x]) *)
  intros a i x t. revert i x. induction t; introv BT Bi; inverts BT; xcf.
  - xgo~. apply~ ZInbound_one_pos_inv.
  - { xmatch. subst. forwards~: length_correct L1. forwards~: btree_size_pos.
      xif; rewrites~ pow2_succ_div in *.
      - xapp_spec~ IHt1. apply~ ZInbound_app_l_inv. intros; unpack.
        xret. hsimpl. exists___. split. constructors~. apply~ ZUpdate_app_l.
      - xapp_spec~ IHt2. apply~ ZInbound_app_r_inv. intros; unpack.
        xret. hsimpl. exists___. split. constructors~. apply~ ZUpdate_app_r. }
Qed.

Hint Extern 1 (RegisterSpec update_tree) => Provide update_tree_spec.

Ltac xapp_xapply_no_simpl K ::=
  xapply_core K ltac:(fun _ => idtac) ltac:(fun _ => idtac).


Lemma lookup_spec_ind :
  forall a i (ts: rlist_ a) p L,
  inv p ts L ->
  ZInbound i L ->
  app lookup [i ts]
    PRE \[]
    POST (fun x => \[ZNth i L x]).
Proof.
  intros a i ts. revert i. induction ts; introv I Bi; inverts I; xcf.
  - xgo. apply~ ZInbound_nil_inv.
  - { xmatch. xapps~. unpack. subst.
      forwards~: length_correct. forwards~: btree_size_correct.
      xapps~. xif.
      xapp_no_simpl~. (* xapp~ T. *)
      - apply~ ZInbound_app_l_inv.
      - hsimpl. apply~ ZNth_app_l.
      - xgo~. apply~ ZInbound_app_r_inv. apply~ ZNth_app_r. }
Qed.

Lemma lookup_spec :
  forall a i (ts: rlist_ a) L,
  Rlist ts L ->
  ZInbound i L ->
  app lookup [i ts]
    PRE \[]
    POST (fun x => \[ZNth i L x]).
Proof.

(* strategy 1 *)
  introv M. asserts (p&M'): (exists p, inv p ts L).
  { unfolds in M; eauto. } clear M. gen i L p.
  induction ts; introv I Bi; inverts I; xcf.
  - xgo. apply~ ZInbound_nil_inv.
  - { xmatch. xapps~. unpack. subst.
      forwards~: length_correct. forwards~: btree_size_correct.
      xapps~. xif.
      xapp_no_simpl~. (* xapp~ T. *)
      - apply~ ZInbound_app_l_inv.
      - hsimpl. apply~ ZNth_app_l.
      - xgo~. apply~ ZInbound_app_r_inv. apply~ ZNth_app_r. }

(*
-- strategy 2.
  introv. unfold Rlist. generalize 0 as p. revert i L.
  induction ts; introv I Bi; inverts I; xcf.
  - xgo. apply~ ZInbound_nil_inv.
  - { xmatch. xapps~. unpack. subst.
      forwards~: length_correct. forwards~: btree_size_correct.
      xapps~. xif.
      xapp_no_simpl~. (* xapp~ T. *)
      - apply~ ZInbound_app_l_inv.
      - hsimpl. apply~ ZNth_app_l.
      - xgo~. apply~ ZInbound_app_r_inv. apply~ ZNth_app_r. }

-- strategy 3.
assert ( de toute la spec )


-- strategy 4
  lemme séparée
*)
Qed.
(* OLD
Proof. intros. xapp_spec~ lookup_spec_ind. Qed.
*)

Lemma update_spec_ind :
  forall a i (x: a) (ts: rlist_ a) p L,
  inv p ts L ->
  ZInbound i L ->
  app update [i x ts]
    PRE \[]
    POST (fun ts' => \[exists L', inv p ts' L' /\ ZUpdate i x L L']).
Proof.
  intros a i x ts. revert i x. induction ts; introv I Bi; inverts I; xcf.
  - xgo. apply~ ZInbound_nil_inv.
  - { xmatch. xapps~. intros; unpack. xret~.
      xapps~. unpack; subst.
      forwards~: length_correct. forwards~: btree_size_correct.
      xif. xapps~. apply~ ZInbound_app_l_inv.
      intros (?&?&?). xret. hsimpl. exists___. split; eauto. apply~ ZUpdate_app_l.
      xapps~. xapps~. apply~ ZInbound_app_r_inv.
      intros (?&?&?). xret. hsimpl. exists___. split; eauto. apply~ ZUpdate_app_r. }
Qed.

Lemma update_spec :
  forall a i (x: a) (ts: rlist_ a) L,
  Rlist ts L ->
  ZInbound i L ->
  app update [i x ts]
    PRE \[]
    POST (fun l' => \[exists L', Rlist l' L' /\ ZUpdate i x L L']).
Proof. intros. xapp_spec~ update_spec_ind. Qed.

End BinaryRandomAccessListSpec.

open NullPointers

type node = {
   mutable data : int;
   mutable left : node;
   mutable right : node;
   }

let rec search x p = 
  if p == null then false else 
  let y = p.data in
  if x < y then search x p.left
  else if x > y then search x p.right
  else true












(**--------------- contest version, C --------------*)

(*
(Tree, int) search_tree_delete_min (Tree t) {
   Tree tt, pp, p;
   int m;
   p = t->left;
   if (p == NULL) {
       m = t->data; tt = t->right; dispose (t); t = tt;
   } else {
       pp = t; tt = p->left;
       while (tt != NULL) {
           pp = p; p = tt; tt = p->left;
       }
       m = p->data; tt = p->right; dispose(p); pp->left= tt;
   }
   return (t,m);
}
*)

(**--------------- contest version, Caml --------------*)

(*
let tree_delete_min t0 =
   let t = ref t0 in
   let tt = ref null in
   let pp = ref null in
   let m = ref 0 in
   let p = !t.left in

   let p = t.left in
   if c == null then begin
      (t.right, t.data)
   end else begin
      let rec loop p c b =
         if b == null 
            then (p.left <- c.right; c.data)
            else loop c b b.left
         in
      (t, loop t c c.left)
   end
*)

(**--------------- contest version, Caml --------------*)

let tree_delete_min_clean t =
   let f = t.left in
   if f == null then begin
      (t.right, t.data)
   end else begin
      let p = ref t in
      let c = ref f in
      let b = ref f.left in
      while !b != null do
         p := !c;
         c := !b;
         b := !b.left;
      done;
      !p.left <- !c.right; 
      (t, !c.data)
   end





(**--------------- simpler proof without holes --------------*)

let tree_delete_min_simple t =
   let c = t.left in
   if c == null then begin
      (t.right, t.data)
   end else begin
      let rec loop p c b =
         if b == null 
            then (p.left <- c.right; c.data)
            else loop c b b.left
         in
      (t, loop t c c.left)
   end
   

(**--------------- bin --------------*)










(*- --temp
let tree_delete_min_cstyle t =
   let r = ref t in
   let c = ref t.left in
   let m = ref 0 in
   if !c == null then begin
      m := t.data;
      r := t.right;
   end else begin
      let p = ref t in
      let b = ref !c.left in
      let rec loop () =
         if b == null then begin
            m := !c.data;
            b := !c.right;
            !p.left <- b;
         end else begin
            p := c;
            c := b;
            b := !c.left;
            loop();
         end in
      loop();   
   end;
   (!r,!m)

let tree_delete_min_original t =
   let r = ref t in
   let c = ref t.left in
   let m = ref 0 in
   let b = ref null in
   if !c == null then begin
      m := t.data;
      b := t.right;
      r := b;
   end else begin
      let p = ref t in
      b := !c.left;
      while !b <> null do
         p := c;
         c := b;
         b := !c.left;
      done;
      m := !c.data;
      b := !c.right;
      !p.left <- b;
   end;
   (!r,!m)

*)
(*

(Tree, int) search_tree_delete_min (Tree t) {
   Tree b, p, c;
   int m;
   c = t->left;
   if (c == NULL) {
       m = t->data; b = t->right; dispose (t); t = b;
   } else {
       p = t; b = p->left;
       while (b != NULL) {
           p = c; c = b; b = p->left;
       }
       m = p->data; b = p->right; dispose(c); pp->left = b;
   }
   return (t,m);
}

*)


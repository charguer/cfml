Set Implicit Arguments.
Require Import CFTactics LibCore LibMultiset.
Require Import OrderedType_ml OrderedType_proof. 
  (* HeapSig_ml HeapSig_proof *)
Require Import PairingHeap_ml.


(* -- copied from HeapSig_proof *)

  Generalizable Variables A.

  (** Definition of the minimum of a multiset *)

  Definition min_of `{Le A} (E:multiset A) (x:A) := 
    x \in E /\ forall_ y \in E, x <= y.

  (** Definition of the removal of the minimum from a multiset *)

  Definition removed_min `{Le A} (E E':multiset A) :=
    exists x, min_of E x /\ E = \{x} \u E'.



(* -- proof -- *)

Module PairingHeapSpec (O:MLOrderedType) (OS:OrderedTypeSpec with Module O:=O).
  (* <: HeapSigSpec with Definition H.MLElement.t := O.t. *)

(** instantiations *)

Module Import H (* <: MLHeap *) := MLPairingHeap O.
Module Import OS := OS.
Definition t := O.t.

Implicit Types x y : t.
Implicit Types q : heap.
Implicit Types E : multiset t.

(** invariant *)

Definition is_ge (x:t) (y:t) := x <= y.

Definition list_union (Hs:list (multiset t)) := 
  fold_right union \{} Hs.

Inductive inv : heap -> multiset t -> Prop :=
  | inv_empty : 
      inv Empty \{} 
  | inv_node : forall x hs Hs E,
      Forall2 inv hs Hs ->
      Forall (fun H => H <> \{}) Hs ->
      Forall (foreach (is_ge x)) Hs ->
      E = \{x} \u list_union Hs ->   
      inv (Node x hs) E.

(** model *)

Fixpoint size (q:heap) : nat :=
  match q with
  | Empty => 1%nat
  | Node x hs => (1 + (1 + List.fold_right (fun t n => (n + size t)%nat) 0%nat hs)%nat)%nat 
  end.

(** automation *)

Hint Extern 1 (_ \in _) => multiset_in.
Hint Extern 1 (_ < _) => simpl; math.
Hint Extern 1 (_ = _ :> multiset _) => permut_simpl : multiset.
Hint Unfold removed_min.

Ltac auto_tilde ::= eauto.
Ltac auto_star ::= try solve [ intuition (eauto with multiset) ].

Hint Constructors inv Forall Forall2.

(** useful facts *)

Lemma foreach_ge_trans : forall (x y : t) (E : multiset t),
  foreach (is_ge x) E -> y <= x -> foreach (is_ge y) E.
Proof. intros. apply~ foreach_weaken. intros_all. apply* le_trans. Qed.

Hint Resolve foreach_ge_trans.

Lemma min_of_prove : forall (x : t) (Hs : list (multiset t)),
  Forall (foreach (is_ge x)) Hs ->
  min_of ('{x} \u list_union Hs) x.
Proof using.
  introv H. split~. 
  introv M. destruct (in_union_inv M) as [N|N].
  rewrite (in_single N). apply le_refl.
  clear M. unfolds list_union. induction Hs; simpls.
    false. apply* @in_empty. typeclass.
    inverts H as Ha HHs. destruct (in_union_inv N) as [P|P].
      apply~ Ha.
      apply~ IHHs.
Qed.

(** verification *)

Lemma empty_spec : inv empty \{}.
Proof. rewrite* empty_cf. Qed.

Hint Extern 1 (RegisterSpec empty) => Provide empty_spec.

Lemma is_empty_spec : Spec is_empty q |R>> 
  forall E, inv q E -> 
  R \[] (fun b => \[b = isTrue (E = \{})]).
Proof using.
  xcf. intros q E Iq. inverts Iq; xmatch; xrets.
  auto. multiset_inv.
Qed. 

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.

Lemma merge_spec : Spec merge q1 q2 |R>>
  forall E1 E2, inv q1 E1 -> inv q2 E2 -> 
  R \[] (fun q => \[inv q (E1 \u E2)]).
Proof using.
  xcf. introv I1 I2. xmatch.
  xrets. inverts I2. equates* 1.
  xrets. inverts I1. equates* 1.
  xcleanpat. inverts keep I1 as I1a I1b I1c. 
  inverts keep I2 as I2a I2b I2c.  
  xapp as n. introv HC. xif; case_if*; xrets.
  { constructors.
      eauto.
      constructors~. multiset_inv.
      clear I2a I2b I2. constructors~.
        rew_foreach. splits~. applys (@foreach_weaken _ (is_ge y)).
          unfold list_union. induction I2c; simpl. auto. rew_foreach. split~.
          intros_all. applys~ le_trans.
    unfold list_union. simple*. }
  { applys_to HC gt_to_ge. rewrite ge_as_sle in HC. constructors.
      eauto. 
      constructors~. multiset_inv.
      clear I1 I1a I1b. constructors~.
        rew_foreach. splits~. applys (@foreach_weaken _ (is_ge x)).
          unfold list_union. induction I1c; simpl. auto. rew_foreach. split~.
          intros_all. applys~ le_trans.
    unfold list_union. simpl. permut_simpl. }
Qed.

Hint Extern 1 (RegisterSpec merge) => Provide merge_spec.

Lemma insert_spec : Spec insert x q |R>> 
  forall E, inv q E -> 
  R \[] (fun q' => \[inv q' (\{x} \u E)]).
Proof using.
  xcf. intros x q E I1. xapp~. xextract as M. xsimpl.
  unfolds list_union. simpl in M. rewrite union_empty_r in M. auto.
Qed.

Hint Extern 1 (RegisterSpec insert) => Provide insert_spec.

Lemma merge_pairs_spec : Spec merge_pairs hs |R>>
  forall Hs, 
  Forall2 inv hs Hs ->
  Forall (fun H => H <> \{}) Hs -> 
  R \[] (fun q => \[inv q (list_union Hs)]).
Proof using.
  xinduction (@List.length heap). xcf. introv IH I1 NE. xmatch.
  xrets. inverts~ I1.
  xrets. inverts I1 as I1a I1b. inverts I1b. unfolds list_union. simpls. equates* 1.
  xcleanpat. inverts I1 as I1a I1b. inverts I1b as I1c I1d.
   inverts NE as NE1 NE2. inverts NE2.
   xapp~ as q'. introv Iq'. xapp~ as hs'. introv IB. xapp~. xsimpl. equates* 1.
Qed.

Hint Extern 1 (RegisterSpec merge_pairs) => Provide merge_pairs_spec.

Lemma pop_min_spec : Spec pop_min q |R>>
  forall E, inv q E -> E <> \{} -> 
  R \[] (fun (p:t*heap) => let '(x,q) := p in
       \[min_of E x /\ exists E', inv q E' /\ removed_min E E']).
Proof using.
  hint min_of_prove.
  xcf. intros q E I1 N. xmatch.
  inverts* I1.
  inverts I1 as I1a I1b I1c. xapp~ as q'. introv I2. xrets. splits~. esplit. splits*.
Qed.

Hint Extern 1 (RegisterSpec pop_min) => Provide pop_min_spec.


End PairingHeapSpec.


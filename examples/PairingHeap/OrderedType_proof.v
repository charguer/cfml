Set Implicit Arguments.
Require Import CFTactics OrderedType_ml.

Module Type OrderedTypeSpec.

Declare Module O : MLOrderedType.
Import O.

Parameter le_inst : Le t.
Parameter le_order : Le_total_order.

Global Existing Instance le_inst.
Global Existing Instance le_order.

Parameter compare_spec : 
  Spec compare (x:t) (y:t) |R>>
    R \[] (fun (n:int) => \[If n <= 0 then x <= y else x > y]).

Hint Extern 1 (RegisterSpec compare) => Provide compare_spec.

End OrderedTypeSpec.

Set Implicit Arguments.
Require Export CFLib.
Require Import 
  TestDepend_OnlyCoq 
  TestDepend_AuxCode_ml.
  
Lemma f_spec : forall (A:Type) (x:A),
  app f [x] \[] (fun y => \[same x y]).
Proof.
  xcf. intros. xret. unfold same. hsimpl~.
Qed.

Hint Extern 1 (RegisterSpec f) => Provide f_spec. 
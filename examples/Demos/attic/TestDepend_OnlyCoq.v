Set Implicit Arguments.
Require Import LibInt.

Definition same (A:Type) (x y : A) := (x = y).

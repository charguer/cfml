Set Implicit Arguments.
Require Import CFLib LibSet LibMap LibArray.
Require Import ReducerSig_ml ReducerSig_proof.
Require Import CapacityParamSig_ml CapacityParamSig_proof.
Require Import ReducerSig_ml ReducerSig_proof.
Require Import FingerTree_ml. (* FingerTree_proof.*)
Require Import FastFingerTree_ml.

Module MakeSpec (C : MLCapacityParam) (R : MLReducer) (RS:ReducerSigSpec with Module R:=R).

(** instantiations *)

Module Import FF := MLMake R C. (* todo: fix order *)
Import FF.

Import RS.

(*
Notation "'reduce'" := (list_reduce Red).
*)
Notation "'item'" := (R.item).

Definition Items := list Item.




(****************************************************)
(** Imperative versions *)

Notation "'len'" := LibList.length.

Parameter Queue : Items -> queue -> hprop.

Axiom queue_create_spec :
  Spec queue_create () |R>>
     R [] (fun q => q ~> Queue nil).

Hint Extern 1 (RegisterSpec queue_create) => Provide queue_create_spec.

Axiom queue_size_spec : 
  Spec queue_size (q:queue) |R>>
     forall L,
     R (q ~> Queue L) (fun (n:int) => [n = len L] \* q ~> Queue L).

Hint Extern 1 (RegisterSpec queue_size) => Provide queue_size_spec.

Axiom queue_is_empty_spec : 
  Spec queue_is_empty (q:queue) |R>>
     forall L,
     R (q ~> Queue L) (fun b => [b = isTrue (L = nil)] \* q ~> Queue L).

Hint Extern 1 (RegisterSpec queue_is_empty) => Provide queue_is_empty_spec.

Definition full (L: Items) :=
  len L = capacity :> int.


Axiom queue_is_full_spec : 
  Spec queue_is_full (q:queue) |R>>
     forall L,
     R (q ~> Queue L) (fun b => [b = isTrue (full L)] \* q ~> Queue L).

Hint Extern 1 (RegisterSpec queue_is_full) => Provide queue_is_full_spec.

Axiom queue_is_partial_spec : 
  Spec queue_is_partial (q:queue) |R>>
     forall L,
     R (q ~> Queue L) (fun b => [b = isTrue (L <> nil /\ ~ full L)] \* q ~> Queue L).

Hint Extern 1 (RegisterSpec queue_is_partial) => Provide queue_is_partial_spec.

Axiom queue_push_head_spec : 
  Spec queue_push_head (x:item) (q:queue) |R>>
     forall X L, Rep X x -> ~ full L -> 
     R (q ~> Queue L) (# q ~> Queue (X::L)).

Hint Extern 1 (RegisterSpec queue_push_head) => Provide queue_push_head_spec.

Axiom queue_push_tail_spec : 
  Spec queue_push_tail (x:item) (q:queue) |R>>
     forall X L, Rep X x -> ~ full L -> 
     R (q ~> Queue L) (# q ~> Queue (L&X)).

Hint Extern 1 (RegisterSpec queue_push_tail) => Provide queue_push_tail_spec.

Axiom queue_pop_head_spec : 
  Spec queue_pop_head (q:queue) |R>>
     forall L, L <> nil ->
     R (q ~> Queue L) (fun x => Hexists X L',
          q ~> Queue L' \* [Rep X x /\ L = X::L']).

Hint Extern 1 (RegisterSpec queue_pop_head) => Provide queue_pop_head_spec.

Axiom queue_pop_tail_spec : 
  Spec queue_pop_tail (q:queue) |R>>
     forall L, L <> nil ->
     R (q ~> Queue L) (fun x => Hexists X L',
          q ~> Queue L' \* [Rep X x /\ L = L'&X]).

Hint Extern 1 (RegisterSpec queue_pop_tail) => Provide queue_pop_tail_spec.



(****************************************************)
(** ---- Finger tree Imperative versions *)

Notation "'ftree'" := MLFtree.ftree.

Parameter Fingertree : Items -> ftree -> hprop.

Axiom fingertree_empty_spec :
  [] ==> (MLFtree.empty ~> Fingertree nil).

Axiom fingertree_is_empty_spec : 
  Spec MLFtree.is_empty (f:ftree) |R>>
     forall L,
     R (f ~> Fingertree L) (fun b => [b = isTrue (L = nil)] \* f ~> Fingertree L).

Hint Extern 1 (RegisterSpec MLFtree.is_empty) => Provide fingertree_is_empty_spec.

Axiom fingertree_push_front_spec : 
  Spec MLFtree.push_front (q:queue) (f:ftree) |R>>
     forall L1 L2, L1 <> nil ->
     R (f ~> Fingertree L2 \* q ~> Queue L1) (fun f' => f' ~> Fingertree (L1 ++ L2)).

Hint Extern 1 (RegisterSpec MLFtree.push_front) => Provide fingertree_push_front_spec.

Axiom fingertree_push_back_spec : 
  Spec MLFtree.push_back (q:queue) (f:ftree) |R>>
     forall L1 L2, L2 <> nil ->
     R (f ~> Fingertree L1 \* q ~> Queue L2) (fun f' => f' ~> Fingertree (L1++L2)).

Hint Extern 1 (RegisterSpec MLFtree.push_back) => Provide fingertree_push_back_spec.

Axiom fingertree_pop_front_spec : 
  Spec MLFtree.pop_front (f:ftree) |R>>
     forall L, L <> nil ->
     R (f ~> Fingertree L) (fun p => let '(q,f') := p in Hexists L1 L2,
          f' ~> Fingertree L2 \* q ~> Queue L1 \* [L = L1 ++ L2 /\ L1 <> nil]).

Hint Extern 1 (RegisterSpec MLFtree.pop_front) => Provide fingertree_pop_front_spec.

Axiom fingertree_pop_back_spec : 
  Spec MLFtree.pop_back (f:ftree) |R>>
     forall L, L <> nil ->
     R (f ~> Fingertree L) (fun p => let '(q,f') := p in Hexists L1 L2,
          f' ~> Fingertree L1 \* q ~> Queue L2 \* [L = L1++L2 /\ L2 <> nil]).

Hint Extern 1 (RegisterSpec MLFtree.pop_back) => Provide fingertree_pop_back_spec.


(****************************************************)
(** Verification *)

Definition FastRecord (T1 T2 T3 T4 T5 : Type) (R1:T1->loc->hprop) 
  (R2:T2->loc->hprop) (R3:T3->MLFtree.t->hprop) (R4:T4->loc->hprop) (R5:T5->loc->hprop)
  X1 X2 X3 X4 X5 :=
  Fftree R4 R5 R2 R1 R3 X4 X5 X2 X1 X3.
(* TODO: unsort ! *)

Notation "'FastOf'" :=
  (FastRecord Queue Queue Fingertree Queue Queue).

(** Invariants on fast finger trees *)



Definition fillorder (L1 L2 : Items) :=
  L1 <> nil -> full L2.

Definition Fast L ff :=
  Hexists L1 L2 L3 L4 L5, 
  ff ~> FastOf L1 L2 L3 L4 L5 \*
  [L = L1 ++ L2 ++ L3 ++ L4 ++ L5
   /\ fillorder L1 L2 /\ fillorder L5 L4].

Lemma Fast_focus : forall ff L,
  ff ~> Fast L ==> Hexists fo fi tm bi bo, Hexists L1 L2 L3 L4 L5,
    ff ~> FastRecord Id Id Id Id Id fo fi tm bi bo \*
    fo ~> Queue L1 \* fi ~> Queue L2 \* tm ~> Fingertree L3 \*
    bi ~> Queue L4 \* bo ~> Queue L5 \* 
    [L = L1 ++ L2 ++ L3 ++ L4 ++ L5 /\ fillorder L1 L2 /\ fillorder L5 L4 ].
Proof.
  intros. unfold Fast, FastRecord. hdata_simpl. hextract. intros. 
  hchange (@Fftree_focus ff). hsimpl*.
Qed.

Lemma Fast_unfocus : forall ff fo fi tm bi bo L1 L2 L3 L4 L5,
  fillorder L1 L2 -> fillorder L5 L4 ->
  ff ~> FastRecord Id Id Id Id Id fo fi tm bi bo \*
  fo ~> Queue L1 \* fi ~> Queue L2 \* tm ~> Fingertree L3 \*
  bi ~> Queue L4 \* bo ~> Queue L5 
  ==> 
  ff ~> Fast (L1 ++ L2 ++ L3 ++ L4 ++ L5).
Proof. 
  intros. unfold Fast, FastRecord. hdata_simpl.
  hchange (@Fftree_unfocus ff). hsimpl*.
Qed.

Axiom capacity_not_zero : capacity <> 0.

Lemma fillorder_not_empty : forall L1 L2,
  fillorder L1 L2 -> L1 <> nil -> L2 <> nil.
Proof.
  introv H N. lets: H N. destruct L2; [|congruence].
  false capacity_not_zero. rew_list in *. auto.
Qed.

Lemma is_empty_spec :
  Spec is_empty ff |R>>
     forall L,
     R (ff ~> Fast L) (fun b => [b = isTrue (L = nil)] \* ff ~> Fast L).
Proof.
  xcf. intros. xchange (@Fast_focus ff) as.
  intros fo fi tm bi bo L1 L2 L3 L4 L5 (E&O1&O2).
  xapps. xapps.   
  xapps. xapps.  
  xapps. xapps.  
  xret. hchange~ (@Fast_unfocus ff). hsimpl. extens.
  (* TODO: bruteforce
  tests: (L2 = nil); tests: (L3 = nil); tests: (L4 = nil).
  *)
  unfolds fillorder. (* use: fillorder_not_empty *)
  skip.
Admitted.

Lemma fillorder_nil_any : forall L,
  fillorder nil L.
Proof. introv H. false. Qed.

Hint Resolve fillorder_nil_any.


Lemma create_spec :
  Spec create () |R>>
     R [] (~> Fast nil).
Proof.
  xcf. xapp*. xapp*. xapp*. xapp*. xapp*. 
  intros ff. hchange (fingertree_empty_spec).
  hchange~ (@Fast_unfocus ff).
Admitted.

(* todo: needed ?*)
Lemma fillorder_push_front : forall L1 L2 X,
  fillorder L1 L2 -> ~ full L2 -> fillorder L1 (X :: L2).
Proof. introv H N2 N1. lets: H N1. false. Qed.

Hint Resolve fillorder_push_front.

Lemma fillorder_elim : forall L1 L2,
  fillorder L1 L2 -> ~ full L2 -> L1 = nil.
Proof. introv H N. apply not_not_elim. intros N1. lets: H N1. false. Qed.

Lemma fillorder_full : forall L1 L2,
  full L2 -> fillorder L1 L2.
Proof. intros. unfolds~. Qed.
Hint Resolve fillorder_full.

Lemma full_nil_not : ~ full nil.
Proof. intros H. unfolds full. rew_list in H. false~ capacity_not_zero. Qed.
Hint Resolve  full_nil_not.

Lemma full_not_nil : forall L, full L -> L <> nil.
Proof. introv H. intro_subst. false* full_nil_not. Qed.
Hint Resolve full_not_nil.



Lemma push_front_spec : 
  Spec push_front x ff |R>>
     forall X L, Rep X x -> 
     R (ff ~> Fast L) (# ff ~> Fast (X::L)).
Proof.
  xcf. introv RX. xchange (@Fast_focus ff) as.
  intros fo fi tm bi bo L1 L2 L3 L4 L5 (E&O1&O2).
  xapps. xapps. xif.
    forwards~: fillorder_elim O1. subst.
     xapp*. intros _. hchange~ (@Fast_unfocus ff).
    xapps. xapps. xseq (# Hexists fo fi tm bi bo, Hexists L1 L2 L3 L4 L5,
      ff ~> FastRecord Id Id Id Id Id fo fi tm bi bo \*
      fo ~> Queue L1 \* fi ~> Queue L2 \* tm ~> Fingertree L3 \*
      bi ~> Queue L4 \* bo ~> Queue L5 \* 
      [L = L1 ++ L2 ++ L3 ++ L4 ++ L5 /\ fillorder L1 L2 /\ fillorder L5 L4 
       /\ ~ full L1 /\ full L2 ]).
    xif.
      xapps. xapp*. xapps. xapp. xapp. xapp. hsimpl. rew_list. subst. splits*.
      xret. hsimpl. subst*.
  gen RX. clears_all. intros RX fo fi tm bi bo L1 L2 L3 L4 L5 (E&O1&O2&N1&N2). 
  xapps. xapps*. hchange~ (@Fast_unfocus ff). hsimpl. subst*. 
Admitted.

Hint Extern 1 (RegisterSpec push_front) => Provide push_front_spec.



Definition is_ItemOrQueue_item ioq :=
  match ioq with
  | ItemOrQueue_item x => True
  | _ => False
  end.

Definition Item_or_queue (L:Items) ioq :=
  match ioq with
  | ItemOrQueue_item x => Hexists X, [L = X::nil /\ Rep X x]
  | ItemOrQueue_queue q => q ~> Queue L \* [L <> nil]
  end.

Lemma ItemOrQueue_item_unfocus : forall x X,
  Rep X x ->
  [] ==> (ItemOrQueue_item x) ~> Item_or_queue (X::nil).
Proof.
  intros. unfold Item_or_queue. hdata_simpl. hsimpl*.
Qed.

Lemma ItemOrQueue_queue_unfocus : forall q L,
  L <> nil ->
  q ~> Queue L ==> (ItemOrQueue_queue q) ~> Item_or_queue L.
Proof.
  intros. unfold Item_or_queue. hdata_simpl. hsimpl*.
Qed.


Lemma fftree_pop_front_from_back_spec : 
  Spec fftree_pop_front_from_back (ff:fftree) (fi:queue) |R>>
     forall fo tm bi bo L L3 L4 L5, L <> nil ->
       L = L3 ++ L4 ++ L5 -> fillorder L5 L4 ->
     R (ff ~> FastRecord Id Id Id Id Id fo fi tm bi bo \*
         fo ~> Queue nil \* fi ~> Queue nil \* tm ~> Fingertree L3 \*
         bi ~> Queue L4 \* bo ~> Queue L5)
       (fun ioq => Hexists (tm':MLFtree.t) (bi':loc) (bo':loc), Hexists (L0' L3' L4' L5' : Items),
         ioq ~> Item_or_queue L0' \*
         ff ~> FastRecord Id Id Id Id Id fo fi tm' bi' bo' \*
         (If is_ItemOrQueue_item ioq then fi ~> Queue nil else []) \*
         fo ~> Queue nil \* tm' ~> Fingertree L3' \*
         bi' ~> Queue L4' \* bo' ~> Queue L5' \* 
         [L = L0' ++ L3' ++ L4' ++ L5' /\ fillorder L5' L4' ]).
Proof.
  xcf. introv N E F. xapps. xapps. xif.
  (* tree not empty *)
  xapps*. xmatch. xextract as L3a L3b (E'&Na). xapps. xret.
   hchange~ (@ItemOrQueue_queue_unfocus q).
   subst. rew_list. case_if. hsimpl* (>> f bi bo L3a L3b L4 L5).
  (* tree empty *)
  xapps. xapps. xapps. xif.
    rew_list in E. subst L. xapps*. intros X L4' (RX&EX). xret.
     hchange* (@ItemOrQueue_item_unfocus x). case_if; [ | false*].
     hsimpl (>> tm bi bo (X::nil) (nil:Items) L4' (nil:Items)).
     subst. rew_list. splits*.
    xapps. xapps. xret. hchange~ (@ItemOrQueue_queue_unfocus bi).
     case_if. hsimpl (>> tm bo empty_queue L4 (nil:Items) L5 (nil:Items)). 
     subst. rew_list*.
Admitted.

Hint Extern 1 (RegisterSpec fftree_pop_front_from_back) => Provide fftree_pop_front_from_back_spec.

Lemma pop_front_spec : 
  Spec pop_front ff |R>>
     forall L, L <> nil ->
     R (ff ~> Fast L) (fun x => Hexists X L', ff ~> Fast L' \* [Rep X x /\ L = X::L']).
Proof.
  xcf. introv N. xchange (@Fast_focus ff) as.
  intros fo fi tm bi bo L1 L2 L3 L4 L5 (E&O1&O2).
  xapps. xapps. xif.
    xapp*. intros x. hextract as X L' (RX&E').
     hchange~ (@Fast_unfocus ff). subst. rew_list. hsimpl*.
    xapps. xapps. xif. 
      xapps. xapp*. intros x. hextract as X L' (RX&E'). 
       hchange~ (@Fast_unfocus ff). subst. rew_list. hsimpl*.
      xapp~ L. clears tm bi bo L3 L4 L5. intros tm bi bo L0 L3 L4 L5 (E&F).
       xmatch.
         xret. case_if; [|false*]. unfold Item_or_queue. hdata_simpl.
           hextract as X (RX&E'). hchange~ (@Fast_unfocus ff).
           subst. rew_list. hsimpl*.
         unfold Item_or_queue. hdata_simpl. case_if. xextract as N0.
          xapp. xapps. xapp*. intros x. hextract as X L' (RX&E'). 
           hchange~ (@Fast_unfocus ff). subst. rew_list. hsimpl*.
Admitted.

Hint Extern 1 (RegisterSpec pop_front) => Provide pop_front_spec.


End MakeSpec.






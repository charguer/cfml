(*-- bug 1 
Require Import Why3.

Require Import JMeq.

Axiom JMeq_sym : forall (A B : Type) (x : A) (y : B), 
  JMeq x y -> JMeq y x.

Lemma test_true_1 : True.
Proof. why3 "alt-ergo". Qed.

Anomaly: File "src/coq-tactic/why3tac.ml", line 645, characters 10-16: Assertion failed.
Please report.

-*)

(*-- bug 2 

Require Import Why3.

Record R (A:Type) := {
  R_hyp : A;
  R_ext : R_hyp = R_hyp }.

Lemma test_true_2 : True.
Proof. why3 "alt-ergo". Qed.

Anomaly: File "src/coq-tactic/why3tac.ml", line 560, characters 20-26: Assertion failed.
Please report.

*)



(**********************************************************)


Set Implicit Arguments.

Require Import Why3.

Ltac ae := intros; why3 "alt-ergo" timelimit 1.



(**********************************************************)
(** CFML environment -- LATER *)

Module LibCFTest.

End LibCFTest.


(**********************************************************)
(** Coq environment *)

Module LibCoqTest.

(*--------------------------------------------------------*)
(** Z *)

Require Export ZArith.
Open Scope Z_scope.

Lemma test_Z_1 : forall (x y : Z),
  x > 0 -> y > 0 -> x * y > 0.
Proof. ae. Qed.

Lemma test_Z_2 : forall (x y : Z),
  x + y > 0 -> x - y < 0 -> y > 0.
Proof. ae. Qed.

Lemma test_Z_3 : forall x,
  (forall a, x > a) -> False.
Proof. ae. Qed.

Lemma test_Z_4 : forall x,
  (forall a, x > a+1) -> False.
Proof. try ae. Admitted.

(*--------------------------------------------------------*)
(** list *)

Require Export List.

Lemma test_list_1 : forall (x y : nat),
  x::y::nil <> nil.
Proof. ae. Qed.

Lemma test_list_2 : forall (L : list nat) a,
  L <> nil -> (length (a :: L) > 1)%nat.
Proof. try ae. Admitted. (* a bit slow *)

Lemma test_list_3 : forall (x y : list nat),
  x <> nil -> x ++ y <> nil.
Proof. try ae. Admitted.

Lemma test_list_4 : forall (x : list nat),
  x <> nil -> exists a y, x = a::y.
Proof. try ae. Admitted.

Lemma test_list_5 : forall (x : list Type),
  x <> nil -> x <> nil.
Proof. try ae. Admitted.


(*--------------------------------------------------------*)
(** map *)

Require Import Map.

Lemma test_map_1 : forall (t : map Z Z) (i v:Z),
  Map.get t i = v.
Proof. try ae. Admitted. (* todo *)

Lemma test_map_2 : forall (t : map Z Z) (i v:Z),
  Map.get (Map.set t i v) i = v.
Proof. try ae. Admitted. (* todo *)

End LibCoqTest.



(**********************************************************)
(** LibCore environment *)

Module LibCoreTest.

(*--------------------------------------------------------*)
(** [Lib*] *)

Require Import LibTactics.

Lemma test_true_1 : True.
Proof. ae. Qed.

Require Import LibAxioms.


Lemma test_quantifiers_1 : forall (P : Prop), 
  P -> P.
Proof. try ae. Admitted. (* todo: could be first order, see below *)

Module TestQuantifiers.
Parameter (P : Prop).
Lemma test_quantifiers_2 : P -> P.
Proof. ae. Qed. 
End TestQuantifiers.


Require Import CFWhyDemosExt.



(* ********************************************************************** *)
(** * General definition of extensionality *)

(** The property [Extensional A] captures the fact that the type [A]
    features an extensional equality, in the sense that to prove the
    equality between two values of type [A] it suffices to prove that
    those two values are related by some binary relation. *)

Class Extensional (A:Type) := {
  extensional_hyp : A -> A -> Prop;
  extensional : forall x y : A, extensional_hyp x y -> x = y }.


Lemma test_true_2 : True.
Proof. ae. Qed.



Lemma test_partial_eq : forall (x:nat), 
  (= x) x.
Proof. ae. Qed.


Anomaly: File "src/coq-tactic/why3tac.ml", line 560, characters 20-26: Assertion failed.
Please report.

assert false (* ensured by Coq typing *)



Require Import LibEqual.
Lemma test_true_2 : True.
Proof. ae. Qed.




Lemma test_true_2 : True.
Proof. ae. Qed.


(* coqc CFWhyDemosExt.v -I tlc *)



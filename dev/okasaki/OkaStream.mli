(*open Okasaki*)

type 'a stream_cell = Nil | Cons of 'a * 'a stream
 and 'a stream = 'a stream_cell Lazy.t

val (++) : 'a stream -> 'a stream -> 'a stream
val take : int -> 'a stream -> 'a stream
val drop : int -> 'a stream -> 'a stream
val reverse : 'a stream -> 'a stream
val to_list : 'a stream -> 'a list
val of_list : 'a list -> 'a stream


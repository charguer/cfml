Set Implicit Arguments. 
Require Import CFLib BST_ml.


(********************************************************************)
(** Search function *)

(*------------------------------------------------------------------*)

Inductive tree :=
  | Leaf : tree
  | Node : tree -> int -> tree -> tree.

Instance tree_Inhab : Inhab tree. 
Proof using. apply (prove_Inhab Leaf). Qed.

(** Representation predicate as trees
    [p ~> Tree T] *)

Fixpoint Tree (T:tree) (t:loc) : hprop :=
  Hexists v, t ~~> v \*
  match T with
  | Leaf =>
     \[v = MLeaf]
  | Node T1 x T2 => 
     Hexists t1 t2, \[v = MNode t1 x t2] \* t1 ~> Tree T1 \* t2 ~> Tree T2
  end.

(** Characterization of binary search trees *)

Definition is_lt (X Y:int) := Y < X.
Definition is_gt (X Y:int) := X < Y.

Inductive stree : tree -> LibSet.set int -> Prop :=
  | stree_Leaf : 
      stree Leaf \{}
  | stree_Node : forall y T1 T2 E1 E2 E,
      stree T1 E1 ->
      stree T2 E2 -> 
      foreach (is_lt y) E1 -> 
      foreach (is_gt y) E2 ->
      E =' (\{y} \u E1 \u E2) ->
      stree (Node T1 y T2) E.

(** Representation predicate for binary search trees
    [p ~> Stree T] *)

Definition Stree (E:set int) (t:loc) :=
  Hexists (T:tree), t ~> Tree T \* \[stree T E].


(***--------------------------------------------------------***)
(** automation *)

Ltac xsimpl_core ::= hsimpl; xclean.

Hint Extern 1 (_ = _ :> LibSet.set _) => permut_simpl : set.

Ltac auto_tilde ::= eauto.
Ltac auto_star ::= try solve [ intuition (eauto with set) ].

Implicit Types E : LibSet.set int.

Lemma foreach_gt_notin : forall E X Y,
  foreach (is_gt Y) E -> lt X Y -> X \notin E.
Proof using. introv F L N. lets: (F _ N). apply* lt_slt_false. Qed.

Lemma foreach_lt_notin : forall E X Y,
  foreach (is_lt Y) E -> lt Y X -> X \notin E.
Proof using. introv F L N. lets: (F _ N). apply* lt_slt_false. Qed.


(***--------------------------------------------------------***)

(* todo remove
Lemma pred_le_antisym : forall A, antisym (@pred_le A).
Proof using. intros_all. applys* prop_ext_1. Qed.
*)

Lemma tree_focus : forall (t:loc) (T:tree),
  t ~> Tree T ==>
  Hexists v, t ~~> v \*
  match v with
  | MLeaf => \[T = Leaf]
  | MNode t1 x t2 => Hexists T1 T2, \[T = Node T1 x T2] \* t1 ~> Tree T1 \* t2 ~> Tree T2
  end.
Proof using.
  intros. destruct T; simpl; rewrite~ hunfold_base_eq; hsimpl; subst; hsimpl*. (* todo bug *)
Qed.

Lemma tree_leaf_unfocus : forall (t:loc),
  (t ~~> MLeaf) ==> (t ~> Tree Leaf).
Proof using. intros. hunfold Tree. hsimpl*. Qed.

Lemma tree_node_unfocus : forall (t:loc) x t1 t2 T1 T2,
  (t ~~> MNode t1 x t2) \* (t1 ~> Tree T1) \* (t2 ~> Tree T2) ==>
  (t ~> Tree (Node T1 x T2)).
Proof using. intros. hunfold Tree. hsimpl*. Qed.

Lemma Stree_focus : forall t E,
  t ~> Stree E ==>
  Hexists (T:tree), t ~> Tree T \* \[stree T E].
Proof using. intros. hunfold Stree. auto. Qed.

Lemma Stree_unfocus : forall t T E,
  t ~> Tree T \* \[stree T E] ==> t ~> Stree E.
Proof using. intros. hunfold Stree. hsimpl*. Qed.

Opaque Tree Stree.


(***--------------------------------------------------------***)

(** Well-founded order on trees *)

Inductive tree_sub : binary (tree) :=
  | tree_sub_1 : forall x T1 T2,
      tree_sub T1 (Node T1 x T2)
  | tree_sub_2 : forall x T1 T2,
      tree_sub T2 (Node T1 x T2).

Lemma tree_sub_wf : wf tree_sub.
Proof using.
  intros T. induction T; constructor; intros t' H; inversions~ H.
Qed.

Hint Constructors tree_sub.
Hint Resolve tree_sub_wf : wf.


(***--------------------------------------------------------***)


Hint Extern 1 (Register xopen (Tree _)) => Provide tree_focus.
Hint Extern 1 (Register xclose (Ref Id (MNode _ _ _))) => Provide tree_node_unfocus.
Hint Extern 1 (Register xclose (Ref Id MLeaf)) => Provide tree_leaf_unfocus.

Hint Extern 1 (Register xopen (Stree _)) => Provide Stree_focus.
Hint Extern 1 (Register xclose (Tree _)) => Provide Stree_unfocus.


Hint Constructors stree.


(***--------------------------------------------------------***)
(** Delete *)
(*
(* todo; protect xfail from xextract *)

Lemma fill_root_spec :
  Spec fill_root (t:loc) |R>> 
     forall T T1 x T2 E, stree T E -> T = Node T1 x T2 ->
     R (t ~> Tree (Node T1 x T2))
       (fun (_:unit) => Hexists T', \[stree T' (E \- \{x})] \* t ~> Tree T').
Proof using.
  xinduction_heap tree_sub. xcf. intros t T IH T1 x T2 E IE ET.
  xopen t as v. xapps. xmatch. 
  xextract as ET'. xfail. false.
  xextract as T1' T2' EQ. inverts EQ. xopen t2 as v2.
  xapps. xmatch.
    xextract as ET2'. subst. xopen t1 as v1. xapps. xapps. 
    intros _. xextract. xclose t1. xsimpl.
  

Qed. 

Lemma delete_spec_ind :
  Spec delete (x:int) (t:loc) |R>> 
     forall T E, stree T E ->
     R (t ~> Tree T)
       (fun (_:unit) => Hexists T', \[stree T' (E \- \{x})] \* t ~> Tree T').
Proof using. 
  xinduction_heap tree_sub. xcf. intros x t T IH E IE.
  xopen t. xextract as v. xapps. xmatch.
  xextract as HT. subst. inverts IE. xret. xclose t. xsimpl*. skip. (* todo *) 
  xextract as T1 T2 HT. subst T. inverts IE as IE1 IE2 F1 F2 EQ.
  xif. xapps*. xextract as T' HT'. xclose t. xsimpl*.
   constructors~. skip. subst E. skip. (* mk no x in E2,y *)
  xif. xapps*. xextract as T' HT'. xclose t. xsimpl*.
   constructors~. skip. subst E. skip. (* mk no x in E1,y *)
  asserts Exy: (x = y). math.
  xapps. xclose t. 
  xsimpl*. 
Qed.

Lemma delete_spec :
  Spec delete (x:int) (t:loc) |R>> 
     forall E,
     R (t ~> Stree E)
       (fun (_:unit) => t ~> Stree (E \- \{x})).
Proof using. 
  xcf. intros. 
  xopen t. xextract as T HT1. xopen t as v. xapps. xmatch.
  { xret. xextracts. xclose t. xclose* t. xsimpl. inverts~ HT1. skip. (* todo *) }
  { xextract as T1 T2 ET. xif.
    { xapps.

xret. xextract as T1 T2 ET. subst T. xclose t. xclose* t. xsimpl.
    inverts HT1. subst. skip. (* todo *) }
Qed.

let rec fill_root t =
  match !t with 
  | MLeaf -> assert false
  | MNode (t1, x, t2) ->
    match !t2 with
    | MLeaf -> t := !t1
    | MNode (t21, y, t22) ->
        t := MNode (t1, y, t2);
        fill_root t2

let rec delete x t =
  match !t with
  | MLeaf -> ()
  | MNode (t1, y, t2) ->
      if x < y then delete x t1
      else if x > y then delete x t2
      else fill_root t




*)

(***--------------------------------------------------------***)
(** Is_empty *)

Lemma is_empty_spec :
  Spec is_empty (t:loc) |R>> 
     forall E,
     R (t ~> Stree E)
       (fun (b:bool) => t ~> Stree E \* \[b = isTrue(E = \{})]).
Proof using. 
  xcf. intros. 
  xopen t. xextract as T HT1. xopen t as v. xapps. xmatch.
  { xret. xextracts. xclose t. xclose* t. xsimpl. inverts~ HT1. }
  { xret. xextract as T1 T2 ET. subst T. xclose t. xclose* t. xsimpl.
    inverts HT1. subst. skip. (* todo *) }
Qed.



(***--------------------------------------------------------***)
(** Add *)


Lemma add_spec_ind :
  Spec add (x:int) (t:loc) |R>> 
     forall T E, stree T E ->
     R (t ~> Tree T)
       (fun (_:unit) => Hexists T', \[stree T' (E \u \{x})] \* t ~> Tree T').
Proof using. 
  xinduction_heap (tree_sub). xcf. intros x t T IH E IE.
  xopen t. xextract as v. xapps. xmatch.
  xextract as HT. subst. inverts IE. xapp as t1. xapp as t2. xapps. 
   intros _. xclose t1 t2 t. xsimpl*.
  xextract as T1 T2 HT. subst T. inverts IE as IE1 IE2 F1 F2 EQ.
  xif. xapps*. xextract as T' HT'. xclose t. xsimpl*.
  xif. xapps*. xextract as T' HT'. xclose t. xsimpl*.
  xret. xclose t. asserts_rewrite (x = y). math. xsimpl*. 
Qed.

Lemma add_spec :
  Spec add (x:int) (t:loc) |R>> 
     forall E,
     R (t ~> Stree E)
       (fun (_:unit) => t ~> Stree (E \u \{x})).
Proof using.
  xweaken add_spec_ind. simpl. intros x t R HR S1 E.
  xopen t as T1 HT1. xapply* S1. xsimpl. xextract as T' HT'.
  xclose* t. xsimpl*.
Qed.


(***--------------------------------------------------------***)
(** Search *)

Lemma search_spec_ind :
  Spec search (x:int) (t:loc) |R>> 
     forall T E, stree T E ->
     R (t ~> Tree T)
       (fun b => \[b = isTrue (x \in E)] \* t ~> Tree T).
Proof using. 
  xinduction_heap (tree_sub). xcf. intros x t T IH E IE.
  xopen t. xextract as v. xapps. xmatch.
  xextract as HT. subst. inverts IE. xret. xclose t.
   xsimpl. applys* @notin_empty. typeclass.
  xextract as T1 T2 HT. subst. inverts IE as IE1 IE2 F1 F2 EQ.
  xif. xapps*. intros b. 
   xextracts. xclose t. xsimpl. subst E. iff M.
     eauto.
     set_in M. math. auto. false* foreach_gt_notin F2.
  xif. xapps*. intros b.
   xextracts. xclose t. xsimpl. subst E. iff M.
     (* too slow: auto / in_union_get. *)
        rewrite <- for_set_union_empty_r.
        repeat rewrite <- for_set_union_assoc.
        apply in_union_get_3. assumption.
     set_in M. math. false* foreach_lt_notin F1. math. auto.
  xret. xclose t. xsimpl. asserts_rewrite (x = y). math. subst E. auto.
Qed.


Lemma search_spec :
  Spec search (x:int) (t:loc) |R>> 
     forall E,
     R (t ~> Stree E)
       (fun b => \[b = isTrue (x \in E)] \* t ~> Stree E).
Proof using.
  xweaken search_spec_ind. simpl. intros x t R HR S1 E. 
  xopen t. xextract as T1 HT1.
  xapply* S1. hsimpl. intros b. xextract as Hb.
  xclose* t. xsimpl*.
Qed.



